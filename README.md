Your Great Library Back-end
===========================


Setting up the local environment
--------------------------------

Some configuration properties, such as `spring.mail` configuration,  are not included in 
the default configuration. You should add this in a `local.application.properties` that is
not uploaded to the repository. Example content:
```
spring.mail.host=smtp.gmail.com
spring.mail.port=587
spring.mail.username=<secret-username>
spring.mail.password=<secret-password>
#spring.mail.properties.mail.debug=true
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true

```
You should then add the following program argument when starting spring boot:
```
--spring.config.location=classpath:/application.properties,local.application.properties
```