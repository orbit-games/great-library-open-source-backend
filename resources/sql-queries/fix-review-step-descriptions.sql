UPDATE `review_step`
SET description = 'You are asked here to <b>review</b> the submitted document. It is from one of your peers in the course. You can view the document by clicking the "View submitted document" button on the left.'
WHERE `step_type` = 'REVIEW';

UPDATE `review_step`
SET description = 'The reviewer was asked to have a look at your submission. You have to <b>evaluate</b> the quality of their reviews and comments: was the review reflective, informative and supportive, or too generic or too negative to be helpful?

Note that you are asked to evaluate whether the reviewer’s comments were helpful to improve your writing. <i>It is not about whether you think the reviewer’s understanding is right or wrong.</i>

You see your original submission by clicking the "View submitted document" button on the left, and the reviewer\'s comments below.

Please evaluate them based on the 3 criteria.'
WHERE `step_type` = 'EVALUATION';

UPDATE `review_step`
SET description = 'Above, you see your original submission and all reviewer\'s comments.

After you have made revisions to your document - based on their comments - please write a <b>rebuttal</b> to them.

The rebuttal is a point-by-point response to the reviewers’ comments. Some tips: identify the major concerns of the reviewers, consider to categorize the comments and/or to add a paragraph that broadly summarizes the major changes that you have made. Make sure to answer each comment and explain how you dealt with that comment in a clear and concise manner. Even if you do not agree with a point or have not made the change suggested, please mention that and provide a reason for your decision.

Keep in mind that the reviewer is always right, also in case you feel the reviewer has misunderstood something. If so, consider it likely to be due to lack of clarity in your presentation. In such cases, point out the misunderstanding politely and provide the necessary clarification.'
WHERE `step_type` = 'REBUTTAL';

UPDATE `review_step`
SET description = 'Below, you see the rebuttal text written by the original submitter. They should have replied content-wise to both reviews, from you and another reviewer.

You are now asked to assess the rebuttal. The author of original document was expected to explain how your comments were interpreted and dealt with. Did they take your feedback seriously, and do they offer an explanation for not following up on specific parts of your peer review?'
WHERE `step_type` = 'ASSESSMENT';