package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import io.swagger.model.ClientVersionCheckResponseStatus;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DomainToRestModelConverterTest {

    @Autowired
    private DomainToRestModelConverter domainToRestModelConverter;

    @Test
    public void convertReviewerRelationGenerationTypeTest() {
        for (ReviewerRelationGenerationType type: ReviewerRelationGenerationType.values()) {
            io.swagger.model.ReviewerRelationGenerationType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertClientVersionCheckResponseStatusTest() {
        for (ClientVersionCheckResultStatus type: ClientVersionCheckResultStatus.values()) {
            ClientVersionCheckResponseStatus converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertPlatformTest() {
        for (Platform type: Platform.values()) {
            io.swagger.model.Platform converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertPeerTypeTest() {
        for (PeerType type: PeerType.values()) {
            io.swagger.model.PeerType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertQuestionTypeTest() {
        for (QuestionType type: QuestionType.values()) {
            io.swagger.model.QuestionType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertStepTypeTest() {
        for (ReviewStepType type: ReviewStepType.values()) {
            io.swagger.model.StepType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertDiscussionTypeTest() {
        for (DiscussionType type: DiscussionType.values()) {
            io.swagger.model.DiscussionType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertRoleTest() {
        for (Role type: Role.values()) {
            io.swagger.model.Role converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertConversationPermissionTypeTest() {
        for (ConversationPermissionType type: ConversationPermissionType.values()) {
            io.swagger.model.ConversationPermissionType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertConversationEmailNotificationTypeTest() {
        for (ConversationEmailNotificationType type: ConversationEmailNotificationType.values()) {
            io.swagger.model.ConversationEmailNotificationType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertConversationTypeTest() {
        for (ConversationType type: ConversationType.values()) {
            io.swagger.model.ConversationType converted = domainToRestModelConverter.convert(type);
            assertNotNull(converted);
        }
    }
}