package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import io.swagger.model.ConversationEmailNotificationType;
import io.swagger.model.ConversationType;
import io.swagger.model.DiscussionType;
import io.swagger.model.QuestionType;
import io.swagger.model.ReviewerRelationGenerationType;
import io.swagger.model.Role;
import io.swagger.model.StepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestToDomainModelConverterTest {

    @Autowired RestToDomainModelConverter restToDomainModelConverter;

    @Test
    public void convertRoleTest() {
        for (Role original: Role.values()) {
            nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role converted = restToDomainModelConverter.convert(original);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertConversationEmailNotificationTypeTest() {
        for (ConversationEmailNotificationType type: ConversationEmailNotificationType.values()) {
            nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationEmailNotificationType converted = restToDomainModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertConversationTypeTest() {
        for (ConversationType type: ConversationType.values()) {
            nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationType converted = restToDomainModelConverter.convert(type);
            assertNotNull(converted);
        }
    }


    @Test
    public void convertQuestionTypeTest() {
        for (QuestionType type: QuestionType.values()) {
            nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType converted = restToDomainModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertStepTypeTest() {
        for (StepType type: StepType.values()) {
            ReviewStepType converted = restToDomainModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertDiscussionTypeTest() {
        for (DiscussionType type: DiscussionType.values()) {
            nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType converted = restToDomainModelConverter.convert(type);
            assertNotNull(converted);
        }
    }

    @Test
    public void convertReviewerRelationGenerationTypeTest() {

        for (ReviewerRelationGenerationType type: ReviewerRelationGenerationType.values()) {
            assertNotNull(restToDomainModelConverter.convert(type));
        }
    }

}