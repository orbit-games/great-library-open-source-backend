package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupBasedReviewerRelationGeneratorTest {

    @Test
    public void testRandomReviewerGeneration() {

        List<User> users = new ArrayList<>();
        final int testUserCount = 29;

        for (int i = 0; i < testUserCount; i++) {
            User newUser = new User();
            newUser.setUsername("user" + i);
            newUser.setRef(RefGenerator.generate());
            newUser.setRole(Role.LEARNER);
            users.add(newUser);
        }

        Chapter chapter = new Chapter();
        chapter.setRef(RefGenerator.generate());

        for (int reviewerCount = 2; reviewerCount < 5; reviewerCount++) {
            ReviewerRelationGenerator generator = new GroupBasedReviewerRelationGenerator();
            List<ReviewerRelation> reviewerRelations = generator.generateRelations(chapter, users, reviewerCount);

            Map<User, List<User>> submitters = reviewerRelations.stream()
                    .collect(Collectors.groupingBy(ReviewerRelation::getReviewer, Collectors.mapping(ReviewerRelation::getSubmitter, Collectors.toList())));

            Map<User, List<User>> reviewers = reviewerRelations.stream()
                    .collect(Collectors.groupingBy(ReviewerRelation::getSubmitter, Collectors.mapping(ReviewerRelation::getReviewer, Collectors.toList())));

            for (User user : users) {
                // Validate that each user indeed has the correct number of reviewers
                Assert.assertThat(reviewers.get(user).size(), Matchers.equalTo(reviewerCount));
                // Assert that the reviewers are unique
                Assert.assertEquals(reviewers.get(user).size(), reviewers.get(user).stream().distinct().count());

                // Validate that each user indeed has the correct number of submitters
                Assert.assertThat(reviewerCount, Matchers.equalTo(reviewerCount));
                // Assert that the submitters are unique
                Assert.assertEquals(submitters.get(user).size(), submitters.get(user).stream().distinct().count());
            }

            reviewerRelations.forEach(r -> Assert.assertNotSame(r.getSubmitter(), r.getReviewer()));
        }
    }
}
