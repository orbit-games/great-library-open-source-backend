package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import org.apache.commons.lang3.tuple.Pair;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CollectionUtilsTests {

    @Test
    public void findAddedTest() {

        List<String> org = Arrays.asList("a", "b", "c");

        // Case: just one addition
        {
            List<String> mod1 = Arrays.asList("a", "b", "c", "d");

            Collection<String> added1 = CollectionUtils.findAdded(mod1, org, Objects::equals);

            Assert.assertThat(added1.size(), Matchers.equalTo(1));
            Assert.assertThat(added1, Matchers.contains("d"));
        }

        // Case: one removal
        {
            List<String> mod2 = Arrays.asList("a", "b");
            Collection<String> added2 = CollectionUtils.findAdded(mod2, org, Objects::equals);

            Assert.assertThat(added2.size(), Matchers.equalTo(0));
        }

        // Case: Removals and additions:
        {
            List<String> mod3 = Arrays.asList("a", "b", "d");
            Collection<String> added3 = CollectionUtils.findAdded(mod3, org, Objects::equals);

            Assert.assertThat(added3.size(), Matchers.equalTo(1));
            Assert.assertThat(added3, Matchers.contains("d"));
        }
    }

    @Test
    public void findRemovedTest() {

        List<String> org = Arrays.asList("a", "b", "c");

        // Case: just one addition
        {
            List<String> mod1 = Arrays.asList("a", "b", "c", "d");

            Collection<String> added1 = CollectionUtils.findRemoved(mod1, org, Objects::equals);

            Assert.assertThat(added1.size(), Matchers.equalTo(0));
        }

        // Case: one removal
        {
            List<String> mod2 = Arrays.asList("a", "b");
            Collection<String> added2 = CollectionUtils.findRemoved(mod2, org, Objects::equals);

            Assert.assertThat(added2.size(), Matchers.equalTo(1));
            Assert.assertThat(added2, Matchers.contains("c"));
        }

        // Case: Removals and additions:
        {
            List<String> mod3 = Arrays.asList("a", "b", "d");
            Collection<String> added3 = CollectionUtils.findRemoved(mod3, org, Objects::equals);

            Assert.assertThat(added3.size(), Matchers.equalTo(1));
            Assert.assertThat(added3, Matchers.contains("c"));
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void findExistingTest() {

        // Case: one addition
        List<String> org = Arrays.asList("a", "b", "c");
        List<String> mod1 = Arrays.asList("a", "b", "c", "d");

        Collection<Pair<String, String>> added1 = CollectionUtils.findExisting(mod1, org, Objects::equals);

        Assert.assertThat(added1.size(), Matchers.equalTo(3));
        Assert.assertThat(added1, Matchers.contains(Pair.of("a", "a"), Pair.of("b", "b"), Pair.of("c", "c")));

        // Case: one removals
        List<String> mod2 = Arrays.asList("a", "b");
        Collection<Pair<String, String>> added2 = CollectionUtils.findExisting(mod2, org, Objects::equals);

        Assert.assertThat(added2.size(), Matchers.equalTo(2));
        Assert.assertThat(added2, Matchers.contains(Pair.of("a", "a"), Pair.of("b", "b")));

        // Case: Removals and additions:
        List<String> mod3 = Arrays.asList("a", "b", "d");
        Collection<Pair<String, String>> added3 = CollectionUtils.findExisting(mod3, org, Objects::equals);

        Assert.assertThat(added3.size(), Matchers.equalTo(2));
        Assert.assertThat(added3, Matchers.contains(Pair.of("a", "a"), Pair.of("b", "b")));
    }

    @Test
    public void anyMatchTest() {

        List<String> lst1 = Arrays.asList("a", "b", "c");

        Assert.assertTrue(CollectionUtils.anyMatch(lst1, "b"::equals));
        Assert.assertFalse(CollectionUtils.anyMatch(lst1, "d"::equals));

        List<String> lst2 = Collections.singletonList("a");
        Assert.assertTrue(CollectionUtils.anyMatch(lst2, "a"::equals));
        Assert.assertFalse(CollectionUtils.anyMatch(lst2, "d"::equals));

        List<String> lst3 = Collections.emptyList();
        Assert.assertFalse(CollectionUtils.anyMatch(lst3, "a"::equals));
        Assert.assertFalse(CollectionUtils.anyMatch(lst3, x -> true));
    }

    @Test
    public void allMatchTest() {

        List<String> org = Arrays.asList("a1", "a2", "a3");

        Assert.assertFalse(CollectionUtils.allMatch(org, "a1"::equals));
        Assert.assertFalse(CollectionUtils.allMatch(org, "b"::equals));
        Assert.assertTrue(CollectionUtils.allMatch(org, x -> x.startsWith("a")));

        List<String> lst2 = Collections.singletonList("a");
        Assert.assertTrue(CollectionUtils.allMatch(lst2, "a"::equals));
        Assert.assertFalse(CollectionUtils.allMatch(lst2, "d"::equals));

        List<String> lst3 = Collections.emptyList();
        Assert.assertTrue(CollectionUtils.allMatch(lst3, "a"::equals));
        Assert.assertTrue(CollectionUtils.allMatch(lst3, x -> true));
    }

    @Test
    public void noneMatchTest() {

        List<String> org = Arrays.asList("a1", "a2", "a3");

        Assert.assertFalse(CollectionUtils.noneMatch(org, "a1"::equals));
        Assert.assertTrue(CollectionUtils.noneMatch(org, "b"::equals));
        Assert.assertFalse(CollectionUtils.noneMatch(org, x -> x.startsWith("a")));

        List<String> lst2 = Collections.singletonList("a");
        Assert.assertFalse(CollectionUtils.noneMatch(lst2, "a"::equals));
        Assert.assertTrue(CollectionUtils.noneMatch(lst2, "d"::equals));

        List<String> lst3 = Collections.emptyList();
        Assert.assertTrue(CollectionUtils.noneMatch(lst3, "a"::equals));
        Assert.assertTrue(CollectionUtils.noneMatch(lst3, x -> true));
    }

    @Test
    public void findByTest() {

        List<String> org = Arrays.asList("car", "house", "rabbit");

        Assert.assertThat(CollectionUtils.findBy(org, x -> x.startsWith("h")).get(), Matchers.equalTo("house"));
        Assert.assertFalse(CollectionUtils.findBy(org, x -> x.startsWith("x")).isPresent());
        Assert.assertFalse(CollectionUtils.findBy(Collections.emptyList(), x -> true).isPresent());

    }

    @Test
    public void mapToListTest() {

        List<String> org = Arrays.asList("a", "b", "c");

        Assert.assertThat(CollectionUtils.mapToList(org, String::toUpperCase), Matchers.equalTo(Arrays.asList("A", "B", "C")));
        Assert.assertThat(CollectionUtils.mapToList(null, Object::toString), Matchers.equalTo(Collections.emptyList()));
        Assert.assertThat(CollectionUtils.mapToList(Collections.singleton("a"), String::toUpperCase), Matchers.equalTo(Collections.singletonList("A")));
    }

    @Test
    public void mapToSetTest() {

        List<String> org = Arrays.asList("a", "b", "c");

        Assert.assertThat(CollectionUtils.mapToSet(org, String::toUpperCase), Matchers.equalTo(new HashSet<>(Arrays.asList("A", "B", "C"))));
        Assert.assertThat(CollectionUtils.mapToSet(null, Object::toString), Matchers.equalTo(Collections.emptySet()));
        Assert.assertThat(CollectionUtils.mapToSet(Collections.singleton("a"), String::toUpperCase), Matchers.equalTo(Collections.singleton("A")));
    }
}
