package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.useragreement;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserAgreementRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserAgreementController {

    private final UserAgreementRepository userAgreementRepository;

    public UserAgreementController(
            @Autowired UserAgreementRepository userAgreementRepository
    ) {
        this.userAgreementRepository = userAgreementRepository;
    }

    @Transactional(readOnly = true)
    public Collection<UserAgreement> getAll() {

        List<UserAgreement> agreements = new ArrayList<>();
        userAgreementRepository.findAll().forEach(agreements::add);
        return agreements;
    }

    /**
     * @return The latest user agreement (or null if there aren't any)
     */
    @Transactional(readOnly = true)
    public UserAgreement getLatestUserAgreement() {

        return userAgreementRepository.findLatest().orElse(null);
    }

    @Transactional(readOnly = true)
    public UserAgreement getUserAgreement(String ref) {
        return userAgreementRepository.findByRef(ref)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user agreement with ref '" + ref + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    @Transactional
    public UserAgreement addUserAgreement(UserAgreement userAgreement) {

        UserAgreement newUserAgreement = new UserAgreement();
        newUserAgreement.setRef(RefGenerator.generate());
        newUserAgreement.setPdfUrl(userAgreement.getPdfUrl());
        newUserAgreement.setStartDate(userAgreement.getStartDate());

        return userAgreementRepository.save(newUserAgreement);

    }

}
