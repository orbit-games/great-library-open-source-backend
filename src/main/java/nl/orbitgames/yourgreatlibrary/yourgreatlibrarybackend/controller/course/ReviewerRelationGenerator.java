package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;

import java.util.List;

public interface ReviewerRelationGenerator {

    /**
     * Generates reviewer relations, and returns all relations that need to be added in order to get a fair distribution of reviewer and submitter.
     * @param users The users
     * @param reviewerCount The number of reviewers wanted per submitter
     * @return The list of reviewer relations to be created
     */
    List<ReviewerRelation> generateRelations(Chapter chapter, List<User> users, int reviewerCount);

    static ReviewerRelation createRelation(Chapter chapter, User submitter, User reviewer) {

        ReviewerRelation reviewerRelation = new ReviewerRelation();
        reviewerRelation.setRef(RefGenerator.generate());
        reviewerRelation.setChapter(chapter);
        reviewerRelation.setSubmitter(submitter);
        reviewerRelation.setReviewer(reviewer);
        return reviewerRelation;
    }
}
