package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class RandomReviewerRelationGenerator implements  ReviewerRelationGenerator {

    private static final Random random = new SecureRandom();

    @Override
    public List<ReviewerRelation> generateRelations(Chapter chapter, List<User> users, int reviewerCount) {

        if (users.size() < reviewerCount + 1) {
            throw new OrbitGamesApplicationException("Not enough users in this course to reach the required number of reviewers", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        // Generate the initial graph by shuffling the list of users, and connect each of them to the two users following
        // e.g., students 1, 2, 3, 4, 5 with two reviewers each:
        // 1 -> 2, 3
        // 2 -> 3, 4
        // 3 -> 4, 5
        // 4 -> 5, 1
        // 5 -> 1, 2
        List<User> shuffledUsers = new ArrayList<>(users);
        Collections.shuffle(shuffledUsers, random);

        List<ReviewerRelation> relations = new ArrayList<>();
        for (int i = 0; i < shuffledUsers.size(); i++) {
            User user = shuffledUsers.get(i);
            for (int j = 1; j <= reviewerCount; j++) {
                User reviewer = shuffledUsers.get((i + j) % shuffledUsers.size());
                relations.add(ReviewerRelationGenerator.createRelation(chapter, user, reviewer));
            }
        }

        // Then we start "swapping" relations around. This will not affect the regularity of the graph, but does make it more random,
        // e.g: edges 1 -> 2, and 4 -> 5 can be switched to: 1 -> 5, 4 -> 2
        // Note that for each swap, we have to check if it doesn't create a duplicate edge
        // There are more switching techniques that also work: https://arxiv.org/pdf/0912.0685.pdf

        // We'll do one potential "swap" per relation:
//        int swapCount = 0;
//        int skippedSwaps = 0;
        for (int i = 0; i < relations.size(); i++) {
            ReviewerRelation relation1 = relations.get(random.nextInt(relations.size()));
            ReviewerRelation relation2 = relations.get(random.nextInt(relations.size()));

            // Note: left and right here are kind of arbitrary, it just means that relation 1 and relation 2 after the swap
            boolean leftSwapExists = relations.stream().anyMatch(r -> relation1.getSubmitter() == r.getSubmitter() && relation2.getReviewer() == r.getReviewer());
            boolean rightSwapExists = relations.stream().anyMatch(r -> relation2.getSubmitter() == r.getSubmitter() && relation1.getReviewer() == r.getReviewer());
            boolean leftBackSwapExists = relations.stream().anyMatch(r -> relation1.getSubmitter() == r.getReviewer() && relation2.getReviewer() == r.getSubmitter());
            boolean rightBackSwapExists = relations.stream().anyMatch(r -> relation2.getSubmitter() == r.getReviewer() && relation1.getReviewer() == r.getSubmitter());
            boolean leftSwapCreatesSelfRelation = relation1.getSubmitter() == relation2.getReviewer();
            boolean rightSwapCreateSelfRelation = relation2.getSubmitter() == relation1.getReviewer();

            if (leftSwapExists || rightSwapExists || leftBackSwapExists || rightBackSwapExists || leftSwapCreatesSelfRelation || rightSwapCreateSelfRelation) {
                // Don't swap into duplicate relationships
//                skippedSwaps++;
                continue;
            }

            // TODO Maybe skip the swap if the relation already existed in a previous chapter? Or start with swapping all relations that existed before?
            User originalRelation1Reviewer = relation1.getReviewer();
            relation1.setReviewer(relation2.getReviewer());
            relation2.setReviewer(originalRelation1Reviewer);
//            swapCount++;
        }
//        System.out.println("Swap count: " + swapCount + ", skipped swaps: " + skippedSwaps);

        return relations;
    }

    /**
     * Indicates if the submitter can (still) review the reviewer, which is the case if he/she is not already a reviewer
     * for that user, and the submitter is not the same user as the reviewer
     * @param submitter The submitter
     * @param reviewer The reviewer
     * @param assignedReviewers Which submitters are currently assigned to which reviewers
     * @return Boolean indicating if the relation is valid
     */
    private boolean canReview(User submitter, User reviewer,  Map<User, Set<User>> assignedReviewers) {
        return submitter != reviewer && !assignedReviewers.get(submitter).contains(reviewer);
    }
}
