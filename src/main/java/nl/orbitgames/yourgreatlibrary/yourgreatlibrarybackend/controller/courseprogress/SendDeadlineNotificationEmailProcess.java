package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.EmailService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Set;

@Component
public class SendDeadlineNotificationEmailProcess {

    private static final Logger logger = LoggerFactory.getLogger(SendDeadlineNotificationEmailProcess.class);

    private EmailService emailService;
    private YourGreatLibraryConfig config;
    private ReviewerRelationGenerationProcess reviewerRelationGenerationProcess;
    private MessageSource messageSource;
    private ReviewStepRepository reviewStepRepository;
    private CourseRepository courseRepository;

    @Autowired
    public SendDeadlineNotificationEmailProcess(
            EmailService emailService,
            YourGreatLibraryConfig config,
            ReviewerRelationGenerationProcess reviewerRelationGenerationProcess,
            MessageSource messageSource,
            ReviewStepRepository reviewStepRepository,
            CourseRepository courseRepository) {

        this.emailService = emailService;
        this.config = config;
        this.reviewerRelationGenerationProcess = reviewerRelationGenerationProcess;
        this.messageSource = messageSource;
        this.reviewStepRepository = reviewStepRepository;
        this.courseRepository = courseRepository;
    }

    @Transactional
    public void sendDeadlineEmailNotification(String courseRef, String chapterRef, String reviewStepRef) {

        ReviewStep reviewStep = reviewStepRepository.findByRef(reviewStepRef)
                .filter(r -> r.getChapter().getRef().equals(chapterRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Can't find review step with ref '" + reviewStepRef + "' in chapter '" + chapterRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        if (reviewStep.isDisabled()) {
            logger.warn("Not sending e-mail for review step with ref '" + reviewStepRef + "' in chapter '" + chapterRef + "'; review step is disabled");
            return;
        }

        Chapter chapter = reviewStep.getChapter();

        Course course = courseRepository.findByRef(courseRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Can't find course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        if (!course.getChapters().contains(chapter)) {
            throw new OrbitGamesApplicationException("Chapter '" + chapter.getRef() + "' is not part of course '" + course.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND);
        }

        Set<UserCourse> userCourses = course.getUserCourses();

        for (UserCourse userCourse: userCourses) {

            Locale locale = ObjectUtils.firstNonNull(userCourse.getUser().getPreferredLocale(), Locale.getDefault());

            String subject = messageSource.getMessage(Messages.DEADLINE_NOTIFICATION_EMAIL_SUBJECT, new Object[]{
                            /*0*/ reviewStep.getName(),
                            /*1*/ course.getChapters().indexOf(chapter) + 1},
                    locale);

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(messageSource.getMessage(Messages.DATE_FORMAT, new Object[0], locale), locale);
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern(messageSource.getMessage(Messages.TIME_FORMAT, new Object[0], locale), locale);

            String additionalInformation = "";
            // If the notification is for the first review step, we give additional information
            ReviewerRelationGenerationSettings reviewerRelationGenerationSettings = reviewerRelationGenerationProcess.getReviewerRelationGenerationSettings(reviewStep.getChapter());

            if (chapter.getReviewSteps().get(0) == reviewStep && reviewerRelationGenerationSettings.isAutoGenerate()) {

                OffsetDateTime reviewerRelationGenerationTime = reviewerRelationGenerationProcess.getReviewerRelationGenerationTime(chapter);
                additionalInformation = messageSource.getMessage(Messages.DEADLINE_NOTIFICATION_EMAIL_ADDITIONAL_FIRST_STEP, new Object[]{
                        /*0*/ reviewerRelationGenerationTime.format(dateFormat),
                        /*1*/ reviewerRelationGenerationTime.format(timeFormat)
                }, locale);
            }

            String text = messageSource.getMessage(Messages.DEADLINE_NOTIFICATION_EMAIL_TEXT, new Object[]{
                    ObjectUtils.firstNonNull(userCourse.getDisplayName(), userCourse.getUser().getFullName()),
                    reviewStep.getDeadline().format(dateFormat),
                    reviewStep.getDeadline().format(timeFormat),
                    reviewStep.getName(),
                    course.getChapters().indexOf(chapter) + 1,
                    chapter.getName(),
                    additionalInformation,
            }, locale);

            final InternetAddress userEmail;
            try {
                userEmail = new InternetAddress(userCourse.getUser().getEmail());
            } catch (AddressException e) {
                logger.warn("Failed to construct InternetAddress for email '" + userCourse.getUser().getEmail() + "', skipping deadline notification e-mail", e);
                continue;
            }

            logger.info("Sending e-mail with subject '" + subject + "' to '" + userEmail.toString() + "'");
            emailService.sendSimpleMessage(userEmail, subject, text);
        }
    }
}
