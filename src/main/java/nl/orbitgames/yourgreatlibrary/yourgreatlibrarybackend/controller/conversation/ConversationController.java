package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationEmailSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationReadUntil;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SystemProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ConversationEmailSettingsRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ConversationMessageRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ConversationReadUntilRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ConversationRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SystemPropertyRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationEmailNotificationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationPermissionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.EmailService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.InternetAddress;
import javax.management.Notification;
import java.io.UnsupportedEncodingException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ConversationController {

    private static final Logger logger = LoggerFactory.getLogger(ConversationController.class);

    private static final String EMAIL_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY = "EmailNotificationLastMessageId";
    private static final String EMAIL_DAILY_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY = "EmailDailyNotificationLastMessageId";

    private final CourseRepository courseRepository;
    private final UserRepository userRepository;
    private final ConversationRepository conversationRepository;
    private final ConversationMessageRepository messageRepository;
    private final ConversationReadUntilRepository conversationReadUntilRepository;
    private final ConversationEmailSettingsRepository conversationEmailSettingsRepository;
    private final SystemPropertyRepository systemPropertyRepository;

    private final PermissionController permissionController;
    private final EmailService emailService;
    private final MessageSource messageSource;
    private final YourGreatLibraryConfig config;

    private final UserSession userSession;

    public ConversationController(
            @Autowired CourseRepository courseRepository,
            @Autowired UserRepository userRepository,
            @Autowired ConversationRepository conversationRepository,
            @Autowired ConversationMessageRepository messageRepository,
            @Autowired ConversationReadUntilRepository conversationReadUntilRepository,
            @Autowired ConversationEmailSettingsRepository conversationEmailSettingsRepository,
            @Autowired SystemPropertyRepository systemPropertyRepository,

            @Autowired PermissionController permissionController,
            @Autowired EmailService emailService,
            @Autowired MessageSource messageSource,
            @Autowired YourGreatLibraryConfig config,

            @Autowired UserSession userSession
    ) {
        this.courseRepository = courseRepository;
        this.userRepository = userRepository;
        this.conversationRepository = conversationRepository;
        this.messageRepository = messageRepository;
        this.conversationReadUntilRepository = conversationReadUntilRepository;
        this.systemPropertyRepository = systemPropertyRepository;
        this.conversationEmailSettingsRepository = conversationEmailSettingsRepository;

        this.permissionController = permissionController;
        this.emailService = emailService;
        this.messageSource = messageSource;
        this.config = config;

        this.userSession = userSession;
    }

    @Transactional(readOnly = true)
    public List<ConversationWrapper> getConversations(String courseRef, OffsetDateTime updated, Set<String> conversationRefs) {
        return getConversations(courseRef, updated, 10, conversationRefs);
    }

    @Transactional(readOnly = true)
    public List<ConversationWrapper> getConversations(String courseRef, OffsetDateTime updated, long messageLimit, Set<String> conversationRefs) {

        this.permissionController.checkCourseViewPermission(courseRef);

        Course course = getCourseInternal(courseRef);
        User user = getUserInternal(userSession.getUserRef());

        if (updated == null) {
            updated = constructMinimumViableOffsetDateTime();
        }

        // HACK: MySQL/Hibernate do not support passing empty collections as parameters, so we're just gonna add something to the collections that can't be matched...
        // TODO: Solve this without a hack...
        if (conversationRefs.isEmpty()) {
            // Note that "0" is never a valid/existing ref.
            conversationRefs = Collections.singleton("0");
        }

        Set<Conversation> conversations =
                this.conversationRepository.findConversationsByCourseAndUserUpdatedSince(course, user, user.getRole(), conversationRefs, updated);
        List<ConversationMessage> messages = conversations.isEmpty()
                ? Collections.emptyList()
                : this.messageRepository.findByConversationIn(new HashSet<>(conversations), messageLimit, updated);

        Collection<ConversationReadUntil> conversationReadUntils = conversationReadUntilRepository.findByUserAndConversationIn(user, conversations);

        Collection<ConversationEmailSettings> emailSettings = conversationEmailSettingsRepository.findByUserAndConversationIn(user, conversations);

        Map<Conversation, List<ConversationMessage>> messageMapping = messages.stream()
                .collect(Collectors.groupingBy(ConversationMessage::getConversation));

        Map<Conversation, Long> conversationsReadUntil = conversationReadUntils.stream()
                .collect(Collectors.toMap(ConversationReadUntil::getConversation, ConversationReadUntil::getReadUntil));

        Map<Conversation, ConversationEmailNotificationType> conversationEmailNotificationType = emailSettings.stream()
                .collect(Collectors.toMap(ConversationEmailSettings::getConversation, ConversationEmailSettings::getNotificationType));

        return conversations.stream()
                .distinct()
                .map(c -> new ConversationWrapper(
                        c,
                        conversationsReadUntil.getOrDefault(c, -1L),
                        conversationEmailNotificationType.computeIfAbsent(c, c2 -> getDefaultEmailNotificationType(user, c2)),
                        messageMapping.getOrDefault(c, Collections.emptyList())))
                .collect(Collectors.toList());
    }

    @Transactional
    public ConversationWrapper createConversation(String courseRef, Conversation conversationToAdd) {

        // Note: Anybody that may view the course may start/create conversations within it (at least for now)
        this.permissionController.checkCourseViewPermission(courseRef);

        logger.info("User {} ({}) is creating a conversation within course {}",
                userSession.getUsername(), userSession.getUserRef(), courseRef);

        Course course = getCourseInternal(courseRef);
        Set<User> users = userRepository.findByRefIn(conversationToAdd.getUsers().stream().map(User::getRef).collect(Collectors.toSet()));

        final User loggedInUser;
        if (userSession.getUsername() != null) {
            loggedInUser = getLoggedInUserInternal();
        } else {
            loggedInUser = null;
        }

        Conversation newConversation = new Conversation();
        newConversation.setRef(RefGenerator.generate());
        newConversation.setCourse(course);
        newConversation.setHidden(conversationToAdd.isHidden());
        newConversation.setAnonymousMembers(conversationToAdd.isAnonymousMembers());
        newConversation.setTitle(conversationToAdd.getTitle());
        newConversation.setUsers(users);
        newConversation.setConversationRoles(conversationToAdd.getConversationRoles());
        newConversation.setType(conversationToAdd.getType());

        // No messages yet, so the last message index is currently "-1"
        newConversation.setLastMessageIndex(-1);

        return new ConversationWrapper(conversationRepository.save(newConversation),
                -1,
                getDefaultEmailNotificationType(loggedInUser, newConversation),
                Collections.emptyList());
    }

    @Transactional(readOnly = true)
    public ConversationWrapper getConversation(String courseRef, String conversationRef, long messageSkip, long messageLimit, OffsetDateTime updated) {

        this.permissionController.checkCourseViewPermission(courseRef);

        if (updated == null) {
            updated = constructMinimumViableOffsetDateTime();
        }

        User user = getLoggedInUserInternal();
        Conversation conversation = getConversationInternal(courseRef, conversationRef);
        long conversationReadUntil = conversationReadUntilRepository.findByUserAndConversation(user, conversation)
                .map(ConversationReadUntil::getReadUntil)
                .orElse(-1L);

        ConversationEmailNotificationType emailNotificationType = conversationEmailSettingsRepository.findByUserAndConversation(user, conversation)
                .map(ConversationEmailSettings::getNotificationType)
                .orElseGet(() -> getDefaultEmailNotificationType(user, conversation));

        List<ConversationMessage> messages = messageRepository.findByConversation(conversation, messageSkip, messageLimit, updated);

        return new ConversationWrapper(conversation, conversationReadUntil, emailNotificationType, messages);
    }


    @Transactional
    public void updateEmailNotificationType(String courseRef, String conversationRef, ConversationEmailNotificationType newEmailNotificationType) {

        this.permissionController.checkCourseViewPermission(courseRef);

        logger.info("User {} ({}) is setting the email notification type for conversation {} in course {} to {}",
                userSession.getUsername(), userSession.getUserRef(), conversationRef, courseRef, newEmailNotificationType);

        User user = getLoggedInUserInternal();
        Conversation conversation = getConversationInternal(courseRef, conversationRef);

        ConversationEmailSettings emailSettings = conversationEmailSettingsRepository.findByUserAndConversation(user, conversation)
                .orElseGet(() -> createConversationEmailSettings(user, conversation));

        emailSettings.setNotificationType(newEmailNotificationType);

        conversationEmailSettingsRepository.save(emailSettings);
    }

    @Transactional
    public ConversationMessage postMessage(String courseRef, String conversationRef, String messageToPost) {

        this.permissionController.checkCourseViewPermission(courseRef);
        logger.info("User {} ({}) is posting a message to conversation {} in course {}",
                userSession.getUsername(), userSession.getUserRef(), conversationRef, courseRef);

        String normalizedMessage = normalize(messageToPost);
        if (normalizedMessage.isEmpty()) {
            throw new OrbitGamesApplicationException("Message may not be empty", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        // The sender is always the logged in user
        User sender = getLoggedInUserInternal();
        Conversation conversation = getConversationInternal(courseRef, conversationRef);

        // Check if this sender is allowed to post a message in this conversation:
        if (!conversation.getUsers().contains(sender) && !roleMayPostConversation(sender.getRole(), conversation)) {
            throw new OrbitGamesAuthorizationException("The logged in user is not allowed to post a message in this conversation");
        }

        conversation.setLastMessageIndex(conversation.getLastMessageIndex() + 1);

        ConversationMessage message = new ConversationMessage();
        message.setRef(RefGenerator.generate());
        message.setConversation(conversation);
        message.setIndexInConversation(conversation.getLastMessageIndex());
        message.setSender(sender);
        message.setSent(OffsetDateTime.now());
        message.setMessage(normalizedMessage);

        // When a user posts a message, we assume that (s)he read at least until this message, so we mark it as read
        markReadUntilInternal(sender, conversation, conversation.getLastMessageIndex());

        return messageRepository.save(message);
    }

    @Transactional
    public ConversationWrapper markAsRead(String courseRef, String conversationRef, Long readUntil) {

        this.permissionController.checkCourseViewPermission(courseRef);
        logger.info("User {} ({}) is marking conversation {} in course {} as read",
                userSession.getUsername(), userSession.getUserRef(), conversationRef, courseRef);


        User user = getLoggedInUserInternal();
        Conversation conversation = getConversationInternal(courseRef, conversationRef);

        if (!conversation.getUsers().contains(user) && !roleMayViewConversation(user.getRole(), conversation)) {
            throw new OrbitGamesAuthorizationException("The logged in user is not allowed to view messages in this conversation");
        }

        long actualReadUntil = Math.min(readUntil, conversation.getLastMessageIndex());

        ConversationReadUntil conversationReadUntil = markReadUntilInternal(user, conversation, actualReadUntil);

        ConversationEmailNotificationType emailNotificationType = conversationEmailSettingsRepository.findByUserAndConversation(user, conversation)
                .map(ConversationEmailSettings::getNotificationType)
                .orElseGet(() -> getDefaultEmailNotificationType(user, conversation));

        List<ConversationMessage> messages = messageRepository.findByConversation(conversation,
                0, conversation.getLastMessageIndex() - actualReadUntil, constructMinimumViableOffsetDateTime());

        return new ConversationWrapper(conversation, conversationReadUntil.getReadUntil(), emailNotificationType, messages);
    }

    private ConversationReadUntil markReadUntilInternal(User user, Conversation conversation, long actualReadUntil) {
        Optional<ConversationReadUntil> readUntilOptional = conversationReadUntilRepository.findByUserAndConversation(user, conversation);
        if (readUntilOptional.isPresent()) {

            readUntilOptional.get().setReadUntil(actualReadUntil);
            return conversationReadUntilRepository.save(readUntilOptional.get());
        } else {

            ConversationReadUntil conversationReadUntil = new ConversationReadUntil();
            conversationReadUntil.setConversation(conversation);
            conversationReadUntil.setUser(user);
            conversationReadUntil.setReadUntil(actualReadUntil);

            return conversationReadUntilRepository.save(conversationReadUntil);

        }
    }


    private String normalize(String message) {
        return message.trim();
    }

    private Course getCourseInternal(String courseRef) {
        return courseRepository.findByRef(courseRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private User getLoggedInUserInternal() {
        return this.getUserInternal(userSession.getUserRef());
    }

    private User getUserInternal(String userRef) {
        return userRepository.findByRef(userRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user with ref '" + userRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Conversation getConversationInternal(String courseRef, String conversationRef) {

        Conversation conversation = conversationRepository.findByRef(conversationRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find conversation with ref '" + conversationRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
        if (!conversation.getCourse().getRef().equals(courseRef)) {
            throw new OrbitGamesApplicationException("Could not find conversation with ref '" + conversationRef + "' within course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND);
        }
        return conversation;
    }

    private boolean roleMayViewConversation(Role role, Conversation conversation) {
        ConversationPermissionType rolePermission = conversation.getConversationRoles().get(role);
        return rolePermission == ConversationPermissionType.POST || rolePermission == ConversationPermissionType.VIEW;
    }

    private boolean roleMayPostConversation(Role role, Conversation conversation) {
        ConversationPermissionType rolePermission = conversation.getConversationRoles().get(role);
        return rolePermission == ConversationPermissionType.POST;
    }

    private OffsetDateTime constructMinimumViableOffsetDateTime() {
        return OffsetDateTime.of(1970, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);
    }

    /**
     * Create the conversations that exist for each user in a course
     * @param user The user that just joined the course for which default conversations are required
     * @param course The course within which the conversations are needed
     */
    @Transactional
    public void createDefaultUserCourseConversations(User user, Course course) {

        // Every user gets a conversation with the admins/developers.
        Conversation adminConversation = new Conversation();
        adminConversation.setTitle("Contact the developers");
        adminConversation.setType(ConversationType.TECHNICAL_SUPPORT);
        adminConversation.setUsers(Collections.singleton(user));
        adminConversation.setConversationRoles(Collections.singletonMap(Role.ADMINISTRATOR, ConversationPermissionType.POST));

        createConversation(course.getRef(), adminConversation);

        // All learners get a conversation with the teachers
        if (user.getRole() == Role.LEARNER) {
            Conversation teacherConversation = new Conversation();
            teacherConversation.setUsers(Collections.singleton(user));
            teacherConversation.setConversationRoles(Collections.singletonMap(Role.TEACHER, ConversationPermissionType.POST));
            teacherConversation.setType(ConversationType.EDUCATIVE_SUPPORT);
            createConversation(course.getRef(), teacherConversation);
        }
    }

    @Transactional
    public void createDefaultCourseConversations(Course course) {

        Conversation allCourseMembersConversation = new Conversation();
        allCourseMembersConversation.setTitle("Everyone in class");
        allCourseMembersConversation.setType(ConversationType.EVERYONE);
        Map<Role, ConversationPermissionType> roles = new HashMap<>();
        roles.put(Role.LEARNER, ConversationPermissionType.POST);
        roles.put(Role.TEACHER, ConversationPermissionType.POST);
        roles.put(Role.ADMINISTRATOR, ConversationPermissionType.POST);
        allCourseMembersConversation.setConversationRoles(roles);
        createConversation(course.getRef(), allCourseMembersConversation);

        Conversation studentsOnlyConversation = new Conversation();
        studentsOnlyConversation.setTitle("Students only");
        studentsOnlyConversation.setType(ConversationType.LEARNERS_ONLY);
        studentsOnlyConversation.setConversationRoles(Collections.singletonMap(Role.LEARNER, ConversationPermissionType.POST));
        createConversation(course.getRef(), studentsOnlyConversation);

        Conversation teachersOnlyConversation = new Conversation();
        teachersOnlyConversation.setTitle("Teachers only");
        teachersOnlyConversation.setType(ConversationType.TEACHERS_ONLY);
        teachersOnlyConversation.setConversationRoles(Collections.singletonMap(Role.TEACHER, ConversationPermissionType.POST));
        createConversation(course.getRef(), teachersOnlyConversation);

        Conversation announcementsConversation = new Conversation();
        announcementsConversation.setTitle("Announcements");
        announcementsConversation.setType(ConversationType.ANNOUNCEMENTS);
        Map<Role, ConversationPermissionType> announcementConversationRoles = new HashMap<>();
        announcementConversationRoles.put(Role.LEARNER, ConversationPermissionType.VIEW);
        announcementConversationRoles.put(Role.TEACHER, ConversationPermissionType.POST);
        announcementConversationRoles.put(Role.ADMINISTRATOR, ConversationPermissionType.POST);
        announcementsConversation.setConversationRoles(announcementConversationRoles);
        createConversation(course.getRef(), announcementsConversation);
    }

    /**
     * Send e-mail notifications to when new messages are sent to them.
     */
    @Transactional
    @Scheduled(initialDelay = 0, fixedDelay = 30000)
    public void sendDirectEmailNotifications() {

        Optional<SystemProperty> lastMessageIdProperty = systemPropertyRepository.findByPropertyKey(EMAIL_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY);
        if (!lastMessageIdProperty.isPresent()) {
            logger.info("Not sending conversation notifications, because no record of previous conversation notifications was found");
            // This is the first time we're sending notifications. Let's not send them for all messages ever, and ignore this round
            SystemProperty newLastMessageIdProperty = new SystemProperty();
            newLastMessageIdProperty.setPropertyKey(EMAIL_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY);
            newLastMessageIdProperty.setPropertyValue(messageRepository.findLast().map(ConversationMessage::getId).orElse(-1L).toString());

            systemPropertyRepository.save(newLastMessageIdProperty);
        } else {
            long lastMessageId = lastMessageIdProperty.map(m -> Long.parseLong(m.getPropertyValue())).orElse(-1L);
            List<ConversationMessage> newMessages = sendEmailNotificationsForMessagesAfter(lastMessageId, ConversationEmailNotificationType.DIRECT);
            if (newMessages == null) return;

            // Update the last notification id
            lastMessageIdProperty.get()
                    .setPropertyValue(Long.toString(newMessages.stream().mapToLong(ConversationMessage::getId).max().orElse(-1L)));

            systemPropertyRepository.save(lastMessageIdProperty.get());
        }
    }

    @Transactional
    @Scheduled(cron = "0 0 21 * * *") // Every evening at 21:00 (9PM)
    public void sendDailyEmailNotifications() {

        Optional<SystemProperty> lastMessageIdProperty = systemPropertyRepository.findByPropertyKey(EMAIL_DAILY_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY);
        if (!lastMessageIdProperty.isPresent()) {
            logger.info("Not sending daily conversation notifications, because no record of previous conversation notifications was found");
            // This is the first time we're sending notifications. Let's not send them for all messages ever, and ignore this round
            SystemProperty newLastMessageIdProperty = new SystemProperty();
            newLastMessageIdProperty.setPropertyKey(EMAIL_DAILY_NOTIFICATION_LAST_MESSAGE_ID_SYSTEM_PROPERTY_KEY);
            newLastMessageIdProperty.setPropertyValue(messageRepository.findLast().map(ConversationMessage::getId).orElse(-1L).toString());

            systemPropertyRepository.save(newLastMessageIdProperty);
        } else {
            long lastMessageId = lastMessageIdProperty.map(m -> Long.parseLong(m.getPropertyValue())).orElse(-1L);
            List<ConversationMessage> newMessages = sendEmailNotificationsForMessagesAfter(lastMessageId, ConversationEmailNotificationType.DAILY);
            if (newMessages == null) return;

            // Update the last notification id
            lastMessageIdProperty.get()
                    .setPropertyValue(Long.toString(newMessages.stream().mapToLong(ConversationMessage::getId).max().orElse(-1L)));

            systemPropertyRepository.save(lastMessageIdProperty.get());
        }

    }

    /**
     * Sends message notification e-mails
     * @param minMessageId The last message ID for which notifications of this type were already sent. Only messages with an ID
     *                     bigger than this one are included in the notification e-mails
     * @param notificationType The notification type is used to determine if a message should be sent at all (or if the user has for example only daily notifications enabled)
     * @return
     */
    private List<ConversationMessage> sendEmailNotificationsForMessagesAfter(long minMessageId, ConversationEmailNotificationType notificationType) {
        List<ConversationMessage> newMessages = messageRepository.findByIdGreaterThan(minMessageId);

        if (newMessages.isEmpty()) {
            logger.debug("No new messages");
            // No new messages
            return null;
        }

        List<Long> conversationIds = newMessages.stream().mapToLong(m -> m.getConversation().getId()).distinct().boxed().collect(Collectors.toList());
        Set<Conversation> relevantConversations = conversationRepository.findByIds(conversationIds);
        Map<Conversation, List<ConversationMessage>> messagesPerConversation =
                newMessages.stream().collect(Collectors.groupingBy(ConversationMessage::getConversation));
        Map<Conversation, List<ConversationEmailSettings>> emailSettingsPerConversation =
                conversationEmailSettingsRepository.findByConversationIn(relevantConversations)
                        .stream()
                        .collect(Collectors.groupingBy(ConversationEmailSettings::getConversation));
        Map<Conversation, Set<ConversationReadUntil>> readUntilPerConversation =
                conversationReadUntilRepository.findByConversationIn(relevantConversations)
                        .stream()
                        .collect(Collectors.groupingBy(ConversationReadUntil::getConversation, Collectors.toSet()));

        Map<User, EmailNotification> notifications = new HashMap<>();
        for (Conversation conversation : relevantConversations) {

            // Find the users for which this conversation is relevant
            Set<User> usersAllowedToView = new HashSet<>(conversation.getUsers());

            for (Role role: Role.values()) {
                if (roleMayViewConversation(role, conversation)) {
                    usersAllowedToView.addAll(getUsersWithRole(conversation, role));
                }
            }

            List<ConversationMessage> newMessagesInConversation = messagesPerConversation.get(conversation);
            Map<User, ConversationEmailNotificationType> conversationEmailSettingsPerUser = emailSettingsPerConversation
                    .getOrDefault(conversation, Collections.emptyList())
                    .stream().collect(Collectors.toMap(ConversationEmailSettings::getUser, ConversationEmailSettings::getNotificationType));

            Map<User, Long> emailNotificationSentUntilPerUser = emailSettingsPerConversation
                    .getOrDefault(conversation, Collections.emptyList())
                    .stream().collect(Collectors.toMap(ConversationEmailSettings::getUser, ConversationEmailSettings::getEmailNotificationsSentUntil));

            Map<User, Long> readUntilPerUser = readUntilPerConversation
                    .getOrDefault(conversation, Collections.emptySet())
                    .stream().collect(Collectors.toMap(ConversationReadUntil::getUser, ConversationReadUntil::getReadUntil));

            for (User user : usersAllowedToView) {

                ConversationEmailNotificationType userNotificationType = conversationEmailSettingsPerUser
                        .computeIfAbsent(user, u -> getDefaultEmailNotificationType(u, conversation));

                if (userNotificationType != notificationType) {
                    // The user is not interested in notifications of the current type, e.g. the user is interested in
                    // daily notifications, but we're sending direct notifications now.
                    continue;
                }

                long userReadUntil = readUntilPerUser.getOrDefault(user, -1L);
                long userNotificationsSentUntil = emailNotificationSentUntilPerUser.getOrDefault(user, -1L);

                List<ConversationMessage> interestingMessages = newMessagesInConversation.stream()
                        // The user is not interested in notifications about messages he/she sent him/herself
                        .filter(m -> m.getSender() != user)
                        // Don't include message the user already read
                        .filter(m -> m.getIndexInConversation() > userReadUntil)
                        // Don't include messages for which notifications were already sent
                        .filter(m -> m.getIndexInConversation() > userNotificationsSentUntil)
                        .collect(Collectors.toList());

                if (!interestingMessages.isEmpty()) {
                    if (!notifications.containsKey(user)) {
                        notifications.put(user, new EmailNotification(user));
                    }
                    notifications.get(user).addConversation(conversation, interestingMessages);
                }
            }
        }

        // Now let's send the e-mails:
        for (EmailNotification notification: notifications.values()) {

            sendNewMessageNotificationEmail(notification);

            // Update the conversation settings to indicate that the notification e-mails have been sent until this point
            for (Pair<Conversation, List<ConversationMessage>> conversationAndMessages: notification.getConversations()) {

                Conversation conversation = conversationAndMessages.getLeft();
                ConversationEmailSettings userConversationEmailSettings = emailSettingsPerConversation.getOrDefault(conversation, Collections.emptyList())
                        .stream()
                        .filter(c -> c.getUser() == notification.getUser())
                        .findAny()
                        .orElseGet(() -> createConversationEmailSettings(notification.getUser(), conversation));

                userConversationEmailSettings.setEmailNotificationsSentUntil(conversation.getLastMessageIndex());
                conversationEmailSettingsRepository.save(userConversationEmailSettings);
            }
        }
        return newMessages;
    }

    private static class EmailNotification {

        private final User user;
        private final List<Pair<Conversation, List<ConversationMessage>>> conversations = new ArrayList<>();

        public EmailNotification(User user) {

            Validate.notNull(user, "The user may not be null");
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public List<Pair<Conversation, List<ConversationMessage>>> getConversations() {
            return conversations;
        }

        public void addConversation(Conversation conversation, List<ConversationMessage> messages) {
            conversations.add(Pair.of(conversation, messages));
        }
    }

    private ConversationEmailNotificationType getDefaultEmailNotificationType(User user, Conversation conversation) {

        if (user == null) {
            return ConversationEmailNotificationType.NONE;
        }

        // By default, conversations where all LEARNERs may post will not have notifications enabled
        if (roleMayPostConversation(Role.LEARNER, conversation)) {
            return ConversationEmailNotificationType.NONE;
        } else {
            // All other conversations have direct notifications
            return ConversationEmailNotificationType.DIRECT;
        }

    }

    private ConversationEmailSettings createConversationEmailSettings(User user, Conversation conversation) {

        ConversationEmailSettings newEmailSettings = new ConversationEmailSettings();
        newEmailSettings.setConversation(conversation);
        newEmailSettings.setUser(user);
        newEmailSettings.setNotificationType(getDefaultEmailNotificationType(user, conversation));
        return newEmailSettings;
    }

    private void sendNewMessageNotificationEmail(EmailNotification emailNotification) {

        User user = emailNotification.getUser();

        logger.info("Sending conversation notification e-mail to user '" + user.getUsername() + "'");
        try {
            Locale userLocale = user.getPreferredLocale();
            if (userLocale == null) {
                userLocale = Locale.getDefault();
            }

            long messageCount = emailNotification.getConversations().stream().flatMap(c -> c.getRight().stream()).filter(m -> m.getSender() != user).count();

            StringBuilder conversationsSummary = new StringBuilder();
            for (Pair<Conversation, List<ConversationMessage>> conversationAndMessages : emailNotification.getConversations()) {
                Conversation conversation = conversationAndMessages.getLeft();
                conversationsSummary.append("In conversation '").append(getDisplayTitle(conversation, user, userLocale)).append("'");

                conversationsSummary.append(" for course '").append(conversation.getCourse().getName()).append("'");
                conversationsSummary.append("\n");

                for (ConversationMessage message : conversationAndMessages.getRight()) {
                    conversationsSummary.append(message.getSent().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(userLocale)));
                    conversationsSummary.append(" ").append(getUserDisplayName(message.getSender(), user, conversation.getCourse())).append(" says");
                    conversationsSummary.append(": ").append(message.getMessage());
                    conversationsSummary.append("\n");
                }
                if (SecurityUtils.canRoleAccess(user.getRole(), Role.TEACHER)) {
                    conversationsSummary.append("Visit ").append(config.getConversationNotificationEmailUrl(conversation.getCourse().getRef(), conversation.getRef())).append(" to respond\n");
                }
                conversationsSummary.append("\n");
            }
            if (user.getRole() == Role.LEARNER) {
                conversationsSummary.append("Visit The Great Library to respond\n\n");
            }

            String senderNames = emailNotification.conversations.stream()
                    .flatMap(n -> n.getRight()
                            .stream()
                            .filter(m -> m.getSender() != user)
                            .map(m -> getUserDisplayName(m.getSender(), user, n.getKey().getCourse()))
                    )
                    .distinct()
                    .collect(Collectors.joining(", "));

            String emailSubject = messageSource.getMessage(Messages.CONVERSATION_NOTIFICATION_EMAIL_SUBJECT,
                    new Object[]{messageCount, senderNames},
                    userLocale);
            String emailBody = messageSource.getMessage(
                    Messages.CONVERSATION_NOTIFICATION_EMAIL_TEXT,
                    new Object[]{user.getFullName(), conversationsSummary.toString()},
                    userLocale
            );
            emailService.sendSimpleMessage(new InternetAddress(user.getEmail(), user.getFullName()), emailSubject, emailBody);

        } catch (UnsupportedEncodingException e) {
            logger.warn("Failed to send conversation notification e-mail to '" + user.getEmail() + "'; The users' email address is not valid");
        }

    }

    private String getDisplayTitle(Conversation conversation, User user, Locale locale) {

        if (StringUtils.isNotBlank(conversation.getTitle())) {
            return conversation.getTitle();
        } else {
            return getConversationMembersSummary(conversation, user, locale);
        }

    }

    private String getConversationMembersSummary(Conversation conversation, User user, Locale locale) {

        List<String> conversationMemberDescriptors = new ArrayList<>();

        if (conversation.getUsers().contains(user)) {
            conversationMemberDescriptors.add(messageSource.getMessage(Messages.YOU, new Object[0],"You", locale));
        }

        for (User conversationMember: conversation.getUsers()) {
            if (conversationMember != user) {
                if (user.getRole() == Role.LEARNER) {
                    // We need to use the display name
                    conversationMemberDescriptors.add(getUserDisplayName(conversationMember, user, conversation.getCourse()));

                } else {
                    // Real name
                    conversationMemberDescriptors.add(conversationMember.getFullName());
                }
            }
        }

        for (Map.Entry<Role, ConversationPermissionType> roleEntry: conversation.getConversationRoles().entrySet()) {

            if (roleEntry.getValue() == ConversationPermissionType.POST) {
                conversationMemberDescriptors.add(roleEntry.getKey().getHumanReadableRoleName());
            } else if (roleEntry.getValue() == ConversationPermissionType.VIEW) {
                conversationMemberDescriptors.add(messageSource.getMessage(Messages.CAN_BE_VIEWED_BY, new Object[] { roleEntry.getKey().getHumanReadableRoleName() }, locale));
            }
        }

        return String.join(", ", conversationMemberDescriptors);
    }

    private String getUserDisplayName(User userToDisplay, User userListening, Course course) {
        if (userListening.getRole() == Role.LEARNER) {
            // We need to use the display name
            return userToDisplay.getUserCourses().stream()
                    .filter(u -> u.getCourse() == course)
                    .findFirst()
                    .map(UserCourse::getDisplayName)
                    // If the user hasn't decided on a full name yet, we have to use the display name.
                    .orElseGet(userToDisplay::getFullName);
        } else {
            return userToDisplay.getFullName();
        }
    }

    private List<User> getUsersWithRole(Conversation conversation, Role role) {
        return conversation.getCourse().getUserCourses().stream()
                .map(UserCourse::getUser)
                .filter(u -> u.getRole() == role)
                .collect(Collectors.toList());
    }

}
