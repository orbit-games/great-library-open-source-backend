package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepArgument;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ReviewStepStatusCalculator {

    /**
     * Returns true iff the review step result is ready to be marked for completion
     * @param reviewStepResult The result of the review step to check.
     * @param allResults A collection of at least the review step results of the same reviewer relation as the review step result that is to be checked.
     * @return Boolean indicating if the review step is ready for completion
     */
    boolean calculateReadyToComplete(ReviewStepResult reviewStepResult, Collection<ReviewStepResult> allResults) {

        ReviewStep step = reviewStepResult.getReviewStep();

        if (step.isDisabled()) {
            return false;
        }

        boolean readyToComplete = true;
        if (step.isFileUpload() && reviewStepResult.getFileUploadMetaData() == null) {
            readyToComplete = false;
        }
        if (step.getQuiz() != null) {
            if (reviewStepResult.getQuizResult() == null) {
                readyToComplete = false;
            } else {
                // Check if the quiz result contains answers to all questions in the quiz
                Set<QuizElement> elementsAnsweredTo = reviewStepResult.getQuizResult().getQuizElementResults().stream()
                        .map(QuizElementResult::getQuizElement)
                        .collect(Collectors.toSet());
                List<QuizElement> elementsToAnswerTo = step.getQuiz().getElements();
                if (!elementsAnsweredTo.containsAll(elementsToAnswerTo)) {
                    // Not all quiz questions answered
                    readyToComplete = false;
                }
            }
        }
        if (step.getDiscussionType() != DiscussionType.NONE) {
            if (reviewStepResult.getArguments() == null || reviewStepResult.getArguments().isEmpty()) {
                readyToComplete = false;
            }
            // If this step is a reply, then we must also ensure that the user replied to all arguments in the previous discussion step
            if (step.getDiscussionType() == DiscussionType.REPLY) {
                Set<ReviewStepArgument> argumentsToReplyTo = findArgumentsToReplyTo(reviewStepResult, allResults);
                Set<ReviewStepArgument> argumentsRepliedTo = reviewStepResult.getArguments().stream().map(ReviewStepArgument::getReplyTo).collect(Collectors.toSet());

                if (!argumentsRepliedTo.containsAll(argumentsToReplyTo)) {
                    readyToComplete = false;
                }
            } else if (step.getDiscussionType() == DiscussionType.RESTRICTED_ARGUMENTS) {

                // In the case of restricted arguments, an argument must be given for at least all required argument types
                Set<ArgumentType> requiredArgumentTypes = step.getArgumentTypes().stream().filter(ArgumentType::isRequired).collect(Collectors.toSet());

                // If there is a required argument type for which there is no argument in the actual review step:
                if (requiredArgumentTypes.stream().anyMatch(r -> reviewStepResult.getArguments().stream().noneMatch(a -> a.getArgumentType() == r))) {
                    readyToComplete = false;
                }
            }
        }

        return readyToComplete;
    }

    OffsetDateTime calculateLastModified(ReviewStepResult reviewStepResult) {

        ReviewStep step = reviewStepResult.getReviewStep();
        List<OffsetDateTime> times = new ArrayList<>();

        times.add(reviewStepResult.getCreated());

        if (step.isFileUpload() && reviewStepResult.getFileUploadMetaData() != null) {
            times.add(reviewStepResult.getFileUploadMetaData().getCreated());
            times.add(reviewStepResult.getFileUploadMetaData().getModified());
        }
        if (step.getQuiz() != null && reviewStepResult.getQuizResult() != null) {
            times.add(reviewStepResult.getQuizResult().getCreated());
            times.add(reviewStepResult.getQuizResult().getModified());
        }
        if (step.getDiscussionType() != DiscussionType.NONE && reviewStepResult.getArguments() != null) {
            times.addAll(reviewStepResult.getArguments().stream().map(ReviewStepArgument::getCreated).collect(Collectors.toSet()));
        }

        return times.stream().filter(Objects::nonNull).max(OffsetDateTime::compareTo).orElseThrow(NullPointerException::new);
    }

    boolean calculateClosed(ReviewStepResult reviewStepResult) {

        // A step is closed if it is marked complete, and the deadline is passed
        return reviewStepResult.getMarkedComplete() != null &&
                (reviewStepResult.getMarkedComplete().equals(OffsetDateTime.now()) || reviewStepResult.getMarkedComplete().isBefore(OffsetDateTime.now())) &&
                reviewStepResult.getReviewStep().getDeadline().isBefore(OffsetDateTime.now());
    }

    Set<ReviewStepArgument> findArgumentsToReplyTo(ReviewStepResult reviewStepResult, Collection<ReviewStepResult> allResults) {

        ReviewStep reviewStepToReplyTo = reviewStepResult.getReviewStep().getChapter().getReviewSteps().stream()
                .filter(r -> r.getDiscussionType() != DiscussionType.NONE)
                .filter(r -> r.getSortOrder() < reviewStepResult.getReviewStep().getSortOrder())
                .max(Comparator.comparing(ReviewStep::getSortOrder))
                .orElseThrow(() -> new OrbitGamesSystemException("Invalid chapter configuration; Could not find a discussion step to reply to"));

        Optional<ReviewStepResult> resultToReplyTo = allResults.stream()
                .filter(r -> r.getReviewStep() == reviewStepToReplyTo)
                .filter(r -> r.getReviewerRelation() == reviewStepResult.getReviewerRelation())
                .findFirst();

        if (!resultToReplyTo.isPresent()) {
            return Collections.emptySet();
        } else {
            return resultToReplyTo.get().getArguments();
        }

    }

}
