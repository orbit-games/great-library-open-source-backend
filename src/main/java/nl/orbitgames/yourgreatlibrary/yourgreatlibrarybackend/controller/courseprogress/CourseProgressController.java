package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation.ConversationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepArgument;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourseProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ArgumentTypeRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.FileUploadMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.QuizResultRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepArgumentRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepResultRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserCourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ChapterProgress;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseProgress;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.PeerType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ResourceResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepResultWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Caches;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.FileService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CourseProgressController {

    private final Logger logger = LoggerFactory.getLogger(CourseProgressController.class);

    private final ReviewerRelationGenerationProcess reviewerRelationGenerationProcess;
    private final ReviewStepStatusCalculator reviewStepStatusCalculator;

    private final UserRepository userRepository;
    private final UserCourseRepository userCourseRepository;
    private final CourseRepository courseRepository;
    private final ReviewerRelationRepository reviewerRelationRepository;
    private final ReviewStepResultRepository reviewStepResultRepository;
    private final QuizResultRepository quizResultRepository;
    private final ChapterRepository chapterRepository;
    private final ReviewStepRepository reviewStepRepository;
    private final FileUploadMetadataRepository fileUploadMetadataRepository;
    private final ReviewStepArgumentRepository reviewStepArgumentRepository;
    private final ArgumentTypeRepository argumentTypeRepository;
    private final ResourceRepository resourceRepository;

    private final PermissionController permissionController;
    private final ConversationController conversationController;

    private final FileService fileService;
    private final YourGreatLibraryConfig config;

    private final UserSession userSession;


    public CourseProgressController(
            @Autowired ReviewerRelationGenerationProcess reviewerRelationGenerationProcess,
            @Autowired ReviewStepStatusCalculator reviewStepStatusCalculator,

            @Autowired UserRepository userRepository,
            @Autowired UserCourseRepository userCourseRepository,
            @Autowired CourseRepository courseRepository,
            @Autowired ReviewerRelationRepository reviewerRelationRepository,
            @Autowired ReviewStepResultRepository reviewStepResultRepository,
            @Autowired QuizResultRepository quizResultRepository,
            @Autowired ChapterRepository chapterRepository,
            @Autowired ReviewStepRepository reviewStepRepository,
            @Autowired FileUploadMetadataRepository fileUploadMetadataRepository,
            @Autowired ReviewStepArgumentRepository reviewStepArgumentRepository,
            @Autowired ArgumentTypeRepository argumentTypeRepository,
            @Autowired ResourceRepository resourceRepository,

            @Autowired FileService fileService,
            @Autowired YourGreatLibraryConfig config,

            @Autowired PermissionController permissionController,
            @Autowired ConversationController conversationController,
            @Autowired UserSession userSession
    ) {
        this.reviewerRelationGenerationProcess = reviewerRelationGenerationProcess;
        this.reviewStepStatusCalculator = reviewStepStatusCalculator;

        this.userRepository = userRepository;
        this.userCourseRepository = userCourseRepository;
        this.courseRepository = courseRepository;
        this.reviewerRelationRepository = reviewerRelationRepository;
        this.reviewStepResultRepository = reviewStepResultRepository;
        this.quizResultRepository = quizResultRepository;
        this.chapterRepository = chapterRepository;
        this.reviewStepRepository = reviewStepRepository;
        this.fileUploadMetadataRepository = fileUploadMetadataRepository;
        this.reviewStepArgumentRepository = reviewStepArgumentRepository;
        this.argumentTypeRepository = argumentTypeRepository;
        this.resourceRepository = resourceRepository;

        this.fileService = fileService;
        this.config = config;

        this.permissionController = permissionController;
        this.conversationController = conversationController;

        this.userSession = userSession;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#result.ref")
    public Course joinCourse(String userRef, String courseCode) {

        logger.info("Logged in user '{}' ('{}') enrolling user '{}' to course with code '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseCode);

        Course course = courseRepository.findByCode(courseCode)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find course with code '" + courseCode + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        return joinCourse(userRef, course);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#result.ref")
    public Course joinCourse(String userRef, Course course) {

        permissionController.checkUserUpdatePermission(userRef);

        User user = getUserInternal(userRef);

        UserCourse userCourse = new UserCourse();
        userCourse.setUser(user);
        userCourse.setCourse(course);
        userCourse.setActive(true);

        // If the user is a teacher or administrator, no fake identity will be used, and the "full name" will be used
        // as the display name:
        if (user.getRole() == Role.TEACHER || user.getRole() == Role.ADMINISTRATOR) {
            userCourse.setDisplayName(user.getFullName());
        }

        userCourseRepository.save(userCourse);

        user.addUserCourse(userCourse);
        course.addUserCourse(userCourse);

        // When a user joins a course, "dummy reviewer relations" will be generated,
        // where the initial submissions can be posted to before the actual reviewer relations are generated.
        // HACK: dummy relations are also generated for teachers and administrators, so that they can open and view
        // the game without rendering issues. However, they will not be included in the review process.
        // TODO: Don't generate dummy relations for teachers and administrators, and handle missing relations in the game
        for (Chapter chapter : course.getChapters()) {
            int reviewerCount = chapter.getReviewerRelationGenerationSettings() == null
                    ? config.getDefaultReviewerRelationGenerationSetting().getReviewerCount()
                    : chapter.getReviewerRelationGenerationSettings().getReviewerCount();

            for (int i = 0; i < reviewerCount; i++) {
                ReviewerRelation reviewerRelation = new ReviewerRelation();
                reviewerRelation.setRef(RefGenerator.generate());
                reviewerRelation.setChapter(chapter);
                reviewerRelation.setSubmitter(user);
                reviewerRelationRepository.save(reviewerRelation);
            }
        }

        conversationController.createDefaultUserCourseConversations(user, course);

        logger.info("User '{}' ('{}') was enrolled to course '{}'", user.getUsername(), user.getRef(), course.getRef());
        return course;

    }

    @Transactional(readOnly = true)
    public UserCourse getUserCourse(String courseRef, String userRef) {

        logger.info("User '{}' ('{}') retrieving UserCourse for user '{}' and course '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef);

        permissionController.checkCourseViewPermission(courseRef);

        UserCourse userCourse = getUserCourseInternal(courseRef, userRef);
        // We fetch the course properties to ensure they exist
        userCourse.getUserCourseProperties().size();

        return userCourse;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public UserCourse updateUserCourse(String courseRef, String userRef, UserCourse userCourseWithUpdates) {

        logger.info("Logged in user '{}' ('{}') updating UserCourse for user '{}' and course '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef);

        permissionController.checkUserCourseUpdatePermission(userRef, courseRef);

        UserCourse userCourse = getUserCourseInternal(courseRef, userRef);
        userCourse.setDisplayName(userCourseWithUpdates.getDisplayName());

        // Only teachers and administrators may update the active state of users
        if (SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER)) {
            userCourse.setActive(userCourseWithUpdates.isActive());
        }

        userCourse.setTopic(userCourseWithUpdates.getTopic());
        userCourseWithUpdates.getUserCourseProperties()
                .forEach((key, value) -> userCourse.putUserCourseProperty(key, value.getPropertyValue()));

        return userCourseRepository.save(userCourse);
    }

    @Transactional(readOnly = true)
    public UserCourseProperty getUserCourseProperty(String courseRef, String userRef, String propertyKey) {

        logger.info("Logged in user '{}' ('{}') retrieving UserCourseProperty for user '{}', course '{}' and property key '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, propertyKey);

        permissionController.checkUserCourseViewPermission(userRef, courseRef);

        UserCourse userCourse = getUserCourseInternal(courseRef, userRef);
        return userCourse.getUserCourseProperties().get(propertyKey);

    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public UserCourseProperty updateUserCourseProperty(String courseRef, String userRef, String oldKey, String newKey, String value) {

        logger.info("Logged in user '{}' ('{}') retrieving UserCourseProperty for user '{}', course '{}' and property key '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, oldKey);

        permissionController.checkUserCourseUpdatePermission(userRef, courseRef);

        UserCourse userCourse = getUserCourseInternal(courseRef, userRef);

        if (!oldKey.equals(newKey)) {
            userCourse.removeUserCourseProperty(oldKey);
        }
        userCourse.putUserCourseProperty(newKey, value);

        userCourseRepository.save(userCourse);

        return userCourse.getUserCourseProperties().get(newKey);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public void generateReviewerRelations(String courseRef, String chapterRef) {

        logger.info("Logged in user '{}' ('{}') generating reviewer relations for course '{}' and chapter '{}'",
                userSession.getUsername(), userSession.getUserRef(), courseRef, chapterRef);

        reviewerRelationGenerationProcess.generateReviewerRelations(courseRef, chapterRef, true);
    }

    @Transactional(readOnly = true)
    public ReviewerRelationGenerationSettings getReviewerRelationGenerationSettings(String courseRef, String chapterRef) {

        Chapter chapter = getChapterInternal(courseRef, chapterRef);

        return reviewerRelationGenerationProcess.getReviewerRelationGenerationSettings(chapter);
    }

    @Transactional(readOnly = true)
    public OffsetDateTime getReviewerRelationGenerationTime(String courseRef, String chapterRef) {

        Chapter chapter = getChapterInternal(courseRef, chapterRef);
        return reviewerRelationGenerationProcess.getReviewerRelationGenerationTime(chapter);
    }

    @Transactional(readOnly = true)
    public CourseProgress getCourseProgress(String userRef, String courseRef) {

        logger.info("Logged in user '{}' ('{}') retrieving course progress for user '{}' and course '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef);

        permissionController.checkFullCourseProgressViewPermission(userRef, courseRef);

        UserCourse userCourse = userCourseRepository.findByCourseRefAndUserRef(courseRef, userRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user course for course '" + courseRef + "' and user '" + userRef + "', are you sure the user is enrolled to the course?", GenericErrorCode.ENTITY_NOT_FOUND));
        User user = getUserInternal(userRef);

        CourseProgress res = new CourseProgress();
        res.setUserCourse(userCourse);

        // All relevant reviewer relations for this user
        Course course = userCourse.getCourse();
        List<ReviewerRelation> reviewerRelations = reviewerRelationRepository.findByReviewerOrSubmitterInCourse(user, course);
        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByUserInCourse(user, course);

        // Force initialization of the discussions
        reviewStepResults.forEach(r -> r.getArguments().size());

        res.setChapterProgress(course.getChapters().stream()
                .map(c -> buildChapterProgress(user, c, reviewerRelations, reviewStepResults))
                .collect(Collectors.toList()));

        List<Quiz> resourceQuizes = course.getResources().stream().map(Resource::getQuiz).collect(Collectors.toList());
        List<QuizResult> resourceQuizResults = quizResultRepository.findByUserAndQuizIn(user, resourceQuizes);
        List<ResourceResult> resourceResults = course.getResources().stream()
                .map(r -> this.buildResourceResult(resourceQuizResults, r))
                .collect(Collectors.toList());
        res.setResourceResults(resourceResults);

        return res;
    }

    private ResourceResult buildResourceResult(List<QuizResult> resourceQuizResults, Resource resource) {
        ResourceResult resourceResult = new ResourceResult();
        resourceResult.setResource(resource);
        if (resource.getQuiz() != null) {
            Optional<QuizResult> quizResult = resourceQuizResults.stream()
                    .filter(rqr -> rqr.getQuiz() == resource.getQuiz())
                    .max(Comparator.comparing(QuizResult::getCreated));
            if (quizResult.isPresent()) {
                enrich(quizResult.get());
                resourceResult.setQuizResult(quizResult.get());
            }
        }

        return resourceResult;
    }

    @Transactional(readOnly = true)
    public ChapterProgress getCourseChapterProgress(String userRef, String courseRef, String chapterRef) {

        logger.info("Logged in user '{}' ('{}') retrieving chapter progress for user '{}', course '{}' and chapter '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef);

        permissionController.checkFullCourseProgressViewPermission(userRef, courseRef);

        User user = getUserInternal(userRef);

        Chapter chapter = getChapterInternal(courseRef, chapterRef);
        List<ReviewerRelation> reviewerRelations = reviewerRelationRepository.findByReviewerOrSubmitterForChapter(user, chapter);
        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByUserInChapter(user, chapter);

        // Force initialization of the discussions
        reviewStepResults.forEach(r -> r.getArguments().size());

        return buildChapterProgress(user, chapter, reviewerRelations, reviewStepResults);
    }

    @Transactional(readOnly = true)
    public List<ReviewerRelationResult> getReviewerRelationResults(String courseRef, String chapterRef) {

        logger.info("Logged in user '{}' ('{}') retrieving all reviewer relations for course '{}' and chapter '{}'",
                userSession.getUsername(), userSession.getUserRef(), courseRef, chapterRef);

        permissionController.checkFullCourseProgressViewPermission(courseRef);

        User user = getUserInternal(userSession.getUserRef());
        Chapter chapter = getChapterInternal(courseRef, chapterRef);

        // We get the review steps so they already become part of the session, and we won't have to fetch them later
        reviewStepRepository.findByChapterFetchRelated(chapter);

        List<ReviewerRelation> chapterReviewerRelations = reviewerRelationRepository.findByChapter(chapter);
        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByChapter(chapter);

        return chapterReviewerRelations.stream()
                .map(rr -> this.buildReviewerRelationResult(user, chapter.getCourse(), rr, reviewStepResults))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ReviewerRelationResult> getReviewerRelationResults(String courseRef) {

        logger.info("Logged in user '{}' ('{}') retrieving all reviewer relations for course '{}'",
                userSession.getUsername(), userSession.getUserRef(), courseRef);

        permissionController.checkFullCourseProgressViewPermission(courseRef);

        User user = getUserInternal(userSession.getUserRef());
        Course course = getCourseInternal(courseRef);

        reviewStepRepository.findByCourseFetchRelated(course);

        List<ReviewerRelation> chapterReviewerRelations = reviewerRelationRepository.findByCourse(course);
        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByCourse(course);

        return chapterReviewerRelations.stream()
                .map(rr -> this.buildReviewerRelationResult(user, course, rr, reviewStepResults))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ReviewerRelationResult getReviewerRelationResult(String courseRef, String chapterRef, String reviewerRelationRef) {

        logger.info("Logged in user '{}' ('{}') retrieving all reviewer relations for course '{}' and chapter '{}'",
                userSession.getUsername(), userSession.getUserRef(), courseRef, chapterRef);

        permissionController.checkFullCourseProgressViewPermission(courseRef);

        User user = getUserInternal(userSession.getUserRef());
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);

        // We get the review steps so they already become part of the session, and we won't have to fetch them later
        reviewStepRepository.findByChapterFetchRelated(reviewerRelation.getChapter());

        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByReviewerRelation(reviewerRelation);

        return this.buildReviewerRelationResult(user, reviewerRelation.getChapter().getCourse(), reviewerRelation, reviewStepResults);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void enrich(QuizResult quizResult) {
        if (quizResult.getScore() == 0) {
            quizResult.setScore(calculateQuizResultScore(quizResult));
        }
        quizResult.getQuizElementResults().forEach(q -> q.getSelectedOptions().size());
        quizResult.getQuizElementResults().forEach(q -> q.getQuizElement().getOptions());
    }

    @Transactional
    public ResourceResult setResourceQuizResult(String userRef, String courseRef, String resourceRef, QuizResult quizResultToAdd) {

        logger.info("Logged in user '{}' ('{}') setting resource quiz result for user '{}', course '{}' and resource '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, resourceRef);

        permissionController.checkUserCourseUpdatePermission(userRef, courseRef);
        User user = getUserInternal(userRef);
        Resource resource = getResourceInternal(resourceRef);

        Quiz quiz = resource.getQuiz();
        if (quiz == null) {
            throw new OrbitGamesApplicationException("The resource with ref '" + resource.getRef() + "' has no quiz; no quiz result can be added for it", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        // Hack: Due to the eager fetching and additional joins in the query, some quiz elements are duplicated in the list, so we remove duplicates with this beautiful stream one-liner:
        quiz.setElements(quiz.getElements().stream().distinct().collect(Collectors.toList()));

        QuizResult newQuizResult = new QuizResult();
        newQuizResult.setQuiz(quiz);
        newQuizResult.setUser(user);

        mergeQuizResults(newQuizResult, quizResultToAdd, quiz);

        QuizResult savedQuizResult = quizResultRepository.save(newQuizResult);

        return buildResourceResult(Collections.singletonList(savedQuizResult), resource);
    }


    @Transactional
    public ReviewStepResultWrapper setReviewStepQuizResult(String userRef, String courseRef, String chapterRef, String reviewerRelationRef, String reviewStepRef, QuizResult quizResultToAdd) {

        logger.info("Logged in user '{}' ('{}') setting review step quiz result for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);

        Quiz quiz = reviewStep.getQuiz();

        if (quiz == null) {
            throw new OrbitGamesApplicationException("The review step with ref '" + reviewStep.getRef() + "' has no quiz; no quiz result can be added for it", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        // Hack: Due to the eager fetching and additional joins in the query, some quiz elements are duplicated in the list, so we remove duplicates with this beautiful stream one-liner:
        quiz.setElements(quiz.getElements().stream().distinct().collect(Collectors.toList()));

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);

        ReviewStepResult reviewStepResult = getOrCreateReviewStepResult(reviewStep, reviewerRelation);

        QuizResult quizResult;
        if (reviewStepResult.getQuizResult() == null) {
            quizResult = new QuizResult();
            quizResult.setQuiz(quiz);
            quizResult.setUser(user);
            reviewStepResult.setQuizResult(quizResult);
        } else {
            quizResult = reviewStepResult.getQuizResult();
        }

        mergeQuizResults(quizResult, quizResultToAdd, quiz);

        quizResultRepository.save(quizResult);
        reviewStepResultRepository.save(reviewStepResult);

        Collection<ReviewStepResult> allResults = reviewStepResultRepository.findByReviewerRelation(reviewerRelation);
        return buildReviewStepResultWrapper(reviewStepResult, allResults);
    }

    private void checkReviewStepReadyForResult(ReviewStep reviewStep) {

        if (reviewStep.isDisabled()) {
            throw new OrbitGamesApplicationException("Cannot set the review step result for disabled step '"  + reviewStep.getRef() +"'", GenericErrorCode.CONSTRAINT_VIOLATION);
        }
    }

    private void mergeQuizResults(QuizResult to, QuizResult from, Quiz quiz) {
        for (QuizElement quizElement: quiz.getElements()) {

            Optional<QuizElementResult> elementResultToAdd = from.getQuizElementResults().stream()
                    .filter(q -> quizElement.getRef().equals(q.getQuizElement().getRef()))
                    .findFirst();

            if (elementResultToAdd.isPresent()) {


                QuizElementResult elementResult = to.getQuizElementResults().stream()
                        .filter(q -> q.getQuizElement().equals(quizElement))
                        .findFirst()
                        .orElse(createQuizElementResultFor(to, quizElement));


                switch (quizElement.getQuestionType()) {
                    case NONE:
                        if (elementResultToAdd.get().getSelectedOptions() != null && !elementResultToAdd.get().getSelectedOptions().isEmpty()) {
                            throw new OrbitGamesApplicationException("Invalid quiz element result, quiz element '" + quizElement.getRef() + "' has question type 'NONE' and therefore does not allow selecting options", GenericErrorCode.CONSTRAINT_VIOLATION);
                        }
                        break;
                    case SELECT_ONE:
                        if (elementResultToAdd.get().getSelectedOptions() == null || elementResultToAdd.get().getSelectedOptions().size() > 1) {
                            throw new OrbitGamesApplicationException("Invalid quiz element result, quiz element '" + quizElement.getRef() + "' has question type 'SELECT_ONE' and therefore should have exactly one option selected", GenericErrorCode.CONSTRAINT_VIOLATION);
                        }
                        break;
                    case SELECT_MULTIPLE:
                        // Anything goes in this case
                        break;
                }

                if (quizElement.getOptions() == null || quizElement.getOptions().isEmpty()) {
                    elementResult.setSelectedOptions(Collections.emptySet());
                } else {
                    elementResult.setSelectedOptions(quizElement.getOptions().stream()
                            .filter(q -> elementResultToAdd.get().getSelectedOptions().stream()
                                    .anyMatch(s -> s.getRef().equals(q.getRef()))
                            )
                            .collect(Collectors.toSet())
                    );
                }

                if (!to.getQuizElementResults().contains(elementResult)) {
                    to.addQuizElementResult(elementResult);
                }
            }
        }

        to.setScore(calculateQuizResultScore(to));
    }

    private int calculateQuizResultScore(QuizResult quizResult) {
        return quizResult.getQuizElementResults().stream().mapToInt(q -> q.getSelectedOptions().stream().mapToInt(QuizElementOption::getPoints).sum()).sum();
    }

    @Transactional
    public ReviewStepResultWrapper setReviewStepFileUpload(String userRef, String courseRef, String chapterRef,
                                                      String reviewerRelationRef, String reviewStepRef,
                                                      String fileName, InputStream uploadedInputStream) {

        logger.info("Logged in user '{}' ('{}') adding file upload for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);

        if (!reviewStep.isFileUpload()) {
            throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' does not include a file upload!", GenericErrorCode.CONSTRAINT_VIOLATION);
        }
        checkReviewStepReadyForResult(reviewStep);
        checkReviewStepValid(courseRef, chapterRef, reviewStep);

        Chapter chapter = reviewStep.getChapter();

        // File Uploads are universal for all reviewer relations in which the current user should complete the step:
        final List<ReviewStepResult> reviewStepResults = getFileUploadReviewStepResults(user, reviewStep);

        // Check if we actually found the referenced reviewer relation:
        ReviewerRelation referencedReviewerRelation = reviewStepResults.stream()
                .map(ReviewStepResult::getReviewerRelation)
                .filter(r -> r.getRef().equals(reviewerRelationRef))
                .findFirst()
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find reviewer relation with ref '" + reviewerRelationRef + "' for chapter '" + chapterRef + "' in which user '" + userRef + "' should execute a file upload", GenericErrorCode.ENTITY_NOT_FOUND));



        for (ReviewStepResult reviewStepResult: reviewStepResults) {
            if (reviewStepResult.getFileUploadMetaData() != null) {
                logger.info("Review step result for review step '{}' and reviewer relation '{}' already contains a file upload with ref '{}' and filename '{}'. " +
                                "This file upload will be replaced by the new one",
                        reviewStepRef, reviewStepResult.getReviewerRelation().getRef(), reviewStepResult.getFileUploadMetaData().getRef(), reviewStepResult.getFileUploadMetaData().getFilename()
                );
            }
        }

        fileService.checkValidExtension(fileName, config.getReviewStepUploadAllowedFileTypes());
        FileService.FileMetaData fileMetaData = fileService.storeFile(fileName, uploadedInputStream, config.getReviewStepUploadMaxSize());
        fileService.checkIsValidFileOfType(fileMetaData.getStoragePath(), config.getReviewStepUploadAllowedMimeTypes());

        FileUploadMetadata fileUploadMetadata = new FileUploadMetadata();
        fileUploadMetadata.setFilename(fileMetaData.getFileName());
        fileUploadMetadata.setRef(fileMetaData.getFileRef());
        fileUploadMetadata.setStoragePath(fileMetaData.getStoragePath());
        fileUploadMetadata.setUser(user);

        fileUploadMetadataRepository.save(fileUploadMetadata);

        for(ReviewStepResult reviewStepResult: reviewStepResults) {
            reviewStepResult.setFileUploadMetaData(fileUploadMetadata);
            reviewStepResultRepository.save(reviewStepResult);
        }

        Collection<ReviewStepResult> allResults = reviewStepResultRepository.findByReviewerRelation(referencedReviewerRelation);
        ReviewStepResult referencedReviewStepResult = reviewStepResults.stream()
                .filter(r -> r.getReviewerRelation() == referencedReviewerRelation)
                .findFirst()
                .orElseThrow(() -> new OrbitGamesSystemException("Could not find review step result for the referenced reviewer relation '" + referencedReviewerRelation.getRef() + "' and step '" + reviewStepRef + "', but it should have been there?"));
        return buildReviewStepResultWrapper(referencedReviewStepResult, allResults);
    }

    /**
     * Returns the review step results for all reviewer relation in which the given user should upload a file for the review step.
     * @param user The user that is uploading the file
     * @param reviewStep The review step that is being uploaded
     * @return
     */
    private List<ReviewStepResult> getFileUploadReviewStepResults(User user, ReviewStep reviewStep) {
        final List<ReviewerRelation> reviewerRelations;
        if (reviewStep.getStepType().getPostedBy() == PeerType.SUBMITTER) {
            reviewerRelations = reviewerRelationRepository.findBySubmitterForChapter(user, reviewStep.getChapter());
        } else {
            reviewerRelations = reviewerRelationRepository.findByReviewerForChapter(user, reviewStep.getChapter());
        }

        for(ReviewerRelation reviewerRelation: reviewerRelations) {
            checkUserMayPostResult(user, reviewStep, reviewerRelation);
        }

        List<ReviewStepResult> reviewStepResults = new ArrayList<>();
        for (ReviewerRelation reviewerRelation: reviewerRelations) {
            ReviewStepResult reviewStepResult = getOrCreateReviewStepResult(reviewStep, reviewerRelation);
            reviewStepResults.add(reviewStepResult);
        }
        return reviewStepResults;
    }

    @Transactional
    public ReviewStepResultWrapper addReviewStepArgument(String userRef, String courseRef, String chapterRef, String reviewerRelationRef, String reviewStepRef, ReviewStepArgument argumentToAdd) {

        logger.info("Logged in user '{}' ('{}') adding argument for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);

        if (reviewStep.getDiscussionType() == null || reviewStep.getDiscussionType() == DiscussionType.NONE) {
            throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' does not allow for a discussion", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);
        checkArgumentValidForStep(argumentToAdd, reviewStep);

        ReviewStepResult reviewStepResult = getOrCreateReviewStepResult(reviewStep, reviewerRelation);


        ReviewStepArgument newArgument = new ReviewStepArgument();
        newArgument.setRef(RefGenerator.generate());
        newArgument.setReviewStepResult(reviewStepResult);

        mergeAndValidateReviewStepArgument(newArgument, argumentToAdd);

        newArgument = reviewStepArgumentRepository.save(newArgument);

        reviewStepResult.addArgument(newArgument);

        Collection<ReviewStepResult> allResults = reviewStepResultRepository.findByReviewerRelation(reviewerRelation);
        return buildReviewStepResultWrapper(reviewStepResult, allResults);
    }


    @Transactional
    public ReviewStepResultWrapper updateReviewStepArgument(String userRef, String courseRef, String chapterRef,
                                                            String reviewerRelationRef, String reviewStepRef, String argumentRef,
                                                            ReviewStepArgument argumentWithUpdates) {

        logger.info("Logged in user '{}' ('{}') updating argument '{}' for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), argumentRef, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);

        if (reviewStep.getDiscussionType() == null || reviewStep.getDiscussionType() == DiscussionType.NONE) {
            throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' does not allow for a discussion", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);
        checkArgumentValidForStep(argumentWithUpdates, reviewStep);

        ReviewStepResult reviewStepResult = getOrCreateReviewStepResult(reviewStep, reviewerRelation);
        ReviewStepArgument existingArgument = reviewStepResult.getArguments().stream().filter(a -> a.getRef().equals(argumentRef))
                .findFirst()
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find argument with ref '" + argumentRef + "' for review step '" + reviewStepRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        mergeAndValidateReviewStepArgument(existingArgument, argumentWithUpdates);
        reviewStepArgumentRepository.save(existingArgument);

        Collection<ReviewStepResult> allResults = reviewStepResultRepository.findByReviewerRelation(reviewerRelation);
        return buildReviewStepResultWrapper(reviewStepResult, allResults);
    }

    @Transactional
    public ReviewStepResultWrapper deleteReviewStepArgument(String userRef, String courseRef, String chapterRef, String reviewerRelationRef, String reviewStepRef, String argumentRef) {

        logger.info("Logged in user '{}' ('{}') deleting argument '{}' for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), argumentRef, userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);

        if (reviewStep.getDiscussionType() == null || reviewStep.getDiscussionType() == DiscussionType.NONE) {
            throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' does not allow for a discussion", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);

        ReviewStepResult reviewStepResult = getOrCreateReviewStepResult(reviewStep, reviewerRelation);

        ReviewStepArgument existingArgument = reviewStepResult.getArguments().stream().filter(a -> a.getRef().equals(argumentRef))
                .findFirst()
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find argument with ref '" + argumentRef + "' for review step '" + reviewStepRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        reviewStepResult.getArguments().remove(existingArgument);
        reviewStepArgumentRepository.delete(existingArgument);

        Collection<ReviewStepResult> allResults = reviewStepResultRepository.findByReviewerRelation(reviewerRelation);
        return buildReviewStepResultWrapper(reviewStepResult, allResults);
    }

    @Transactional
    public ReviewStepResultWrapper markReviewStepComplete(String userRef, String courseRef, String chapterRef, String reviewerRelationRef, String reviewStepRef) {

        logger.info("Logged in user '{}' ('{}') marking review step complete for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);

        List<ReviewStepResult> reviewStepResultsToMark;
        if (reviewStep.isFileUpload() && reviewStep.getQuiz() == null && reviewStep.getDiscussionType() == DiscussionType.NONE) {
            // Special case: if this review step is ONLY a file upload, the file upload is shared between all reviewer relations this user should submit,
            // and therefore all review step results for this review step and user should also be marked as complete:
            reviewStepResultsToMark = getFileUploadReviewStepResults(user, reviewStep);
        } else {
            reviewStepResultsToMark = Collections.singletonList(getOrCreateReviewStepResult(reviewStep, reviewerRelation));
        }

        List<ReviewStepResult> allReviewStepResults = reviewStepResultRepository.findByUserInChapter(user, reviewStep.getChapter());
        ReviewStepResult finalReviewStepResult = null;
        for (ReviewStepResult reviewStepResult: reviewStepResultsToMark) {
            boolean readyToComplete = reviewStepStatusCalculator.calculateReadyToComplete(reviewStepResult, allReviewStepResults);

            if (!readyToComplete) {
                throw new OrbitGamesApplicationException("Review step result for step '" + reviewStepRef + "' and user '" + userRef + "' is not ready to be marked as complete", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            reviewStepResult.setMarkedComplete(OffsetDateTime.now());
            ReviewStepResult savedResult = reviewStepResultRepository.save(reviewStepResult);
            if (savedResult.getReviewStep() == reviewStep) {
                finalReviewStepResult = savedResult;
            }
        }

        if (finalReviewStepResult == null) {
            throw new OrbitGamesSystemException("Could not find review step result for the current step, but it should exist?");
        }

        return buildReviewStepResultWrapper(finalReviewStepResult, allReviewStepResults);
    }

    @Transactional
    public ReviewStepResultWrapper revokeReviewStepComplete(String userRef, String courseRef, String chapterRef, String reviewerRelationRef, String reviewStepRef) {

        logger.info("Logged in user '{}' ('{}') revoking review step completion for user '{}', course '{}', chapter '{}', reviewer relation '{}' and review step '{}'",
                userSession.getUsername(), userSession.getUserRef(), userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        permissionController.checkCourseProgressUpdatePermission(userRef, courseRef, chapterRef, reviewerRelationRef);

        User user = getUserInternal(userRef);
        ReviewStep reviewStep = getReviewStepInternal(reviewStepRef);
        ReviewerRelation reviewerRelation = getReviewerRelationInternal(reviewerRelationRef);

        checkReviewStepValid(courseRef, chapterRef, reviewStep);
        checkReviewStepReadyForResult(reviewStep);
        checkUserMayPostResult(user, reviewStep, reviewerRelation);

        List<ReviewStepResult> reviewStepResultsToMark;
        if (reviewStep.isFileUpload() && reviewStep.getQuiz() == null && reviewStep.getDiscussionType() == DiscussionType.NONE) {
            // Special case: if this review step is ONLY a file upload, the file upload is shared between all reviewer relations this user should submit,
            // and therefore all review step results for this review step and user should also be marked as complete:
            reviewStepResultsToMark = getFileUploadReviewStepResults(user, reviewStep);
        } else {
            reviewStepResultsToMark = Collections.singletonList(getOrCreateReviewStepResult(reviewStep, reviewerRelation));
        }

        // TODO: Should we check here if the deadline has already passed? Are users allowed to revoke a step for which the deadline has already passed?

        List<ReviewStepResult> allReviewStepResults = reviewStepResultRepository.findByUserInChapter(user, reviewStep.getChapter());

        // If there's not review step result to mark as incomplete, we'll just ignore it
        ReviewStepResult finalReviewStepResult = allReviewStepResults.get(0);
        for (ReviewStepResult reviewStepResult: reviewStepResultsToMark) {

            if (reviewStepResult.getMarkedComplete() == null) {
                // Review step already not marked as complete
                continue;
            }

            reviewStepResult.setMarkedComplete(null);
            ReviewStepResult savedResult = reviewStepResultRepository.save(reviewStepResult);
            if (savedResult.getReviewStep() == reviewStep) {
                finalReviewStepResult = savedResult;
            }
        }

        return buildReviewStepResultWrapper(finalReviewStepResult, allReviewStepResults);
    }

    private void checkArgumentValidForStep(ReviewStepArgument argumentToAdd, ReviewStep reviewStep) {
        String reviewStepRef = reviewStep.getRef();
        switch(reviewStep.getDiscussionType()) {
            case FREE_FORM_ARGUMENTS:
                if (argumentToAdd.getArgumentType() != null) {
                    throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' with discussion type '" + reviewStep.getDiscussionType() + "' does not allow setting an argument type", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
                break;
            case RESTRICTED_ARGUMENTS:
                if (argumentToAdd.getArgumentType() == null) {
                    throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' with discussion type '" + reviewStep.getDiscussionType() + "' does not have a valid argument type. Ensure that the argument type is not null, and in the list of allowed argument types for this review step.", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
                break;
            case REPLY:
                if (argumentToAdd.getReplyTo() == null) {
                    throw new OrbitGamesApplicationException("Review step '" + reviewStepRef + "' with discussion type '" + reviewStep.getDiscussionType() + "' requires setting a 'replyTo' argument", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
                break;
        }
    }

    private void mergeAndValidateReviewStepArgument(ReviewStepArgument to, ReviewStepArgument from) {

        to.setMessage(from.getMessage());

        if (from.getArgumentType() != null) {
            ArgumentType argType = argumentTypeRepository.findByRef(from.getArgumentType().getRef())
                    .orElseThrow(() -> new OrbitGamesApplicationException("Argument type with ref '" + from.getArgumentType().getRef() + "' could not be found", GenericErrorCode.ENTITY_NOT_FOUND));

            // Validate that the argument type is allowed for this review step:
            if (!to.getReviewStepResult().getReviewStep().getArgumentTypes().contains(argType)) {
                throw new OrbitGamesApplicationException("Argument type '" + argType.getName() + "' ('" + argType.getRef() + "') not allowed for review step '" + to.getReviewStepResult().getReviewStep().getRef() + "'", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            to.setArgumentType(argType);
        } else {
            to.setArgumentType(null);
        }

        if (from.getReplyTo() != null) {

            ReviewStepArgument replyToArgument = reviewStepArgumentRepository.findByRef(from.getReplyTo().getRef())
                    .orElseThrow(() -> new OrbitGamesApplicationException("Review Step Argument with ref '" + from.getReplyTo().getRef() + "' could not be found", GenericErrorCode.ENTITY_NOT_FOUND));

            if (replyToArgument.getReviewStepResult().getReviewerRelation() != to.getReviewStepResult().getReviewerRelation()) {
                throw new OrbitGamesApplicationException("Argument replies must be to arguments within the same reviewer relation", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            if (to.getReviewStepResult().getArguments().stream().filter(a -> a != to).anyMatch(a -> a.getReplyTo().equals(from.getReplyTo()))) {
                throw new OrbitGamesApplicationException("There already is a reply to argument '" + to.getReplyTo() + "' within this review step.", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            to.setReplyTo(replyToArgument);
        }
    }

    private QuizElementResult createQuizElementResultFor(QuizResult quizResult, QuizElement quizElement) {

        QuizElementResult elementResult = new QuizElementResult();
        elementResult.setQuizResult(quizResult);
        elementResult.setQuizElement(quizElement);
        return elementResult;
    }

    private ReviewStepResult getOrCreateReviewStepResult(ReviewStep reviewStep, ReviewerRelation reviewerRelation) {
        return reviewStepResultRepository.findByReviewStepAndReviewerRelation(reviewStep, reviewerRelation)
                .orElseGet(() -> createReviewStepResultFor(reviewStep, reviewerRelation));
    }

    private ReviewStepResult createReviewStepResultFor(ReviewStep reviewStep, ReviewerRelation reviewerRelation) {

        ReviewStepResult newReviewStepResult = new ReviewStepResult();
        newReviewStepResult.setReviewerRelation(reviewerRelation);
        newReviewStepResult.setReviewStep(reviewStep);
        return reviewStepResultRepository.save(newReviewStepResult);
    }

    /**
     * Checks if the given review step actually belong to the chapter and course that are provided.
     * Throws an exception in case it isn't.
     *
     * @param courseRef The ref of the course to check against
     * @param chapterRef The ref of the chapter to check against
     * @param reviewStep The review step
     */
    private void checkReviewStepValid(String courseRef, String chapterRef, ReviewStep reviewStep) {
        if (!reviewStep.getChapter().getRef().equals(chapterRef) || !reviewStep.getChapter().getCourse().getRef().equals(courseRef)) {
            throw new OrbitGamesApplicationException("The review step with ref '" + reviewStep.getRef() + "' " +
                    "is not part of chapter '" + chapterRef + "' or course '" + courseRef +"'",
                    GenericErrorCode.CONSTRAINT_VIOLATION);
        }
    }

    private void checkUserMayPostResult(User user, ReviewStep reviewStep, ReviewerRelation reviewerRelation) {
        if (!userMayPostResult(user, reviewStep, reviewerRelation)) {
            throw new OrbitGamesApplicationException("User '" + user.getUsername() + "' ('" + user.getRef() + "')" +
                    " doesn't have the correct peer type (" + reviewStep.getStepType().getPostedBy() + ") within reviewer relation '" + reviewerRelation.getRef() +
                    "' to post a result for review step '" + reviewStep.getRef() + "'", GenericErrorCode.CONSTRAINT_VIOLATION);
        }
    }

    /**
     * @param user The user
     * @param reviewStep Which review step a submission is being posted for
     * @param reviewerRelation The reviewer relation within which the result is posted
     * @return Boolean indicating if this user has the correct {@link PeerType} within this reviewer relation to post the result of this review step
     */
    private boolean userMayPostResult(User user, ReviewStep reviewStep, ReviewerRelation reviewerRelation) {
        return (reviewStep.getStepType().getPostedBy() == PeerType.SUBMITTER && reviewerRelation.getSubmitter().equals(user)) ||
                (reviewStep.getStepType().getPostedBy() == PeerType.REVIEWER && reviewerRelation.getReviewer().equals(user));
    }

    private ChapterProgress buildChapterProgress(User progressUser, Chapter chapter, List<ReviewerRelation> reviewerRelations, List<ReviewStepResult> reviewStepResults) {

        ChapterProgress chapterProgress = new ChapterProgress();
        chapterProgress.setChapter(chapter);

        List<ReviewerRelationResult> reviewerRelationResults = reviewerRelations.stream()
                .filter(r -> r.getChapter() == chapter)
                .map(r -> buildReviewerRelationResult(progressUser, chapter.getCourse(), r, reviewStepResults))
                .collect(Collectors.toList());

        chapterProgress.setReviewerRelationResults(reviewerRelationResults);

        return chapterProgress;
    }

    private ReviewerRelationResult buildReviewerRelationResult(User progressUser, Course course, ReviewerRelation reviewerRelation,
                                                               Collection<ReviewStepResult> allReviewStepResults
                                                                ) {

        ReviewerRelationResult relationResult = new ReviewerRelationResult();

        relationResult.setReviewerRelationRef(reviewerRelation.getRef());

        if (course.isAnonymousReviews() && userSession.getRole() == Role.LEARNER) {
            if (reviewerRelation.getSubmitter().equals(progressUser)) {
                relationResult.setSubmitter(reviewerRelation.getSubmitter());
            } else if (reviewerRelation.getReviewer().equals(progressUser)) {
                relationResult.setReviewer(reviewerRelation.getReviewer());
            }
        } else {
            // The reviews are not anonymous, or the user has permission to view them, so we return the users
            relationResult.setSubmitter(reviewerRelation.getSubmitter());
            relationResult.setReviewer(reviewerRelation.getReviewer());
        }

        relationResult.setStepResults(allReviewStepResults.stream()
                .filter(rsr -> rsr.getReviewerRelation() == reviewerRelation)
                .distinct()
                .map(rsr -> buildReviewStepResultWrapper(rsr, allReviewStepResults))
                .collect(Collectors.toList()));

        return relationResult;
    }

    private ReviewStepResultWrapper buildReviewStepResultWrapper(ReviewStepResult reviewStepResult, Collection<ReviewStepResult> allReviewStepResults) {

        // Ensure that all the related entities get loaded
        if (reviewStepResult.getQuizResult() != null && reviewStepResult.getQuizResult().getQuizElementResults() != null) {
            reviewStepResult.getQuizResult().getQuizElementResults().forEach(q -> q.getSelectedOptions().size());
            enrich(reviewStepResult.getQuizResult());
        }
        reviewStepResult.getArguments().size();
        return new ReviewStepResultWrapper(
                reviewStepStatusCalculator.calculateLastModified(reviewStepResult),
                reviewStepStatusCalculator.calculateReadyToComplete(reviewStepResult, allReviewStepResults),
                reviewStepStatusCalculator.calculateClosed(reviewStepResult), reviewStepResult);
    }

    private User getUserInternal(String userRef) {
        return getUserInternal(userRef, false);
    }

    private User getUserInternal(String userRef, boolean fetchProperties) {
        return (fetchProperties ? userRepository.findByRefFetchUserProperties(userRef) : userRepository.findByRef(userRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user with ref: '" + userRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private ReviewerRelation getReviewerRelationInternal(String ref) {
        return reviewerRelationRepository.findByRef(ref)
                .orElseThrow(() -> new OrbitGamesApplicationException("No reviewer relation with ref '" + ref + "' was found", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Course getCourseInternal(String courseRef) {
        return courseRepository.findByRef(courseRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Chapter getChapterInternal(String courseRef, String chapterRef) {

        return chapterRepository.findByRef(chapterRef)
                .filter(c -> c.getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + chapterRef + "', or the chapter is not parse of course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private ReviewStep getReviewStepInternal(String reviewStepRef) {
        return reviewStepRepository.findByRef(reviewStepRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find review step with ref '" + reviewStepRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private UserCourse getUserCourseInternal(String courseRef, String userRef) {

        return this.userCourseRepository.findByCourseRefAndUserRef(courseRef, userRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user course information for course '" + courseRef + "' and user '" + userRef + "'. Please ensure that the user is enrolled in this course", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Resource getResourceInternal(String resourceRef) {
        return resourceRepository.findByRef(resourceRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find resource with ref '" + resourceRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

}
