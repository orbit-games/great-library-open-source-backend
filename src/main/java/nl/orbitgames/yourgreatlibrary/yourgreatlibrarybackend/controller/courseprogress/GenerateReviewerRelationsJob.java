package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class GenerateReviewerRelationsJob extends QuartzJobBean {

    public static final String COURSE_REF_KEY = "courseRef";
    public static final String CHAPTER_REF_KEY = "chapterRef";

    private static final Logger logger = LoggerFactory.getLogger(GenerateReviewerRelationsJob.class);

    // Since this class is initialized by Quartz, we can't use constructor injection, and instead need to use field injection.
    @Autowired
    private ReviewerRelationGenerationProcess reviewerRelationGenerationProcess;


    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        logger.info("Executing reviewer relation generation job '{}'", context.getJobDetail().getKey());

        String courseRef = context.getJobDetail().getJobDataMap().getString(COURSE_REF_KEY);
        String chapterRef = context.getJobDetail().getJobDataMap().getString(CHAPTER_REF_KEY);
        reviewerRelationGenerationProcess.generateReviewerRelations(courseRef, chapterRef, false);

    }
}
