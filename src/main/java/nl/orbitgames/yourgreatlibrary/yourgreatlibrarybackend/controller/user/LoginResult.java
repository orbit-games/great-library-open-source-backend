package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;

public class LoginResult {

    private final String token;
    private final User user;

    public LoginResult(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }
}
