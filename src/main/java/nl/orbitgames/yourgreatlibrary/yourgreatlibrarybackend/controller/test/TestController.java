package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.test;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation.ConversationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.CourseProgressController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserAgreementRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.interceptor.RequireTestEnvironment;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.PasswordHashUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;

@Component
@RequireTestEnvironment
public class TestController {

    private final UserRepository userRepository;
    private final CourseRepository courseRepository;
    private final UserAgreementRepository userAgreementRepository;

    private final CreateTestCourseProcess createTestCourseProcess;

    private final CourseProgressController courseProgressController;
    private final ConversationController conversationController;

    public TestController(
            @Autowired UserRepository userRepository,
            @Autowired CourseRepository courseRepository,
            @Autowired UserAgreementRepository userAgreementRepository,

            @Autowired CreateTestCourseProcess createTestCourseProcess,

            @Autowired CourseProgressController courseProgressController,
            @Autowired ConversationController conversationController
            ) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
        this.userAgreementRepository = userAgreementRepository;

        this.createTestCourseProcess = createTestCourseProcess;

        this.courseProgressController = courseProgressController;
        this.conversationController = conversationController;
    }

    @Transactional(readOnly = true)
    public boolean checkUserExists(String username) {

        return userRepository.findByUsername(username).isPresent();

    }

    @Transactional
    public Course createTestCourse(OffsetDateTime firstSubmissionDeadline, String courseName, String courseCode,
                                   boolean anonymousUsers, boolean anonymousReviews) {
        return createTestCourseProcess.createTestCourse(firstSubmissionDeadline, courseName, courseCode, anonymousUsers, anonymousReviews);
    }

    @Transactional
    public UserAgreement createTestUserAgreement(String pdfUrl) {

        UserAgreement newAgreement = new UserAgreement();
        newAgreement.setRef(RefGenerator.generate());
        newAgreement.setPdfUrl(pdfUrl);
        newAgreement.setStartDate(OffsetDateTime.now().minusDays(7));

        return userAgreementRepository.save(newAgreement);
    }


    @Transactional
    public void generateCourseReviewerRelations(String courseRef) {

        Course course = courseRepository.findByRef(courseRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
        for (Chapter chapter: course.getChapters()) {
            courseProgressController.generateReviewerRelations(courseRef, chapter.getRef());
        }

    }

    @Transactional(readOnly = true)
    public String getUserRef(String username) {

        return userRepository.findByUsername(username)
                .map(User::getRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("User '" + username + "' could not be found", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    @Transactional
    public User createTestUser(String username, String email, String password, String fullName, Role role) {

        User user = new User();
        user.setRef(RefGenerator.generate());
        user.setUsername(username);
        user.setEmail(email);
        user.setPasswordHash(PasswordHashUtils.hashPassword(password));
        user.setFullName(fullName);
        user.setRole(role);
        user.setValidated(true);
        user.setSignedUserAgreement(userAgreementRepository.findLatest().orElse(null));
        user.setUserAgreementSignDate(OffsetDateTime.now());
        return userRepository.save(user);

    }

    public void sendDailyConversationNotificationEmail() {
        this.conversationController.sendDailyEmailNotifications();
    }
}
