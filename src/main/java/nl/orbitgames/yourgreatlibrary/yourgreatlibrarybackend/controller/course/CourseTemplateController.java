package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateChapterMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateReviewStepMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseTemplateChapterMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseTemplateMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseTemplateReviewStepMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.ContentPropertiesReader;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ChapterId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.CourseTemplateParser;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ReviewStepId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.time.temporal.ChronoUnit.DAYS;

@Component
public class CourseTemplateController {

    public static final String TEMPLATE_ROOT_FILE = "messages.properties";

    private final CourseTemplateMetadataRepository courseTemplateMetadataRepository;
    private final CourseTemplateChapterMetadataRepository courseTemplateChapterMetadataRepository;
    private final CourseTemplateReviewStepMetadataRepository courseTemplateReviewStepMetadataRepository;

    private final PermissionController permissionController;

    private final ApplicationContext applicationContext;

    private final Path templatesPath;
    private final String contentRoot;

    public CourseTemplateController(
            @Autowired CourseTemplateMetadataRepository courseTemplateMetadataRepository,
            @Autowired CourseTemplateChapterMetadataRepository courseTemplateChapterMetadataRepository,
            @Autowired CourseTemplateReviewStepMetadataRepository courseTemplateReviewStepMetadataRepository,

            @Autowired PermissionController permissionController,

            @Autowired ApplicationContext applicationContext,

            @Value("${" + YourGreatLibraryBackendApplicationProperties.TEMPLATES_FOLDER + "}") String templatePath,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.COURSE_TEMPLATES_CONTENT_ROOT + "}") String contentRoot
    ) {

        this.courseTemplateMetadataRepository = courseTemplateMetadataRepository;
        this.courseTemplateChapterMetadataRepository = courseTemplateChapterMetadataRepository;
        this.courseTemplateReviewStepMetadataRepository = courseTemplateReviewStepMetadataRepository;

        this.permissionController = permissionController;

        this.applicationContext = applicationContext;

        this.templatesPath = Paths.get(templatePath);
        this.contentRoot = contentRoot;
    }


    public String getDefaultId(String templateName) {

        return templateName
                .toLowerCase()
                .replaceAll("\\s+", "-")
                .replaceAll("[^a-z0-9\\-]", "-");
    }

    @Transactional(readOnly = true)
    public boolean existsCourseTemplate(String templateId) {
        return courseTemplateMetadataRepository.existsByTemplateId(templateId);
    }

    @Transactional(readOnly = true)
    public List<CourseTemplateMetadata> getCourseTemplates() {

        return StreamSupport.stream(courseTemplateMetadataRepository.findAll().spliterator(), false)
                .peek(this::fetchRelated)
                .collect(Collectors.toList());
    }


    @Transactional(readOnly = true)
    public CourseTemplateMetadata getTemplateMetadata(String templateId, int version) {

        return getCourseTemplateMetadataInternal(templateId, version);
    }

    @Transactional
    public void createTemplate(String templateId, List<TemplateFile> templateFiles) {

        int version = 0;

        try {
            Path templatePath = getTemplatePath(templateId, version);
            Files.createDirectories(templatePath);

            if (CollectionUtils.noneMatch(templateFiles, f -> Paths.get(f.getFilename()).getFileName().toString().equals(TEMPLATE_ROOT_FILE))) {
                throw new OrbitGamesApplicationException("Template '" + templateId + "' is missing required 'messages.properties' file", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            for (TemplateFile templateFile : templateFiles) {
                Files.copy(templateFile.getInputStream(), templatePath.resolve(templateFile.getFilename()), StandardCopyOption.REPLACE_EXISTING);
            }

            String templateRootFile = "file:" + templatePath.resolve(TEMPLATE_ROOT_FILE).toString();
            ContentPropertiesReader courseTemplateReader = new ContentPropertiesReader(applicationContext, templateRootFile);
            CourseTemplateParser templateParser = new CourseTemplateParser(courseTemplateReader, contentRoot);

            CourseTemplateMetadata templateMetadata = new CourseTemplateMetadata();
            templateMetadata.setTemplateId(templateId);
            templateMetadata.setVersion(0);
            templateMetadata.setName(templateParser.getTemplateName());
            templateMetadata.setDescription(templateParser.getTemplateDescription());

            templateMetadata = courseTemplateMetadataRepository.save(templateMetadata);

            List<ChapterId> chapterIds = templateParser.getChapterIds();
            OffsetDateTime firstDeadline = templateParser.getChapterFirstDeadline(chapterIds.get(0), Collections.emptyMap());

            for (int i = 0; i < chapterIds.size(); i++) {

                ChapterId chapterId = chapterIds.get(i);
                int chapterDeadlineOffset = (int)
                        DAYS.between(firstDeadline, templateParser.getChapterFirstDeadline(chapterId, Collections.emptyMap()));

                CourseTemplateChapterMetadata chapterMetadata = new CourseTemplateChapterMetadata();
                chapterMetadata.setCourseTemplateMetadata(templateMetadata);
                chapterMetadata.setChapterId(chapterId.toString());
                chapterMetadata.setSortOrder(i * 10);
                chapterMetadata.setName(templateParser.getChapterTitle(chapterId));

                chapterMetadata = courseTemplateChapterMetadataRepository.save(chapterMetadata);
                templateMetadata.addChapter(chapterMetadata);

                List<ReviewStepId> reviewStepIds = templateParser.getChapterReviewStepIds(chapterId);
                for (int j = 0; j < reviewStepIds.size(); j++) {
                    ReviewStepId reviewStepId = reviewStepIds.get(j);

                    CourseTemplateReviewStepMetadata reviewStepMetadata = new CourseTemplateReviewStepMetadata();
                    reviewStepMetadata.setReviewStepId(reviewStepId.toString());
                    reviewStepMetadata.setCourseTemplateMetadata(templateMetadata);
                    reviewStepMetadata.setCourseTemplateChapterMetadata(chapterMetadata);
                    reviewStepMetadata.setSortOrder(j * 10);
                    reviewStepMetadata.setName(templateParser.getReviewStepName(reviewStepId));
                    reviewStepMetadata.setDeadlineOffset(chapterDeadlineOffset + templateParser.getReviewStepDeadlineOffset(reviewStepId));

                    courseTemplateReviewStepMetadataRepository.save(reviewStepMetadata);
                    chapterMetadata.addReviewStep(reviewStepMetadata);
                }
            }

        } catch (IOException e) {
            throw new OrbitGamesSystemException("IO Error while creating a template", e);
        }

    }

    @Transactional(readOnly = true)
    public CourseTemplateParser getTemplateParser(String templateId, int version) {

        CourseTemplateMetadata courseTemplateMetadata = getCourseTemplateMetadataInternal(templateId, version);

        permissionController.checkCourseTemplateViewPermission(courseTemplateMetadata);

        Path templatePath = getTemplatePath(courseTemplateMetadata);
        String templateRootFile =  "file:" + templatePath.resolve(TEMPLATE_ROOT_FILE).toString();
        ContentPropertiesReader courseTemplateReader = new ContentPropertiesReader(applicationContext, templateRootFile);

        return new CourseTemplateParser(courseTemplateReader, contentRoot);
    }

    private Path getTemplatePath(CourseTemplateMetadata courseTemplateMetadata) {
        return getTemplatePath(courseTemplateMetadata.getTemplateId(), courseTemplateMetadata.getVersion());
    }

    private Path getTemplatePath(String templateId, int version) {
        return this.templatesPath.resolve(templateId).resolve(String.valueOf(version));
    }

    private CourseTemplateMetadata getCourseTemplateMetadataInternal(String templateId, int version) {
        if (version < 0) {
            return courseTemplateMetadataRepository.findLatestByTemplateId(templateId)
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find template with id '" + templateId + "'", GenericErrorCode.ENTITY_NOT_FOUND));
        } else {
            return courseTemplateMetadataRepository.findByTemplateIdAndVersion(templateId, version)
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find template with id '" + templateId + "' and version number " + version, GenericErrorCode.ENTITY_NOT_FOUND));
        }
    }

    private void fetchRelated(CourseTemplateMetadata courseTemplateMetadata) {
        courseTemplateMetadata.getChapters().forEach(this::fetchRelated);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(CourseTemplateChapterMetadata courseTemplateChapterMetadata) {
        courseTemplateChapterMetadata.getReviewSteps().size();
    }


    public static class TemplateFile {
        private final String filename;
        private final InputStream inputStream;

        public TemplateFile(String filename, InputStream inputStream) {
            this.filename = filename;
            this.inputStream = inputStream;
        }

        public String getFilename() {
            return filename;
        }

        public InputStream getInputStream() {
            return inputStream;
        }
    }

}
