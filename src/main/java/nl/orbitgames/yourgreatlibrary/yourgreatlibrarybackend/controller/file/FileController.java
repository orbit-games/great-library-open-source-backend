package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.file;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.FileUploadMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.FileService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;

@Component
public class FileController {

    private final UserRepository userRepository;
    private final FileUploadMetadataRepository fileUploadMetadataRepository;

    private final FileService fileService;
    private final PermissionController permissionController;

    private final UserSession userSession;

    private final YourGreatLibraryConfig config;

    public FileController(
            @Autowired UserRepository userRepository,
            @Autowired FileUploadMetadataRepository fileUploadMetadataRepository,
            @Autowired FileService fileService,
            @Autowired PermissionController permissionController,
            @Autowired UserSession userSession,
            @Autowired YourGreatLibraryConfig config
            ) {
        this.userRepository = userRepository;
        this.fileUploadMetadataRepository = fileUploadMetadataRepository;
        this.fileService = fileService;
        this.permissionController = permissionController;
        this.userSession = userSession;

        this.config = config;
    }

    @Transactional
    public FileUploadMetadata uploadFile(InputStream uploadedInputStream, String fileName) {

        this.permissionController.checkFileUploadPermission();

        User user = userRepository.findByRef(userSession.getUserRef())
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find logged in user", GenericErrorCode.ENTITY_NOT_FOUND));

        fileService.checkValidExtension(fileName, config.getContentUploadAllowedFileTypes());
        FileService.FileMetaData fileMetaData = fileService.storeFile(fileName, uploadedInputStream, config.getContentUploadMaxSize());
        fileService.checkIsValidFileOfType(fileMetaData.getStoragePath(), config.getContentUploadAllowedMimeTypes());

        FileUploadMetadata fileUploadMetadata = new FileUploadMetadata();
        fileUploadMetadata.setFilename(fileMetaData.getFileName());
        fileUploadMetadata.setRef(fileMetaData.getFileRef());
        fileUploadMetadata.setStoragePath(fileMetaData.getStoragePath());
        fileUploadMetadata.setUser(user);

        return fileUploadMetadataRepository.save(fileUploadMetadata);
    }
}
