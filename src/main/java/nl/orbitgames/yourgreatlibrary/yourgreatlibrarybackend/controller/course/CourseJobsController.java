package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.CourseProgressController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.GenerateReviewerRelationsJob;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.SendDeadlineNotificationEmailJob;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.JobGroups;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.JobTriggers;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.Instant;
import java.time.OffsetDateTime;

@Component
public class CourseJobsController {

    private static final Logger logger = LoggerFactory.getLogger(CourseJobsController.class);

    private final CourseProgressController courseProgressController;

    private final Scheduler scheduler;


    public CourseJobsController(
            @Autowired CourseProgressController courseProgressController,

            // For some reason, Intellij IDEA thinks there are multiple beans that can be injected here, but it works fine...
            @Autowired Scheduler scheduler) {

        this.courseProgressController = courseProgressController;

        this.scheduler = scheduler;
    }

    /**
     * (Re)schedules all jobs for the given course.
     * @param course The course for which the jobs should be rescheduled
     */
    public void scheduleJobsForCourse(Course course) {

        for (Chapter chapter: course.getChapters()) {

            scheduleReviewerRelationGenerationJob(course, chapter);

            for(ReviewStep reviewStep: chapter.getReviewSteps()) {
                scheduleSendDeadlineNotificationEmailJob(reviewStep);
            }
        }
    }

    /**
     * Schedules or updates an existing job to generate the reviewer relations for the given chapter.
     * The job will not be scheduled (or unscheduled) if the generation time has already passed, auto generate is
     * disabled for the given chapter, or there are no (enabled) deadlines in the current chapter.
     * @param course The course for which the job should be generated
     * @param chapter The chapter for which the job should be generated.
     */
    public void scheduleReviewerRelationGenerationJob(Course course, Chapter chapter) {

        try {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put(GenerateReviewerRelationsJob.COURSE_REF_KEY, course.getRef());
            jobDataMap.put(GenerateReviewerRelationsJob.CHAPTER_REF_KEY, chapter.getRef());

            JobKey jobKey = JobKey.jobKey("chapter-" + chapter.getRef(), JobGroups.GENERATE_REVIEWER_RELATIONS_JOB);
            TriggerKey triggerKey = TriggerKey.triggerKey(jobKey.getName(), JobTriggers.GENERATE_REVIEWER_RELATIONS_TRIGGER);

            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(jobKey);

            ReviewerRelationGenerationSettings reviewerRelationGenerationSettings = courseProgressController.getReviewerRelationGenerationSettings(course.getRef(), chapter.getRef());
            OffsetDateTime reviewerRelationGenerationTime = courseProgressController.getReviewerRelationGenerationTime(course.getRef(), chapter.getRef());

            if (reviewerRelationGenerationTime == null) {
                // Chapter without (enabled) review steps, so no need to create reviewer relations
                return;
            }
            Instant reviewerRelationGenerationInstant = reviewerRelationGenerationTime.toInstant();

            String jobDescription = "Generate reviewer relations for chapter '" + chapter.getName() + "' of course '" + course.getName() + " (Q" + course.getQuarter() + ")'";

            if (reviewerRelationGenerationInstant.isAfter(Instant.now()) && reviewerRelationGenerationSettings.isAutoGenerate()) {
                JobDetail job = JobBuilder.newJob(GenerateReviewerRelationsJob.class)
                        .withIdentity(jobKey)
                        .withDescription(jobDescription)
                        .usingJobData(jobDataMap)
                        .storeDurably()
                        .build();

                Trigger trigger = TriggerBuilder.newTrigger()
                        .forJob(job)
                        .withIdentity(triggerKey)
                        .withDescription(job.getDescription() + " trigger")
                        .startAt(Date.from(reviewerRelationGenerationInstant))
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                        .build();

                // Note that schedule job will overwrite jobs with the same key
                logger.info("Scheduling job '" + job.getDescription() + "' to run at " + reviewerRelationGenerationTime);
                scheduler.scheduleJob(job, trigger);
            } else {
                // If the reviewer relations should have already been generated, there's no point in creating the job now (either they're already generated or we're too late)
                logger.info("Unscheduling job '" + jobDescription + "' (autogenerate " + (reviewerRelationGenerationSettings.isAutoGenerate() ? "enabled" : "disabled") + ")");
            }

        } catch (SchedulerException e) {
            throw new OrbitGamesSystemException("Failed to schedule review step generation  job for chapter '" + chapter.getRef() + "'", e);
        }
    }

    /**
     * Schedules a job to send deadline notifications to all students for whom they are relevant. Will unschedule the job
     * if the review step is disabled, or deadline notifications are disabled for the review step, or the deadline has already passed.
     * @param reviewStep The review step for which the job should be scheduled
     */
    public void scheduleSendDeadlineNotificationEmailJob(ReviewStep reviewStep) {

        Instant deadlineNotificationJobInstant = reviewStep.getDeadline().minusSeconds(reviewStep.getDeadlineNotificationEmailOffsetSeconds()).toInstant();

        try {

            JobKey jobKey = JobKey.jobKey("review-step-" + reviewStep.getRef(), JobGroups.SEND_DEADLINE_NOTIFICATION_EMAIL_JOB);
            TriggerKey triggerKey = TriggerKey.triggerKey(jobKey.getName(), JobTriggers.SEND_DEADLINE_NOTIFICATION_EMAIL_TRIGGER);
            scheduler.unscheduleJob(triggerKey);
            scheduler.deleteJob(jobKey);

            String jobDescription = "Send deadline notification e-mail for '" + reviewStep.getName() + "' step of chapter '" + reviewStep.getChapter().getName() +
                    "' in course '" + reviewStep.getChapter().getCourse().getName() + " (Q" + reviewStep.getChapter().getCourse().getQuarter() + ")'";

            if (!reviewStep.isDisabled() && deadlineNotificationJobInstant.isAfter(Instant.now()) && reviewStep.isDeadlineNotificationEmailEnabled()) {

                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put(SendDeadlineNotificationEmailJob.COURSE_REF_KEY, reviewStep.getChapter().getCourse().getRef());
                jobDataMap.put(SendDeadlineNotificationEmailJob.CHAPTER_REF_KEY, reviewStep.getChapter().getRef());
                jobDataMap.put(SendDeadlineNotificationEmailJob.REVIEW_STEP_REF_KEY, reviewStep.getRef());

                JobDetail job = JobBuilder.newJob(SendDeadlineNotificationEmailJob.class)
                        .withIdentity(jobKey)
                        .withDescription(jobDescription)
                        .usingJobData(jobDataMap)
                        .storeDurably()
                        .build();

                Trigger trigger = TriggerBuilder.newTrigger()
                        .forJob(job)
                        .withIdentity(triggerKey)
                        .withDescription(job.getDescription() + " trigger")
                        .startAt(Date.from(deadlineNotificationJobInstant))
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                        .build();

                // Note that schedule job will overwrite jobs with the same key
                logger.info("Scheduling job '" + jobDescription + "'");
                scheduler.scheduleJob(job, trigger);
            } else {
                // Unscheduled the job if it should have already happened
                logger.info("Unscheduled job '" + jobDescription + "'");
            }
        } catch (SchedulerException e) {
            throw new OrbitGamesSystemException("Failed to schedule deadline notification job for review step '" + reviewStep.getRef() + "'", e);
        }

    }

}
