package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.client;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.BugReport;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.BugReportScreenshot;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.AnnouncementRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.BugReportRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ClientVersionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SystemPropertyRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.BugReportScreenshotWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ClientVersionCheckResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ClientVersionCheckResultStatus;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Platform;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.EmailService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.FileService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Controller for data related to the game client (e.g. announcements, game client versions)
 */
@Component
public class ClientController {

    private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    private static final String BUG_REPORT_NOTIFICATION_EMAIL_LAST_BUG_REPORT_ID_SYSTEM_PROPERTY_KEY = "BugReportEmailNotificationLastMessageId";

    private final AnnouncementRepository announcementRepository;
    private final ClientVersionRepository clientVersionRepository;
    private final BugReportRepository bugReportRepository;
    private final SystemPropertyRepository systemPropertyRepository;

    private final UserSession userSession;
    private final FileService fileService;
    private final EmailService emailService;
    private final YourGreatLibraryConfig config;
    private final MessageSource messageSource;

    public ClientController(
            @Autowired AnnouncementRepository announcementRepository,
            @Autowired ClientVersionRepository clientVersionRepository,
            @Autowired BugReportRepository bugReportRepository,
            @Autowired SystemPropertyRepository systemPropertyRepository,
            @Autowired UserSession userSession,
            @Autowired FileService fileService,
            @Autowired EmailService emailService,
            @Autowired YourGreatLibraryConfig config,
            @Autowired MessageSource messageSource
    ) {
        this.announcementRepository = announcementRepository;
        this.clientVersionRepository = clientVersionRepository;
        this.bugReportRepository = bugReportRepository;
        this.systemPropertyRepository = systemPropertyRepository;

        this.userSession = userSession;
        this.fileService = fileService;
        this.emailService = emailService;
        this.config = config;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<Announcement> getActiveAnnouncements() {
        return this.announcementRepository.getAnnouncementValidOn(OffsetDateTime.now());
    }

    @Transactional
    public Announcement addAnnouncement(Announcement announcement) {

        logger.info("User '{}' ('{}') adding announcement", userSession.getUsername(), userSession.getUserRef());

        Announcement newAnnouncement = new Announcement();
        newAnnouncement.setRef(RefGenerator.generate());
        mergeAnnouncements(announcement, newAnnouncement);

        return announcementRepository.save(newAnnouncement);
    }

    private void mergeAnnouncements(Announcement source, Announcement destination) {

        destination.setStartDate(source.getStartDate());
        destination.setEndDate(source.getEndDate());
        destination.setTitle(source.getTitle());
        destination.setText(source.getText());
        destination.setUrl(source.getUrl());
    }

    @Transactional(readOnly = true)
    public ClientVersionCheckResult versionCheck(Platform platform, int buildNumber) {

        Optional<ClientVersion> current = this.clientVersionRepository.findByPlatformAndBuildNumber(platform, buildNumber);

        ClientVersion latest = this.clientVersionRepository.findLatestUnrevokedByPlatform(platform)
                .orElseThrow(() -> new OrbitGamesApplicationException("There are no releases yet for platform '" + platform + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        ClientVersionCheckResult res = new ClientVersionCheckResult();
        // If we can't find the current version, we just return "null" for current. Apparently the user is using some unknown version
        res.setCurrent(current.orElse(null));
        res.setLatest(latest);

        if (!current.isPresent()) {
            // If the current can't be found, it's probably a new build that the back-end doesn't know again, so it's okay
            res.setStatus(ClientVersionCheckResultStatus.OK);
        } else if (current.get().equals(latest)) {
            res.setStatus(ClientVersionCheckResultStatus.OK);
        } else if (current.get().isRevoked()) {
            res.setStatus(ClientVersionCheckResultStatus.ROLLBACK);
        } else if (this.clientVersionRepository.findForcedUpdateSince(platform, buildNumber).isPresent()) {
            res.setStatus(ClientVersionCheckResultStatus.UPDATE_REQUIRED);
        } else {
            res.setStatus(ClientVersionCheckResultStatus.UPDATE_AVAILABLE);
        }

        return res;
    }

    public ClientVersion addClientVersion(ClientVersion clientVersion) {

        // There isn't really any data in a client version that the server should control, so we can safely just save it here without any validation
        return clientVersionRepository.save(clientVersion);

    }

    @Transactional
    public void createBugReport(BugReport bugReport, Set<BugReportScreenshotWrapper> screenshots) {

        logger.info("Creating bug report with {} screenshots " + screenshots.size());

        // Store the screenshots on disk and create references to them:
        Set<BugReportScreenshot> screenshotEntities = new HashSet<>();

        int i = 0;
        for (BugReportScreenshotWrapper screenshot: screenshots) {

            try (InputStream is = new ByteArrayInputStream(screenshot.getScreenshot())) {

                String fileName = String.format("screenshot-%d.png", i + 1);
                FileService.FileMetaData fileMetaData = fileService.storeFile(fileName, is, 16_000_000);

                BugReportScreenshot screenshotEntity = new BugReportScreenshot();
                screenshotEntity.setBugReport(bugReport);
                screenshotEntity.setClientTime(screenshot.getClientTime());
                screenshotEntity.setScreenshotFilename(fileMetaData.getRelativeStoragePath());

                screenshotEntities.add(screenshotEntity);

            } catch (IOException e) {
                throw new OrbitGamesSystemException("Could not read/write screenshot", e);
            }
            i++;
        }

        // The bug report is ready to be stored directly
        bugReport.setScreenshots(screenshotEntities);

        bugReportRepository.save(bugReport);
    }


    /**
     * Periodically send e-mail notifications to all administrators for bug reports
     */
    @Transactional
    @Scheduled(initialDelay = 15000, fixedDelay = 30000)
    public void sendBugReportNotificationEmails() {

        List<BugReport> newBugReports = bugReportRepository.findByNotificationEmailSentFalse();

        if (newBugReports.isEmpty()) {
            logger.debug("No new bug reports");
            // No new messages
            return;
        }

        Collection<InternetAddress> toEmails = new ArrayList<>();
        for (String email : config.getBugReportEmails()) {
            try {
                InternetAddress internetAddress = new InternetAddress(email);
                toEmails.add(internetAddress);
            } catch (AddressException e) {
                throw new OrbitGamesSystemException("Invalid e-mail address: '" + email + "'", e);
            }
        }

        for (BugReport bugReport: newBugReports) {
            sendBugReportNotification(bugReport, toEmails);
        }
    }

    private void sendBugReportNotification(BugReport bugReport, Collection<InternetAddress> toEmails) {

        logger.info("Sending bug report notification for bug report {} with email '{}'", bugReport.getId(), bugReport.getEmail());

        String emailSubject = this.messageSource.getMessage(Messages.BUG_REPORT_NOTIFICATION_EMAIL_SUBJECT, new Object[] {bugReport.getEmail(), bugReport.getCreated().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))}, Locale.getDefault());
        String emailText = this.messageSource.getMessage(Messages.BUG_REPORT_NOTIFICATION_EMAIL_TEXT, new Object[] {bugReport.getEmail(), bugReport.getUserInput()}, Locale.getDefault());

        List<EmailService.AttachmentWrapper> attachments = new ArrayList<>();
        try {
            attachments.add(createAttachmentWrapper("device_info.txt", bugReport.getDeviceInfo()));
            attachments.add(createAttachmentWrapper("communication.log", bugReport.getCommunicationLogs()));
            attachments.add(createAttachmentWrapper("game.log", bugReport.getGameLogs()));
            attachments.add(createAttachmentWrapper("prefs.json", bugReport.getPrefs()));
            attachments.add(createAttachmentWrapper("knowledge.json", bugReport.getKnowledge()));

            String screenshotsZipFilename = "screenshots.zip";
            Path tempFolderPath = Paths.get(System.getProperty("java.io.tmpdir")).resolve("great-library");
            Files.createDirectories(tempFolderPath);
            Path screenshotsZipTempFilePath = tempFolderPath.resolve(screenshotsZipFilename);

            if (bugReport.getScreenshots() != null && !bugReport.getScreenshots().isEmpty()) {
                // The first screenshot is added as a separate screenshot to the mail
                attachments.add(wrapScreenshot(
                        bugReport.getScreenshots().stream()
                                .min(Comparator.comparing(BugReportScreenshot::getId))
                                .orElseThrow(RuntimeException::new)));

                // The rest of the screenshots are added to a zip file in the mail
                if (bugReport.getScreenshots().size() > 1) {

                    try (OutputStream fos = Files.newOutputStream(screenshotsZipTempFilePath);
                         ZipOutputStream zos = new ZipOutputStream(fos)
                    ) {
                        for (BugReportScreenshot screenshot : bugReport.getScreenshots()) {
                            zos.putNextEntry(new ZipEntry(Paths.get(screenshot.getScreenshotFilename()).getFileName().toString()));
                            File screenshotFile = fileService.getFile(screenshot.getScreenshotFilename());
                            try (InputStream is = new FileInputStream(screenshotFile)) {
                                IOUtils.copy(is, zos);
                            }
                            zos.closeEntry();
                        }
                    }


                    attachments.add(new EmailService.AttachmentWrapper(
                            new FileSystemResource(screenshotsZipTempFilePath.toFile()),
                            screenshotsZipFilename
                    ));
                }
            }

            InternetAddress replyTo = null;
            try {
                replyTo = new InternetAddress(bugReport.getEmail());
            } catch (AddressException e) {
                // Ignore any address exception when sending the e-mail, we'll just not set the replyTo header
            }

            emailService.sendMessage(toEmails, replyTo, emailSubject, emailText, attachments);
            try {
                // We try to delete the temp screenshots.zip file, but if it fails we ignore
                Files.deleteIfExists(screenshotsZipTempFilePath);
            } catch(Exception e) {
                logger.warn("Failed to delete temporary screenshots zip file: " + e.getMessage(), e);
            }

            bugReport.setNotificationEmailSent(true);
            bugReportRepository.save(bugReport);
        } catch(Exception e) {
            logger.error("Failed to send bug-report e-mail: " + e.getMessage(), e);
        }
    }

    private EmailService.AttachmentWrapper wrapScreenshot(BugReportScreenshot screenshot) {
        return new EmailService.AttachmentWrapper(
                new FileSystemResource(fileService.getFile(screenshot.getScreenshotFilename())),
                Paths.get(screenshot.getScreenshotFilename()).getFileName().toString());
    }


    private EmailService.AttachmentWrapper createAttachmentWrapper(String fileName, String fileContent) {
        return new EmailService.AttachmentWrapper(new ByteArrayResource(fileContent.getBytes()), fileName);
    }
}
