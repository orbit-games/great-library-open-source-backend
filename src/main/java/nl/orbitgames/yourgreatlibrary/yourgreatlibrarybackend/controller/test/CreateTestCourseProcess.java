package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.test;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation.ConversationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ArgumentTypeRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterSectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.QuizRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationGenerationSettingsRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SkillRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationGenerationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.AcademicYearUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.Lipsum;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CreateTestCourseProcess {

    private static final List<String> TEST_CHAPTERS = Collections.unmodifiableList(Arrays.asList(
            "Problem Introduction",
            "Research Question",
            "Research Approach",
            "Research Flow Diagram",
            "Alignment"
    ));
    private static final List<String> RESOURCE_NAMES = Arrays.asList(
            "Tips on writing a good review", "Assessing your peer", "Argument types", "How to write a good argument",
            "Determining the usefulness for society"
    );
    private static final Map<ReviewStepType, String> REVIEW_STEP_SKILL_NAMES =
            Stream.of(
                    Pair.of(ReviewStepType.REVIEW, "Review"),
                    Pair.of(ReviewStepType.SUBMISSION, "Submission"),
                    Pair.of(ReviewStepType.EVALUATION, "Evaluate"),
                    Pair.of(ReviewStepType.REBUTTAL, "Rebuttal"),
                    Pair.of(ReviewStepType.ASSESSMENT, "Assess")
            ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    private final CourseRepository courseRepository;
    private final ResourceRepository resourceRepository;
    private final ChapterRepository chapterRepository;
    private final ReviewStepRepository reviewStepRepository;
    private final QuizRepository quizRepository;
    private final ArgumentTypeRepository argumentTypeRepository;
    private final ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository;
    private final SkillRepository skillRepository;
    private final SectionRepository sectionRepository;
    private final SectionResourceRepository sectionResourceRepository;
    private final ChapterSectionRepository chapterSectionRepository;

    private final ConversationController conversationController;

    public CreateTestCourseProcess(
            @Autowired CourseRepository courseRepository,
            @Autowired ResourceRepository resourceRepository,
            @Autowired ChapterRepository chapterRepository,
            @Autowired ReviewStepRepository reviewStepRepository,
            @Autowired QuizRepository quizRepository,
            @Autowired ArgumentTypeRepository argumentTypeRepository,
            @Autowired ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository,
            @Autowired SkillRepository skillRepository,
            @Autowired SectionRepository sectionRepository,
            @Autowired SectionResourceRepository sectionResourceRepository,
            @Autowired ChapterSectionRepository chapterSectionRepository,

            @Autowired ConversationController conversationController
    ) {
        this.courseRepository = courseRepository;
        this.resourceRepository = resourceRepository;
        this.chapterRepository = chapterRepository;
        this.reviewStepRepository = reviewStepRepository;
        this.quizRepository = quizRepository;
        this.argumentTypeRepository = argumentTypeRepository;
        this.reviewerRelationGenerationSettingsRepository = reviewerRelationGenerationSettingsRepository;
        this.skillRepository = skillRepository;
        this.sectionRepository = sectionRepository;
        this.sectionResourceRepository = sectionResourceRepository;
        this.chapterSectionRepository = chapterSectionRepository;

        this.conversationController = conversationController;
    }

    @Transactional
    public Course createTestCourse(OffsetDateTime firstSubmissionDeadline, String courseName, String courseCode,
                                   boolean anonymousUsers, boolean anonymousReviews) {

        Course course = new Course();
        course.setRef(RefGenerator.generate());
        course.setName(courseName);
        course.setCode(courseCode);
        course.setYear(AcademicYearUtils.calculateAcademicYearOfDate(firstSubmissionDeadline));
        course.setQuarter(AcademicYearUtils.calculateQuarterOfDate(firstSubmissionDeadline));
        course.setAnonymousUsers(anonymousUsers);
        course.setAnonymousReviews(anonymousReviews);

        course = courseRepository.save(course);

        ReviewerRelationGenerationSettings groupBasedGeneration = new ReviewerRelationGenerationSettings();
        groupBasedGeneration.setGenerationType(ReviewerRelationGenerationType.GROUP_BASED);
        groupBasedGeneration.setReviewerCount(2);
        groupBasedGeneration.setAutoGenerate(true);
        groupBasedGeneration.setAutoMarkComplete(true);
        // For the test course, we just include all the peers in the review process
        groupBasedGeneration.setIncludeLatePeers(true);
        groupBasedGeneration.setAutoGenerateDelay(0);
        groupBasedGeneration = reviewerRelationGenerationSettingsRepository.save(groupBasedGeneration);

        ReviewerRelationGenerationSettings randomGeneration = new ReviewerRelationGenerationSettings();
        randomGeneration.setGenerationType(ReviewerRelationGenerationType.RANDOM);
        randomGeneration.setReviewerCount(2);
        randomGeneration.setAutoGenerate(true);
        randomGeneration.setAutoMarkComplete(true);
        // For the test course, we just include all the peers in the review process
        randomGeneration.setIncludeLatePeers(true);
        randomGeneration.setAutoGenerateDelay(0);
        randomGeneration = reviewerRelationGenerationSettingsRepository.save(randomGeneration);

        List<ArgumentType> argumentTypes = generateArgumentTypes(course);
        List<Skill> skills = generateSkills(course);
        Set<Resource> resources = generateResources(course, skills, 5);
        Set<Section> sections = generateSections(course, resources, 3, 3);
        List<Chapter> chapters = generateChapters(firstSubmissionDeadline, course, sections, skills, Arrays.asList(randomGeneration, groupBasedGeneration), argumentTypes);

        conversationController.createDefaultCourseConversations(course);

        return course;
    }

    private Set<Section> generateSections(Course course, Set<Resource> resources, int... sectionCountPerLevel) {

        Set<Section> sections = new HashSet<>();
        List<Section> previousLayerSections = new ArrayList<>();
        previousLayerSections.add(null);
        int curResourceIdx = 0;
        Random rnd = new Random();
        for (int sectionCount: sectionCountPerLevel) {

            List<Section> layerSections = new ArrayList<>();

            for (Section previousLayerSection: previousLayerSections) {
                for (int i = 0; i < sectionCount; i++) {
                    Section section = new Section();
                    section.setRef(RefGenerator.generate());
                    section.setCourse(course);
                    section.setName("Section " + i);
                    section.setDescription("Section description " + i + ": " + Lipsum.sentences(rnd.nextInt(Lipsum.SENTENCE_COUNT / 2)));
                    if (previousLayerSection != null) {
                        section.setParentSection(previousLayerSection);
                        section.setSortOrder(i * 10);
                        section.setParentRelationDescription("This sub section is interesting because: " + Lipsum.sentences(rnd.nextInt(Lipsum.SENTENCE_COUNT / 2)));
                    }

                    section = sectionRepository.save(section);
                    // Assign some resources to the section:
                    for (int j = 0; j < i % 3; j++) {
                        SectionResource sectionResource = new SectionResource();
                        sectionResource.setSection(section);
                        sectionResource.setResource(resources.stream().skip(curResourceIdx % resources.size()).findFirst().orElseThrow(() -> new OrbitGamesSystemException("No resource?")));
                        curResourceIdx++;
                        sectionResource.setOptional(j % 2 == 0);
                        sectionResource.setDescription("You should read this resource because: " + Lipsum.sentences(rnd.nextInt(3)));
                        sectionResource.setSortOrder(j * 10);

                        sectionResource = sectionResourceRepository.save(sectionResource);

                        section.addSectionResource(sectionResource);
                    }

                    sections.add(section);
                    layerSections.add(section);
                }
            }
            previousLayerSections.clear();
            previousLayerSections.addAll(layerSections);
        }
        return sections;
    }

    private List<Skill> generateSkills(Course course) {

        List<Skill> courseSkills = new ArrayList<>();
        // For now, we just generate 1 skill for each review step, and 1 skill for each resource title.
        for(ReviewStepType stepType: ReviewStepType.values()) {
            Skill stepSkill = new Skill();
            stepSkill.setRef(RefGenerator.generate());
            stepSkill.setName(REVIEW_STEP_SKILL_NAMES.get(stepType));
            stepSkill.setCourse(course);

            stepSkill = skillRepository.save(stepSkill);
            courseSkills.add(stepSkill);
        }

        for (String resourceName: RESOURCE_NAMES) {
            Skill resourceSkill = new Skill();
            resourceSkill.setRef(RefGenerator.generate());
            resourceSkill.setName(resourceName);
            resourceSkill.setCourse(course);

            resourceSkill = skillRepository.save(resourceSkill);
            courseSkills.add(resourceSkill);
        }

        course.setSkills(new HashSet<>(courseSkills));

        return courseSkills;
    }

    private List<ArgumentType> generateArgumentTypes(Course course) {

        List<ArgumentType> argumentTypes = new ArrayList<>();

        ArgumentType englishProficiency = new ArgumentType();
        englishProficiency.setRef(RefGenerator.generate());
        englishProficiency.setCourse(course);
        englishProficiency.setName("English proficiency");
        englishProficiency.setRequired(true);
        englishProficiency = argumentTypeRepository.save(englishProficiency);
        argumentTypes.add(englishProficiency);

        ArgumentType reportingClarity = new ArgumentType();
        reportingClarity.setRef(RefGenerator.generate());
        reportingClarity.setCourse(course);
        reportingClarity.setName("Reporting clarity");
        reportingClarity.setRequired(true);
        reportingClarity = argumentTypeRepository.save(reportingClarity);
        argumentTypes.add(reportingClarity);

        ArgumentType referencing = new ArgumentType();
        referencing.setRef(RefGenerator.generate());
        referencing.setCourse(course);
        referencing.setName("Referencing");
        referencing.setRequired(true);
        referencing = argumentTypeRepository.save(referencing);
        argumentTypes.add(referencing);

        ArgumentType societalRelevance = new ArgumentType();
        societalRelevance.setRef(RefGenerator.generate());
        societalRelevance.setCourse(course);
        societalRelevance.setName("Societal relevance");
        societalRelevance.setRequired(true);
        societalRelevance = argumentTypeRepository.save(societalRelevance);
        argumentTypes.add(societalRelevance);

        ArgumentType researchObjectives = new ArgumentType();
        researchObjectives.setRef(RefGenerator.generate());
        researchObjectives.setCourse(course);
        researchObjectives.setName("Research objectives");
        researchObjectives.setRequired(true);
        researchObjectives = argumentTypeRepository.save(researchObjectives);
        argumentTypes.add(researchObjectives);


        ArgumentType feedback = new ArgumentType();
        feedback.setRef(RefGenerator.generate());
        feedback.setCourse(course);
        feedback.setName("Feedback");
        feedback = argumentTypeRepository.save(feedback);
        argumentTypes.add(feedback);

        ArgumentType fallacy = new ArgumentType();
        fallacy.setRef(RefGenerator.generate());
        fallacy.setCourse(course);
        fallacy.setName("Fallacy");
        fallacy.setParent(feedback);
        fallacy = argumentTypeRepository.save(fallacy);
        argumentTypes.add(fallacy);

        ArgumentType style = new ArgumentType();
        style.setRef(RefGenerator.generate());
        style.setCourse(course);
        style.setName("Style");
        style.setParent(feedback);
        style = argumentTypeRepository.save(style);
        argumentTypes.add(style);


        ArgumentType comment = new ArgumentType();
        comment.setRef(RefGenerator.generate());
        comment.setCourse(course);
        comment.setName("Comment");
        comment = argumentTypeRepository.save(comment);
        argumentTypes.add(comment);

        return argumentTypes;

    }

    private Set<Resource> generateResources(Course course, List<Skill> skills, int count) {

        Set<Resource> resources = new HashSet<>();

        Random rnd = new Random();
        for (int i = 0; i < count; i++) {

            String resourceName = RESOURCE_NAMES.get(rnd.nextInt(RESOURCE_NAMES.size()));

            Resource resource = new Resource();
            resource.setRef(RefGenerator.generate());
            resource.setCourse(course);
            resource.setSortOrder(i * 10);
            resource.setName(resourceName);
            resource.setDescription(Lipsum.sentences((int) (Lipsum.SENTENCE_COUNT * (double) 1 / (double) Lipsum.SENTENCE_COUNT)));
            if (i % 3 < 2) {
                resource.setUrl(rnd.nextInt(10) < 1 ? "https://www.youtube.com/watch?v=dQw4w9WgXcQ" : "https://en.wikipedia.org/wiki/Peer_review");
            }
            if (i % 3 > 0) {
                resource.setQuiz(generateQuiz("Resource Quiz", Arrays.asList(randomQuizQuestion(), randomQuizQuestion(), randomQuizQuestion())));
                resource.setQuizMinimalScore(2);
            }
            resource.setSkills(skills.stream().filter(s -> s.getName().equals(resourceName)).collect(Collectors.toSet()));

            resource = resourceRepository.save(resource);
            resources.add(resource);
        }

        course.setResources(resources);
        return resources;

    }


    private Pair<String, List<Pair<String, Integer>>> randomQuizQuestion() {
        return EXAMPLE_QUIZ_QUESTIONS.get(new Random().nextInt(EXAMPLE_QUIZ_QUESTIONS.size()));
    }

    private List<Chapter> generateChapters(OffsetDateTime firstSubmissionDeadline, Course course, Set<Section> sections, List<Skill> skills, List<ReviewerRelationGenerationSettings> generationSettings, List<ArgumentType> argumentTypes) {

        List<Chapter> chapters = new ArrayList<>(TEST_CHAPTERS.size());

        int order = 0;
        OffsetDateTime chapterStart = firstSubmissionDeadline;
        int sectionIdx = 0;
        for (String testChapterName: TEST_CHAPTERS) {

            int chapterIndex = TEST_CHAPTERS.indexOf(testChapterName);

            Chapter chapter = new Chapter();
            chapter.setRef(RefGenerator.generate());
            chapter.setCourse(course);
            chapter.setSortOrder(order);
            chapter.setName(testChapterName);
            chapter.setReviewerRelationGenerationSettings(generationSettings.get(chapterIndex % generationSettings.size()));

            chapter = chapterRepository.save(chapter);

            chapters.add(chapter);

            for (int i = 0; i < (chapterIndex + 1) % 3; i++) {
                ChapterSection chapterSection = new ChapterSection();
                chapterSection.setChapter(chapter);
                chapterSection.setSection(sections.stream().skip(sectionIdx % sections.size()).findFirst().orElseThrow(() -> new OrbitGamesSystemException("No sections?")));
                chapterSection.setDescription("Section " + i + " is relevant for the chapter because: " + Lipsum.sentences(i));
                chapterSection.setSortOrder(i * 10);
                chapterSection.setOptional(i % 2 == 0);

                chapterSection = chapterSectionRepository.save(chapterSection);

                chapter.addChapterSection(chapterSection);
            }


            List<ReviewStep> reviewSteps = generateReviewSteps(argumentTypes, skills, chapterStart, chapterIndex, chapter);

            chapter.setReviewSteps(reviewSteps);

            order += 10;
            chapterStart = chapterStart.plus(7, ChronoUnit.DAYS);
        }

        course.setChapters(chapters);

        return chapters;
    }

    private List<ReviewStep> generateReviewSteps(List<ArgumentType> argumentTypes, List<Skill> skills, OffsetDateTime chapterStart, int chapterIndex, Chapter chapter) {
        List<ReviewStep> reviewSteps = new ArrayList<>();

        // For now, I'll just hardcode the review steps here
        ReviewStep submission = new ReviewStep();
        submission.setRef(RefGenerator.generate());
        submission.setChapter(chapter);
        submission.setSortOrder(0);
        submission.setStepType(ReviewStepType.SUBMISSION);
        submission.setName("Submission");
        submission.setDescription(SUBMISSION_DESCRIPTION);
        submission.setFileUploadName("Upload the document to be reviewed");
        submission.setFileUpload(true);
        submission.setDeadline(chapterStart);
        submission.setDiscussionType(DiscussionType.NONE);
        submission.setSkills(skills.stream().filter(s -> s.getName().equals(REVIEW_STEP_SKILL_NAMES.get(ReviewStepType.SUBMISSION))).collect(Collectors.toSet()));

        submission = reviewStepRepository.save(submission);
        reviewSteps.add(submission);

        ReviewStep review = new ReviewStep();
        review.setRef(RefGenerator.generate());
        review.setChapter(chapter);
        review.setSortOrder(10);
        review.setStepType(ReviewStepType.REVIEW);
        review.setName("Review");
        review.setDescription(REVIEW_DESCRIPTION);
        if (chapterIndex % 2 == 0) {
            review.setArgumentTypes(new HashSet<>(argumentTypes));
            review.setDiscussionType(DiscussionType.RESTRICTED_ARGUMENTS);
        } else {
            review.setDiscussionType(DiscussionType.FREE_FORM_ARGUMENTS);
        }
        review.setDeadline(chapterStart.plus(5, ChronoUnit.DAYS));
        review.setSkills(skills.stream().filter(s -> s.getName().equals(REVIEW_STEP_SKILL_NAMES.get(ReviewStepType.REVIEW))).collect(Collectors.toSet()));

        Quiz reviewQuiz = generateQuiz("Review", Arrays.asList(
                Pair.of("English proficiency", Arrays.asList(
                        Pair.of("Multiple grammar and/or spelling errors", 1),
                        Pair.of("No, or few typos", 2),
                        Pair.of("Clear and simple language", 3),
                        Pair.of("Well written", 4),
                        Pair.of("Proficient in writing", 5)
                )),
                Pair.of( "Reporting clarity", Arrays.asList(
                        Pair.of("Fragmented, no or weak line of reasoning", 1),
                        Pair.of("Unfocused line of reasoning", 2),
                        Pair.of("Logic and straightforward line of reasoning", 3),
                        Pair.of("Well structured, sub headings when appropriate", 4),
                        Pair.of("Very well structured and fluently argued", 5)
                )),
                Pair.of( "Referencing", Arrays.asList(
                        Pair.of("Many references missing, no reference style", 1),
                        Pair.of("In-text statements without sources. Sloppy reference list, but style based.", 2),
                        Pair.of("Carefully documented, all statements made referenced and listed", 3),
                        Pair.of("Detailed referencing, perfect reference list", 4),
                        Pair.of("Referencing to the point, adequate and perfect. Authors input clearly distinguished.", 5)
                )),
                Pair.of("Societal relevance", Arrays.asList(
                        Pair.of("Undetermined if and how research contributes to society", 1),
                        Pair.of("Indicated but not well argued for", 2),
                        Pair.of("Well-argued societal problem statement", 3),
                        Pair.of("Intriguing explanation of societal challenge", 4),
                        Pair.of("Outstanding societal problem analysis", 5)
                )),
                Pair.of("Research objectives", Arrays.asList(
                        Pair.of("Underdeveloped or no objectives mentioned from the program perspective", 1),
                        Pair.of("Vaguely addressed link with program", 2),
                        Pair.of("Well-defined with MSc program", 3),
                        Pair.of("Perfect match with program", 4),
                        Pair.of("Exemplary for the program", 5)
                ))
        ));
        review.setQuiz(reviewQuiz);

        review = reviewStepRepository.save(review);
        reviewSteps.add(review);

        ReviewStep evaluation = new ReviewStep();
        evaluation.setRef(RefGenerator.generate());
        evaluation.setChapter(chapter);
        evaluation.setSortOrder(20);
        evaluation.setStepType(ReviewStepType.EVALUATION);
        evaluation.setName("Evaluation");
        evaluation.setDescription(EVALUATE_DESCRIPTION);
        evaluation.setDeadline(chapterStart.plus(7, ChronoUnit.DAYS));
        evaluation.setDiscussionType(DiscussionType.NONE);
        evaluation.setSkills(skills.stream().filter(s -> s.getName().equals(REVIEW_STEP_SKILL_NAMES.get(ReviewStepType.EVALUATION))).collect(Collectors.toSet()));

        Quiz evaluationQuiz = generateQuiz("Evaluation", Arrays.asList(
                Pair.of("Style", Arrays.asList(
                        Pair.of("Prescriptive, or guiding only", -1),
                        Pair.of("Helpful, but not really critical", 0),
                        Pair.of("Critical and constructive", 1)
                )),
                Pair.of("With regard to content", Arrays.asList(
                        Pair.of("Indicative feedback, lacks substantiated explanation.", -1),
                        Pair.of("Original argument disputed, commented", 0),
                        Pair.of("Argues constructively and substantive, makes supportive suggestion on how to improve", 1)
                )),
                Pair.of("Support", Arrays.asList(
                        Pair.of("Comments can be applied straight forward, like focus on language", -1),
                        Pair.of("Comments demand to improve explanation or adapt structure", 0),
                        Pair.of("Comments need reflection of the argument, and debate scientifically", 1)
                ))
        ));

        evaluationQuiz = quizRepository.save(evaluationQuiz);
        evaluation.setQuiz(evaluationQuiz);

        evaluation = reviewStepRepository.save(evaluation);
        reviewSteps.add(evaluation);

        ReviewStep rebuttal = new ReviewStep();
        rebuttal.setRef(RefGenerator.generate());
        rebuttal.setChapter(chapter);
        rebuttal.setDisabled(chapterIndex == 2);
        rebuttal.setSortOrder(30);
        rebuttal.setStepType(ReviewStepType.REBUTTAL);
        rebuttal.setName("Rebuttal");
        rebuttal.setDescription(REBUTTAL_DESCRIPTION);
        rebuttal.setDeadline(chapterStart.plus(16, ChronoUnit.DAYS));
        rebuttal.setDiscussionType(DiscussionType.REPLY);
        rebuttal.setSkills(skills.stream().filter(s -> s.getName().equals(REVIEW_STEP_SKILL_NAMES.get(ReviewStepType.REBUTTAL))).collect(Collectors.toSet()));

        rebuttal = reviewStepRepository.save(rebuttal);
        reviewSteps.add(rebuttal);

        ReviewStep assessment = new ReviewStep();
        assessment.setRef(RefGenerator.generate());
        assessment.setChapter(chapter);
        assessment.setDisabled(chapterIndex == 2);
        assessment.setSortOrder(40);
        assessment.setStepType(ReviewStepType.ASSESSMENT);
        assessment.setName("Assessment");
        assessment.setDescription(ASSESSMENT_DESCRIPTION);
        assessment.setDeadline(chapterStart.plus(23, ChronoUnit.DAYS));
        assessment.setDiscussionType(DiscussionType.NONE);
        assessment.setSkills(skills.stream().filter(s -> s.getName().equals(REVIEW_STEP_SKILL_NAMES.get(ReviewStepType.ASSESSMENT))).collect(Collectors.toSet()));

        Quiz assessmentQuiz = generateQuiz("Assessment", Arrays.asList(
                Pair.of("Reply on Feedback", Arrays.asList(
                        Pair.of("Not to point, not taking comments seriously", -1),
                        Pair.of("Informative response to reviewer, most important comments addressed", 0),
                        Pair.of("Well argued academic reply, all comments addressed", 1)
                )),
                Pair.of("Style", Arrays.asList(
                        Pair.of("Defensive", -1),
                        Pair.of("Appreciative", 0),
                        Pair.of("Considering seriously", 1)
                ))
        ));

        assessmentQuiz = quizRepository.save(assessmentQuiz);
        assessment.setQuiz(assessmentQuiz);

        assessment = reviewStepRepository.save(assessment);
        reviewSteps.add(assessment);
        return reviewSteps;
    }

    private Quiz generateQuiz(String name, List<Pair<String, List<Pair<String, Integer>>>> quizElementsDefinition) {
        Quiz quiz = new Quiz();
        quiz.setRef(RefGenerator.generate());
        quiz.setName(name);
        quiz.setElements(
                quizElementsDefinition.stream()
                        .map(qe -> {
                            QuizElement quizElement = new QuizElement();
                            quizElement.setRef(RefGenerator.generate());
                            quizElement.setQuiz(quiz);
                            quizElement.setText(qe.getLeft());
                            quizElement.setSortOrder(quizElementsDefinition.indexOf(qe) * 10);
                            quizElement.setQuestionType(QuestionType.SELECT_ONE);
                            quizElement.setOptions(
                                    qe.getRight().stream().map(p -> {
                                        QuizElementOption quizOption = new QuizElementOption();
                                        quizOption.setRef(RefGenerator.generate());
                                        quizOption.setQuizElement(quizElement);
                                        quizOption.setSortOrder((p.getRight() - 1) * 10);
                                        quizOption.setPoints(p.getRight());
                                        quizOption.setText(p.getLeft());
                                        return quizOption;
                                    }).collect(Collectors.toSet())
                            );
                            return quizElement;
                        }).collect(Collectors.toList()));

        quizRepository.save(quiz);
        return quiz;
    }

    /**
     * @param items A collection of items
     * @param <T> Some type
     * @return A random entry from the collection of items
     */
    private <T> T randomItem(Collection<T> items) {
        if (items == null || items.isEmpty()) {
            return null;
        }
        return items.stream().skip(new Random(System.nanoTime()).nextInt(items.size())).findFirst()
                .orElseGet(() -> items.iterator().next());
    }

    private static final String SUBMISSION_DESCRIPTION = "Please upload your submission";
    private static final String REVIEW_DESCRIPTION =
            "You are asked here to <b>review</b> the document above. It is from one of your peers in the course.";
    private static final String EVALUATE_DESCRIPTION =
            "The reviewer was asked to have a look at your submission. You have to <b>evaluate</b> the quality of their reviews and comments: was the review reflective, informative and supportive, or too generic or too negative to be helpful?\n" +
            "\n" +
            "Note that you are asked to evaluate whether the reviewer’s comments were helpful to improve your writing. <i>It is not about whether you think the reviewer’s understanding is right or wrong.</i>\n" +
            "\n" +
            "Above, you see your original submission and the reviewer's comments.\n" +
            "\n" +
            "Please evaluate them based on the 3 criteria below.";

    private static final String REBUTTAL_DESCRIPTION =
            "Above, you see your original submission and all reviewer's comments.\n" +
            "\n" +
            "After you have made revisions to your document - based on their comments - please write a <b>rebuttal</b> to them.\n" +
            "\n" +
            "The rebuttal is a point-by-point response to the reviewers’ comments. Some tips: identify the major concerns of the reviewers, consider to categorize the comments and/or to add a paragraph that broadly summarizes the major changes that you have made. Make sure to answer each comment and explain how you dealt with that comment in a clear and concise manner. Even if you do not agree with a point or have not made the change suggested, please mention that and provide a reason for your decision.\n" +
            "\n" +
            "Keep in mind that the reviewer is always right, also in case you feel the reviewer has misunderstood something. If so, consider it likely to be due to lack of clarity in your presentation. In such cases, point out the misunderstanding politely and provide the necessary clarification.";

    private static final String ASSESSMENT_DESCRIPTION =
            "Above, you see the rebuttal text written by the original submitter. They should have replied content-wise to both reviews, from you and another reviewer.\n" +
            "\n" +
            "You are now asked to assess the rebuttal. The author of original document was expected to explain how your comments were interpreted and dealt with. Did they take your feedback seriously, and do they offer an explanation for not following up on specific parts of your peer review?";

    private static final List<Pair<String, List<Pair<String, Integer>>>> EXAMPLE_QUIZ_QUESTIONS = Arrays.asList(
            Pair.of("Learn from yesterday, live for today, hope for tomorrow. The important thing is not to stop _________.’ Fill in the blank to complete this quotation by Albert Einstein.", Arrays.asList(
                    Pair.of("Wishing", 0), Pair.of("Questioning", 1), Pair.of("Thinking", 0)
            )),
            Pair.of("The name of which spice comes from the French word for ‘nail’?", Arrays.asList(
                    Pair.of("Cinnamon", 0), Pair.of("Cardamom", 0), Pair.of("Clove", 1)
            )),
            Pair.of("In India, the train Lifeline Express is a ...", Arrays.asList(
                    Pair.of("Hospital", 1), Pair.of("Bank", 0), Pair.of("Primary School", 0)
            )),
            Pair.of("Which Asian mountain is also known as the Savage Mountain due to the extreme difficulty of ascent?", Arrays.asList(
                    Pair.of("Kanchenjunga", 0), Pair.of("K2", 1), Pair.of("Lhotse", 0)
            )),
            Pair.of("In 1964, which portfolio was given to Indira Gandhi in the government of Lal Bahadur Shastri?", Arrays.asList(
                    Pair.of("Defence", 0), Pair.of("Home", 0), Pair.of("Information and Broadcasting", 1)
            )),
            Pair.of("In Alice’s Adventures in Wonderland, which game was played by the Queen of Hearts using hedgehogs as balls?", Arrays.asList(
                    Pair.of("Quintet", 0), Pair.of("Quidditch", 0), Pair.of("Croquet", 1)
            )),
            Pair.of("With which unfortunate incident would you associate the warplane Enola Gay?", Arrays.asList(
                    Pair.of("Sinking of the ship Bismark", 0), Pair.of("The Hiroshima bombing", 1), Pair.of("Storming of Bastille", 0)
            )),
            Pair.of("Odhra Magadha is the precursor to which Indian dance form?", Arrays.asList(
                    Pair.of("Kuchipudi", 0), Pair.of("Kathak", 0), Pair.of("Odissi", 1)
            )),
            Pair.of("Who composed music for the 1969 film Goopy Gyne Bagha Byne?", Arrays.asList(
                    Pair.of("Satyajit Ray", 1), Pair.of("Ravi Shankar", 0), Pair.of("Shiv-Hari", 0)
            )),
            Pair.of("Where in the human body is the stapedius muscle situated?", Arrays.asList(
                    Pair.of("Nose", 0), Pair.of("Ears", 1), Pair.of("Leg", 0)
            )),
            Pair.of("What is the airspeed velocity of an unladen swallow?", Arrays.asList(
                    Pair.of("African or European Swallow?", 1), Pair.of("11 m/s", 0), Pair.of("25 m/s", 0)
            )),
            Pair.of("What is your favorite color?", Arrays.asList(
                    Pair.of("Blue", 1), Pair.of("Yellow", 1), Pair.of("Red", 1), Pair.of("Green", 1)
            ))
    );

}
