package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.ReviewerRelationGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.ReviewerRelationGeneratorFactory;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepResultRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Caches;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class ReviewerRelationGenerationProcess {

    private static final Logger logger = LoggerFactory.getLogger(ReviewerRelationGenerationProcess.class);

    private final ReviewStepStatusCalculator reviewStepStatusCalculator;

    private final ReviewerRelationRepository reviewerRelationRepository;
    private final ReviewStepResultRepository reviewStepResultRepository;
    private final ChapterRepository chapterRepository;

    private final YourGreatLibraryConfig config;

    @Autowired
    public ReviewerRelationGenerationProcess(
            ReviewStepStatusCalculator reviewStepStatusCalculator,
            ReviewerRelationRepository reviewerRelationRepository,
            ReviewStepResultRepository reviewStepResultRepository,
            ChapterRepository chapterRepository,
            YourGreatLibraryConfig config) {

        this.reviewStepStatusCalculator = reviewStepStatusCalculator;
        this.reviewerRelationRepository = reviewerRelationRepository;
        this.reviewStepResultRepository = reviewStepResultRepository;
        this.chapterRepository = chapterRepository;
        this.config = config;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public void generateReviewerRelations(String courseRef, String chapterRef, boolean force) {

        logger.info("Generating reviewer relations for course '{}' and chapter '{}'",
                courseRef, chapterRef);

        Chapter chapter = getChapterInternal(courseRef, chapterRef);

        if (CollectionUtils.isEmpty(chapter.getReviewSteps())) {
            return;
        }

        ReviewerRelationGenerationSettings generationSettings = getReviewerRelationGenerationSettings(chapter);

        if(!force && !generationSettings.isAutoGenerate()) {
            // Don't generate if auto generate is disabled
            return;
        }

        List<ReviewStepResult> reviewStepResults = reviewStepResultRepository.findByChapter(chapter);

        List<ReviewerRelation> existingReviewerRelations = reviewerRelationRepository.findByChapter(chapter);
        if (existingReviewerRelations.stream().anyMatch(r -> r.getReviewer() != null && r.getReviewer().getRole() == Role.LEARNER)) {
            throw new OrbitGamesApplicationException("Non-dummy reviewer relations already exist for chapter '"+ chapterRef + "'. Automatic augmentation of existing relations is not yet supported.", GenericErrorCode.ENTITY_ALREADY_EXISTS);
        }

        List<User> learners = chapter.getCourse().getUserCourses().stream()
                // Only active users are included in the review process
                .filter(UserCourse::isActive)
                // Only "learner" users should be included in the reviewer relations
                .filter(u -> u.getUser().getRole() == Role.LEARNER)
                .map(UserCourse::getUser).collect(Collectors.toList());

        // Now we check if all users have submissions, maybe (automatically) mark them as complete, and only include the
        // the ones that are ready:
        List<User> includedLearners = filterLearnersBasedOnGenerationSettings(chapter, generationSettings, reviewStepResults, existingReviewerRelations, learners);

        ReviewerRelationGenerator generator = ReviewerRelationGeneratorFactory.construct(generationSettings.getGenerationType());

        List<ReviewerRelation> generatedRelations = generator.generateRelations(chapter, includedLearners, generationSettings.getReviewerCount());

        // Replace the dummy reviewer relations with actual ones
        for (User learner: includedLearners) {

            List<ReviewerRelation> actualSubmitterRelations = existingReviewerRelations.stream().filter(r -> r.getSubmitter() == learner).collect(Collectors.toList());
            List<ReviewerRelation> generatedSubmitterRelations = generatedRelations.stream().filter(r -> r.getSubmitter() == learner).collect(Collectors.toList());
            if (actualSubmitterRelations.size() < generatedSubmitterRelations.size()) {
                logger.warn("Not enough dummy relations for user '" + learner.getRef() +  "' in chapter '" + chapter.getReviewerRelationGenerationSettings() + "'");
                ReviewerRelation newDummyRelation = new ReviewerRelation();
                newDummyRelation.setRef(RefGenerator.generate());
                newDummyRelation.setChapter(chapter);
                newDummyRelation.setSubmitter(learner);
                actualSubmitterRelations.add(newDummyRelation);
            } else if (actualSubmitterRelations.size() > generatedSubmitterRelations.size()) {
                logger.warn("Too many dummy relations for user '" + learner.getRef() +  "' in chapter '" + chapter.getReviewerRelationGenerationSettings() + "'");
                int removeCount = actualSubmitterRelations.size() - generatedSubmitterRelations.size();
                for (int i = 0; i < removeCount; i++) {
                    ReviewerRelation removedRelation = actualSubmitterRelations.remove(actualSubmitterRelations.size() - 1);
                    reviewerRelationRepository.delete(removedRelation);
                }
            }

            for (int i = 0; i < generatedSubmitterRelations.size(); i++) {
                ReviewerRelation actualSubmitterRelation = actualSubmitterRelations.get(i);
                ReviewerRelation generatedSubmitterRelation = generatedSubmitterRelations.get(i);
                actualSubmitterRelation.setReviewer(generatedSubmitterRelation.getReviewer());
                actualSubmitterRelation.setGroup(generatedSubmitterRelation.getGroup());

                reviewerRelationRepository.save(actualSubmitterRelation);
            }
        }
    }

    ReviewerRelationGenerationSettings getReviewerRelationGenerationSettings(Chapter chapter) {
        return Optional.ofNullable(chapter.getReviewerRelationGenerationSettings())
                .orElseGet(config::getDefaultReviewerRelationGenerationSetting);
    }

    public OffsetDateTime getReviewerRelationGenerationTime(Chapter chapter) {

        Optional<ReviewStep> firstReviewStep = chapter.getReviewSteps().stream()
                .filter(r -> !r.isDisabled())
                .min(Comparator.comparing(ReviewStep::getDeadline));

        if (!firstReviewStep.isPresent()) {
            return null;
        }

        ReviewerRelationGenerationSettings reviewerRelationGenerationSettings = getReviewerRelationGenerationSettings(chapter);

        return firstReviewStep.get().getDeadline().plusSeconds(reviewerRelationGenerationSettings.getAutoGenerateDelay());
    }

    private List<User> filterLearnersBasedOnGenerationSettings(Chapter chapter, ReviewerRelationGenerationSettings generationSettings, List<ReviewStepResult> reviewStepResults, List<ReviewerRelation> existingReviewerRelations, List<User> students) {

        ReviewStep firstReviewStepInChapter = chapter.getReviewSteps().get(0);
        List<User> includedStudents = new ArrayList<>();
        if (generationSettings.isIncludeLatePeers()) {
            includedStudents.addAll(students);
        } else {
            for (User user : students) {

                List<ReviewerRelation> relationsAsSubmitter = existingReviewerRelations.stream()
                        .filter(r -> r.getSubmitter() == user)
                        .collect(Collectors.toList());

                List<ReviewStepResult> results = reviewStepResults.stream()
                        .filter(rsr -> relationsAsSubmitter.contains(rsr.getReviewerRelation()))
                        .filter(rsr -> rsr.getReviewStep().getRef().equals(firstReviewStepInChapter.getRef()))
                        .collect(Collectors.toList());

                // Check if we need to mark any results as complete
                if (generationSettings.isAutoMarkComplete()) {
                    results.stream().filter(rsr -> rsr.getMarkedComplete() == null && reviewStepStatusCalculator.calculateReadyToComplete(rsr, reviewStepResults))
                            .forEach(rsr -> {
                                logger.info("Automatically marking review step result for step '{}' and reviewer relation '{}' as complete", rsr.getReviewStep().getRef(), rsr.getReviewerRelation().getRef());
                                rsr.setMarkedComplete(OffsetDateTime.now());
                                reviewStepResultRepository.save(rsr);
                            });
                }

                // If all the results (for the first review step) are marked as complete, we'll include the reviewer in the review process
                if (results.size() == relationsAsSubmitter.size() && results.stream().allMatch(r -> r.getMarkedComplete() != null)) {
                    includedStudents.add(user);
                }
            }
        }
        return includedStudents;
    }

    private Chapter getChapterInternal(String courseRef, String chapterRef) {

        return chapterRepository.findByRef(chapterRef)
                .filter(c -> c.getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + chapterRef + "', or the chapter is not parse of course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }
}
