package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class SendDeadlineNotificationEmailJob extends QuartzJobBean {

    public static final String COURSE_REF_KEY = "courseRef";
    public static final String CHAPTER_REF_KEY = "chapterRef";
    public static final String REVIEW_STEP_REF_KEY = "reviewStepRef";

    private static final Logger logger = LoggerFactory.getLogger(GenerateReviewerRelationsJob.class);

    // Since this class is initialized by Quartz, we can't use constructor injection, and instead need to use field injection.
    @Autowired
    private SendDeadlineNotificationEmailProcess sendDeadlineNotificationEmailProcess;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        JobDataMap data = jobExecutionContext.getJobDetail().getJobDataMap();
        String courseRef = data.getString(COURSE_REF_KEY);
        String chapterRef = data.getString(CHAPTER_REF_KEY);
        String reviewStepRef = data.getString(REVIEW_STEP_REF_KEY);

        logger.info("Sending deadline notification e-mails for review step '" + reviewStepRef + "'");
        sendDeadlineNotificationEmailProcess.sendDeadlineEmailNotification(courseRef, chapterRef, reviewStepRef);
    }
}
