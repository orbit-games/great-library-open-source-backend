package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserCourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PermissionController {

    private final Logger logger = LoggerFactory.getLogger(PermissionController.class);

    private final UserCourseRepository userCourseRepository;
    private final ReviewerRelationRepository reviewerRelationRepository;

    private final UserSession userSession;

    public PermissionController(
            @Autowired UserCourseRepository userCourseRepository,
            @Autowired ReviewerRelationRepository reviewerRelationRepository,
            @Autowired UserSession userSession
    ) {
        this.userCourseRepository = userCourseRepository;
        this.reviewerRelationRepository = reviewerRelationRepository;
        this.userSession = userSession;
    }


    /**
     * Checks if the currently logged in user has permission to update the user with the given ref
     * @param userRef The ref of the user that is to be updated
     */
    @Transactional(readOnly = true)
    public void checkUserUpdatePermission(String userRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        // Teachers and administrators may modify all users, other users may only edit themselves
        if (userSession.getRole() != Role.TEACHER &&
                userSession.getRole() != Role.ADMINISTRATOR &&
                !userSession.getUserRef().equals(userRef)
        ) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() + "') is not allowed to modify user with ref '" + userRef + "'");
        }
    }

    @Transactional(readOnly = true)
    public void checkCourseCreatePermission() {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            logger.info("User is not logged in");
            throw new OrbitGamesAuthorizationException("User must be logged in to access courses");
        }

        // TEACHER and higher roles may update the courses, lower roles (such as students) may not
        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER))) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "' is not allowed to create courses");
        }
    }

    @Transactional(readOnly = true)
    public void checkCourseUpdatePermission(String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            logger.info("User is not logged in");
            throw new OrbitGamesAuthorizationException("User must be logged in to access courses");
        }

        // TEACHER and higher roles may update the courses, lower roles (such as students) may not
        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER))) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "' is not allowed to access course with ref '" + courseRef + "', or the course does not exist");
        }
    }

    @Transactional(readOnly = true)
    public void checkCourseViewPermission(String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            throw new OrbitGamesAuthorizationException("User must be logged in to access courses");
        }

        // TEACHER and higher roles may view all the courses (for now anyway)
        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER) ||
                // If the role is lower, the user must be "enrolled" in the course, which means that a UserCourse must
                // exist linking the logged in user to the course
                userCourseRepository.findByCourseRefAndUserRef(courseRef, userSession.getUserRef()).isPresent())) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "') is not allowed to access course with ref '" + courseRef + "', or the course does not exist");
        }

    }

    @Transactional(readOnly = true)
    public void checkUserCourseUpdatePermission(String userRef, String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            throw new OrbitGamesAuthorizationException("User must be logged in to update user course info");
        }

        // TEACHER and higher roles may edit all user courses (for now anyway)
        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER) ||
                // If the role is lower (e.g. LEARNER) the user must be "enrolled" in the course,
                // and may only edit his/her own properties:
                (userCourseRepository.findByCourseRefAndUserRef(courseRef, userSession.getUserRef()).isPresent() && userRef.equals(userSession.getUserRef())))) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "' is not allowed to modify course user for course with ref '" + courseRef + "' and user '" + userRef + "', or the course does not exist");
        }
    }

    @Transactional(readOnly = true)
    public void checkUserCourseViewPermission(String userRef, String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        // User that may view the course may also view the user-course
        checkCourseViewPermission(courseRef);
    }

    @Transactional(readOnly = true)
    public void checkFullCourseProgressViewPermission(String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            throw new OrbitGamesAuthorizationException("User must be logged in to access course progress");
        }

        // TEACHER and higher roles may view the course progress for all users and courses(for now anyway)
        if(!SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER)) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "') is not allowed to access the course progress of all users for the course with ref '" + courseRef + "'");
        }
    }

    @Transactional(readOnly = true)
    public void checkFullCourseProgressViewPermission(String userRef, String courseRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            throw new OrbitGamesAuthorizationException("User must be logged in to access course progress");
        }

        // TEACHER and higher roles may view the course progress for all users and courses(for now anyway)
        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER) ||
                // If the role is lower, the user must be "enrolled" in the course, which means that a UserCourse must
                // exist linking the logged in user to the course
                (userCourseRepository.findByCourseRefAndUserRef(courseRef, userSession.getUserRef()).isPresent()
                        && userRef.equals(userSession.getUserRef())))) {
            throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() +
                    "') is not allowed to access course with ref '" + courseRef + "', or the course does not exist");
        }

    }
    @Transactional(readOnly = true)
    public void checkCourseProgressUpdatePermission(String userRef, String courseRef, String chapterRef, String reviewerRelationRef) {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            throw new OrbitGamesAuthorizationException("User must be logged in to access course progress");
        }

        // TEACHER and higher roles may view the course progress for all users and courses(for now anyway)
        if (!SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER)) {
            // If the role is lower, the user reviewer relation ref must be to a valid reviewer relation in the
            // chapter and role, and the user must be part of the relation.
            if (!userRef.equals(userSession.getUserRef())) {
                throw new OrbitGamesAuthorizationException("Logged in user '" + userSession.getUsername() + "' ('" + userSession.getUserRef() + "') " +
                        "is not allowed to update a reviewer relation in the name of another user");
            }
        }

        ReviewerRelation reviewerRelation = reviewerRelationRepository.findByRef(reviewerRelationRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("No review step with ref '" + reviewerRelationRef + "' was found", GenericErrorCode.ENTITY_NOT_FOUND));

        if (!reviewerRelation.getSubmitter().getRef().equals(userRef) && !reviewerRelation.getReviewer().getRef().equals(userRef)) {
            throw new OrbitGamesAuthorizationException("The given user '" + userRef + "' is not part of reviewer relation with ref '" + reviewerRelationRef + "'");
        }

        if (!reviewerRelation.getChapter().getRef().equals(chapterRef)) {
            throw new OrbitGamesAuthorizationException("The reviewer relation with ref '" + reviewerRelationRef + "' is not for the chapter with ref '" + chapterRef + "'");
        }

        if (!reviewerRelation.getChapter().getCourse().getRef().equals(courseRef)) {
            throw new OrbitGamesAuthorizationException("The reviewer relation with ref '" + reviewerRelationRef + "' is not for the course with ref '" + courseRef + "'");
        }
    }

    public void checkFileUploadPermission() {

        if (userSession.getSecurityRoles().contains(SecurityRole.TEST_API)) {
            return;
        }

        if (!userSession.isLoggedIn()) {
            logger.info("User is not logged in");
            throw new OrbitGamesAuthorizationException("User must be logged in to access courses");
        }

        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER))) {
            logger.warn("Non-teacher user tried to 'directly' upload a file");
            throw new OrbitGamesAuthorizationException("User must be a teacher to upload resource files");
        }
    }

    public void checkCourseTemplateViewPermission(CourseTemplateMetadata courseTemplateMetadata) {

        if (!userSession.isLoggedIn()) {
            logger.info("User is not logged in");
            throw new OrbitGamesAuthorizationException("User must be logged in to access course templates");
        }

        if (!(SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER))) {
            logger.warn("Non-teacher user tried to get a course template");
            throw new OrbitGamesAuthorizationException("User must be a teacher to view course templates");
        }
    }
}
