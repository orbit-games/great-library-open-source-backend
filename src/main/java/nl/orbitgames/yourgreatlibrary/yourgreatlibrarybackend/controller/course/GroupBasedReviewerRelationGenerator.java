package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GroupBasedReviewerRelationGenerator implements ReviewerRelationGenerator {

    @Override
    public List<ReviewerRelation> generateRelations(Chapter chapter, List<User> users, int reviewerCount) {

        int groupMemberCount = reviewerCount + 1;
        int groupCount = users.size() / groupMemberCount;
        List<List<User>> groups = new ArrayList<>(groupCount);
        for (int i = 0; i < groupCount; i++) {
            groups.add(new ArrayList<>());
        }

        // Put all users in groups:
        int curGroup = 0;
        List<User> unassignedUsers = new LinkedList<>(users);
        Random rnd = new Random();
        while(!unassignedUsers.isEmpty()) {

            int index = rnd.nextInt(unassignedUsers.size());
            User user = unassignedUsers.remove(index);
            groups.get(curGroup).add(user);

            curGroup = (curGroup + 1) % groupCount;
        }

        // Generate the reviewer relations to connect each user to all the members in his/her group:
        List<ReviewerRelation> reviewerRelations = new ArrayList<>();
        for (int groupId = 0; groupId < groups.size(); groupId++) {
            List<User> group = groups.get(groupId);
            for (int i = 0; i < group.size(); i++) {
                User submitter = group.get(i);
                for (int j = i + 1; j < i + 1 + reviewerCount; j++) {
                    User reviewer = group.get(j % group.size());
                    ReviewerRelation newRelation = ReviewerRelationGenerator.createRelation(chapter, submitter, reviewer);
                    newRelation.setGroup("Group " + (groupId + 1));

                    reviewerRelations.add(newRelation);
                }
            }
        }
        return reviewerRelations;
    }
}
