package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation.ConversationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ArgumentTypeRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterSectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.QuizRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationGenerationSettingsRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SkillRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.AcademicYearUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ArgumentTypeId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ChapterId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.CourseTemplateParser;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.QuizId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.QuizQuestionAnswer;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.QuizQuestionId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ResourceId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.ReviewStepId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.SectionId;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.SectionRelationId;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CourseFromTemplateController {

    private static final Logger logger = LoggerFactory.getLogger(CourseFromTemplateController.class);

    private final CourseRepository courseRepository;
    private final ResourceRepository resourceRepository;
    private final ChapterRepository chapterRepository;
    private final ReviewStepRepository reviewStepRepository;
    private final QuizRepository quizRepository;
    private final ArgumentTypeRepository argumentTypeRepository;
    private final ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository;
    private final SkillRepository skillRepository;
    private final SectionRepository sectionRepository;
    private final SectionResourceRepository sectionResourceRepository;
    private final ChapterSectionRepository chapterSectionRepository;

    private final ConversationController conversationController;
    private final CourseTemplateController courseTemplateController;

    private final YourGreatLibraryConfig config;

    public CourseFromTemplateController(
            @Autowired CourseRepository courseRepository,
            @Autowired ResourceRepository resourceRepository,
            @Autowired ChapterRepository chapterRepository,
            @Autowired ReviewStepRepository reviewStepRepository,
            @Autowired QuizRepository quizRepository,
            @Autowired ArgumentTypeRepository argumentTypeRepository,
            @Autowired ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository,
            @Autowired SkillRepository skillRepository,
            @Autowired SectionRepository sectionRepository,
            @Autowired SectionResourceRepository sectionResourceRepository,
            @Autowired ChapterSectionRepository chapterSectionRepository,

            @Autowired ConversationController conversationController,
            @Autowired CourseTemplateController courseTemplateController,

            @Autowired YourGreatLibraryConfig config
    ) {
        this.courseRepository = courseRepository;
        this.resourceRepository = resourceRepository;
        this.chapterRepository = chapterRepository;
        this.reviewStepRepository = reviewStepRepository;
        this.quizRepository = quizRepository;
        this.argumentTypeRepository = argumentTypeRepository;
        this.reviewerRelationGenerationSettingsRepository = reviewerRelationGenerationSettingsRepository;
        this.skillRepository = skillRepository;
        this.sectionRepository = sectionRepository;
        this.sectionResourceRepository = sectionResourceRepository;
        this.chapterSectionRepository = chapterSectionRepository;

        this.conversationController = conversationController;
        this.courseTemplateController = courseTemplateController;

        this.config = config;
    }

    @Transactional
    public Course createCourse(String templateId, int version,
                               String courseName, int year, int quarter,
                               String courseCode,
                               boolean anonymousUsers, boolean anonymousReviews,
                               Map<String, OffsetDateTime> rawDeadlinesOverride) {

        CourseTemplateMetadata courseTemplateMetadata = courseTemplateController.getTemplateMetadata(templateId, version);
        CourseTemplateParser courseTemplateParser = courseTemplateController.getTemplateParser(templateId, version);

        Map<ReviewStepId, OffsetDateTime> deadlinesOverride = rawDeadlinesOverride.entrySet().stream()
                .collect(Collectors.toMap(e -> new ReviewStepId(e.getKey()), Map.Entry::getValue));

        Course course = new Course();
        course.setRef(RefGenerator.generate());
        course.setName(courseName);
        course.setCode(courseCode);
        course.setYear(year);
        course.setQuarter(quarter);
        course.setAnonymousUsers(anonymousUsers);
        course.setAnonymousReviews(anonymousReviews);
        course.setCourseTemplateMetadata(courseTemplateMetadata);

        course = courseRepository.save(course);

        ReviewerRelationGenerationSettings defaultRelationGenerationSettings = config.getDefaultReviewerRelationGenerationSetting();
        ReviewerRelationGenerationSettings relationGeneration = new ReviewerRelationGenerationSettings();
        relationGeneration.setGenerationType(defaultRelationGenerationSettings.getGenerationType());
        relationGeneration.setReviewerCount(defaultRelationGenerationSettings.getReviewerCount());
        relationGeneration.setAutoGenerate(defaultRelationGenerationSettings.isAutoGenerate());
        relationGeneration.setAutoGenerateDelay(defaultRelationGenerationSettings.getAutoGenerateDelay());
        relationGeneration.setIncludeLatePeers(defaultRelationGenerationSettings.isIncludeLatePeers());
        relationGeneration.setAutoMarkComplete(defaultRelationGenerationSettings.isAutoMarkComplete());
        relationGeneration = reviewerRelationGenerationSettingsRepository.save(relationGeneration);

        List<SectionId> sectionIds = courseTemplateParser.getSectionIds();
        Map<SectionId, Section> sections = new HashMap<>();
        for (int i = 0; i < sectionIds.size(); i++) {
            SectionId sectionId = sectionIds.get(i);
            sections.putAll(generateSection(courseTemplateParser, course, sectionId, null, i * 10));
        }
        // The map will be filled when the argument types are used
        Map<ArgumentTypeId, ArgumentType> argumentTypes = new HashMap<>();
        List<Chapter> chapters = generateChapters(courseTemplateParser, course, sections, relationGeneration, argumentTypes, deadlinesOverride);

        conversationController.createDefaultCourseConversations(course);

        return course;

    }

    @Transactional
    public void amendCourse(String templateId, int version, Course course, Map<String, String> rawSectionAmendments) {

        CourseTemplateParser courseTemplateParser = courseTemplateController.getTemplateParser(templateId, version);

        Map<String, SectionId> sectionAmendments = rawSectionAmendments.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> new SectionId(e.getValue())));
        Map<Section, Section> sectionReplacements = new HashMap<>();

        // Currently only sections without resources can be amended
        for (Map.Entry<String, SectionId> sectionAmendment: sectionAmendments.entrySet()) {

            // The tactic is: we delete the old section, and create a new section based on the template
            String oldSectionRef = sectionAmendment.getKey();
            Section oldSection = course.getSections().stream()
                    .filter(s -> s.getRef().equals(oldSectionRef))
                    .findAny()
                    .orElseThrow(() -> new OrbitGamesApplicationException("Can't find section with ref '" + oldSectionRef + "' within course '" + course.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

            deleteSectionRecursive(course, oldSection);

            Map<SectionId, Section> newSections = generateSection(courseTemplateParser, course, sectionAmendment.getValue(), oldSection.getParentSection(), oldSection.getSortOrder());

            sectionReplacements.put(oldSection, newSections.get(sectionAmendment.getValue()));
        }

        // Now we update the chapter-sections to update their references to existing sections
        for (Chapter chapter: course.getChapters()) {
            chapter.getChapterSections().stream()
                    .filter(cs -> sectionReplacements.containsKey(cs.getSection()))
                    .forEach(cs -> cs.setSection(sectionReplacements.get(cs.getSection())));
        }
    }

    /**
     * Delete a section and it's subsections. Will throw an error if the section has resources (since "safely" deleting resources is not yet implemented)
     * @param course The course within which the sections reside
     * @param section The section to delete
     */
    private void deleteSectionRecursive(Course course, Section section) {

        if (section == null) {
            return;
        }

        if (section.getSectionResources() != null && !section.getSectionResources().isEmpty()) {
            throw new OrbitGamesApplicationException("Can't delete section '" + section.getRef() + "' ('" + section.getName() + "') because it has dependent resources", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        // Find and delete the child sections
        course.getSections().stream()
                .filter(s -> s.getParentSection() == section)
                .forEach(s -> deleteSectionRecursive(course, s));

        sectionRepository.delete(section);
        course.removeSection(section);

    }

    private List<Chapter> generateChapters(CourseTemplateParser courseTemplateParser, Course course, Map<SectionId, Section> sections, ReviewerRelationGenerationSettings generationSettings, Map<ArgumentTypeId, ArgumentType> argumentTypes, Map<ReviewStepId, OffsetDateTime> deadlinesOverride) {

        List<Chapter> chapters = new ArrayList<>();

        List<ChapterId> chapterIds = courseTemplateParser.getChapterIds();
        for (int i = 0; i < chapterIds.size(); i++) {
            ChapterId chapterId = chapterIds.get(i);
            chapters.add(generateChapter(courseTemplateParser, course, chapterId, sections, generationSettings, argumentTypes, i * 10, deadlinesOverride));
        }
        course.setChapters(chapters);
        return chapters;
    }

    private Chapter generateChapter(CourseTemplateParser courseTemplateParser, Course course, ChapterId chapterId, Map<SectionId, Section> sections, ReviewerRelationGenerationSettings generationSettings, Map<ArgumentTypeId, ArgumentType> argumentTypes, int sortOrder, Map<ReviewStepId, OffsetDateTime> deadlinesOverride) {

        logger.info("Generating chapter {}", chapterId);

        OffsetDateTime chapterStart = courseTemplateParser.getChapterFirstDeadline(chapterId, deadlinesOverride);

        Chapter chapter = createChapter(course,
                courseTemplateParser.getChapterTitle(chapterId),
                courseTemplateParser.getChapterDescription(chapterId),
                sortOrder, generationSettings);

        List<SectionRelationId> sectionRelationIds = courseTemplateParser.getChapterSectionRelationIds(chapterId);
        for (int i = 0; i < sectionRelationIds.size(); i++) {
            SectionRelationId sectionRelationId = sectionRelationIds.get(i);
            SectionId sectionId = courseTemplateParser.getSectionRelationSection(sectionRelationId);
            String relationDescription = courseTemplateParser.getSectionRelationDescription(sectionRelationId);
            Section relatedSection = sections.get(sectionId);
            chapter.addChapterSection(createChapterSection(chapter, relatedSection, relationDescription, i*10, false));
        }

        List<ReviewStepId> reviewStepIds = courseTemplateParser.getChapterReviewStepIds(chapterId);
        if (!reviewStepIds.isEmpty()) {
            List<ReviewStep> reviewSteps = generateReviewSteps(courseTemplateParser, course, chapter, chapterStart, reviewStepIds, argumentTypes, deadlinesOverride);
        }

        return chapterRepository.save(chapter);
    }

    private ChapterSection createChapterSection(Chapter chapter, Section section, String description, int sortOrder, boolean optional) {

        ChapterSection chapterSection = new ChapterSection();
        chapterSection.setChapter(chapter);
        chapterSection.setSection(section);
        chapterSection.setDescription(description);
        chapterSection.setSortOrder(sortOrder);
        chapterSection.setOptional(optional);

        return chapterSectionRepository.save(chapterSection);
    }

    private Chapter createChapter(Course course, String name, String description, int sortOrder, ReviewerRelationGenerationSettings generationSettings) {
        Chapter chapter = new Chapter();
        chapter.setRef(RefGenerator.generate());
        chapter.setCourse(course);
        chapter.setSortOrder(sortOrder);
        chapter.setName(name);
        chapter.setDescription(description);
        chapter.setReviewerRelationGenerationSettings(generationSettings);

        return chapterRepository.save(chapter);
    }

    /**
     * @param course The course to which the sections should be added
     * @param sectionId The section ID in the template
     * @param parent (optional) parent section
     * @param sortOrder Order of this section within the parent (or within the course if there is no parent)
     * @return A map containing for each sectionId in the template, what section was created for it
     */
    private Map<SectionId, Section> generateSection(CourseTemplateParser courseTemplateParser, Course course, SectionId sectionId, Section parent, int sortOrder) {

        logger.info("Generating section {}", sectionId);
        Map<SectionId, Section> sections = new HashMap<>();
        final Section section;
        if (parent == null) {
            section = createRootSection(course, courseTemplateParser.getSectionTitle(sectionId), courseTemplateParser.getSectionText(sectionId), sortOrder);
        } else {
            section = createSection(course,
                    courseTemplateParser.getSectionTitle(sectionId),
                    courseTemplateParser.getSectionText(sectionId),
                    parent,
                    courseTemplateParser.getSectionParentRelationDescription(sectionId), sortOrder);
        }
        sections.put(sectionId, section);

        List<SectionId> children = courseTemplateParser.getSectionChildrenIds(sectionId);
        for (int i = 0; i < children.size(); i++) {
            sections.putAll(generateSection(courseTemplateParser, course, children.get(i), section, i * 10));
        }

        List<ResourceId> resources = courseTemplateParser.getSectionResources(sectionId);
        for (int i = 0; i < resources .size(); i++) {
            generateResource(courseTemplateParser, course, section, resources.get(i), i * 10);
        }

        return sections;
    }

    private Resource generateResource(CourseTemplateParser courseTemplateParser, Course course, Section section, ResourceId resourceId, int sortOrder) {

        logger.info("Generating resource {}", resourceId);
        String skillName = courseTemplateParser.getResourceSkill(resourceId);
        Skill skill = StringUtils.isBlank(skillName) ? null : getOrCreateSkill(course, skillName.trim());
        String quizTitle = courseTemplateParser.getResourceQuizTitle(resourceId);
        Quiz quiz;
        int quizMinimalScore = 1;
        if (StringUtils.isBlank(quizTitle)) {
            quiz = null;
        } else if (courseTemplateParser.hasResourceFullQuiz(resourceId)) {
            // Custom question(s)
            quiz = generateQuiz(courseTemplateParser, courseTemplateParser.getResourceQuizId(resourceId));
            quizMinimalScore = courseTemplateParser.getResourceQuizMinimalScore(resourceId);
        } else {
            // General "completion quiz"
            quiz = createCheckCompletionQuiz(
                    quizTitle,
                    courseTemplateParser.getResourceCompletionQuizDescription(resourceId),
                    courseTemplateParser.getResourceCompletionQuizQuestionText(resourceId)
            );
        }
        Resource resource = createResource(course, 0,

                courseTemplateParser.getResourceTitle(resourceId),
                courseTemplateParser.getResourceDescription(resourceId),
                courseTemplateParser.getResourceContentUrl(resourceId),
                quiz,
                quizMinimalScore,
                skill == null ? Collections.emptySet() : Collections.singleton(skill));

        SectionResource sectionResource = createSectionResource(section, resource,
                courseTemplateParser.isResourceOptional(resourceId),
                courseTemplateParser.getResourceRelationDescription(resourceId),
                sortOrder);
        section.addSectionResource(sectionResource);

        return resource;
    }

    private Quiz generateQuiz(CourseTemplateParser courseTemplateParser, QuizId quizId) {

        logger.info("Generating quiz {}", quizId);

        Quiz quiz = createQuiz(
                courseTemplateParser.getQuizTitle(quizId),
                courseTemplateParser.getQuizDescription(quizId));

        List<QuizQuestionId> questionIds = courseTemplateParser.getQuizQuestionIds(quizId);
        for (int i = 0; i < questionIds.size(); i++) {
            QuizQuestionId questionId = questionIds.get(i);

            QuizElement quizElement = createQuizElement(quiz,
                    courseTemplateParser.getQuizQuestionText(questionId),
                    i * 10,
                    courseTemplateParser.getQuizQuestionType(questionId)
                    );

            List<QuizQuestionAnswer> answers = courseTemplateParser.getQuizQuestionAnswers(questionId);
            for (int j = 0; j < answers.size(); j++) {
                QuizQuestionAnswer answer = answers.get(j);
                createQuizElementOption(quizElement, answer.getText(), j * 10, answer.getPoints());
            }
        }
        return quizRepository.save(quiz);
    }

    private Skill getOrCreateSkill(Course course, String skillName) {
        return course.getSkills().stream()
                .filter(s -> s.getName().equals(skillName))
                .findAny()
                .orElseGet(() -> createSkill(course, skillName));
    }

    private Section createRootSection(Course course, String name, String description, int sortOrder) {
        Section section = new Section();
        section.setRef(RefGenerator.generate());
        section.setCourse(course);
        section.setName(name);
        section.setDescription(description);
        section.setSortOrder(sortOrder);

        section = sectionRepository.save(section);
        course.addSection(section);
        return section;
    }

    private Section createSection(Course course, String name, String description, Section parent, String parentRelationDescription, int sortOrder) {
        Section section = new Section();
        section.setRef(RefGenerator.generate());
        section.setCourse(course);
        section.setName(name);
        section.setDescription(description);

        if (parent != null) {
            section.setParentSection(parent);
            section.setParentRelationDescription(parentRelationDescription);
            section.setSortOrder(sortOrder);
        }

        course.addSection(section);

        return sectionRepository.save(section);
    }

    private Resource createResource(Course course, int sortOrder, String name, String description, String url, Quiz quiz, int quizMinimalScore, Collection<Skill> skills) {
        Resource resource = new Resource();
        resource.setRef(RefGenerator.generate());
        resource.setCourse(course);
        resource.setSortOrder(sortOrder);
        resource.setName(name);
        resource.setDescription(description);
        resource.setUrl(url);
        resource.setQuiz(quiz);
        resource.setQuizMinimalScore(quizMinimalScore);

        resource.setSkills(new HashSet<>(skills));

        resource = resourceRepository.save(resource);;
        course.addResource(resource);
        return resource;
    }

    private SectionResource createSectionResource(Section section, Resource resource, boolean optional, String description, int sortOrder) {

        SectionResource sectionResource = new SectionResource();
        sectionResource.setSection(section);
        sectionResource.setResource(resource);
        sectionResource.setOptional(optional);
        sectionResource.setDescription(description);
        sectionResource.setSortOrder(sortOrder);

        sectionResource = sectionResourceRepository.save(sectionResource);
        section.addSectionResource(sectionResource);
        return sectionResource;
    }

    private Quiz createCheckCompletionQuiz(String quizName, String quizDescription, String completionQuestionText) {

        Quiz quiz = createQuiz(quizName, quizDescription);
        QuizElement quizElement = createQuizElement(quiz, completionQuestionText, 0, QuestionType.SELECT_ONE);
        createQuizElementOption(quizElement, "Yes", 0, 1);
        createQuizElementOption(quizElement, "No", 1, 0);

        quiz.recalculateMaxScore();
        return quizRepository.save(quiz);
    }

    private Quiz createQuiz(String quizName, String quizDescription) {
        Quiz quiz = new Quiz();
        quiz.setRef(RefGenerator.generate());
        quiz.setName(quizName);
        quiz.setDescription(quizDescription);
        return quizRepository.save(quiz);
    }

    private QuizElement createQuizElement(Quiz quiz, String text, int sortOrder, QuestionType questionType) {

        QuizElement quizElement = new QuizElement();
        quizElement.setRef(RefGenerator.generate());
        quizElement.setQuiz(quiz);
        quizElement.setText(text);
        quizElement.setSortOrder(sortOrder);
        quizElement.setQuestionType(questionType);

        quiz.addElement(quizElement);

        return quizElement;
    }

    private QuizElementOption createQuizElementOption(QuizElement quizElement, String text, int sortOrder, int points) {
        QuizElementOption quizOption = new QuizElementOption();
        quizOption.setRef(RefGenerator.generate());
        quizOption.setQuizElement(quizElement);
        quizOption.setText(text);
        quizOption.setSortOrder(sortOrder);
        quizOption.setPoints(points);

        quizElement.addOption(quizOption);
        return quizOption;
    }

    private ArgumentType getOrCreateArgumentType(CourseTemplateParser courseTemplateParser, Course course, ArgumentTypeId argumentTypeId, Map<ArgumentTypeId, ArgumentType> argumentTypes) {

        if (argumentTypes.containsKey(argumentTypeId)) {
            return argumentTypes.get(argumentTypeId);
        }

        logger.info("Generating argument type {}", argumentTypeId);

        ArgumentType argumentType = new ArgumentType();
        argumentType.setRef(RefGenerator.generate());
        argumentType.setCourse(course);
        argumentType.setName(courseTemplateParser.getArgumentTypeName(argumentTypeId));
        argumentType.setDescription(courseTemplateParser.getArgumentTypeDescription(argumentTypeId));
        argumentType.setRequired(courseTemplateParser.isArgumentTypeRequired(argumentTypeId));

        ArgumentTypeId parentArgumentId = courseTemplateParser.getArgumentTypeParentId(argumentTypeId);
        if (parentArgumentId != null) {
            argumentType.setParent(getOrCreateArgumentType(courseTemplateParser, course, parentArgumentId, argumentTypes));
        }
        argumentType = argumentTypeRepository.save(argumentType);
        argumentTypes.put(argumentTypeId, argumentType);

        return argumentType;
    }

    private List<ReviewStep> generateReviewSteps(CourseTemplateParser courseTemplateParser, Course course, Chapter chapter, OffsetDateTime chapterStart, List<ReviewStepId> reviewStepIds, Map<ArgumentTypeId, ArgumentType> argumentTypes, Map<ReviewStepId, OffsetDateTime> deadlinesOverride) {

        List<ReviewStep> reviewSteps = new ArrayList<>();

        for (int i = 0; i < reviewStepIds.size(); i++) {

            ReviewStepId reviewStepId = reviewStepIds.get(i);

            ReviewStep reviewStep = new ReviewStep();
            reviewStep.setRef(RefGenerator.generate());
            reviewStep.setChapter(chapter);
            reviewStep.setSortOrder(i * 10);
            reviewStep.setDisabled(false);
            reviewStep.setDeadlineNotificationEmailEnabled(true);
            reviewStep.setDeadlineNotificationEmailOffsetSeconds(24 * 60 * 60);
            reviewStep.setStepType(courseTemplateParser.getReviewStepType(reviewStepId));
            reviewStep.setName(courseTemplateParser.getReviewStepName(reviewStepId));
            reviewStep.setDescription(courseTemplateParser.getReviewStepDescription(reviewStepId));

            if (courseTemplateParser.hasReviewStepFileUpload(reviewStepId)) {
                reviewStep.setFileUploadName(courseTemplateParser.getReviewStepFileUploadName(reviewStepId));
                reviewStep.setFileUpload(true);
            }

            if (deadlinesOverride.containsKey(reviewStepId)) {
                reviewStep.setDeadline(deadlinesOverride.get(reviewStepId));
            } else {
                int deadlineDaysSinceStart = courseTemplateParser.getReviewStepDeadlineOffset(reviewStepId);
                reviewStep.setDeadline(chapterStart.plus(deadlineDaysSinceStart, ChronoUnit.DAYS));
            }

            DiscussionType discussionType = courseTemplateParser.getReviewStepDiscussionType(reviewStepId);
            reviewStep.setDiscussionType(discussionType);
            if (discussionType == DiscussionType.RESTRICTED_ARGUMENTS) {

                List<ArgumentTypeId> argumentTypeIds = courseTemplateParser.getReviewStepArgumentTypeIds(reviewStepId);
                Set<ArgumentType> stepArgumentTypes = new HashSet<>();
                for (ArgumentTypeId argumentTypeId: argumentTypeIds) {
                    stepArgumentTypes.add(getOrCreateArgumentType(courseTemplateParser, course, argumentTypeId, argumentTypes));
                }
                reviewStep.setArgumentTypes(stepArgumentTypes);
            }

            if (courseTemplateParser.hasReviewStepQuiz(reviewStepId)) {
                reviewStep.setQuiz(generateQuiz(courseTemplateParser, courseTemplateParser.getReviewStepQuizId(reviewStepId)));
            }

            reviewStep.setSkills(courseTemplateParser.getReviewStepSkills(reviewStepId)
                    .stream()
                    .map(s -> getOrCreateSkill(course, s))
                    .collect(Collectors.toSet()));

            reviewSteps.add(reviewStepRepository.save(reviewStep));
        }

        chapter.setReviewSteps(reviewSteps);

        return reviewSteps;
    }

    private Skill createSkill(Course course, String name) {

        Skill skill = new Skill();

        skill.setRef(RefGenerator.generate());
        skill.setName(name);
        skill.setCourse(course);

        skill = skillRepository.save(skill);
        course.addSkill(skill);
        return skill;
    }

}
