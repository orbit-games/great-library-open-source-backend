package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationGenerationType;

public class ReviewerRelationGeneratorFactory {

    public static ReviewerRelationGenerator construct(ReviewerRelationGenerationType type) {

        switch(type) {
            case RANDOM:
                return new RandomReviewerRelationGenerator();
            case GROUP_BASED:
                return new GroupBasedReviewerRelationGenerator();
            default:
                throw new OrbitGamesSystemException("Unknown generator type: " + type);
        }
    }

}
