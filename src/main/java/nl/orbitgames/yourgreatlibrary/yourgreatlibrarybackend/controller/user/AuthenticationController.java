package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.YourGreatLibraryLoginException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static java.time.temporal.ChronoUnit.SECONDS;


@Component
public class AuthenticationController {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    private static final String USERNAME_CLAIM = "username";
    private static final String ROLE_CLAIM = "role";

    private final String jwtSecret;
    private final String jwtIssuer;
    private final long jwtExpirySeconds;
    private final String testApiToken;

    private final UserRepository userRepository;
    private final UserSession userSession;

    private final YourGreatLibraryConfig config;

    public AuthenticationController(
            @Value("${" + YourGreatLibraryBackendApplicationProperties.JWT_SECRET + "}") String jwtSecret,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.JWT_ISSUER + "}") String jwtIssuer,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.JWT_EXPIRY + "}") long jwtExpirySeconds,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.TEST_API_TOKEN + "}") String testApiToken,
            @Autowired UserRepository userRepository,
            @Autowired UserSession userSession,
            @Autowired YourGreatLibraryConfig config
    ) {
        this.jwtSecret = jwtSecret;
        this.jwtIssuer = jwtIssuer;
        this.jwtExpirySeconds = jwtExpirySeconds;
        this.testApiToken = testApiToken;
        this.userRepository = userRepository;
        this.userSession = userSession;
        this.config = config;

    }

    /**
     * Generate a JWT authentication token for the given user
     * @param user Creates an authentication token for the given user
     * @return The authentication token
     */
    public String createAuthenticationToken(User user) {

        UsernameRoleClaim claim = new UsernameRoleClaim(user.getUsername(), user.getRole());
        return createJWTToken(claim);
    }

    /**
     * Logs in using the given token and returns the user that was logged in
     * @param token A JWT token that should be used for login
     * @return The user that was logged in using the token
     * @throws  YourGreatLibraryLoginException In case the token is invalid
     */
    @Transactional
    public TokenLoginResult tokenLogin(String token) {

        if (tryTestApiTokenLogin(token)) {
            return new TokenLoginResult(null, userSession.getSecurityRoles());
        }

        UsernameRoleClaim claim = decryptJwtToken(token);

        // Verify that the claim is (still) valid in the database; we don't want users that have been demoted
        // or deleted to still use their claimed role.

        final Optional<User> user = userRepository.findByUsername(claim.getUsername());
        if (!user.isPresent()) {
            logger.info("User '" + claim.getUsername() + "' tried to log in using JWT, but does not exist");
            throw new YourGreatLibraryLoginException();
        }
        if (!user.get().getRole().equals(claim.getRole())) {
            logger.info("User '" + claim.getUsername() + "' claimed to have role '" + claim.getRole() + "', but doesn't");
            throw new YourGreatLibraryLoginException();
        }

        // None of the validations above have failed, so the JWT claims
        // are valid. Therefore the user is now logged in.
        updateLastActive(user.get());

        Set<String> securityRoles = new HashSet<>(Arrays.asList(SecurityRole.getSecurityRole(user.get().getRole()), SecurityRole.LOGGED_IN));

        userSession.setUsername(user.get().getUsername());
        userSession.setUserRef(user.get().getRef());
        userSession.setLocale(user.get().getPreferredLocale() == null ? Locale.getDefault() : user.get().getPreferredLocale());
        userSession.setRole(user.get().getRole());
        userSession.setSecurityRoles(securityRoles);

        return new TokenLoginResult(user.get(), securityRoles);
    }

    private boolean tryTestApiTokenLogin(String token) {

        if (config.isTestApplicationEnvironment() && StringUtils.isNotBlank(testApiToken) && token.equals(testApiToken)) {

            logger.info("Logging in as test user using the test API token");
            userSession.setSecurityRoles(Collections.singleton(SecurityRole.TEST_API));

            return true;
        }
        return false;
    }

    private void updateLastActive(User user) {
        // Set the last active datetime to the current datetime
        user.setLastActive(OffsetDateTime.now());
        userRepository.save(user);
    }
    private UsernameRoleClaim decryptJwtToken(String jwtToken)  {

        Algorithm jwtAlgorithm = getJWTAlgorithm();
        JWTVerifier verifier = JWT.require(jwtAlgorithm)
                .withIssuer(jwtIssuer)
                .build();

        final DecodedJWT decodedJWT;
        try {
            decodedJWT = verifier.verify(jwtToken);
        } catch (JWTVerificationException e) {
            logger.info("JWT Token login failed: " + e.getMessage(), e);
            throw new YourGreatLibraryLoginException();
        }

        String usernameClaim = decodedJWT.getClaim(USERNAME_CLAIM).asString();
        final Role roleClaim;
        try {
            roleClaim = Role.valueOf(decodedJWT.getClaim(ROLE_CLAIM).asString());
        } catch (IllegalArgumentException e) {
            // This essentially just means the token is invalid, so we throw an invalidCredentialsException
            logger.warn("Found an unknown role in a passed JWT token: '" + decodedJWT.getClaim(ROLE_CLAIM));
            throw new YourGreatLibraryLoginException();
        }


        return new UsernameRoleClaim(usernameClaim, roleClaim);

    }

    private String createJWTToken(UsernameRoleClaim usernameRoleClaim) {

        Validate.notBlank(usernameRoleClaim.getUsername());
        Validate.notNull(usernameRoleClaim.getRole());

        Algorithm jwtAlgorithm = getJWTAlgorithm();
        return JWT.create()
                .withIssuer(jwtIssuer)
                .withClaim(USERNAME_CLAIM, usernameRoleClaim.getUsername())
                .withClaim(ROLE_CLAIM, usernameRoleClaim.getRole().toString())
                .withExpiresAt(new Date(OffsetDateTime.now().plus(jwtExpirySeconds, SECONDS).toInstant().toEpochMilli()))
                .sign(jwtAlgorithm);
    }

    private Algorithm getJWTAlgorithm() {
        try {
            return Algorithm.HMAC256(jwtSecret);
        } catch (IllegalArgumentException e) {
            // Should never happen (only when the given JWT secret is null)
            throw new RuntimeException("Invalid JWT Secret. Configuration error?", e);
        }
    }

    public static class TokenLoginResult {

        private final User user;
        private final Set<String> securityRoles;

        public TokenLoginResult(User user, Set<String> securityRoles) {
            this.user = user;
            this.securityRoles = Collections.unmodifiableSet(securityRoles);
        }

        public User getUser() {
            return user;
        }

        public Set<String> getSecurityRoles() {
            return securityRoles;
        }
    }

    /**
     * Generates a JWT secret
     */
    public static void main(String... args) {

        SecureRandom rnd = new SecureRandom();
        byte[] secretBytes = new byte[256];
        rnd.nextBytes(secretBytes);

        System.out.println(Base64.getEncoder().encodeToString(secretBytes));
    }

    private static class UsernameRoleClaim {

        private final String username;
        private final Role role;

        UsernameRoleClaim(String username, Role role) {
            this.username = username;
            this.role = role;
        }

        String getUsername() {
            return username;
        }

        Role getRole() {
            return role;
        }

    }
}
