package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.CourseProgressController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ArgumentTypeRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ChapterSectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.CourseRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.FileUploadMetadataRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.QuizRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.QuizResultRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewStepRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ReviewerRelationGenerationSettingsRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SectionResourceRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.SkillRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ChapterDeadlines;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseDeadlines;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseUpdateRequest;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepDeadline;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Caches;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.UriUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class CourseController {

    private static final Logger logger = LoggerFactory.getLogger(CourseController.class);

    private final CourseRepository courseRepository;
    private final ChapterRepository chapterRepository;
    private final ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository;
    private final ChapterSectionRepository chapterSectionRepository;
    private final ReviewStepRepository reviewStepRepository;
    private final ArgumentTypeRepository argumentTypeRepository;
    private final SectionRepository sectionRepository;
    private final ResourceRepository resourceRepository;
    private final SectionResourceRepository sectionResourceRepository;
    private final FileUploadMetadataRepository fileUploadMetadataRepository;
    private final QuizRepository quizRepository;
    private final QuizResultRepository quizResultRepository;
    private final SkillRepository skillRepository;

    private final PermissionController permissionController;
    private final CourseProgressController courseProgressController;
    private final CourseFromTemplateController courseFromTemplateController;
    private final CourseTemplateController courseTemplateController;
    private final CourseJobsController courseJobsController;

    private final UserSession userSession;
    private final YourGreatLibraryConfig config;

    public CourseController(
            @Autowired CourseRepository courseRepository,
            @Autowired ChapterRepository chapterRepository,
            @Autowired ReviewerRelationGenerationSettingsRepository reviewerRelationGenerationSettingsRepository,
            @Autowired ChapterSectionRepository chapterSectionRepository,
            @Autowired ReviewStepRepository reviewStepRepository,
            @Autowired ArgumentTypeRepository argumentTypeRepository,
            @Autowired SectionRepository sectionRepository,
            @Autowired ResourceRepository resourceRepository,
            @Autowired FileUploadMetadataRepository fileUploadMetadataRepository,
            @Autowired QuizRepository quizRepository,
            @Autowired QuizResultRepository quizResultRepository,
            @Autowired SkillRepository skillRepository,

            @Autowired PermissionController permissionController,
            @Autowired CourseProgressController courseProgressController,
            @Autowired CourseFromTemplateController courseFromTemplateController,
            @Autowired CourseTemplateController courseTemplateController,
            @Autowired CourseJobsController courseJobsController,

            @Autowired UserSession userSession,
            @Autowired YourGreatLibraryConfig config,
            @Autowired SectionResourceRepository sectionResourceRepository
    ) {
        this.courseRepository = courseRepository;
        this.chapterRepository = chapterRepository;
        this.reviewerRelationGenerationSettingsRepository = reviewerRelationGenerationSettingsRepository;
        this.chapterSectionRepository = chapterSectionRepository;
        this.reviewStepRepository = reviewStepRepository;
        this.argumentTypeRepository = argumentTypeRepository;
        this.sectionRepository = sectionRepository;
        this.resourceRepository = resourceRepository;
        this.sectionResourceRepository = sectionResourceRepository;
        this.fileUploadMetadataRepository = fileUploadMetadataRepository;
        this.quizRepository = quizRepository;
        this.quizResultRepository = quizResultRepository;
        this.skillRepository = skillRepository;

        this.permissionController = permissionController;
        this.courseProgressController = courseProgressController;
        this.courseFromTemplateController = courseFromTemplateController;
        this.courseTemplateController = courseTemplateController;
        this.courseJobsController = courseJobsController;


        this.userSession = userSession;
        this.config = config;
    }

    @Transactional(readOnly = true)
    @Cacheable(Caches.COURSE_CACHE)
    public Course getCourse(String courseRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Course course = getCourseInternal(courseRef);

        // Now we need to initialize all related collections because the domain to rest converter can't do that
        fetchRelated(course);

        // Enrichment is kind of a hack which will fill in some default values, so that these can be controlled here instead of in the rest layer
        enrich(course);

        return course;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Course updateCourse(String courseRef, CourseUpdateRequest request) {

        logger.info("User '{}' ('{}') updating course '{}'", userSession.getUsername(), userSession.getUserRef(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourseInternal(courseRef);
        course.setName(request.getName());
        course.setYear(request.getYear());
        course.setQuarter(request.getQuarter());
        course.setCode(request.getCode());
        course.setAnonymousUsers(request.isAnonymousUsers());
        course.setAnonymousReviews(request.isAnonymousReviews());

        Course updatedCourse = courseRepository.save(course);
        fetchRelated(updatedCourse);
        enrich(updatedCourse);

        return updatedCourse;
    }

    @Transactional(readOnly = true)
    public Chapter getChapter(String courseRef, String chapterRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Chapter chapter = getChapterInternal(courseRef, chapterRef);

        fetchRelated(chapter);
        enrich(chapter);

        return chapter;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Chapter updateChapter(String courseRef, String chapterRef, Chapter chapterWithUpdates) {

        logger.info("User '{}' ('{}') updating chapter '{}' ('{}') in course '{}'", userSession.getUsername(), userSession.getUserRef(), chapterWithUpdates.getName(), chapterRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Chapter existingChapter = getChapterInternal(courseRef, chapterRef);

        mergeChapters(chapterWithUpdates, existingChapter);

        Chapter updatedChapter = chapterRepository.save(existingChapter);
        fetchRelated(updatedChapter);
        enrich(updatedChapter);

        return updatedChapter;
    }

    private Course getCourseInternal(String courseRef) {
        return this.courseRepository.findByRefAndFetchRelated(courseRef)
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find course with ref '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    @Transactional
    public Course createCourseFromTemplate(String templateId, int version,
                                           String courseName, int year, int quarter,
                                           String courseCode,
                                           boolean anonymousUsers, boolean anonymousReviews,
                                           Map<String, OffsetDateTime> deadlinesOverride) {

        logger.info("User '" + userSession.getUsername() + "' ('" + userSession.getUserRef() + "') creating course '" + courseName + "' ('" + courseCode + "') from template");

        permissionController.checkCourseCreatePermission();

        Course createdCourse = courseFromTemplateController.createCourse(
                templateId, version, courseName, year, quarter, courseCode, anonymousUsers, anonymousReviews, deadlinesOverride);

        Course fullCourse = courseProgressController.joinCourse(userSession.getUserRef(), createdCourse);
        courseJobsController.scheduleJobsForCourse(fullCourse);

        return fullCourse;
    }

    @Transactional
    public void rescheduleJobs() {

        for (Course course: courseRepository.findAll()) {
            courseJobsController.scheduleJobsForCourse(course);
        }
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Course amendCourseFromTemplate(String templateId, int version,
                                          String courseRef, Map<String, String> sectionAmendments) {

        logger.info("User '" + userSession.getUsername() + "' ('" + userSession.getUserRef() + "') amending course '" + courseRef + "' from template");

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourse(courseRef);
        courseFromTemplateController.amendCourse(templateId, version, course, sectionAmendments);

        return course;
    }

    @Transactional(readOnly = true)
    public ReviewStep getReviewStep(String courseRef, String chapterRef, String reviewStepRef) {

        permissionController.checkCourseViewPermission(courseRef);
        ReviewStep reviewStep = getReviewStepInternal(courseRef, chapterRef, reviewStepRef);
        fetchRelated(reviewStep);
        return reviewStep;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public ReviewStep updateReviewStep(String courseRef, String chapterRef, String reviewStepRef, ReviewStep reviewStepWithUpdates) {

        logger.info("User '{}' ('{}') updating review step '{}' ('{}') to course '{}'", userSession.getUsername(), userSession.getUserRef(), reviewStepRef, reviewStepWithUpdates.getName(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        ReviewStep reviewStepToUpdate = getReviewStepInternal(courseRef, chapterRef, reviewStepRef);
        Collection<Skill> courseSkills = skillRepository.findByCourse_Ref(courseRef);

        boolean deadlinesChanged = !reviewStepWithUpdates.getDeadline().isEqual(reviewStepToUpdate.getDeadline()) ||
                // If the disabled state of a review step changed, this effectively means that a deadline is removed/added,
                // so the scheduler must also be updated
                reviewStepWithUpdates.isDisabled() != reviewStepToUpdate.isDisabled();

        // We first validate the new deadline
        if (deadlinesChanged && !isDeadlineValid(reviewStepToUpdate, reviewStepWithUpdates.getDeadline())) {
            throw new OrbitGamesApplicationException("The deadline for review step '" + reviewStepWithUpdates.getName() + "' in chapter '" + reviewStepToUpdate.getChapter().getName() + "' is not valid. It must be after previous deadlines", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        mergeReviewSteps(reviewStepWithUpdates, reviewStepToUpdate, courseSkills);

        ReviewStep updatedReviewStep = reviewStepRepository.save(reviewStepToUpdate);

        if (deadlinesChanged) {
            Course course = getCourseInternal(courseRef);
            courseJobsController.scheduleReviewerRelationGenerationJob(course, updatedReviewStep.getChapter());
            courseJobsController.scheduleSendDeadlineNotificationEmailJob(updatedReviewStep);
        }

        fetchRelated(updatedReviewStep);
        return updatedReviewStep;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public CourseDeadlines updateDeadlines(String courseRef, CourseDeadlines updatedDeadlines) {

        logger.info("User '{}' ('{}') updating deadlines for course '{}'", userSession.getUsername(), userSession.getUserRef(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourseInternal(courseRef);
        // Clear double chapters
        course.setChapters(course.getChapters().stream().distinct().collect(Collectors.toList()));

        Set<ReviewStep> updatedReviewSteps = new HashSet<>();

        for (ChapterDeadlines chapterDeadlines: updatedDeadlines.getChapters()) {
            Chapter chapter = CollectionUtils.findBy(course.getChapters(), c -> c.getRef().equals(chapterDeadlines.getChapterRef()))
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + chapterDeadlines.getChapterRef() + "' in course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

            for (ReviewStepDeadline reviewStepDeadline: chapterDeadlines.getReviewSteps()) {

                ReviewStep reviewStep = CollectionUtils.findBy(chapter.getReviewSteps(), rs -> rs.getRef().equals(reviewStepDeadline.getReviewStepRef()))
                        .orElseThrow(() -> new OrbitGamesApplicationException("Could not find review step with ref '" + reviewStepDeadline.getReviewStepRef() + "' in chapter '" + chapter.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

                reviewStep.setDeadline(reviewStepDeadline.getDeadline());
                updatedReviewSteps.add(reviewStep);
            }
        }

        for (ReviewStep reviewStep: updatedReviewSteps) {

            if (!isDeadlineValid(reviewStep, reviewStep.getDeadline())) {
                throw new OrbitGamesApplicationException("The deadline for review step '" + reviewStep.getName() + "' in chapter '" + reviewStep.getChapter().getName() + "' is not valid. It must be after previous review steps, and before upcoming ones", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            reviewStepRepository.save(reviewStep);
        }

        courseJobsController.scheduleJobsForCourse(course);

        return new CourseDeadlines(
                CollectionUtils.mapToList(course.getChapters(), c -> new ChapterDeadlines(
                        c.getRef(),
                        CollectionUtils.mapToList(c.getReviewSteps(), rs -> new ReviewStepDeadline(
                                rs.getRef(),
                                rs.getDeadline()
                        )
                )
        )));
    }

    private boolean isDeadlineValid(ReviewStep reviewStep, OffsetDateTime deadline) {

        // For disabled review steps, any deadline is valid
        if (reviewStep.isDisabled()) {
            return true;
        }

        List<ReviewStep> chapterReviewSteps = reviewStep.getChapter().getReviewSteps();
        int stepIndex = chapterReviewSteps.indexOf(reviewStep);
        return isAfterPreviousDeadline(chapterReviewSteps, stepIndex, reviewStep, deadline) &&
                isBeforeNextDeadline(chapterReviewSteps, stepIndex, deadline);
    }

    private boolean isAfterPreviousDeadline(List<ReviewStep> chapterReviewSteps, int stepIndex, ReviewStep reviewStep, OffsetDateTime deadline) {

        if (stepIndex == 0) {

            // If this isn't the first chapter, the deadline must (at least) be after the first deadline of the previous chapter
            List<Chapter> courseChapters = reviewStep.getChapter().getCourse().getChapters();
            int chapterIndex = courseChapters.indexOf(reviewStep.getChapter());
            return chapterIndex == 0 || courseChapters.get(chapterIndex - 1).getReviewSteps().get(0).getDeadline().isBefore(deadline);
        } else {

            // Otherwise, it must be after the previous deadline in this chapter
            return chapterReviewSteps.get(stepIndex - 1).getDeadline().isBefore(deadline);
        }
    }

    private boolean isBeforeNextDeadline(List<ReviewStep> chapterReviewSteps, int stepIndex, OffsetDateTime deadline) {

        return chapterReviewSteps.size() <= stepIndex + 1
                // Note that if the next step is disabled, then all following steps in this chapter must also be disabled
                // So we don't need to check those
                || chapterReviewSteps.get(stepIndex + 1).isDisabled()
                || chapterReviewSteps.get(stepIndex + 1).getDeadline().isAfter(deadline);
    }

    @Transactional(readOnly = true)
    public List<Section> getSections(String courseRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Course course = getCourseInternal(courseRef);
        course.getSections().forEach(this::fetchRelated);

        return new ArrayList<>(course.getSections());
    }

    @Transactional(readOnly = true)
    public Section getSection(String courseRef, String sectionRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Section section = getSectionInternal(courseRef, sectionRef);
        fetchRelated(section);
        return section;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Section createSection(String courseRef, Section sectionToAdd) {

        logger.info("User '{}' ('{}') adding section '{}' to course '{}'", userSession.getUsername(), userSession.getUserRef(), sectionToAdd.getName(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourseInternal(courseRef);

        Section newSection = new Section();
        newSection.setRef(RefGenerator.generate());
        newSection.setCourse(course);
        newSection.setName(sectionToAdd.getName());

        newSection = sectionRepository.save(newSection);

        mergeSections(sectionToAdd, newSection);

        return sectionRepository.save(newSection);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Section updateSection(String courseRef, String sectionRef, Section sectionWithUpdates) {

        logger.info("User '{}' ('{}') updating section '{}' to course '{}'",
                userSession.getUsername(), userSession.getUserRef(), sectionRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Section existingSection = getSectionInternal(courseRef, sectionRef);
        fetchRelated(existingSection);
        mergeSections(sectionWithUpdates, existingSection);

        Section updatedSection = sectionRepository.save(existingSection);
        fetchRelated(updatedSection);
        return updatedSection;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Section deleteSection(String courseRef, String sectionRef) {

        logger.info("User '{}' ('{}') deleting section '{}' to course '{}'",
                userSession.getUsername(), userSession.getUserRef(), sectionRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Section sectionToDelete = getSectionInternal(courseRef, sectionRef);
        fetchRelated(sectionToDelete);

        // Recursively find child sections and delete them. We use a stack of child sections to made sure that the
        // furthers children from the root are deleted first (i.e. LIFO)
        Stack<Section> sectionsToDelete = new Stack<>();
        sectionsToDelete.add(sectionToDelete);

        Queue<Section> parentsToCheck = new LinkedList<>();
        parentsToCheck.add(sectionToDelete);

        while(!parentsToCheck.isEmpty()) {
            Section parentToCheck = parentsToCheck.remove();
            Set<Section> childSections = sectionRepository.findByParentSection(parentToCheck);

            childSections.stream().filter(s -> !sectionsToDelete.contains(s))
                    .forEach(parentsToCheck::add);

            childSections.forEach(sectionsToDelete::push);
        }

        Set<ChapterSection> chapterSections = chapterSectionRepository.findBySectionIn(sectionsToDelete);
        chapterSections.forEach(chapterSectionRepository::delete);

        while(!sectionsToDelete.empty()) {
            sectionRepository.delete(sectionsToDelete.pop());
        }

        return sectionToDelete;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public SectionResource createSectionResource(String courseRef, String sectionRef, SectionResource sectionResourceToAdd) {

        logger.info("User '{}' ('{}') adding section resource '{}' to course '{}', section '{}'",
                userSession.getUsername(), userSession.getUserRef(), sectionResourceToAdd.getResource().getRef(), courseRef, sectionRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        if (sectionResourceToAdd.getResource() == null || StringUtils.isBlank(sectionResourceToAdd.getResource().getRef())) {
            throw new OrbitGamesApplicationException("The referenced resource may not be null", GenericErrorCode.CONSTRAINT_VIOLATION);
        }

        Section section = getSectionInternal(courseRef, sectionRef);

        SectionResource newSectionResource = new SectionResource();
        newSectionResource.setSection(section);

        merge(sectionResourceToAdd, newSectionResource, courseRef);

        SectionResource addedSectionResource = sectionResourceRepository.save(newSectionResource);
        section.addSectionResource(addedSectionResource);

        return addedSectionResource;
    }

    @Transactional(readOnly = true)
    public Collection<Resource> getResources(String courseRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Course course = getCourseInternal(courseRef);
        Collection<Resource> resources = course.getResources();
        resources.forEach(this::fetchRelated);

        return resources;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Resource createResource(String courseRef, Resource resourceToAdd) {

        logger.info("User '{}' ('{}') adding resource '{}' to course '{}'", userSession.getUsername(), userSession.getUserRef(), resourceToAdd.getName(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourseInternal(courseRef);

        Resource newResource = new Resource();
        newResource.setRef(RefGenerator.generate());
        newResource.setCourse(course);

        mergeAndValidateResources(resourceToAdd, newResource, course.getSkills());

        return resourceRepository.save(newResource);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Resource updateResource(String courseRef, String resourceRef, Resource resourceWithUpdates) {

        logger.info("User '{}' ('{}') updating resource '{}' of course '{}'", userSession.getUsername(), userSession.getUserRef(), resourceRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Set<Skill> courseSkills = skillRepository.findByCourse_Ref(courseRef);
        Resource existingResource = getResourceInternal(courseRef, resourceRef);

        mergeAndValidateResources(resourceWithUpdates, existingResource, courseSkills);
        Resource updatedResource = resourceRepository.save(existingResource);
        fetchRelated(updatedResource);

        return updatedResource;
    }

    @Transactional(readOnly = true)
    public Resource getResource(String courseRef, String resourceRef) {

        permissionController.checkCourseViewPermission(courseRef);

        Resource resource = getResourceInternal(courseRef, resourceRef);
        fetchRelated(resource);

        return resource;
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Resource deleteResource(String courseRef, String resourceRef) {

        logger.info("User '{}' ('{}') deleting resource '{}' of course '{}'", userSession.getUsername(), userSession.getUserRef(), resourceRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Resource resource = getResourceInternal(courseRef, resourceRef);
        fetchRelated(resource);

        if (resource.getQuiz() != null) {
            if (hasQuizResults(resource.getQuiz())) {
                // If a resource is deleted that has quiz results, the results will have to be deleted as well
                Set<QuizResult> quizResults = quizResultRepository.findByQuiz(resource.getQuiz());
                quizResults.forEach(quizResultRepository::delete);
            }
            quizRepository.delete(resource.getQuiz());
        }

        resourceRepository.delete(resource);

        return resource;
    }

    @Transactional(readOnly = true)
    public Collection<Skill> getSkills(String courseRef) {

        permissionController.checkCourseViewPermission(courseRef);

        return skillRepository.findByCourse_Ref(courseRef);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Skill createSkill(String courseRef, Skill skillToAdd) {

        logger.info("User '{}' ('{}') adding skill '{}' to course '{}'", userSession.getUsername(), userSession.getUserRef(), skillToAdd.getName(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Course course = getCourseInternal(courseRef);

        Skill newSkill = new Skill();
        newSkill.setRef(RefGenerator.generate());
        newSkill.setCourse(course);

        mergeSkills(skillToAdd, newSkill);

        return skillRepository.save(newSkill);
    }

    @Transactional(readOnly = true)
    public Skill getSkill(String courseRef, String skillRef) {

        permissionController.checkCourseViewPermission(courseRef);
        return getSkillInternal(courseRef, skillRef);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Skill updateSkill(String courseRef, String skillRef, Skill skillWithUpdates) {

        logger.info("User '{}' ('{}') updating skill '{}' in course '{}'", userSession.getUsername(), userSession.getUserRef(), skillWithUpdates.getName(), courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Skill skillToUpdate = getSkillInternal(courseRef, skillRef);
        mergeSkills(skillWithUpdates, skillToUpdate);

        return skillRepository.save(skillToUpdate);
    }

    @Transactional
    @CacheEvict(value = Caches.COURSE_CACHE, key = "#courseRef")
    public Skill deleteSkill(String courseRef, String skillRef) {

        logger.info("User '{}' ('{}') deleting skill '{}' in course '{}'", userSession.getUsername(), userSession.getUserRef(), skillRef, courseRef);

        permissionController.checkCourseUpdatePermission(courseRef);

        Skill skill = getSkillInternal(courseRef, skillRef);
        skillRepository.delete(skill);

        return skill;
    }

    private void mergeChapters(Chapter from, Chapter to) {

        to.setName(from.getName());
        to.setSortOrder(from.getSortOrder());
        to.setDescription(from.getDescription());

        // Merge section relations/chapter sections
        Function<ChapterSection, String> chapterSectionKeyRetriever = s -> s.getSection().getRef();
        Collection<ChapterSection> addedChapterSections = CollectionUtils.findAdded(from.getChapterSections(), to.getChapterSections(), chapterSectionKeyRetriever);
        Collection<Pair<ChapterSection, ChapterSection>> existingChapterSections = CollectionUtils.findExisting(from.getChapterSections(), to.getChapterSections(), chapterSectionKeyRetriever);
        Collection<ChapterSection> removedChapterSections = CollectionUtils.findRemoved(from.getChapterSections(), to.getChapterSections(), chapterSectionKeyRetriever);

        for (ChapterSection addedChapterSection: addedChapterSections) {
            ChapterSection newChapterSection = new ChapterSection();
            newChapterSection.setChapter(to);
            mergeChapterSections(addedChapterSection, newChapterSection);

            ChapterSection savedNewChapterSection = chapterSectionRepository.save(newChapterSection);
            to.addChapterSection(savedNewChapterSection);
        }

        for (Pair<ChapterSection, ChapterSection> existingChapterSection: existingChapterSections) {
            mergeChapterSections(existingChapterSection.getLeft(), existingChapterSection.getRight());
            chapterSectionRepository.save(existingChapterSection.getRight());
        }

        for (ChapterSection removedChapterSection: removedChapterSections) {
            to.removeChapterSection(removedChapterSection);
            chapterSectionRepository.delete(removedChapterSection);
        }

        // Note: we completely ignore review steps when merging chapters; review steps should be modified using their own update method

        // Merge reviewer relation generation settings
        if (from.getReviewerRelationGenerationSettings() != null) {
            ReviewerRelationGenerationSettings defaultSettings = config.getDefaultReviewerRelationGenerationSetting();
            ReviewerRelationGenerationSettings newSettings = from.getReviewerRelationGenerationSettings();
            if (isSameSettings(newSettings, defaultSettings)) {
                to.setReviewerRelationGenerationSettings(null);
            } else {
                ReviewerRelationGenerationSettings toReviewerRelationGenerationSettings = ObjectUtils.firstNonNull(to.getReviewerRelationGenerationSettings(), new ReviewerRelationGenerationSettings());
                mergeReviewerRelationGenerationSettings(from.getReviewerRelationGenerationSettings(), toReviewerRelationGenerationSettings);
                reviewerRelationGenerationSettingsRepository.save(toReviewerRelationGenerationSettings);
            }
        } else {
            to.setReviewerRelationGenerationSettings(null);
        }
    }

    private void mergeChapterSections(ChapterSection from, ChapterSection to) {

        if (to.getSection() == null || !from.getSection().getRef().equals(to.getSection().getRef())) {
            String courseRef = to.getChapter().getCourse().getRef();
            Section managedSection = sectionRepository.findByRef(from.getSection().getRef())
                    .filter(s -> s.getCourse().getRef().equals(courseRef))
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find section with ref '" + from.getSection().getRef() + "' in course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

            to.setSection(managedSection);
        }

        to.setOptional(from.isOptional());
        to.setDescription(from.getDescription());
        to.setSortOrder(from.getSortOrder());
    }

    private void mergeReviewerRelationGenerationSettings(ReviewerRelationGenerationSettings from, ReviewerRelationGenerationSettings to) {

        to.setGenerationType(from.getGenerationType());
        to.setReviewerCount(from.getReviewerCount());
        to.setAutoGenerate(from.isAutoGenerate());
        to.setAutoGenerateDelay(from.getAutoGenerateDelay());
        to.setIncludeLatePeers(from.isIncludeLatePeers());
        to.setAutoMarkComplete(from.isAutoMarkComplete());
    }

    private boolean isSameSettings(ReviewerRelationGenerationSettings s1, ReviewerRelationGenerationSettings s2) {

        ReviewerRelationGenerationSettings defaultSettings = config.getDefaultReviewerRelationGenerationSetting();

        return isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::getGenerationType) &&
                isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::getReviewerCount) &&
                isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::isAutoGenerate) &&
                isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::getAutoGenerateDelay) &&
                isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::isIncludeLatePeers) &&
                isEqualOrDefaultProperty(s1, s2, defaultSettings, ReviewerRelationGenerationSettings::isAutoMarkComplete);


    }

    private <T, V> boolean isEqualOrDefaultProperty(T o1, T o2, T defaultObject, Function<T, V> valueGetter) {
        return isEqualOrDefault(valueGetter.apply(o1), valueGetter.apply(o2), valueGetter.apply(defaultObject));
    }

    private <T> boolean isEqualOrDefault(T v1, T v2, T defaultValue) {
        if (v1 == null && v2 == null) {
            return true;
        }
        if (v1 == null) {
            return v2.equals(defaultValue);
        }
        if (v2 == null) {
            return v1.equals(defaultValue);
        }
        return v1.equals(v2);
    }

    Optional<ReviewStep> getNextReviewStepInChapter(ReviewStep reviewStep) {
        List<ReviewStep> chapterSteps = reviewStep.getChapter().getReviewSteps();
        int reviewStepIndex = chapterSteps.indexOf(reviewStep);
        if (reviewStepIndex >= chapterSteps.size() - 1) {
            // This is the last step
            return Optional.empty();
        } else {
            return Optional.of(chapterSteps.get(reviewStepIndex + 1));
        }
    }

    Optional<ReviewStep> getPreviousReviewStepInChapter(ReviewStep reviewStep) {
        List<ReviewStep> chapterSteps = reviewStep.getChapter().getReviewSteps();
        int reviewStepIndex = chapterSteps.indexOf(reviewStep);
        if (reviewStepIndex <= 0) {
            // This is the first step
            return Optional.empty();
        } else {
            return Optional.of(chapterSteps.get(reviewStepIndex - 1));
        }
    }

    private void mergeReviewSteps(ReviewStep from, ReviewStep to, Collection<Skill> courseSkills) {

        to.setStepType(from.getStepType());
        to.setSortOrder(from.getSortOrder());
        to.setName(from.getName());
        to.setDescription(from.getDescription());
        to.setDeadline(from.getDeadline());
        to.setDiscussionType(from.getDiscussionType());

        if (to.isDisabled() != from.isDisabled()) {
            // Review steps can only be disabled if there are no steps further in the chapter that are enabled:
            if (from.isDisabled()) {
                Optional<ReviewStep> nextStep = getNextReviewStepInChapter(to);
                if (nextStep.isPresent() && !nextStep.get().isDisabled()) {
                    throw new OrbitGamesApplicationException("Cannot disable review step '" + from.getName() + "'. Review steps can only be disabled if there are no steps further in the chapter that are enabled, but the next step ('" + nextStep.get().getName() + "') is enabled", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
            }
            // Review steps can only be enabled if all previous steps in the chapter are enabled as well:
            if (!from.isDisabled()) {
                Optional<ReviewStep> previousStep = getPreviousReviewStepInChapter(to);
                if (previousStep.isPresent() && previousStep.get().isDisabled()) {
                    throw new OrbitGamesApplicationException("Cannot enable review step '" + from.getName() + "'. Review steps can only be enabled if all previous steps in the chapter are enabled as well, but the previous step ('" + previousStep.get().getName() + "') is disabled", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
            }
            to.setDisabled(from.isDisabled());
        }

        if (from.getDiscussionType().equals(DiscussionType.RESTRICTED_ARGUMENTS)) {

            Collection<ArgumentType> addedArgumentTypes = CollectionUtils.findAdded(from.getArgumentTypes(), to.getArgumentTypes(), ArgumentType::getRef);
            Collection<Pair<ArgumentType, ArgumentType>> existingArgumentTypes = CollectionUtils.findExisting(from.getArgumentTypes(), to.getArgumentTypes(), ArgumentType::getRef);
            Collection<ArgumentType> removedArgumentTypes = CollectionUtils.findRemoved(from.getArgumentTypes(), to.getArgumentTypes(), ArgumentType::getRef);

            Set<String> parentRefs = from.getArgumentTypes().stream()
                    .map(ArgumentType::getParent)
                    .filter(Objects::nonNull)
                    .map(ArgumentType::getRef)
                    .collect(Collectors.toSet());
            Collection<ArgumentType> allExistingParentArguments = argumentTypeRepository.findByRefIn(parentRefs);

            // Maps argument types (in the "from" part) to their "to" (persisted) arguments
            Map<ArgumentType, ArgumentType> fromToArgumentType = new HashMap<>();

            // Match all parent argument refs in the "from" part to persisted arguments (if they exist)
            from.getArgumentTypes().stream()
                    .map(ArgumentType::getParent)
                    .filter(Objects::nonNull)
                    .forEach(fa -> {
                        CollectionUtils.findBy(allExistingParentArguments, ta -> ta.getRef().equals(fa.getRef()))
                                .ifPresent(ta -> fromToArgumentType.put(fa, ta));
                    });

            Set<ArgumentType> argumentTypesToAdd = new HashSet<>(addedArgumentTypes);

            while (!argumentTypesToAdd.isEmpty()) {

                // Find an argument type for which the parent has been added (or is null)
                ArgumentType argumentTypeToAdd = argumentTypesToAdd.stream()
                        .filter(a -> a.getParent() == null || fromToArgumentType.containsKey(a.getParent()))
                        .findFirst()
                        .orElseThrow(() -> new OrbitGamesSystemException("Can't find valid argument type to add, but not all missing have been added. Maybe there are missing parents or circular parent references?"));

                ArgumentType newArgumentType = new ArgumentType();
                newArgumentType.setRef(RefGenerator.generate());
                newArgumentType.setCourse(to.getChapter().getCourse());

                mergeArgumentTypes(argumentTypeToAdd, newArgumentType, fromToArgumentType);

                ArgumentType addedArgumentType = argumentTypeRepository.save(newArgumentType);
                fromToArgumentType.put(argumentTypeToAdd, addedArgumentType);

                to.addArgumentType(addedArgumentType);

                // Done adding so we don't need to add it anymore
                argumentTypesToAdd.remove(argumentTypeToAdd);
            }

            // By now all arguments that should be added have been added
            for (Pair<ArgumentType, ArgumentType> argumentTypesWithUpdates : existingArgumentTypes) {

                ArgumentType argumentTypeWithUpdates = argumentTypesWithUpdates.getLeft();
                ArgumentType argumentTypeToUpdate = argumentTypesWithUpdates.getRight();
                mergeArgumentTypes(argumentTypeWithUpdates, argumentTypeToUpdate, fromToArgumentType);

                ArgumentType updatedArgumentType = argumentTypeRepository.save(argumentTypeToUpdate);
            }

            for (ArgumentType argumentType : removedArgumentTypes) {
                to.removeArgumentType(argumentType);
            }
        } else {
            to.getArgumentTypes().forEach(argumentTypeRepository::delete);
            to.setArgumentTypes(Collections.emptySet());
        }

        if (!isSameQuiz(from.getQuiz(), to.getQuiz())) {

            if (to.getQuiz() != null) {
                if (hasQuizResults(to.getQuiz())) {
                    throw new OrbitGamesApplicationException("Can't update a quiz that already has results", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
                // Since there's no results for the quiz yet, we just delete it and create a new one
                quizRepository.delete(to.getQuiz());
                to.setQuiz(null);
            }

            if (from.getQuiz() != null) {
                // Create a new quiz
                to.setQuiz(createQuiz(from.getQuiz()));
            }
        }

        to.setFileUpload(from.isFileUpload());
        to.setFileUploadName(from.getFileUploadName());

        Set<Skill> managedSkills = getManagedSkills(from.getSkills(), courseSkills);
        to.setSkills(managedSkills);

    }

    private Set<Skill> getManagedSkills(Collection<Skill> skillDummies, Collection<Skill> courseSkills) {
        return skillDummies.stream().map(s ->
                CollectionUtils.findBy(courseSkills, cs -> cs.getRef().equals(s.getRef()))
                        .orElseThrow(() -> new OrbitGamesApplicationException("Can't find skill with ref '" + s.getRef() + "'", GenericErrorCode.CONSTRAINT_VIOLATION)))
                .collect(Collectors.toSet());
    }

    private void mergeArgumentTypes(ArgumentType from, ArgumentType to, Map<ArgumentType, ArgumentType> fromToArgumentTypeRef) {

        to.setName(from.getName());
        to.setDescription(from.getDescription());
        to.setRequired(from.isRequired());
        if (from.getParent() != null) {
            if (to.getParent() == null || !to.getParent().getRef().equals(from.getParent().getRef())) {

                ArgumentType parent = fromToArgumentTypeRef.get(from.getParent());
                if (parent == null) {
                    throw new OrbitGamesApplicationException("Could not find parent argument with ref '" + from.getParent().getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND);
                }
                to.setParent(parent);
            }
        } else {
            to.setParent(null);
        }
    }

    private void mergeSections(Section from, Section to) {

        to.setName(from.getName());
        to.setDescription(from.getDescription());
        if (from.getParentSection() != null && StringUtils.isNotBlank(from.getParentSection().getRef())) {
            Section parent = getSectionInternal(to.getCourse().getRef(), from.getParentSection().getRef());
            to.setParentSection(parent);
        } else {
            to.setParentSection(null);
        }

        to.setSortOrder(from.getSortOrder());
        to.setParentRelationDescription(from.getParentRelationDescription());

        Collection<SectionResource> addedSectionResources = CollectionUtils.findAdded(from.getSectionResources(), to.getSectionResources(),
                s -> s.getResource() == null ? null : s.getResource().getRef());
        Collection<Pair<SectionResource, SectionResource>> existingSectionResources = CollectionUtils.findExisting(from.getSectionResources(), to.getSectionResources(),
                s -> s.getResource() == null ? null : s.getResource().getRef());
        Collection<SectionResource> removedSectionResources = CollectionUtils.findRemoved(from.getSectionResources(), to.getSectionResources(),
                s -> s.getResource() == null ? null : s.getResource().getRef());

        for (SectionResource sectionResourceToAdd: addedSectionResources) {
            SectionResource newSectionResource = new SectionResource();
            newSectionResource.setSection(to);

            if (sectionResourceToAdd.getResource() == null || StringUtils.isBlank(sectionResourceToAdd.getResource().getRef())) {
                throw new OrbitGamesApplicationException("The referenced resource may not be null", GenericErrorCode.CONSTRAINT_VIOLATION);
            }
            String courseRef = to.getCourse().getRef();
            merge(sectionResourceToAdd, newSectionResource, courseRef);

            to.addSectionResource(sectionResourceRepository.save(newSectionResource));
        }

        for (Pair<SectionResource, SectionResource> sectionResourceUpdate: existingSectionResources) {
            merge(sectionResourceUpdate.getLeft(), sectionResourceUpdate.getRight(), to.getCourse().getRef());
        }

        for (SectionResource sectionResourceToRemove: removedSectionResources) {
            sectionResourceRepository.delete(sectionResourceToRemove);
            to.removeSectionResource(sectionResourceToRemove);
        }
    }

    private void mergeAndValidateResources(Resource from, Resource to, Set<Skill> courseSkills) {


        to.setSortOrder(from.getSortOrder());
        to.setName(from.getName());
        to.setDescription(from.getDescription());

        if (StringUtils.isNotBlank(from.getUrl())) {

            if (StringUtils.isNotBlank(from.getUrl()) && !UriUtils.isValidURI(from.getUrl())) {
                throw new OrbitGamesApplicationException("The resource URL '" + from.getUrl() + "' is not a valid", GenericErrorCode.CONSTRAINT_VIOLATION);
            }
            to.setUrl(from.getUrl());
        } else {
            to.setUrl(null);
        }

        if (from.getFileUploadMetadata() != null && StringUtils.isNotBlank(from.getFileUploadMetadata().getRef())) {
            to.setFileUploadMetadata(getFileUploadMetadataInternal(from.getFileUploadMetadata().getRef()));
        } else {
            to.setFileUploadMetadata(null);
        }

        if (!isSameQuiz(from.getQuiz(), to.getQuiz())) {

            if (to.getQuiz() != null) {
                if (hasQuizResults(to.getQuiz())) {
                    throw new OrbitGamesApplicationException("Can't update a resource quiz that already has results", GenericErrorCode.CONSTRAINT_VIOLATION);
                }
                // Since there's no results for the quiz yet, we just delete it and create a new one
                quizRepository.delete(to.getQuiz());
                to.setQuiz(null);
            }

            if (from.getQuiz() != null) {
                // Create a new quiz
                to.setQuiz(createQuiz(from.getQuiz()));
            }

        }
        if (to.getQuiz() != null) {

            // Just to be sure: recalculate the max score
            to.getQuiz().recalculateMaxScore();

            // Use Math.min to ensure that the minimal score is not higher than the max score
            to.setQuizMinimalScore(Math.min(from.getQuizMinimalScore(), to.getQuiz().getMaxScore()));
        } else {
            to.setQuizMinimalScore(0);
        }

        Set<Skill> managedSkills = getManagedSkills(from.getSkills(), courseSkills);
        to.setSkills(managedSkills);
    }

    private void mergeSkills(Skill from, Skill to) {

        to.setName(from.getName());
        to.setDescription(from.getDescription());
        to.setIcon(from.getIcon());

    }

    private boolean isSameQuiz(Quiz quiz1, Quiz quiz2) {

        if (quiz1 == null || quiz2 == null) {
            return false;
        }

        boolean basePropertiesEqual = new EqualsBuilder()
                .append(quiz1.getName(), quiz2.getName())
                .append(quiz1.getDescription(), quiz2.getDescription())
                .append(quiz1.getMaxScore(), quiz2.getMaxScore())
                .append(quiz1.getElements().size(), quiz2.getElements().size())
                .isEquals();

        if (!basePropertiesEqual) {
            return false;
        }

        Comparator<QuizElement> quizElementComparator = Comparator.comparing(QuizElement::getSortOrder).thenComparing(QuizElement::getText);

        // We sort the options by SortOrder; the exact value for SortOrder doesn't matter as long as they end up in the same order
        List<QuizElement> quiz1Elements = quiz1.getElements().stream().sorted(quizElementComparator).collect(Collectors.toList());
        List<QuizElement> quiz2Elements = quiz2.getElements().stream().sorted(quizElementComparator).collect(Collectors.toList());

        for (int i = 0; i < quiz1.getElements().size(); i++) {

            QuizElement quiz1Element = quiz1Elements.get(i);
            QuizElement quiz2Element = quiz2Elements.get(i);

            boolean elementEqual = new EqualsBuilder()
                    .append(quiz1Element.getText(), quiz2Element.getText())
                    .append(quiz1Element.getQuestionType(), quiz2Element.getQuestionType())
                    .append(quiz1Element.getOptions() == null ? 0 : quiz1Element.getOptions().size(), quiz2Element.getOptions() == null ? 0 : quiz2Element.getOptions().size())
                    .isEquals();

            if (!elementEqual) {
                return false;
            }

            Comparator<QuizElementOption> quizElementOptionComparator = Comparator.comparing(QuizElementOption::getSortOrder).thenComparing(QuizElementOption::getText);

            List<QuizElementOption> quiz1ElementOptions = quiz1Element.getOptions().stream().sorted(quizElementOptionComparator).collect(Collectors.toList());
            List<QuizElementOption> quiz2ElementOptions = quiz2Element.getOptions().stream().sorted(quizElementOptionComparator).collect(Collectors.toList());

            for (int j = 0; j < quiz1Element.getOptions().size(); j++) {
                QuizElementOption quiz1ElementOption = quiz1ElementOptions.get(j);
                QuizElementOption quiz2ElementOption = quiz2ElementOptions.get(j);

                boolean optionsEqual = new EqualsBuilder()
                        .append(quiz1ElementOption.getText(), quiz2ElementOption.getText())
                        .append(quiz1ElementOption.getPoints(), quiz2ElementOption.getPoints())
                        .isEquals();

                if (!optionsEqual) {
                    return false;
                }
            }
        }

        // No differences found
        return true;

    }

    private Quiz createQuiz(Quiz quizToAdd) {

        Quiz newQuiz = new Quiz();
        newQuiz.setRef(RefGenerator.generate());
        newQuiz.setName(quizToAdd.getName());
        newQuiz.setDescription(quizToAdd.getDescription());
        newQuiz.setHiddenPoints(quizToAdd.isHiddenPoints());

        for (QuizElement elementToAdd: quizToAdd.getElements()) {

            QuizElement newElement = new QuizElement();
            newElement.setRef(RefGenerator.generate());
            newElement.setQuiz(newQuiz);

            newElement.setSortOrder(elementToAdd.getSortOrder());
            newElement.setText(elementToAdd.getText());
            newElement.setQuestionType(elementToAdd.getQuestionType());

            if (elementToAdd.getOptions() != null) {
                for (QuizElementOption optionToAdd : elementToAdd.getOptions()) {

                    QuizElementOption newOption = new QuizElementOption();
                    newOption.setRef(RefGenerator.generate());
                    newOption.setQuizElement(newElement);
                    newOption.setSortOrder(optionToAdd.getSortOrder());
                    newOption.setText(optionToAdd.getText());
                    newOption.setPoints(optionToAdd.getPoints());

                    newElement.addOption(newOption);
                }
            }
            newQuiz.addElement(newElement);
        }

        return quizRepository.save(newQuiz);
    }

    private FileUploadMetadata getFileUploadMetadataInternal(String fileUploadMetadataRef) {

        return fileUploadMetadataRepository.findByRef(fileUploadMetadataRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find file upload metadata with ref '" + fileUploadMetadataRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

    }

    private void merge(SectionResource from, SectionResource to, String courseRef) {
        to.setResource(getResourceInternal(courseRef, from.getResource().getRef()));
        to.setDescription(from.getDescription());
        to.setSortOrder(from.getSortOrder());
    }

    private void enrich(Course course) {

        course.getChapters().forEach(this::enrich);
    }

    private void enrich(Chapter chapter) {
        if (chapter.getReviewerRelationGenerationSettings() == null) {
            chapter.setReviewerRelationGenerationSettings(config.getDefaultReviewerRelationGenerationSetting());
        }
    }

    private void fetchRelated(Course course) {

        // TODO Optimize this: loading a course takes quite long because of all the queries. This could be optimized (maybe with caching or smarter querying)
        course.setChapters(course.getChapters().stream().distinct().collect(Collectors.toList()));
        course.getChapters().forEach(this::fetchRelated);
        course.getSections().forEach(this::fetchRelated);
        course.getResources().forEach(this::fetchRelated);
        course.getSkills().size();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(Section section) {

        if (section.getParentSection() != null) {
            section.getParentSection().getRef();
        }
        section.getSectionResources().forEach(this::fetchRelated);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(SectionResource sectionResource) {

        sectionResource.getResource().getRef();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(Resource resource) {

        resource.getSkills().size();
        fetchQuizRelated(resource.getQuiz());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(Chapter chapter) {

        logger.debug("Loading chapter '" + chapter.getRef() + "' ('" + chapter.getName() + "')");
        chapter.getChapterSections().size();
        chapter.getReviewSteps().forEach(this::fetchRelated);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchRelated(ReviewStep reviewStep) {

        logger.debug("Loading review step '" + reviewStep.getRef() + "' ('" + reviewStep.getName() + "')");
        fetchQuizRelated(reviewStep.getQuiz());
        reviewStep.getArgumentTypes().forEach(a -> a.getParent() );
        reviewStep.getSkills().size();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void fetchQuizRelated(Quiz quiz) {
        if (quiz != null) {
            quiz.setElements(quiz.getElements().stream().distinct().collect(Collectors.toList()));
            quiz.getElements().forEach(qe -> { if (qe.getOptions() != null) qe.getOptions().size(); });
        }
    }

    private Chapter getChapterInternal(String courseRef, String chapterRef) {
        return chapterRepository.findByRef(chapterRef)
                .filter(c -> c.getCourse().getRef().equalsIgnoreCase(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + chapterRef + "' within course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private ReviewStep getReviewStepInternal(String courseRef, String chapterRef, String reviewStepRef) {

        return reviewStepRepository.findByRef(reviewStepRef)
                .filter(c -> c.getChapter().getRef().equals(chapterRef))
                .filter(c -> c.getChapter().getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find review step with ref '" + reviewStepRef + "' within chapter '" + chapterRef + "' of course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Section getSectionInternal(String courseRef, String sectionRef) {

        return sectionRepository.findByRef(sectionRef)
                .filter(c -> c.getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find section with ref '" + sectionRef + "' within course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Resource getResourceInternal(String courseRef, String resourceRef) {

        return resourceRepository.findByRef(resourceRef)
                .filter(r -> r.getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find resource with ref '" + resourceRef + "' within course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private Skill getSkillInternal(String courseRef, String skillRef) {

        return skillRepository.findByRef(skillRef)
                .filter(c -> c.getCourse().getRef().equals(courseRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find skill with ref '" + skillRef + "' within course '" + courseRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private boolean hasQuizResults(Quiz quiz) {
        return quizResultRepository.existsByQuiz(quiz);
    }


}
