package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.PermissionController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ForgotPasswordToken;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.ForgotPasswordTokenRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserAgreementRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.YourGreatLibraryLoginException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.EmailService;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.AuthenticationKeyFormatter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.HashUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.PasswordHashUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RandomUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.RESET_PASSWORD_TOKEN_ALPHABET;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.RESET_PASSWORD_TOKEN_FORMAT;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.RESET_PASSWORD_TOKEN_LENGTH;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.RESET_PASSWORD_TOKEN_VALID_SECONDS;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.RESET_PASSWORD_URL;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.USER_ALLOWED_EMAIL_DOMAINS;

@Component
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final int forgotPasswordTokenLength;
    private final String forgotPasswordTokenAlphabet;
    private final long forgotPasswordTokenValidSeconds;
    private final String forgotPasswordTokenFormat;
    private final String resetPasswordUrl;
    private final Set<String> allowedEmailDomains;

    private final UserRepository userRepository;
    private final UserAgreementRepository userAgreementRepository;
    private final ForgotPasswordTokenRepository forgotPasswordTokenRepository;

    private final AuthenticationController authenticationController;
    private final PermissionController permissionController;

    private final EmailService emailService;
    private final MessageSource messageSource;

    private final UserSession userSession;

    public UserController(
            @Autowired UserRepository userRepository,
            @Autowired UserAgreementRepository userAgreementRepository,
            @Autowired ForgotPasswordTokenRepository forgotPasswordTokenRepository,

            @Autowired AuthenticationController authenticationController,
            @Autowired PermissionController permissionController,
            @Autowired EmailService emailService,
            @Autowired UserSession userSession,
            @Autowired MessageSource messageSource,

            @Value("${" + RESET_PASSWORD_TOKEN_LENGTH + "}") int forgotPasswordTokenLength,
            @Value("${" + RESET_PASSWORD_TOKEN_ALPHABET + "}") String forgotPasswordTokenAlphabet,
            @Value("${" + RESET_PASSWORD_TOKEN_VALID_SECONDS + "}") long forgotPasswordTokenValidSeconds,
            @Value("${" + RESET_PASSWORD_TOKEN_FORMAT + "}") String forgotPasswordTokenFormat,
            @Value("${" + RESET_PASSWORD_URL + "}") String resetPasswordUrl,
            @Value("${" + USER_ALLOWED_EMAIL_DOMAINS + "}") String allowedEmailDomains
    ) {
        this.userRepository = userRepository;
        this.userAgreementRepository = userAgreementRepository;
        this.forgotPasswordTokenRepository = forgotPasswordTokenRepository;

        this.authenticationController = authenticationController;
        this.permissionController = permissionController;

        this.emailService = emailService;
        this.userSession = userSession;
        this.messageSource = messageSource;

        this.forgotPasswordTokenLength = forgotPasswordTokenLength;
        this.forgotPasswordTokenAlphabet = forgotPasswordTokenAlphabet;
        this.forgotPasswordTokenValidSeconds = forgotPasswordTokenValidSeconds;
        this.forgotPasswordTokenFormat = forgotPasswordTokenFormat;
        this.resetPasswordUrl = resetPasswordUrl;
        this.allowedEmailDomains = Arrays.stream(allowedEmailDomains.split(","))
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public LoginResult login(String username, String password) {

        logger.info("Login attempt for user '{}'", username);

        Optional<User> user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            logger.info("User  '" + username + "' could not be found");
            PasswordHashUtils.dummyPasswordCheck();
            throw new YourGreatLibraryLoginException();
        }

        boolean passwordCorrect = PasswordHashUtils.checkPassword(password, user.get().getPasswordHash());
        if (!passwordCorrect) {
            logger.info("Incorrect password for user  '" + username + "'");
            throw new YourGreatLibraryLoginException();
        }

        String token = authenticationController.createAuthenticationToken(user.get());
        updateLastActive(user.get());

        logger.info("Successful login for user '" + username + "' with role '" + user.get().getRole() + "'");
        return new LoginResult(token,
                userRepository.findByRefFetchRelated(user.get().getRef())
                        .map(u -> {
                            u.setUserCourses(u.getUserCourses().stream().distinct().collect(Collectors.toList()));
                            return u;
                        })
                        .orElseThrow(() -> new OrbitGamesApplicationException("User with ref '" + user.get().getRef() + "' could not be found", GenericErrorCode.ENTITY_NOT_FOUND)));
    }

    private void updateLastActive(User user) {
        user.setLastActive(OffsetDateTime.now());
        userRepository.save(user);
    }

    @Transactional
    public User registerUser(String username, String email, String fullName, String signedUserAgreementRef, String password, Role role) {

        logger.info("User is being registered: '" + username + "' with role '" + role + "'");

        // We check that only admins can create admins or teachers, and only teachers can create teachers:
        if (!(role == Role.LEARNER ||
                (role == Role.TEACHER && (userSession.getRole() == Role.TEACHER || userSession.getRole() == Role.ADMINISTRATOR)) ||
                (role == Role.ADMINISTRATOR && userSession.getRole() == Role.ADMINISTRATOR))
        ) {
            throw new OrbitGamesAuthorizationException();
        }

        Optional<User> existingUser = userRepository.findByUsername(username);
        if (existingUser.isPresent()) {
            throw new OrbitGamesApplicationException(GenericErrorCode.ENTITY_ALREADY_EXISTS);
        }

        validateEmail(email);

        UserAgreement userAgreement = userAgreementRepository.findByRef(signedUserAgreementRef)
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user agreement with ref '" + signedUserAgreementRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setFullName(fullName);
        user.setSignedUserAgreement(userAgreement);
        user.setUserAgreementSignDate(OffsetDateTime.now());
        user.setPasswordHash(PasswordHashUtils.hashPassword(password));
        user.setRole(role);
        user.setRef(RefGenerator.generate());

        userRepository.save(user);

        return user;

    }

    @Transactional
    public void forgotPassword(String username) {

        logger.info("Generating forgot password token for user '{}'", username);
        Optional<User> user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            logger.info("User {} could not be found, returning", username);
            // If the given username could not be found, we pretend to have sent the e-mail
            return;
        }

        // Mark existing tokens as "used" since the new token will replace any existing tokens for this user
        Optional<ForgotPasswordToken> existingToken = forgotPasswordTokenRepository.findUnusedValidForUser(user.get(), OffsetDateTime.now());
        if (existingToken.isPresent()) {
            // To make sure that only one token can be active for a user at any given time, we
            existingToken.get().setUsed(true);
            forgotPasswordTokenRepository.save(existingToken.get());
        }

        String tokenString = RandomUtils.generateRandomString(forgotPasswordTokenLength, forgotPasswordTokenAlphabet);

        try {
            String emailSubject = messageSource.getMessage(Messages.RESET_PASSWORD_EMAIL_SUBJECT, new Object[0], userSession.getLocale());
            String emailBody = messageSource.getMessage(
                    Messages.RESET_PASSWORD_EMAIL_TEXT,
                    new Object[] { AuthenticationKeyFormatter.format(tokenString, forgotPasswordTokenFormat), resetPasswordUrl},
                    user.get().getPreferredLocale() == null ? Locale.getDefault() : user.get().getPreferredLocale()
            );
            emailService.sendSimpleMessage(new InternetAddress(user.get().getEmail(), user.get().getFullName()), emailSubject, emailBody);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        ForgotPasswordToken token = new ForgotPasswordToken();
        token.setTokenHash(HashUtils.toSha256Base64String(tokenString));
        token.setUser(user.get());
        token.setValidUntil(OffsetDateTime.now().plus(forgotPasswordTokenValidSeconds, SECONDS));

        forgotPasswordTokenRepository.save(token);

        logger.info("Forgot password token generated and e-mail sent to '{}' for user '{}'", user.get().getEmail(), username);

    }

    @Transactional
    public void resetPassword(String username, String forgotPasswordToken, String newPassword) {

        logger.info("Starting password reset for user '{}'", username);
        Optional<User> user = userRepository.findByUsername(username);

        if (!user.isPresent()) {
            logger.info("Could not find user '{}'", username);
            throw new YourGreatLibraryLoginException();
        }

        String filteredForgetPasswordToken = forgotPasswordToken.replaceAll("[^" + forgotPasswordTokenAlphabet + "]", "");
        Optional<ForgotPasswordToken> tokenEntity = forgotPasswordTokenRepository.findUnusedValidForUser(user.get(), OffsetDateTime.now());

        String forgotPasswordTokenHash = HashUtils.toSha256Base64String(filteredForgetPasswordToken);

        if (!tokenEntity.isPresent() ||
                !tokenEntity.get().getTokenHash().equals(forgotPasswordTokenHash)) {
            logger.info("Invalid password reset token: '{}' (filtered: '{}')", forgotPasswordToken, filteredForgetPasswordToken);
            throw new YourGreatLibraryLoginException();
        }

        // At this point we validated that the token is valid, so let's consume the token and update the password
        tokenEntity.get().setUsed(true);
        forgotPasswordTokenRepository.save(tokenEntity.get());

        user.get().setPasswordHash(PasswordHashUtils.hashPassword(newPassword));
        userRepository.save(user.get());

    }


    @Transactional(readOnly = true)
    public Collection<User> getUsers(String courseRef, Set<Role> roleFilter) {

        if (!SecurityUtils.canRoleAccess(userSession.getRole(), Role.TEACHER)) {
            throw new OrbitGamesAuthorizationException("Only teachers and administrators may retrieve all users (in a course)");
        }

        final Collection<User> users;
        if (StringUtils.isBlank(courseRef)) {
            // Only admins are allowed to retrieve all users (without a courseRef):
            if (userSession.getRole() != Role.ADMINISTRATOR) {
                throw new OrbitGamesAuthorizationException("Only administrators are allowed to retrieve all users without conditions");
            }

            if (roleFilter.isEmpty()) {
                users = userRepository.findAllAndFetchRelated();
            } else {
                users = userRepository.findByRoleInAndFetchRelated(roleFilter);
            }
        } else {
            if (roleFilter.isEmpty()) {
                users = userRepository.findByCourseRefAndFetchRelated(courseRef);
            } else {
                users = userRepository.findByCourseRefAndRolesAndFetchRelated(courseRef, roleFilter);
            }
        }

        return new HashSet<>(users);
    }

    @Transactional(readOnly = true)
    public User getUser(String userRef) {

        return userRepository.findByRefFetchRelated(userRef)
                .map(u -> {
                    u.setUserCourses(u.getUserCourses().stream().distinct().collect(Collectors.toList()));
                    return u;
                })
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user with ref: '" + userRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    @Transactional
    public User updateUser(String userRef, User userWithUpdates) {

        permissionController.checkUserUpdatePermission(userRef);

        validateEmail(userWithUpdates.getEmail());

        User user = getUser(userRef);
        user.setUsername(userWithUpdates.getUsername());
        user.setEmail(userWithUpdates.getEmail());
        user.setFullName(userWithUpdates.getFullName());
        user.setStudentNumber(userWithUpdates.getStudentNumber());
        user.setPreferredLocale(userWithUpdates.getPreferredLocale());

        // If the signed user agreement changed, we update it and we also update the sign date (because apparently the user signed an agreement)
        if ((user.getSignedUserAgreement() == null && userWithUpdates.getSignedUserAgreement() != null && StringUtils.isNotBlank(userWithUpdates.getSignedUserAgreement().getRef()))
                || user.getSignedUserAgreement() != null && userWithUpdates.getSignedUserAgreement() != null && StringUtils.equals(user.getSignedUserAgreement().getRef(), userWithUpdates.getSignedUserAgreement().getRef())) {

            String updatedSignedUserAgreementRef = userWithUpdates.getSignedUserAgreement().getRef();
            UserAgreement updatedSignedUserAgreement = userAgreementRepository.findByRef(updatedSignedUserAgreementRef)
                    .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user agreement with ref '" + updatedSignedUserAgreementRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));

            logger.info("Updating signed user agreement for user '{}' (ref '{}') from '{}' to '{}'", user.getUsername(), user.getRef(), user.getSignedUserAgreement() == null ? null : user.getSignedUserAgreement().getRef(), updatedSignedUserAgreementRef);

            user.setSignedUserAgreement(updatedSignedUserAgreement);
            user.setUserAgreementSignDate(OffsetDateTime.now());
        }

        for (Map.Entry<String, UserProperty> userPropertyWithUpdate: userWithUpdates.getUserProperties().entrySet()) {
            user.putUserProperty(userPropertyWithUpdate.getKey(), userPropertyWithUpdate.getValue().getPropertyValue());
        }

        userRepository.save(user);

        // We fetch the user with all it's related properties here, so that when converting to JSON, all the data is there
        return userRepository.findByRefFetchRelated(user.getRef())
                .map(u -> {
                    u.setUserCourses(u.getUserCourses().stream().distinct().collect(Collectors.toList()));
                    return u;
                })
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user with ref '" + user.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    @Transactional(readOnly = true)
    public UserProperty getUserProperty(String userRef, String propertyKey) {

        User user = getUserInternal(userRef, true);
        return user.getUserProperties().get(propertyKey);
    }

    @Transactional
    public UserProperty updateUserProperty(String userRef, String oldKey, String newKey, String value) {

        permissionController.checkUserUpdatePermission(userRef);

        User user = getUserInternal(userRef, true);

        if (!oldKey.equals(newKey)) {
            user.removeUserProperty(oldKey);
        }

        user.putUserProperty(newKey, value);

        userRepository.save(user);

        return user.getUserProperties().get(newKey);
    }

    private User getUserInternal(String userRef) {
        return getUserInternal(userRef, false);
    }

    private User getUserInternal(String userRef, boolean fetchProperties) {
        return (fetchProperties ? userRepository.findByRefFetchUserProperties(userRef) : userRepository.findByRef(userRef))
                .orElseThrow(() -> new OrbitGamesApplicationException("Could not find user with ref: '" + userRef + "'", GenericErrorCode.ENTITY_NOT_FOUND));
    }

    private void validateEmail(String email) {

        try {
            // By constructing an "InternetAddress", we confirm that the passed e-mail is indeed valid.
            new InternetAddress(email);

            String domain = email.substring(email.indexOf('@') + 1);
            if (!allowedEmailDomains.contains(domain.toLowerCase())) {
                throw new OrbitGamesApplicationException("The domain '" + domain + "' of e-mail address '" + email + "' is not allowed", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

        } catch (AddressException e) {
            throw new OrbitGamesApplicationException("The given e-mail address '" + email + "' is not valid", GenericErrorCode.CONSTRAINT_VIOLATION);
        }
    }

}
