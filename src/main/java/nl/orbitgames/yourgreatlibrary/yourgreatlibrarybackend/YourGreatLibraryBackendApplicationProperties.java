package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend;

public final class YourGreatLibraryBackendApplicationProperties {

    public static final String APPLICATION_ENVIRONMENT = "yourgreatlibrary.application.environment";

    public static final String SIGNATURE_PRIVATE_KEY = "yourgreatlibrary.application.signature.private-key";
    public static final String SIGNATURE_PUBLIC_KEY = "yourgreatlibrary.application.signature.public-key";

    public static final String JWT_SECRET = "yourgreatlibrary.authentication.jwt.secret";
    public static final String JWT_ISSUER = "yourgreatlibrary.authentication.jwt.issuer";
    public static final String JWT_EXPIRY = "yourgreatlibrary.authentication.jwt.expiry";
    public static final String TEST_API_TOKEN = "yourgreatlibrary.authentication.test-api-token";

    public static final String DEFAULT_ADMIN_USERNAME = "yourgreatlibrary.application.default-admin.username";
    public static final String DEFAULT_ADMIN_EMAIL = "yourgreatlibrary.application.default-admin.email";
    public static final String DEFAULT_ADMIN_FULL_NAME = "yourgreatlibrary.application.default-admin.full-name";
    public static final String DEFAULT_ADMIN_PASSWORD = "yourgreatlibrary.application.default-admin.password";

    public static final String RESET_PASSWORD_TOKEN_LENGTH = "yourgreatlibrary.application.reset-password.token.length";
    public static final String RESET_PASSWORD_TOKEN_ALPHABET = "yourgreatlibrary.application.reset-password.token.alphabet";
    public static final String RESET_PASSWORD_TOKEN_VALID_SECONDS = "yourgreatlibrary.application.reset-password.token.valid-seconds";
    public static final String RESET_PASSWORD_TOKEN_FORMAT = "yourgreatlibrary.application.reset-password.token.format";
    public static final String RESET_PASSWORD_URL = "yourgreatlibrary.application.reset-password.url";

    public static final String EMAIL_FROM_ADDRESS = "yourgreatlibrary.application.email.from-address";
    public static final String EMAIL_FROM_NAME = "yourgreatlibrary.application.email.from-name";

    public static final String USER_ALLOWED_EMAIL_DOMAINS = "yourgreatlibrary.application.user.allowed-email-domains";

    public static final String DATA_FOLDER = "yourgreatlibrary.application.data-folder";
    public static final String TEMPLATES_FOLDER = "yourgreatlibrary.application.templates-folder";
    public static final String DOWNLOAD_ROOT_PATH = "yourgreatlibrary.application.download-root-path";
    public static final String UPLOAD_MAX_SIZE_BYTES = "yourgreatlibrary.application.upload.max-size";
    public static final String UPLOAD_ALLOWED_FILE_TYPE = "yourgreatlibrary.application.upload.allowed-file-type";
    public static final String UPLOAD_ALLOWED_MIME_TYPE = "yourgreatlibrary.application.upload.allowed-mime-type";

    public static final String REVIEW_STEP_UPLOAD_MAX_SIZE_BYTES = "yourgreatlibrary.application.review-step.upload.max-size";
    public static final String REVIEW_STEP_UPLOAD_ALLOWED_FILE_TYPE = "yourgreatlibrary.application.review-step.upload.allowed-file-type";
    public static final String REVIEW_STEP_UPLOAD_ALLOWED_MIME_TYPE = "yourgreatlibrary.application.review-step.upload.allowed-mime-type";

    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_TYPE = "yourgreatlibrary.application.reviewer-relation-generation.default.type";
    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_REVIEWER_COUNT = "yourgreatlibrary.application.reviewer-relation-generation.default.reviewer-count";
    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE = "yourgreatlibrary.application.reviewer-relation-generation.default.auto-generate";
    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE_DELAY = "yourgreatlibrary.application.reviewer-relation-generation.default.auto-generate-delay";
    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_INCLUDE_LATE_PEERS = "yourgreatlibrary.application.reviewer-relation-generation.default.include-late-peers";
    public static final String REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_MARK_COMPLETE = "yourgreatlibrary.application.reviewer-relation-generation.default.auto-mark-complete";

    public static final String COURSE_TEMPLATES_DEFAULTS = "yourgreatlibrary.application.course-templates.defaults";
    /**
     * Some default templates use a "{content-root}" property. This is used as a root location for external files.
     * It is a property and not part of the template to allow distinguishing between different environments where the application is run.
     */
    public static final String COURSE_TEMPLATES_CONTENT_ROOT = "yourgreatlibrary.application.course-templates.content-root";

    public static final String CONVERSATION_NOTIFICATION_EMAIL_URL = "yourgreatlibrary.application.conversation.notification-email.url";

    public static final String BUG_REPORT_EMAILS = "yourgreatlibrary.application.bug-report.emails";
}

