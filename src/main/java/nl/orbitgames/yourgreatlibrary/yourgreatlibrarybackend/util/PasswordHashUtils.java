package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordHashUtils {

    private static final int LOG_ROUNDS = 12;

    private static final String DUMMY_PASSWORD = "Test123*";
    private static final String DUMMY_PASSWORD_HASH = hashPassword(DUMMY_PASSWORD);

    /**
     * Hashes a password using BCrypt and returns the hashed password.
     * @param plainTextPassword The plain text password to be hashed
     * @return The hashed password, safe to be persisted.
     */
    public static String hashPassword(String plainTextPassword) {

        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt(LOG_ROUNDS));
    }

    /**
     * Validates a given password
     * @param plainTextPassword The plain text password to check
     * @param hashedPassword The hashed password to validate against
     * @return A boolean indicating if the given plain text password fits with the hashed password
     */
    public static boolean checkPassword(String plainTextPassword, String hashedPassword) {
        return BCrypt.checkpw(plainTextPassword, hashedPassword);
    }

    /**
     * Executes a password check on a dummy password. Useful to prevent timing attacks to determine valid usernames.
     */
    public static void dummyPasswordCheck() {
        checkPassword(DUMMY_PASSWORD, DUMMY_PASSWORD_HASH);
    }
}
