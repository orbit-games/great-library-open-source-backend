package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate;

public class QuizQuestionAnswer {

    private final String text;
    private final int points;

    public QuizQuestionAnswer(String text, int points) {
        this.text = text;
        this.points = points;
    }

    public String getText() {
        return text;
    }

    public int getPoints() {
        return points;
    }
}
