package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    private static SimpleCache<Long> cache = new SimpleCache<>(1000);

    public static long getFileSize(String path) {

        return cache.getOrCompute(path, () -> {
            Path filePath = Paths.get(path);
            try (FileChannel fileChannel = FileChannel.open(filePath)) {
                return fileChannel.size();
            } catch (IOException e) {
                throw new OrbitGamesSystemException("Error while opening file '" + path + "': " + e.getMessage(), e);
            }
        });
    }
}
