package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate;

import java.util.Objects;

public abstract class TemplateMemberId {

    private final String id;

    public TemplateMemberId(String id) {
        this.id = id;
    }

    public String get() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemplateMemberId that = (TemplateMemberId) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
