package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

public class AcademicYearUtils {

    private static final int[] WEEKS_PER_QUARTER = new int[]{
            10, // approx. sept -> nov
            13, // approx nov -> feb (includes christmas break)
            10, // approx feb -> apr
            11, // approx apr -> july
            8,  // approx july -> sept (summer break)
    };

    /**
     * Calculates in which academic year the date falls
     * @param date the date to check
     * @return the academic year
     */
    public static int calculateAcademicYearOfDate(OffsetDateTime date) {

        // We assume that the academic year starts on the first monday of september
        LocalDate startOfAcademicYear = calculateStartOfAcademicYear(date.getYear());
        if (date.toLocalDate().isBefore(startOfAcademicYear)) {
            return startOfAcademicYear.getYear() - 1;
        } else {
            return startOfAcademicYear.getYear();
        }
    }

    public static int calculateQuarterOfDate(OffsetDateTime date) {

        int quarter = 0;
        int year = calculateAcademicYearOfDate(date);
        LocalDate quarterStartDate = calculateStartOfAcademicYear(year);
        while (quarter < WEEKS_PER_QUARTER.length) {
            LocalDate quarterEndDate = quarterStartDate.plus(WEEKS_PER_QUARTER[quarter], ChronoUnit.WEEKS);
            quarter++;
            if (date.toLocalDate().isBefore(quarterEndDate)) {
                return quarter;
            }
            quarterStartDate = quarterEndDate;
        }
        // Shouldn't really happen...
        return quarter;

    }

    public static LocalDate calculateStartOfAcademicYear(int year) {
        return LocalDate.of(year, Month.SEPTEMBER, 1).with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
    }

}
