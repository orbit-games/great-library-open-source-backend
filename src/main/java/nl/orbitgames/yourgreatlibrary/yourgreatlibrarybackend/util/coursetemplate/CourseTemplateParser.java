package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.ContentPropertiesReader;
import org.apache.commons.lang3.StringUtils;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CourseTemplateParser {

    private final ContentPropertiesReader courseTemplateReader;
    private final String contentRoot;

    public CourseTemplateParser(ContentPropertiesReader courseTemplateReader, String contentRoot) {
        this.courseTemplateReader = courseTemplateReader;
        this.contentRoot = contentRoot;
    }

    public String getTemplateName() {
        return getString("template.name");
    }

    public String getTemplateDescription() {
        return getString("template.description");
    }

    public List<ChapterId> getChapterIds() {
        return getIds("chapters", ChapterId::new);
    }

    public List<SectionId> getSectionIds() {
        return getIds("sections", SectionId::new);
    }

    public String getSectionTitle(SectionId sectionId) {
        return getString(sectionId + ".section.title");
    }

    public String getSectionText(SectionId sectionId) {
        return getString(sectionId + ".section.text");
    }

    public String getSectionParentRelationDescription(SectionId sectionId) {
        return getString(sectionId + ".section.parent.relation.description");
    }

    public List<SectionId> getSectionChildrenIds(SectionId sectionId) {
        return getIds(sectionId + ".section.children", SectionId::new);
    }

    public List<ResourceId> getSectionResources(SectionId sectionId) {
        return getIds(sectionId + ".section.resources", ResourceId::new);
    }
    
    public String getResourceSkill(ResourceId resourceId) {
        return getString(resourceId + ".skill");
    }
    
    public String getResourceQuizTitle(ResourceId resourceId) {
        return getString(resourceId + ".quiz.title");
    }

    public QuizId getResourceQuizId(ResourceId resourceId) {
        return new QuizId(resourceId + ".quiz");
    }

    public String getResourceCompletionQuizDescription(ResourceId resourceId) {
        return getString(resourceId + ".quiz.description");
    }

    public String getResourceCompletionQuizQuestionText(ResourceId resourceId) {
        return getString(resourceId + ".quiz.question.text");
    }
    
    public int getResourceQuizMinimalScore(ResourceId resourceId) {
        return getInt(resourceId + ".quiz.minimal-score", 1);
    }

    public String getResourceTitle(ResourceId resourceId) {
        return getString(resourceId + ".title");
    }

    public String getResourceDescription(ResourceId resourceId) {
        return getString(resourceId + ".description");
    }

    public String getResourceContentUrl(ResourceId resourceId) {
        return getContentUrl(resourceId + ".url");
    }

    public boolean isResourceOptional(ResourceId resourceId) {
        return getBoolean(resourceId + ".optional");
    }

    public String getResourceRelationDescription(ResourceId resourceId) {
        return resourceId + ".relation.description";
    }

    public boolean hasResourceFullQuiz(ResourceId resourceId) {
        return StringUtils.isNotBlank(getRaw(resourceId + ".quiz.questions"));
    }

    public String getQuizTitle(QuizId quizId) {
        return getString(quizId + ".title");
    }

    public String getQuizDescription(QuizId quizId) {
        return getString(quizId + ".description");
    }


    public List<QuizQuestionId> getQuizQuestionIds(QuizId quizId) {
        // The quiz Ids are always prefixed with {{quizId}}.question.
        return getIds(quizId + ".questions", q -> new QuizQuestionId(quizId + ".question." + q));
    }

    public QuestionType getQuizQuestionType(QuizQuestionId quizQuestionId) {
        return getEnum(quizQuestionId + ".type", QuestionType.SELECT_ONE, QuestionType.class);
    }

    public String getQuizQuestionText(QuizQuestionId quizQuestionId) {
        return getString(quizQuestionId + ".text");
    }

    public List<QuizQuestionAnswer> getQuizQuestionAnswers(QuizQuestionId quizQuestionId) {

        String rawAnswers = getRaw(quizQuestionId + ".answers");
        List<String> optionsStrings = extractStringList(rawAnswers, "\\|");
        Pattern optionPattern = Pattern.compile("^(.+):(-?[0-9]+)$", Pattern.MULTILINE | Pattern.DOTALL);

        List<QuizQuestionAnswer> answers = new ArrayList<>(optionsStrings.size());

        for (String optionString : optionsStrings) {

            Matcher matcher = optionPattern.matcher(optionString);
            if (!matcher.matches()) {
                throw new OrbitGamesApplicationException("Answer '" + optionString + "' for quiz question '" + quizQuestionId + "' in the template is not valid", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            answers.add(new QuizQuestionAnswer(matcher.group(1), Integer.parseInt(matcher.group(2))));
        }
        return answers;
    }

    public OffsetDateTime getChapterFirstDeadline(ChapterId chapterId, Map<ReviewStepId, OffsetDateTime> deadlinesOverride) {

        List<ReviewStepId> reviewStepIds = getChapterReviewStepIds(chapterId);
        // Check if there is a deadline override for the first deadline, otherwise, return the one from the template
        if (!reviewStepIds.isEmpty() && deadlinesOverride.containsKey(reviewStepIds.get(0))) {
            return deadlinesOverride.get(reviewStepIds.get(0));
        } else {
            return OffsetDateTime.parse(getRaw(chapterId + ".first-deadline"), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }
    }

    public List<ReviewStepId> getChapterReviewStepIds(ChapterId chapterId) {
        return getIds(chapterId + ".review-steps", ReviewStepId::new);
    }

    public String getChapterTitle(ChapterId chapterId) {
        return getString(chapterId + ".title");
    }

    public String getChapterDescription(ChapterId chapterId) {
        return getString(chapterId + ".description");
    }

    public List<SectionRelationId> getChapterSectionRelationIds(ChapterId chapterId) {
        return getIds(chapterId + ".section-relations", s -> new SectionRelationId(chapterId + ".section-relation." + s));
    }

    public SectionId getSectionRelationSection(SectionRelationId sectionRelationId) {
        return getId(sectionRelationId + ".section", SectionId::new);
    }

    public String getSectionRelationDescription(SectionRelationId sectionRelationId) {
        return getString(sectionRelationId + ".description");
    }

    public ReviewStepType getReviewStepType(ReviewStepId reviewStepId) {
        return getEnum(reviewStepId + ".type", null, ReviewStepType.class);
    }

    public String getReviewStepName(ReviewStepId reviewStepId) {
        return getString(reviewStepId + ".name");
    }

    public String getReviewStepDescription(ReviewStepId reviewStepId) {
        return getString(reviewStepId + ".description");
    }

    public boolean hasReviewStepFileUpload(ReviewStepId reviewStepId) {
        return StringUtils.isNotBlank(getRaw(reviewStepId + ".file-upload.name"));
    }

    public String getReviewStepFileUploadName(ReviewStepId reviewStepId) {
        return getString(reviewStepId + ".file-upload.name");
    }

    public int getReviewStepDeadlineOffset(ReviewStepId reviewStepId) {
        return getInt(reviewStepId + ".deadline");
    }

    public DiscussionType getReviewStepDiscussionType(ReviewStepId reviewStepId) {
        return getEnum(reviewStepId + ".arguments.type", DiscussionType.NONE, DiscussionType.class);
    }

    public List<ArgumentTypeId> getReviewStepArgumentTypeIds(ReviewStepId reviewStepId) {
        return getIds(reviewStepId + ".arguments", ArgumentTypeId::new);
    }

    public boolean hasReviewStepQuiz(ReviewStepId reviewStepId) {
        return StringUtils.isNotBlank(getRaw(reviewStepId + ".quiz.questions"));
    }

    public QuizId getReviewStepQuizId(ReviewStepId reviewStepId) {
        return new QuizId(reviewStepId + ".quiz");
    }

    public List<String> getReviewStepSkills(ReviewStepId reviewStepId) {
        return extractStringList(getRaw(reviewStepId + ".skills"), ",");
    }

    public String getArgumentTypeName(ArgumentTypeId argumentTypeId) {
        return getString(argumentTypeId + ".name");
    }

    public String getArgumentTypeDescription(ArgumentTypeId argumentTypeId) {
        return getString(argumentTypeId + ".description");
    }

    public boolean isArgumentTypeRequired(ArgumentTypeId argumentTypeId) {
        return getBoolean(argumentTypeId + ".required");
    }

    public ArgumentTypeId getArgumentTypeParentId(ArgumentTypeId argumentTypeId) {
        return getId(argumentTypeId + ".parent", ArgumentTypeId::new);
    }

    private String getString(String key) {
        return getRaw(key);
    }

    private boolean getBoolean(String key) {
        String raw = getRaw(key);
        return StringUtils.isNotBlank(raw) && Boolean.parseBoolean(raw);
    }

    private <T extends Enum<T>> T getEnum(String key, T defaultValue, Class<T> enumType) {

        String raw = getRaw(key);
        if (StringUtils.isBlank(raw)) {
            return defaultValue;
        }
        return Arrays.stream(enumType.getEnumConstants())
                .filter(q -> q.toString().equals(raw))
                .findAny()
                .orElseThrow(() -> new OrbitGamesApplicationException("Unknown " + enumType.getName() + ": '" + raw + "'", GenericErrorCode.CONSTRAINT_VIOLATION));
    }

    private int getInt(String key) {
        return getInt(key, 0);
    }

    private int getInt(String key, int defaultValue) {
        String raw = getRaw(key);
        if (StringUtils.isNotBlank(raw)) {
            return Integer.parseInt(raw);
        }
        return defaultValue;
    }

    private String getContentUrl(String key) {

        String rawUrl = getRaw(key);
        if (StringUtils.isBlank(rawUrl)) {
            return null;
        }

        return rawUrl.replaceAll("\\{content-root}", contentRoot);
    }

    private <T extends TemplateMemberId> List<T> getIds(String key, Function<String, T> idFactory) {
        return extractIds(getRaw(key), idFactory);
    }

    private <T extends TemplateMemberId> T getId(String key, Function<String, T> idFactory) {
        String raw = getRaw(key);
        if (StringUtils.isBlank(raw)) {
            return null;
        }
        return idFactory.apply(raw);
    }

    private String getRaw(String key) {
        return courseTemplateReader.getMessage(key);
    }
    
    private <T extends TemplateMemberId> List<T> extractIds(String str, Function<String, T> idFactory) {

        return extractStringStream(str, ",")
                .map(idFactory)
                .collect(Collectors.toList());
    }

    private List<String> extractStringList(String str, String separator) {
        return extractStringStream(str, separator)
                .collect(Collectors.toList());
    }

    private Stream<String> extractStringStream(String str, String separator) {

        if (org.apache.commons.lang3.StringUtils.isBlank(str)) {
            return Stream.empty();
        }
        return Arrays.stream(
                str.split(separator))
                .map(String::trim)
                .filter(StringUtils::isNotBlank);
    }
}
