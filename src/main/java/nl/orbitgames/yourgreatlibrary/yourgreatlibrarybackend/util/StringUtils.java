package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

public class StringUtils {

    public static int wordCount(String input) {

        if (input == null || input.isEmpty()) {
            return 0;
        }

        return input.split("\\s+").length;
    }
}
