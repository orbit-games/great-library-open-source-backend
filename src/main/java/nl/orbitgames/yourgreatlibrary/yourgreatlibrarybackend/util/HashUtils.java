package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Utility function for standard hashing
 */
public class HashUtils {

    public static String toSha256Base64String(String source) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hashed = digest.digest(source.getBytes());
            return Base64.getEncoder().encodeToString(hashed);
        } catch (NoSuchAlgorithmException e) {
            throw new OrbitGamesSystemException("Could not find SHA-256 algorithm", e);
        }
    }

}
