package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CollectionUtils {

    /**
     * Find entries that are added to the source collection compared to the original. Entries are compared based on the compareFieldGetter.
     * @param source The source list (with new entries)
     * @param original The original list (old list to which new entries from the source list should be added)
     * @param compareFieldGetter A function that gets the field to use to compare equality between the two values
     * @param <T> The type of entries in the collection
     * @param <R> The type of the field for which equality should be compared.
     * @return A new collection containing the entries of the source collection that do not exist in the original. Equality is based on the "compareFieldGetter"
     */
    public static <T, R> Collection<T> findAdded(Collection<T> source, Collection<T> original, Function<T, R> compareFieldGetter) {

        return findAdded(source, original, equalityComparerForField(compareFieldGetter));
    }

    /**
     * Find entries that are added to the source collection compared to the original, based on some equality check
     * @param source The source list (with new entries)
     * @param original The original list (old list to which new entries from the source list should be added)
     * @param equalityComparer Function that returns true iff two entries in the collections are equal, e.g. {@link Objects#equals}
     * @param <T> The type of entries in the collection
     * @return A new collection containing the entries of the source collection that do not exist in the original.
     */
    public static <T> Collection<T> findAdded(Collection<T> source, Collection<T> original, BiFunction<T, T, Boolean> equalityComparer) {

        return source.stream()
                .filter(s -> !anyMatch(original, v -> equalityComparer.apply(s, v)))
                .collect(Collectors.toList());

    }

    /**
     * Find entries that are removed from the original collection compared to the source collection.
     * @param source The source list
     * @param original The original list (from which some entries are expected to be removed)
     * @param compareFieldGetter A function that gets the field to use to compare equality between the two values
     * @param <T> The type of entries in the collection
     * @param <R> The type of the field for which equality should be compared.
     * @return A new collection containing the entries of the original collection that do not exist in the source.
     */
    public static <T, R> Collection<T> findRemoved(Collection<T> source, Collection<T> original, Function<T, R> compareFieldGetter) {

        return findRemoved(source, original, equalityComparerForField(compareFieldGetter));
    }

    /**
     * Find entries that are removed from the original collection compared to the source collection.
     * @param source The source list
     * @param original The original list (from which some entries are expected to be removed)
     * @param equalityComparer Function that returns true iff two entries in the collections are equal, e.g. {@link Objects#equals}
     * @param <T> The type of entries in the collection
     * @return A new collection containing the entries of the original collection that do not exist in the source.
     */
    public static <T> Collection<T> findRemoved(Collection<T> source, Collection<T> original, BiFunction<T, T, Boolean> equalityComparer) {

        // Finding the removed entries is the same as finding the added entries comparing the other way around
        return findAdded(original, source, equalityComparer);
    }

    /**
     * Find entries that exists in both the source and original collection.
     * @param source The source collection
     * @param original The original collection
     * @param compareFieldGetter A function that gets the field to use to compare equality between the two values
     * @param <T> The type of entries in the collection
     * @param <R> The type of the field for which equality should be compared
     * @return A new collection containing the equal entries in the source and original list (as pairs).
     */
    public static <T, R> Collection<Pair<T, T>> findExisting(Collection<T> source, Collection<T> original, Function<T, R> compareFieldGetter) {

        return findExisting(source, original, equalityComparerForField(compareFieldGetter));
    }

    /**
     * Find entries that exists in both the source and original collection.
     * @param source The source collection
     * @param original The original collection
     * @param equalityComparer Function that returns true iff two entries in the collections are equal, e.g. {@link Objects#equals}
     * @param <T> The type of entries in the collection
     * @return A new collection containing the equal entries in the source and original list (as pairs).
     */
    public static <T> Collection<Pair<T, T>> findExisting(Collection<T> source, Collection<T> original, BiFunction<T, T, Boolean> equalityComparer) {

        //noinspection OptionalGetWithoutIsPresent
        return source.stream()
                .map(se -> Pair.of(se, findBy(original, oe -> equalityComparer.apply(se, oe))))
                .filter(e -> e.getRight().isPresent())
                .map(e -> Pair.of(e.getLeft(), e.getRight().get()))
                .collect(Collectors.toList());
    }

    /**
     * Returns true iff any entry in the collection matches the predicate. Always false for empty lists.
     * @param collection The collection to check
     * @param predicate Predicate to check
     * @param <T> The type of entries in the collection
     * @return True iff the predicate is true for at least one entry in the collection
     */
    public static <T> boolean anyMatch(Collection<T> collection, Predicate<T> predicate) {

        for (T entry: collection) {
            if (predicate.test(entry)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true iff all entries in the collection match the predicate. Always returns true for empty lists.
     * @param collection The collection to check
     * @param predicate Predicate to use
     * @param <T> The type of entries in the collection
     * @return True iff the predicate is true for all entries in the collection
     */
    public static <T> boolean allMatch(Collection<T> collection, Predicate<T> predicate) {

        for (T entry: collection) {
            if (!predicate.test(entry)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true iff none of the entries in the collection match the predicate. Always true for empty lists.
     * @param collection The collection to check
     * @param predicate Predicate to use
     * @param <T> The type of entries in the collection
     * @return True iff the predicate is false for all entries in the collection
     */
    public static <T> boolean noneMatch(Collection<T> collection, Predicate<T> predicate) {
        return !anyMatch(collection, predicate);
    }

    /**
     * Returns some entry in the collection that matches the predicate, or an empty optional if no entries match
     * @param collection The collection to check
     * @param predicate Predicate to check
     * @param <T> The type of entries in the collection
     * @return An optional containing an entry in the collection that matched the predicate, or an empty optional if no such entry could be found.
     */
    public static <T> Optional<T> findBy(Collection<T> collection, Predicate<T> predicate) {

        for (T entry: collection) {
            if (predicate.test(entry)) {
                return Optional.of(entry);
            }
        }
        return Optional.empty();
    }

    public static <T1, T2> Set<T2> mapToSet(Collection<T1> coll, Function<T1, T2> converter) {

        if (coll == null || coll.isEmpty()) {
            return Collections.emptySet();
        }

        Set<T2> res = new HashSet<>(coll.size());
        for(T1 item: coll) {
            res.add(converter.apply(item));
        }
        return res;
    }

    public static <T1, T2> List<T2> mapToList(Collection<T1> coll, Function<T1, T2> converter) {

        if (coll == null || coll.isEmpty()) {
            return Collections.emptyList();
        }

        List<T2> res = new ArrayList<>(coll.size());
        for(T1 item: coll) {
            res.add(converter.apply(item));
        }
        return res;
    }

    public static <T1, T2> List<T2> mapToSortedList(Collection<T1> coll, Function<T1, T2> converter, Comparator<T2> comparator) {

        List<T2> res = mapToList(coll, converter);
        res.sort(comparator);
        return res;
    }

    private static <T, R> BiFunction<T, T, Boolean> equalityComparerForField(Function<T, R> compareFieldGetter) {
        return (t1, t2) -> Objects.equals(compareFieldGetter.apply(t1), compareFieldGetter.apply(t2));
    }
}
