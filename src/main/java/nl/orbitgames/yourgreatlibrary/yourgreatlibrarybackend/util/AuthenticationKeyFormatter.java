package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationKeyFormatter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationKeyFormatter.class);

    private static final char FORMAT_WILDCARD_CHARACTER = 'X';

    public static String format(String randomCharacters, String format) {

        int formatWildcardCount = StringUtils.countMatches(format, FORMAT_WILDCARD_CHARACTER);
        if (formatWildcardCount != randomCharacters.length()) {
            logger.warn("The given format '{}' contains {} wildcards, but there are {} random characters to fit in the format", format, formatWildcardCount, randomCharacters.length());
            throw new OrbitGamesSystemException("Invalid authentication key formatter input");
        }

        int formatIndex = format.indexOf(FORMAT_WILDCARD_CHARACTER);
        int i = 0;
        StringBuilder res = new StringBuilder(format);
        while (formatIndex >= 0) {
            res.setCharAt(formatIndex, randomCharacters.charAt(i++));
            formatIndex = format.indexOf(FORMAT_WILDCARD_CHARACTER, formatIndex + 1);
        }

        return  res.toString();
    }

}
