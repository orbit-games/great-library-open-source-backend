package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import java.net.URI;
import java.net.URISyntaxException;

public class UriUtils {

    public static boolean isValidURI(String uriStr) {
        try {
            URI uri = new URI(uriStr);
            return true;
        }
        catch (URISyntaxException e) {
            return false;
        }
    }

}
