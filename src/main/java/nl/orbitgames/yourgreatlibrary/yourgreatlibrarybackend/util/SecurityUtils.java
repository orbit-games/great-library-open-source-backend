package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityUtils {

    private static final Logger logger = LoggerFactory.getLogger(SecurityUtils.class);

    public static boolean canRoleAccess(Role role, Role requiredRole) {

        // No role but a role is required
        if (role == null && requiredRole != null) {
            return false;
        }

        // There are no role requirements apparently
        if (requiredRole == null) {
            return true;
        }

        // Administrators are like super users and may access everything
        if (role == Role.ADMINISTRATOR) {
            return true;
        }

        switch (requiredRole) {
            case ADMINISTRATOR:
                // We already established that the user is not an administrator, so if the required role is "administrator", the user is not allowed to access this
                return false;
            case TEACHER:
                return role == Role.TEACHER;
            case LEARNER:
                return role == Role.TEACHER || role == Role.LEARNER;
            default:
                logger.warn("Unknown role: " + requiredRole + ", blocking access");
                return false;
        }
    }

}
