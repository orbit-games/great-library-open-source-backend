package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * Class used to generate the "ref", a unique reference for an object
 */
public class RefGenerator {

    private static final SecureRandom rnd = new SecureRandom();

    public static String generate() {

        // 18 bytes will gives us 144 bits of randomness, or 2.2300745e+43 unique options. That should be enough to
        // prevent collisions.
        byte[] randomData = new byte[18];
        rnd.nextBytes(randomData);

        return Base64.getUrlEncoder().encodeToString(randomData);
    }

    public static void main(String... args) {

        for(int i = 0; i < 10; i++) {
            System.out.println(generate());
        }
    }
}
