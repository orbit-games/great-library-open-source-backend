package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

public class SimpleCache<T>  {

    private Map<String, T> cache;

    public SimpleCache(int maxEntries) {
        cache = Collections.synchronizedMap(new LinkedHashMapCache<>(maxEntries));
    }

    public T getOrCompute(String key, Supplier<T> supplier) {

        if (cache.containsKey(key)) {
            return cache.get(key);
        } else {
            T val = supplier.get();
            put(key, val);
            return val;
        }
    }

    public T get(String key) {
        return cache.get(key);
    }

    public void put(String key, T value) {
        cache.put(key, value);
    }

    public void clear() {
        cache.clear();
    }



    private static class LinkedHashMapCache<K, V> extends LinkedHashMap<K, V> {

        private final int maxEntries;

        LinkedHashMapCache(int maxEntries) {
            this.maxEntries = maxEntries;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > maxEntries;
        }
    }


}
