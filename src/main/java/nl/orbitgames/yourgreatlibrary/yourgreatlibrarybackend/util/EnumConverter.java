package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

public class EnumConverter {

    /**
     * Converts enums with the exact same values into each other.
     * @param value The source enum value
     * @param targetClass The class of the target enum
     * @param <S> The source enum type
     * @param <T> The target enum type
     * @return The value of the source enum in the target enum type
     * @throws IllegalArgumentException if the specified enum type has
     *               no constant corresponding with the source enum
     */
    public static <S extends Enum<S>, T extends Enum<T>> T convert(S value, Class<T> targetClass) {

        if (value == null) {
            return null;
        }

        return Enum.valueOf(targetClass, value.toString());

    }

}
