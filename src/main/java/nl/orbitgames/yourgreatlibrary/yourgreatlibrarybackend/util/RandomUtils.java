package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Utilities around the random generation of strings and numbers
 */
public class RandomUtils {

    private static final SecureRandom rnd;

    static {
        try {
            rnd = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new OrbitGamesSystemException(e);
        }
    }

    /**
     * Generates a random string of the given length using the given alphabet.
     * e.g.:
     * <code>
     *     generateRandomString(4, "ABCDEF")
     *     // returns something like:
     *     "DAEF"
     * </code>
     * @param length The desired length of the resulting random String
     * @param alphabet The alphabet of the random String
     * @return
     */
    public static String generateRandomString(int length, String alphabet) {

        char[] res = new char[length];
        for (int i = 0; i < length; i++) {
            res[i] = alphabet.charAt(rnd.nextInt(alphabet.length()));
        }
        return new String(res);
    }

}
