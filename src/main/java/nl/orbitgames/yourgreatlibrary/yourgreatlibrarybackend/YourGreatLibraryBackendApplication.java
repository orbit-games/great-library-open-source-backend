package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.PropertiesLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
public class YourGreatLibraryBackendApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(YourGreatLibraryBackendApplication.class);
		app.addListeners(new PropertiesLogger());
		app.run(args);
	}
}
