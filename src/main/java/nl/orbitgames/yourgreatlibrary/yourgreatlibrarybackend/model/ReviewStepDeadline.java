package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import java.time.OffsetDateTime;

public class ReviewStepDeadline {

    private final String reviewStepRef;
    private final OffsetDateTime deadline;

    public ReviewStepDeadline(String reviewStepRef, OffsetDateTime deadline) {
        this.reviewStepRef = reviewStepRef;
        this.deadline = deadline;
    }

    public String getReviewStepRef() {
        return reviewStepRef;
    }

    public OffsetDateTime getDeadline() {
        return deadline;
    }
}
