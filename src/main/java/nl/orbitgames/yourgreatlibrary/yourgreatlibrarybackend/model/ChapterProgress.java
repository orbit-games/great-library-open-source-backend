package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;

import java.util.List;

public class ChapterProgress {

    private Chapter chapter;
    private List<ReviewerRelationResult> reviewerRelationResults;

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public List<ReviewerRelationResult> getReviewerRelationResults() {
        return reviewerRelationResults;
    }

    public void setReviewerRelationResults(List<ReviewerRelationResult> reviewerRelationResults) {
        this.reviewerRelationResults = reviewerRelationResults;
    }
}
