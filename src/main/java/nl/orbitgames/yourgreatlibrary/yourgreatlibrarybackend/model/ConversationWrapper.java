package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage;
import org.apache.commons.lang3.Validate;

import java.util.List;

public class ConversationWrapper {

    private final Conversation conversation;
    private final long readUntil;
    private final ConversationEmailNotificationType notificationType;
    private final List<ConversationMessage> messages;

    public ConversationWrapper(Conversation conversation, long readUntil, ConversationEmailNotificationType notificationType, List<ConversationMessage> messages) {

        Validate.notNull(conversation);
        Validate.notNull(messages);

        this.conversation = conversation;
        this.readUntil = readUntil;
        this.notificationType = notificationType;
        this.messages = messages;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public long getReadUntil() {
        return readUntil;
    }

    public ConversationEmailNotificationType getNotificationType() {
        return notificationType;
    }

    public List<ConversationMessage> getMessages() {
        return messages;
    }
}
