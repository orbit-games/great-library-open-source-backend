package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CourseDeadlines {

    private final List<ChapterDeadlines> chapters;

    public CourseDeadlines(List<ChapterDeadlines> chapters) {
        this.chapters = Collections.unmodifiableList(new ArrayList<>(chapters));
    }

    public List<ChapterDeadlines> getChapters() {
        return chapters;
    }
}
