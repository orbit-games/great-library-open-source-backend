package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;

import java.util.List;

public class ReviewerRelationResult {

    private String reviewerRelationRef;
    private User reviewer;
    private User submitter;
    private List<ReviewStepResultWrapper> stepResults;

    public String getReviewerRelationRef() {
        return reviewerRelationRef;
    }

    public void setReviewerRelationRef(String reviewerRelationRef) {
        this.reviewerRelationRef = reviewerRelationRef;
    }

    public User getReviewer() {
        return reviewer;
    }

    public void setReviewer(User reviewer) {
        this.reviewer = reviewer;
    }

    public User getSubmitter() {
        return submitter;
    }

    public void setSubmitter(User submitter) {
        this.submitter = submitter;
    }

    public List<ReviewStepResultWrapper> getStepResults() {
        return stepResults;
    }

    public void setStepResults(List<ReviewStepResultWrapper> stepResults) {
        this.stepResults = stepResults;
    }
}
