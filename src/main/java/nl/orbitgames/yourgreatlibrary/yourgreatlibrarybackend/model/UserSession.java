package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Locale;
import java.util.Set;

@Component
@RequestScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSession {

    private String userRef;
    private String username;
    private Role role;
    private Set<String> securityRoles;
    private Locale locale;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public Set<String> getSecurityRoles() {
        return securityRoles;
    }

    public void setSecurityRoles(Set<String> securityRoles) {
        this.securityRoles = securityRoles;
    }

    public boolean isLoggedIn() {
        return StringUtils.isNotBlank(username) && StringUtils.isNotBlank(userRef);
    }
}