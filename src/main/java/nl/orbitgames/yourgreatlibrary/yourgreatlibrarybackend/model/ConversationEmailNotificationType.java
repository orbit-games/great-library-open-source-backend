package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ConversationEmailNotificationType {

    NONE,
    DIRECT,
    DAILY
}
