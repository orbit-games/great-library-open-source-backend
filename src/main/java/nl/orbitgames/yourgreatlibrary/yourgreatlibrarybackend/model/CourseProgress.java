package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;

import java.util.List;

public class CourseProgress {

    private UserCourse userCourse;
    private List<ChapterProgress> chapterProgress;
    private List<ResourceResult> resourceResults;


    public UserCourse getUserCourse() {
        return userCourse;
    }

    public void setUserCourse(UserCourse userCourse) {
        this.userCourse = userCourse;
    }

    public List<ChapterProgress> getChapterProgress() {
        return chapterProgress;
    }

    public void setChapterProgress(List<ChapterProgress> chapterProgress) {
        this.chapterProgress = chapterProgress;
    }

    public List<ResourceResult> getResourceResults() {
        return resourceResults;
    }

    public void setResourceResults(List<ResourceResult> resourceResults) {
        this.resourceResults = resourceResults;
    }
}
