package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ConversationType {
    ANNOUNCEMENTS,
    EVERYONE,
    LEARNERS_ONLY,
    TEACHERS_ONLY,
    ADMINS_ONLY,
    TOPIC,
    DIRECT,
    EDUCATIVE_SUPPORT,
    TECHNICAL_SUPPORT,
    GROUP,
}
