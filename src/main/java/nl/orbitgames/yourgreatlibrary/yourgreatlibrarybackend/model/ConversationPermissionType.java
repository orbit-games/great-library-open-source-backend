package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ConversationPermissionType {

    /**
     * Indicates no permissions to view or post messages to this conversation
     */
    NONE,
    /**
     * Indicates permission to view the conversation, but not post messages to it
     */
    VIEW,
    /**
     * Indicates the permission to post messages to the conversation
     */
    POST

}
