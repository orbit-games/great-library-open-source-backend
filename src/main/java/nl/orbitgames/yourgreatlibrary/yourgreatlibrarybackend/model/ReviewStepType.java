package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ReviewStepType {

    /**
     * The original submission
     */
    SUBMISSION(PeerType.SUBMITTER, true, false),
    /**
     * A review of the original submission
     */
    REVIEW(PeerType.REVIEWER, false, true),
    /**
     * An evaluation of the review
     */
    EVALUATION(PeerType.SUBMITTER, true, false),
    /**
     * A rebuttal/new submission based on the review
     */
    REBUTTAL(PeerType.SUBMITTER, true, false),
    /**
     * An assessment of the rebuttal
     */
    ASSESSMENT(PeerType.REVIEWER, false, true),
    ;

    private final PeerType postedBy;
    private final boolean accessByReviewerAllowed;
    private final boolean accessBySubmitterAllowed;

    ReviewStepType(PeerType postedBy, boolean accessByReviewerAllowed, boolean accessBySubmitterAllowed) {
        this.postedBy = postedBy;
        this.accessByReviewerAllowed = accessByReviewerAllowed;
        this.accessBySubmitterAllowed = accessBySubmitterAllowed;
    }

    public PeerType getPostedBy() {
        return postedBy;
    }

    public boolean isAccessByReviewerAllowed() {
        return accessByReviewerAllowed;
    }

    public boolean isAccessBySubmitterAllowed() {
        return accessBySubmitterAllowed;
    }
}
