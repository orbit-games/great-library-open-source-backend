package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum PeerType {
    SUBMITTER,
    REVIEWER;

    public PeerType opposite() {
        return this == SUBMITTER ? REVIEWER : SUBMITTER;
    }
}
