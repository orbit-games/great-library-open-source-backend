package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChapterDeadlines {

    private final String chapterRef;
    private final List<ReviewStepDeadline> reviewSteps;

    public ChapterDeadlines(String chapterRef, List<ReviewStepDeadline> reviewSteps) {
        this.chapterRef = chapterRef;
        this.reviewSteps = Collections.unmodifiableList(new ArrayList<>(reviewSteps));
    }

    public String getChapterRef() {
        return chapterRef;
    }

    public List<ReviewStepDeadline> getReviewSteps() {
        return reviewSteps;
    }
}
