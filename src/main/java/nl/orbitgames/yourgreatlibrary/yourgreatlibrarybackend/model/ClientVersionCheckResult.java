package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion;

public class ClientVersionCheckResult {

    private ClientVersionCheckResultStatus status;

    private ClientVersion current;
    private ClientVersion latest;

    public ClientVersion getCurrent() {
        return current;
    }

    public void setCurrent(ClientVersion current) {
        this.current = current;
    }

    public ClientVersion getLatest() {
        return latest;
    }

    public void setLatest(ClientVersion latest) {
        this.latest = latest;
    }

    public ClientVersionCheckResultStatus getStatus() {
        return status;
    }

    public void setStatus(ClientVersionCheckResultStatus status) {
        this.status = status;
    }
}
