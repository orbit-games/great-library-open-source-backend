package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum DiscussionType {
    NONE, RESTRICTED_ARGUMENTS, FREE_FORM_ARGUMENTS, REPLY
}
