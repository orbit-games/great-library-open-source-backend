package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import java.time.OffsetDateTime;

public class BugReportScreenshotWrapper {

    private final OffsetDateTime clientTime;
    private final byte[] screenshot;

    public BugReportScreenshotWrapper(OffsetDateTime clientTime, byte[] screenshot) {
        this.clientTime = clientTime;
        this.screenshot = screenshot;
    }

    public OffsetDateTime getClientTime() {
        return clientTime;
    }

    public byte[] getScreenshot() {
        return screenshot;
    }
}
