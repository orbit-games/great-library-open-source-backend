package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum QuestionType {
    SELECT_ONE, SELECT_MULTIPLE, NONE
}
