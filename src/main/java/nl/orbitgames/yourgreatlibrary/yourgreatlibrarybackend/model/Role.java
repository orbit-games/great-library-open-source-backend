package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum Role {
    LEARNER("Learners"),
    TEACHER("Teachers"),
    ADMINISTRATOR("Administrators");

    private final String humanReadableRoleName;

    Role(String humanReadableRoleName) {
        this.humanReadableRoleName = humanReadableRoleName;
    }

    public String getHumanReadableRoleName() {
        return humanReadableRoleName;
    }
}
