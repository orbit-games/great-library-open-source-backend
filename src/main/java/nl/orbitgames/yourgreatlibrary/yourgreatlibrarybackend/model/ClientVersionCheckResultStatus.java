package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ClientVersionCheckResultStatus {

    /**
     * Indicates that the client version is the latest
     */
    OK,
    /**
     * Indicates that an update is available, but it is not required to install
     */
    UPDATE_AVAILABLE,
    /**
     * Indicates that an update is available, and required to continue using the application
     */
    UPDATE_REQUIRED,
    /**
     * Indicates that the version the user uses has been rolled back/revoked, and the user should downgrade
     */
    ROLLBACK

}
