package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

/**
 * Client application platform
 */
public enum Platform {
    /**
     * iOS (iPhone, iPad, etc)
     */
    IOS,
    /**
     * Android phone or tablet
     */
    ANDROID,
    /**
     * WebGL (Browser based)
     */
    WEBGL,
    /**
     * Windows/PC
     */
    WIN,
    /**
     * Linux (any distribution)
     */
    LINUX,
    /**
     * Mac OS X (Apple)
     */
    MACOS
}
