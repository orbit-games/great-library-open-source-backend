package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;

public class ResourceResult {

    private Resource resource;
    private QuizResult quizResult;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public QuizResult getQuizResult() {
        return quizResult;
    }

    public void setQuizResult(QuizResult quizResult) {
        this.quizResult = quizResult;
    }
}
