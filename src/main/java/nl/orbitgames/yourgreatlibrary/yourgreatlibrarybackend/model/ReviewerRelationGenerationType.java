package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public enum ReviewerRelationGenerationType {

    /**
     * Reviewer relations are generated as groups, where the members of the groups review each other.
     */
    GROUP_BASED,
    /**
     * Reviewer relations are generated completely randomly
     */
    RANDOM,

}
