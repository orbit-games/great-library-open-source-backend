package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;

import java.time.OffsetDateTime;

public class ReviewStepResultWrapper {

    private final OffsetDateTime lastModified;
    private final boolean readyToComplete;
    private final boolean closed;

    private final ReviewStepResult reviewStepResult;

    public ReviewStepResultWrapper(OffsetDateTime lastModified, boolean readyToComplete, boolean closed, ReviewStepResult reviewStepResult) {
        this.lastModified = lastModified;
        this.readyToComplete = readyToComplete;
        this.closed = closed;
        this.reviewStepResult = reviewStepResult;
    }

    public OffsetDateTime getLastModified() {
        return lastModified;
    }

    public boolean isReadyToComplete() {
        return readyToComplete;
    }

    public boolean isClosed() {
        return closed;
    }

    public ReviewStepResult getReviewStepResult() {
        return reviewStepResult;
    }
}
