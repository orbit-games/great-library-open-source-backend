package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model;

public class CourseUpdateRequest {

    private final String name;
    private final int year;
    private final int quarter;
    private final String code;
    private final boolean anonymousUsers;
    private final boolean anonymousReviews;

    public CourseUpdateRequest(String name, int year, int quarter, String code, boolean anonymousUsers, boolean anonymousReviews) {
        this.name = name;
        this.year = year;
        this.quarter = quarter;
        this.code = code;
        this.anonymousUsers = anonymousUsers;
        this.anonymousReviews = anonymousReviews;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public int getQuarter() {
        return quarter;
    }

    public String getCode() {
        return code;
    }

    public boolean isAnonymousUsers() {
        return anonymousUsers;
    }

    public boolean isAnonymousReviews() {
        return anonymousReviews;
    }
}
