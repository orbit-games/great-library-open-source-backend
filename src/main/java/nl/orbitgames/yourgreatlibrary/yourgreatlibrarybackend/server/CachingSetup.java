package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.stereotype.Component;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import java.util.concurrent.TimeUnit;

@Component
public class CachingSetup implements JCacheManagerCustomizer {

    @Override
    public void customize(CacheManager cacheManager) {
        cacheManager.createCache("course", new MutableConfiguration<String, Course>()
                .setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new Duration(TimeUnit.MINUTES, 15)))
                .setTypes(String.class, Course.class)
                .setStoreByValue(false)
                .setStatisticsEnabled(true));
    }
}