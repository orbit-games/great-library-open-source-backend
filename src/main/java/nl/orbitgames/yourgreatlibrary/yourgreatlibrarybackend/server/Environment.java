package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

public enum Environment {

    LOCAL(true),
    DEV(true),
    UAT(false),
    PROD(false);

    private final boolean testEnvironment;

    Environment(boolean testEnvironment) {
        this.testEnvironment = testEnvironment;
    }

    public boolean isTestEnvironment() {
        return testEnvironment;
    }
}
