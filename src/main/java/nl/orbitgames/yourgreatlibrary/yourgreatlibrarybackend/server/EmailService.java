package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.stream.Collectors;

import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.EMAIL_FROM_ADDRESS;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.EMAIL_FROM_NAME;

@Component
public class EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
  
    private final JavaMailSender emailSender;

    private final String fromAddress;
    private final String fromName;

    public EmailService(
            @Autowired JavaMailSender emailSender,
            @Value("${" + EMAIL_FROM_ADDRESS + "}") String fromAddress,
            @Value("${" + EMAIL_FROM_NAME + "}") String fromName
    ) {
        this.emailSender = emailSender;

        this.fromAddress = fromAddress;
        this.fromName = fromName;
    }

    public void sendSimpleMessage(InternetAddress to, String subject, String text) {

        logger.info("Sending simple e-mail to '{}' with subject '{}'", to, subject);

        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(new InternetAddress(fromAddress, fromName));
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);
//            FileSystemResource file
//                    = new FileSystemResource(new File(pathToAttachment));
//            helper.addAttachment("Invoice", file);

            emailSender.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new OrbitGamesSystemException("Could not construct email: " + e.getMessage(), e);
        }
    }

    public void sendMessage(Collection<InternetAddress> toEmails, InternetAddress replyTo, String subject, String text, Collection<AttachmentWrapper> attachments) {

        logger.info("Sending e-mail to '{}' with subject '{}'", toEmails.stream().map(InternetAddress::toString).collect(Collectors.joining(";")), subject);
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(new InternetAddress(fromAddress, fromName));
            InternetAddress[] toEmailsArray = new InternetAddress[toEmails.size()];
            toEmailsArray = toEmails.toArray(toEmailsArray);
            helper.setTo(toEmailsArray);
            helper.setSubject(subject);
            helper.setText(text);
            if (replyTo != null) {
                helper.setReplyTo(replyTo);
            }
            for (AttachmentWrapper attachment: attachments) {
                helper.addAttachment(attachment.getFileName(), attachment.getInputStreamSource());
            }

            emailSender.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new OrbitGamesSystemException("Could not construct email: " + e.getMessage(), e);
        }
    }

    public static class AttachmentWrapper{

        private final InputStreamSource inputStreamSource;
        private final String fileName;

        public AttachmentWrapper(InputStreamSource inputStreamSource, String fileName) {
            this.inputStreamSource = inputStreamSource;
            this.fileName = fileName;
        }

        public InputStreamSource getInputStreamSource() {
            return inputStreamSource;
        }

        public String getFileName() {
            return fileName;
        }
    }
}