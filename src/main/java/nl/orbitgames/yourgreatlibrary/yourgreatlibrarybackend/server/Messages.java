package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

public final class Messages {

    public static final String YOU = "you";
    public static final String CAN_BE_VIEWED_BY = "can-be-viewed-by";
    public static final String FULL_NAME = "full-name";
    public static final String AVATAR_NAME = "avatar-name";
    public static final String EMAIL = "email";
    public static final String STUDENT_NUMBER = "student-number";
    public static final String MINUTES_LATE = "minutes-late";
    public static final String SCORE = "score";
    public static final String WORD_COUNT = "word-count";
    public static final String RECEIVED = "received";
    public static final String GIVEN = "given";
    public static final String TOTAL = "total";
    public static final String INCOMPLETE = "incomplete";

    public static final String DATE_FORMAT = "date-format";
    public static final String TIME_FORMAT = "time-format";
    public static final String DATE_TIME_FORMAT = "date-time-format";


    public static final String RESET_PASSWORD_EMAIL_SUBJECT = "reset-password-email.subject";
    public static final String RESET_PASSWORD_EMAIL_TEXT = "reset-password-email.text";

    public static final String CONVERSATION_NOTIFICATION_EMAIL_SUBJECT = "conversation-notification-email.subject";
    public static final String CONVERSATION_NOTIFICATION_EMAIL_TEXT = "conversation-notification-email.text";

    public static final String BUG_REPORT_NOTIFICATION_EMAIL_SUBJECT = "bug-report-notification-email.subject";
    public static final String BUG_REPORT_NOTIFICATION_EMAIL_TEXT = "bug-report-notification-email.text";

    public static final String DEADLINE_NOTIFICATION_EMAIL_SUBJECT = "deadline-notification-email.subject";
    public static final String DEADLINE_NOTIFICATION_EMAIL_TEXT = "deadline-notification-email.text";
    public static final String DEADLINE_NOTIFICATION_EMAIL_ADDITIONAL_FIRST_STEP = "deadline-notification-email.additional.first-step";

    public static final String DEADLINE_CALENDAR_EVENT_SUMMARY = "deadline-calendar-event.summary";
    public static final String DEADLINE_CALENDAR_EVENT_LOCATION = "deadline-calendar-event.location";
    public static final String DEADLINE_CALENDAR_EVENT_DESCRIPTION = "deadline-calendar-event.description";
}
