package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.LimitedSizeInputStream;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    private final Path dataFolder;

    public FileService(
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DATA_FOLDER + "}") String dataFolder
    ) {
        this.dataFolder = Paths.get(dataFolder);
    }

    /**
     * Stores a file on the disk and returns the path where it is stored
     * @param fileName The name of the file that is being uploaded
     * @param inputStream Input stream containing the file contents
     * @param maxFileSize The maximum size of the file. An error is throw if the file exceeds this size
     * @return The meta data of where the file is stored and can be reached
     */
    public FileMetaData storeFile(String fileName, InputStream inputStream, long maxFileSize) {

        InputStream is;
        if (maxFileSize > 0) {
            is = new LimitedSizeInputStream(inputStream, maxFileSize);
        } else {
            is = inputStream;
        }

        String fileRef = RefGenerator.generate();
        String normalizedFilename = normalize(fileName);
        Path relativeStoragePath = Paths.get(fileRef).resolve(normalizedFilename);
        Path fileStoragePath = dataFolder.resolve(relativeStoragePath);

        try {
            Files.createDirectories(fileStoragePath.getParent());
            Files.copy(is, fileStoragePath);
        } catch (IOException e) {
            throw new OrbitGamesSystemException("Could not copy file: " + e.getMessage(), e);
        }

        return new FileMetaData(fileName, fileRef, fileStoragePath.toString(), relativeStoragePath.toString());
    }

    public InputStream openFile(String fileRef, String fileName) {

        String normalizedFilename = normalize(fileName);
        return openFile(Paths.get(fileRef).resolve(normalizedFilename).toString());

    }

    public InputStream openFile(String relativeStoragePath) {

        Path fileStoragePath = dataFolder.resolve(Paths.get(relativeStoragePath));

        if (!Files.exists(fileStoragePath)) {
            logger.warn("File '{}' does not exist found", fileStoragePath);
            throw new OrbitGamesApplicationException("File could not be found", GenericErrorCode.ENTITY_NOT_FOUND);
        }

        try {
            return new BufferedInputStream(Files.newInputStream(fileStoragePath));
        } catch (IOException e) {
            throw new OrbitGamesSystemException("Could not open file: " + e.getMessage(), e);
        }
    }

    public File getFile(String fileRef, String fileName) {

        String normalizedFilename = normalize(fileName);
        return getFile(Paths.get(fileRef).resolve(normalizedFilename).toString());

    }

    public File getFile(String relativeStoragePath) {

        Path fileStoragePath = dataFolder.resolve(Paths.get(relativeStoragePath));

        if (!Files.exists(fileStoragePath)) {
            logger.warn("File '{}' does not exist found", fileStoragePath);
            throw new OrbitGamesApplicationException("File could not be found", GenericErrorCode.ENTITY_NOT_FOUND);
        }

        return fileStoragePath.toFile();
    }

    /**
     * Checks if the filename has a valid extension, and throws an exception if it is not.
     * @param fileName The name of the file to check
     * @param allowedExtensions The list of allowed extensions. If this list is empty, all extensions are assumed to be allowed.
     */
    public void checkValidExtension(String fileName, Collection<String> allowedExtensions) {

        // An empty list of allowed extensions indicate that all extensions are valid (no filter)
        if (allowedExtensions == null || allowedExtensions.isEmpty()) {
            return;
        }

        String extension = FilenameUtils.getExtension(fileName);
        if(StringUtils.isBlank(extension) || allowedExtensions.stream().noneMatch(extension::equalsIgnoreCase)) {
            throw new OrbitGamesApplicationException("Invalid file extension: '" + extension + "', should be one of " +
                    allowedExtensions.stream().map(e -> "'" + e + "'").collect(Collectors.joining(", ")),
                    GenericErrorCode.CONSTRAINT_VIOLATION);
        }

    }

    public void checkIsValidFileOfType(String fileStoragePath, Collection<String> allowedMimeTypes) {

        // If the list of allowed MIME types is empty, any type of file is allowed; short-circuit
        if (allowedMimeTypes.isEmpty()) {
            return;
        }

        Path file = Paths.get(fileStoragePath);
        try {
            Tika tika = new Tika();
            String contentType = tika.detect(file);
            if (StringUtils.isBlank(contentType)) {
                throw new OrbitGamesApplicationException("Invalid file: The MIME type of the uploaded file could not be determined", GenericErrorCode.CONSTRAINT_VIOLATION);
            }
            if (allowedMimeTypes.stream().noneMatch(contentType::equalsIgnoreCase)) {
                throw new OrbitGamesApplicationException("Invalid file: The MIME type of the uploaded file '" + contentType + "' is not valid", GenericErrorCode.CONSTRAINT_VIOLATION);
            }

            // For PDF files, we check if we can successfully load the file
            if (contentType.equalsIgnoreCase("application/pdf")) {
                try {
                    PdfReader pdfReader = new PdfReader(fileStoragePath);
                    try {
                        // As a smoke check, we try to extract the text on the first page of the document:
                        PdfTextExtractor.getTextFromPage(pdfReader, 1);
                    } finally {
                        pdfReader.close();
                    }
                } catch (Exception e) {
                    throw new OrbitGamesApplicationException("Failed to read PDF. Is the PDF encrypted?", GenericErrorCode.CONSTRAINT_VIOLATION, e);
                }
            }
        } catch (IOException e) {
            throw new OrbitGamesSystemException("Error while trying to determine MIME type of file '" + fileStoragePath + "': " + e.getMessage(), e);
        }
    }

    private String normalize(String fileName) {
        return fileName.replaceAll("[^a-zA-Z0-9_. \\-]", "_");
    }

    public static class FileMetaData {
        private final String fileName;
        private final String fileRef;
        private final String storagePath;
        private final String relativeStoragePath;

        FileMetaData(String fileName, String fileRef, String storagePath, String relativeStoragePath) {
            this.fileName = fileName;
            this.fileRef = fileRef;
            this.storagePath = storagePath;
            this.relativeStoragePath = relativeStoragePath;
        }

        public String getFileName() {
            return fileName;
        }

        public String getFileRef() {
            return fileRef;
        }

        public String getStoragePath() {
            return storagePath;
        }

        public String getRelativeStoragePath() {
            return relativeStoragePath;
        }
    }
}
