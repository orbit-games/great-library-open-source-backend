package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.CourseTemplateController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.coursetemplate.CourseTemplateParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DefaultTemplatesInitializer {

    private final Logger logger = LoggerFactory.getLogger(DefaultTemplatesInitializer.class);

    private final ApplicationContext applicationContext;

    private final CourseTemplateController courseTemplateController;

    private final List<String> defaultCourseTemplatePaths;
    private final String contentRoot;

    public DefaultTemplatesInitializer(
            @Autowired ApplicationContext applicationContext,
            @Autowired CourseTemplateController courseTemplateController,

            @Value("${" + YourGreatLibraryBackendApplicationProperties.COURSE_TEMPLATES_DEFAULTS + "}") String defaultCourseTemplates,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.COURSE_TEMPLATES_CONTENT_ROOT + "}") String contentRoot
    ) {

        this.applicationContext = applicationContext;

        this.courseTemplateController = courseTemplateController;

        this.defaultCourseTemplatePaths = Collections.unmodifiableList(
                Arrays.stream(defaultCourseTemplates.split(","))
                        .map(String::trim)
                        .filter(StringUtils::isNotBlank)
                        .collect(Collectors.toList()));
        this.contentRoot = contentRoot;
    }

    @PostConstruct
    public void init() {

        createTemplatesIfNotExists();
    }

    private void createTemplatesIfNotExists() {

        for(String templatePath: defaultCourseTemplatePaths) {

            // TODO move this to the controller?
            ContentPropertiesReader courseTemplateReader1 = new ContentPropertiesReader(applicationContext, templatePath + "/messages.properties");
            CourseTemplateParser courseTemplateParser = new CourseTemplateParser(courseTemplateReader1, contentRoot);

            String templateName = courseTemplateParser.getTemplateName();
            String defaultTemplateId = courseTemplateController.getDefaultId(templateName);

            if (!courseTemplateController.existsCourseTemplate(defaultTemplateId)) {

                try {
                    ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(applicationContext);
                    String searchableTemplatePath = templatePath + "/**";
                    if (searchableTemplatePath.startsWith("classpath:")) {
                        searchableTemplatePath = "classpath*:" + searchableTemplatePath.substring("classpath:".length());
                    }
                    Resource[] templateResources = resolver.getResources(searchableTemplatePath);
                    List<CourseTemplateController.TemplateFile> templateFiles = new ArrayList<>(templateResources.length);

                    try {
                        for (Resource templateResource : templateResources) {
                            if (templateResource.getFilename() == null || StringUtils.isBlank(templateResource.getFilename())) {
                                continue;
                            }
                            templateFiles.add(new CourseTemplateController.TemplateFile(templateResource.getFilename(), templateResource.getInputStream()));
                        }

                        courseTemplateController.createTemplate(defaultTemplateId, templateFiles);
                    } finally {
                        for (CourseTemplateController.TemplateFile templateFile : templateFiles) {
                            try {
                                templateFile.getInputStream().close();
                            } catch(IOException e) {
                                logger.error("Error while trying to close a resource file stream", e);
                            }
                        }
                    }
                } catch (IOException e) {
                    throw new OrbitGamesSystemException("Can't read template path '" + templatePath + "'", e);
                }

            }

        }
    }
}
