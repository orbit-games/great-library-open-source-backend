package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

public final class JobTriggers {

    public static final String GENERATE_REVIEWER_RELATIONS_TRIGGER = "GenerateReviewerRelations";
    public static final String SEND_DEADLINE_NOTIFICATION_EMAIL_TRIGGER = "SendDeadlineNotificationEmailTrigger";
}
