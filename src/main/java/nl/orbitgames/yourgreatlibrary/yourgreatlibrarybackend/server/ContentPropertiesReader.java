package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * This is a helper class for reading "content properties". These are special properties files,
 * where the properties are used to store text-content. This content can either be stored
 * directly in the "messages.properties" file, or the messages.properties file may store
 * a link to the actual file location (relative to the content directory)
 */
public class ContentPropertiesReader {

    private final ResourceLoader resourceLoader;
    /**
     * The path where the messages.properties file is stored
     */
    private final String contentMessagesPath;
    private final Properties contentMessages;
    private final Resource messagesResource;

    /**
     *
     * @param resourceLoader The resource loader to use
     * @param contentMessagesPath The path to the original messages file
     */
    public ContentPropertiesReader(
            ResourceLoader resourceLoader,
            String contentMessagesPath
    ) {
        this.resourceLoader = resourceLoader;
        this.contentMessagesPath = contentMessagesPath;
        this.messagesResource = resourceLoader.getResource(contentMessagesPath);
        try (InputStream is = messagesResource.getInputStream()) {
            Properties messages = new Properties();
            messages.load(is);
            this.contentMessages = messages;
        } catch (IOException e) {
            throw new OrbitGamesSystemException("Could not read resource '" + contentMessagesPath + "'", e);
        }
    }

    public String getMessage(String key) {
        String directValue = contentMessages.getProperty(key);
        if (directValue == null) {
            return null;
        }
        if (directValue.startsWith("file:")) {
            return readFromFile(directValue);

        } else {
            return directValue;
        }
    }

    private String readFromFile(String directValue) {
        String filePath = directValue.substring("file:".length());
        Resource fileResource;
        if (!filePath.startsWith("/") && !filePath.contains(":")) {
            try {
                fileResource = messagesResource.createRelative(filePath);
            } catch (IOException e) {
                throw new OrbitGamesSystemException("Could not load relative resource "+ filePath, e);
            }
        } else {
            fileResource = resourceLoader.getResource(filePath);
        }
        try (InputStream is = fileResource.getInputStream()) {
            String rawFileContent = IOUtils.toString(is, StandardCharsets.UTF_8);
            if (!rawFileContent.isEmpty() && rawFileContent.charAt(0) == 0xFEFF) {
                // Skip the first character if it's a UTF-8 BOM character
                return rawFileContent.substring(1);
            } else {
                return rawFileContent;
            }
        } catch (IOException e) {
            throw new OrbitGamesSystemException("Could not read resource '" + filePath + "'", e);
        }
    }

}
