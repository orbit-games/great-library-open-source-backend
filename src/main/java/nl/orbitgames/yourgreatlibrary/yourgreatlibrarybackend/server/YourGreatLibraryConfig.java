package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationGenerationType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.BUG_REPORT_EMAILS;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.CONVERSATION_NOTIFICATION_EMAIL_URL;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE_DELAY;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_MARK_COMPLETE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_INCLUDE_LATE_PEERS;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_REVIEWER_COUNT;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEWER_RELATION_GENERATION_DEFAULT_TYPE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEW_STEP_UPLOAD_ALLOWED_FILE_TYPE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEW_STEP_UPLOAD_ALLOWED_MIME_TYPE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.REVIEW_STEP_UPLOAD_MAX_SIZE_BYTES;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.UPLOAD_ALLOWED_FILE_TYPE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.UPLOAD_ALLOWED_MIME_TYPE;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.UPLOAD_MAX_SIZE_BYTES;
import static nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties.USER_ALLOWED_EMAIL_DOMAINS;

@Component
public class YourGreatLibraryConfig {

    private final Environment applicationEnvironment;

    private final long reviewStepUploadMaxSize;
    private final List<String> reviewStepUploadAllowedFileTypes;
    private final List<String> reviewStepUploadAllowedMimeTypes;

    private final long contentUploadMaxSize;
    private final List<String> contentUploadAllowedFileTypes;
    private final List<String> contentUploadAllowedMimeTypes;

    private final Set<String> userAllowedEmailDomains;

    private final String conversationNotificationEmailUrlTemplate;

    private final List<String> bugReportEmails;

    private final ReviewerRelationGenerationType defaultReviewerRelationGenerationType;
    private final int defaultReviewerRelationGenerationReviewerCount;
    private final boolean defaultReviewerRelationGenerationAutoGenerate;
    private final long defaultReviewerRelationGenerationAutoGenerateDelay;
    private final boolean defaultReviewerRelationGenerationAutoMarkComplete;
    private final boolean defaultReviewerRelationGenerationIncludeLatePeers;

    public YourGreatLibraryConfig(
            @Value("${" + YourGreatLibraryBackendApplicationProperties.APPLICATION_ENVIRONMENT + "}") Environment applicationEnvironment,
            @Value("${" + REVIEW_STEP_UPLOAD_MAX_SIZE_BYTES + "}") long reviewStepUploadMaxSize,
            @Value("${" + REVIEW_STEP_UPLOAD_ALLOWED_FILE_TYPE + "}") String reviewStepUploadAllowedFileTypes,
            @Value("${" + REVIEW_STEP_UPLOAD_ALLOWED_MIME_TYPE + "}") String reviewStepUploadAllowedMimeTypes,
            @Value("${" + UPLOAD_MAX_SIZE_BYTES + "}") long contentUploadMaxSize,
            @Value("${" + UPLOAD_ALLOWED_FILE_TYPE + "}") String contentUploadAllowedFileTypes,
            @Value("${" + UPLOAD_ALLOWED_MIME_TYPE+ "}") String contentUploadAllowedMimeTypes,
            @Value("${" + USER_ALLOWED_EMAIL_DOMAINS + "}") String userAllowedEmailDomains,
            @Value("${" + CONVERSATION_NOTIFICATION_EMAIL_URL + "}") String conversationNotificationEmailUrlTemplate,
            @Value("${" + BUG_REPORT_EMAILS + "}") String bugReportEmails,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_TYPE + "}") ReviewerRelationGenerationType defaultReviewerRelationGenerationType,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_REVIEWER_COUNT + "}") int defaultReviewerRelationGenerationReviewerCount,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE + "}") boolean defaultReviewerRelationGenerationAutoGenerate,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_GENERATE_DELAY + "}") long defaultReviewerRelationGenerationAutoGenerateDelay,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_AUTO_MARK_COMPLETE + "}") boolean defaultReviewerRelationGenerationAutoMarkComplete,
            @Value("${" + REVIEWER_RELATION_GENERATION_DEFAULT_INCLUDE_LATE_PEERS + "}") boolean defaultReviewerRelationGenerationIncludeLatePeers
    ) {
        this.applicationEnvironment = applicationEnvironment;

        this.reviewStepUploadMaxSize = reviewStepUploadMaxSize;
        this.reviewStepUploadAllowedFileTypes = parseCommaSeparatedList(reviewStepUploadAllowedFileTypes);
        this.reviewStepUploadAllowedMimeTypes = parseCommaSeparatedList(reviewStepUploadAllowedMimeTypes);

        this.contentUploadMaxSize = contentUploadMaxSize;
        this.contentUploadAllowedFileTypes = parseCommaSeparatedList(contentUploadAllowedFileTypes);
        this.contentUploadAllowedMimeTypes = parseCommaSeparatedList(contentUploadAllowedMimeTypes);

        this.userAllowedEmailDomains = parseCommaSeparatedSet(userAllowedEmailDomains);
        this.conversationNotificationEmailUrlTemplate = conversationNotificationEmailUrlTemplate;

        this.bugReportEmails = parseCommaSeparatedList(bugReportEmails);

        this.defaultReviewerRelationGenerationType = defaultReviewerRelationGenerationType;
        this.defaultReviewerRelationGenerationReviewerCount = defaultReviewerRelationGenerationReviewerCount;
        this.defaultReviewerRelationGenerationAutoGenerate = defaultReviewerRelationGenerationAutoGenerate;
        this.defaultReviewerRelationGenerationAutoGenerateDelay = defaultReviewerRelationGenerationAutoGenerateDelay;
        this.defaultReviewerRelationGenerationAutoMarkComplete = defaultReviewerRelationGenerationAutoMarkComplete;
        this.defaultReviewerRelationGenerationIncludeLatePeers = defaultReviewerRelationGenerationIncludeLatePeers;

    }

    private List<String> parseCommaSeparatedList(String str) {
        return Collections.unmodifiableList(parseCommaSeparated(str).collect(Collectors.toList()));
    }

    private Set<String> parseCommaSeparatedSet(String str) {
        return Collections.unmodifiableSet(parseCommaSeparated(str).collect(Collectors.toSet()));
    }

    private Stream<String> parseCommaSeparated(String str) {
        return StringUtils.isBlank(str) ? Stream.empty() : Arrays.stream(str.split(",")).map(String::trim).filter(StringUtils::isNotBlank);
    }

    public Environment getApplicationEnvironment() {
        return applicationEnvironment;
    }

    public boolean isTestApplicationEnvironment() {
        return applicationEnvironment.isTestEnvironment();
    }

    public long getReviewStepUploadMaxSize() {
        return reviewStepUploadMaxSize;
    }

    public List<String> getReviewStepUploadAllowedFileTypes() {
        return reviewStepUploadAllowedFileTypes;
    }

    public List<String> getReviewStepUploadAllowedMimeTypes() {
        return reviewStepUploadAllowedMimeTypes;
    }

    public long getContentUploadMaxSize() {
        return contentUploadMaxSize;
    }

    public List<String> getContentUploadAllowedFileTypes() {
        return contentUploadAllowedFileTypes;
    }

    public List<String> getContentUploadAllowedMimeTypes() {
        return contentUploadAllowedMimeTypes;
    }

    public Set<String> getUserAllowedEmailDomains() {
        return userAllowedEmailDomains;
    }

    public String getConversationNotificationEmailUrlTemplate() {
        return conversationNotificationEmailUrlTemplate;
    }

    public String getConversationNotificationEmailUrl(String courseRef, String conversationRef) {
        return conversationNotificationEmailUrlTemplate
                .replace("{courseRef}", courseRef)
                .replace("{conversationRef}", conversationRef);
    }

    public List<String> getBugReportEmails() {
        return bugReportEmails;
    }

    public ReviewerRelationGenerationSettings getDefaultReviewerRelationGenerationSetting() {

        ReviewerRelationGenerationSettings defaultSettings = new ReviewerRelationGenerationSettings();
        defaultSettings.setGenerationType(defaultReviewerRelationGenerationType);
        defaultSettings.setReviewerCount(defaultReviewerRelationGenerationReviewerCount);
        defaultSettings.setAutoGenerate(defaultReviewerRelationGenerationAutoGenerate);
        defaultSettings.setAutoGenerateDelay(defaultReviewerRelationGenerationAutoGenerateDelay);
        defaultSettings.setIncludeLatePeers(defaultReviewerRelationGenerationIncludeLatePeers);
        defaultSettings.setAutoMarkComplete(defaultReviewerRelationGenerationAutoMarkComplete);
        return defaultSettings;
    }
}
