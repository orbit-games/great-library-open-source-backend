package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository.UserRepository;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.PasswordHashUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.RefGenerator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

/**
 * Responsible for creating the default admin account, which is needed to initially set up the rest of the course
 */
@Component
public class DefaultAdminInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DefaultAdminInitializer.class);

    private final String defaultAdminUsername;
    private final String defaultAdminEmail;
    private final String defaultAdminFullName;
    private final String defaultAdminPassword;

    private final UserRepository userRepository;

    public DefaultAdminInitializer(
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DEFAULT_ADMIN_USERNAME + "}") String defaultAdminUsername,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DEFAULT_ADMIN_EMAIL + "}") String defaultAdminEmail,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DEFAULT_ADMIN_PASSWORD + "}") String defaultAdminPassword,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DEFAULT_ADMIN_FULL_NAME + "}") String defaultAdminFullName,
            @Autowired UserRepository userRepository
    ) {
        this.defaultAdminUsername = defaultAdminUsername;
        this.defaultAdminEmail = defaultAdminEmail;
        this.defaultAdminFullName = defaultAdminFullName;
        this.defaultAdminPassword = defaultAdminPassword;

        this.userRepository = userRepository;
    }

    @PostConstruct
    public void init() {

        if (StringUtils.isBlank(defaultAdminUsername) || StringUtils.isBlank(defaultAdminPassword)) {
            return;
        }

        Optional<User> existingUser = userRepository.findByUsername(defaultAdminUsername);
        if (existingUser.isPresent()) {
            if (existingUser.get().getRole() == Role.ADMINISTRATOR) {
                logger.info("The default administrator user already exists; S");
            } else {
                logger.warn("A user with the default administrator username already exists, but is not an administrator...");
            }
            return;
        }

        logger.info("Creating default admin with username '" + defaultAdminUsername + "'...");
        User user = new User();
        user.setUsername(defaultAdminUsername);
        user.setEmail(defaultAdminEmail);
        user.setFullName(defaultAdminFullName);
        user.setPasswordHash(PasswordHashUtils.hashPassword(defaultAdminPassword));
        user.setRole(Role.ADMINISTRATOR);
        user.setRef(RefGenerator.generate());

        userRepository.save(user);
        logger.info("Created default admin with username '" + defaultAdminUsername + "'...");
    }
}
