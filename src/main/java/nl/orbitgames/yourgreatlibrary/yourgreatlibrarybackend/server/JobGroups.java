package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server;

public final class JobGroups {

    public static final String GENERATE_REVIEWER_RELATIONS_JOB = "GenerateReviewerRelations";
    public static final String SEND_DEADLINE_NOTIFICATION_EMAIL_JOB = "SendDeadlineNotificationEmail";

}
