package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.FileUploadMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.file.FileController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.FileService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@Component
@Path("file")
@Timed
public class FileBd {

    private static final Logger logger = LoggerFactory.getLogger(FileBd.class);

    private final FileController fileController;
    private final FileService fileService;
    private final DomainToRestModelConverter domainToRestModelConverter;

    public FileBd(
            @Autowired FileController fileController,
            @Autowired FileService fileService,
            @Autowired DomainToRestModelConverter domainToRestModelConverter
    ) {
        this.fileController = fileController;
        this.fileService = fileService;
        this.domainToRestModelConverter = domainToRestModelConverter;
    }

    @Path("{ref}/{fileName}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
//    @RolesAllowed(SecurityRole.LOGGED_IN)
    @PermitAll
    public Response downloadFile(@PathParam("ref") String ref, @Encoded @PathParam("fileName") String encodedFileName) {

        try {
            // TODO: check if the logged in user actually has access to this file (besides apparently knowing the URL)

            // When using the path parameters for the filename directly, spaces are not decoded (and are left as '+',
            // so URLDecoder is used here instead.
            String fileName = URLDecoder.decode(encodedFileName, StandardCharsets.UTF_8.name());

            logger.debug("Decoded file name '{}' to '{}'", encodedFileName, fileName);

            InputStream fileInputStream = fileService.openFile(ref, fileName);

            return Response.ok(fileInputStream)
                    .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
                    .build();
        } catch (UnsupportedEncodingException e) {
            throw new OrbitGamesSystemException(e);
        }
    }

    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public FileUploadMetadata uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail
    ) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata fileMetadata = fileController
                .uploadFile(uploadedInputStream, fileDetail.getFileName());

        return domainToRestModelConverter.convert(fileMetadata);
    }
}
