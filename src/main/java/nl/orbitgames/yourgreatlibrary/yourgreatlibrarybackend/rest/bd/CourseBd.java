package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.Chapter;
import io.swagger.model.Conversation;
import io.swagger.model.ConversationMessage;
import io.swagger.model.Conversations;
import io.swagger.model.Course;
import io.swagger.model.CourseAmendFromTemplateRequest;
import io.swagger.model.CourseDeadlines;
import io.swagger.model.CourseProgressExportSpecification;
import io.swagger.model.CourseProgressSummary;
import io.swagger.model.CourseResource;
import io.swagger.model.CourseResources;
import io.swagger.model.CourseTemplates;
import io.swagger.model.CourseUpdateRequest;
import io.swagger.model.CreateCourseFromTemplateRequest;
import io.swagger.model.PostConversationMessageRequest;
import io.swagger.model.PostConversationReadRequest;
import io.swagger.model.Property;
import io.swagger.model.ResourceRelation;
import io.swagger.model.ReviewStep;
import io.swagger.model.ReviewerRelation;
import io.swagger.model.Section;
import io.swagger.model.Sections;
import io.swagger.model.Skill;
import io.swagger.model.Skills;
import io.swagger.model.UpdateConversationEmailNotificationTypeRequest;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.conversation.ConversationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.CourseController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.CourseTemplateController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.CourseProgressController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourseProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DeadlineICalConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.ProgressExportConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.RestToDomainModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Path("course")
@Produces("application/json")
@Consumes("application/json")
@Timed
public class CourseBd {

    private final CourseController courseController;
    private final CourseTemplateController courseTemplateController;
    private final CourseProgressController courseProgressController;
    private final ConversationController conversationController;

    private final RestToDomainModelConverter restToDomainModelConverter;
    private final DomainToRestModelConverter domainToRestModelConverter;
    private final ProgressExportConverter progressExportConverter;
    private final DeadlineICalConverter deadlineICalConverter;


    @Autowired
    public CourseBd(
            CourseController courseController,
            CourseTemplateController courseTemplateController,
            CourseProgressController courseProgressController,
            ConversationController conversationController,
            RestToDomainModelConverter restToDomainModelConverter,
            DomainToRestModelConverter domainToRestModelConverter,
            ProgressExportConverter progressExportConverter,
            DeadlineICalConverter deadlineICalConverter
    ) {
        this.courseController = courseController;
        this.courseTemplateController = courseTemplateController;
        this.courseProgressController = courseProgressController;
        this.conversationController = conversationController;
        this.restToDomainModelConverter = restToDomainModelConverter;
        this.domainToRestModelConverter = domainToRestModelConverter;
        this.progressExportConverter = progressExportConverter;
        this.deadlineICalConverter = deadlineICalConverter;
    }

    @Path("{courseRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Course getCourse(@PathParam("courseRef") String courseRef, @QueryParam("includeDisabled") boolean includeDisabled) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course course = courseController.getCourse(courseRef);
        return domainToRestModelConverter.convert(course, includeDisabled);
    }

    @Path("{courseRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public Course updateCourse(@PathParam("courseRef") String courseRef, CourseUpdateRequest request) {

        return domainToRestModelConverter.convert(
                courseController.updateCourse(
                        courseRef,
                        restToDomainModelConverter.convert(request)
                ), true
        );
    }

    @Path("template")
    @GET
    @RolesAllowed(SecurityRole.TEACHER)
    public CourseTemplates getCourseTemplates() {

        return domainToRestModelConverter.convertToCourseTemplates(
                courseTemplateController.getCourseTemplates()
        );
    }

    @Path("from-template")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public Course createCourseFromTemplate(CreateCourseFromTemplateRequest request) {

        return domainToRestModelConverter.convert(
                courseController.createCourseFromTemplate(
                        request.getTemplateId(), request.getTemplateVersion() == null ? -1 : request.getTemplateVersion(),
                        request.getCourseName(), request.getYear(), request.getQuarter(),
                        request.getCourseCode(), request.isisAnonymousUsers(), request.isisAnonymousReviews(),
                        request.getDeadlinesOverride()),
                true
        );
    }

    @Path("{courseRef}/amend-from-template")
    @PUT
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public Course courseAmendFromTemplate(@PathParam("courseRef") String courseRef, CourseAmendFromTemplateRequest request) {

        return domainToRestModelConverter.convert(
                courseController.amendCourseFromTemplate(
                        request.getTemplateId(), request.getTemplateVersion() == null ? -1 : request.getTemplateVersion(),courseRef,
                        request.getSections()),
                true
        );
    }

    @Path("{courseRef}/chapter/{chapterRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Chapter getCourseChapter(@PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef, @QueryParam("includeDisabled") boolean includeDisabled) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter chapter = courseController.getChapter(courseRef, chapterRef);
        return domainToRestModelConverter.convert(chapter, includeDisabled);
    }

    @Path("{courseRef}/chapter/{chapterRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public Chapter updateCourseChapter(@PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef, Chapter chapterWithUpdates) {

        return domainToRestModelConverter.convert(
                courseController.updateChapter(
                        courseRef,
                        chapterRef,
                        restToDomainModelConverter.convert(chapterWithUpdates)
                ),
                true
        );
    }

    @Path("{courseRef}/chapter/{chapterRef}/review-step/{reviewStepRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStep getReviewStep(@PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef, @PathParam("reviewStepRef") String reviewStepRef) {

        return domainToRestModelConverter.convert(courseController.getReviewStep(courseRef, chapterRef, reviewStepRef));
    }

    @Path("{courseRef}/chapter/{chapterRef}/review-step/{reviewStepRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public ReviewStep getReviewStep(
            @PathParam("courseRef") String courseRef,
            @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewStepRef") String reviewStepRef,
            ReviewStep reviewStepWithUpdates
            ) {

        return domainToRestModelConverter.convert(
                courseController.updateReviewStep(courseRef, chapterRef, reviewStepRef,
                        restToDomainModelConverter.convert(reviewStepWithUpdates)
                )
        );
    }

    @Path("{courseRef}/chapter/{chapterRef}/reviewer-relations")
    @GET
    @RolesAllowed(SecurityRole.TEACHER)
    public List<ReviewerRelation> getCourseChapterReviewerRelations(@PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef) {

        List<ReviewerRelationResult> relationResults = courseProgressController.getReviewerRelationResults(courseRef, chapterRef);
        return relationResults.stream().map(domainToRestModelConverter::convert).collect(Collectors.toList());

    }

    @Path("{courseRef}/chapter/{chapterRef}/reviewer-relations/{reviewerRelationRef}")
    @GET
    @RolesAllowed(SecurityRole.TEACHER)
    public ReviewerRelation getCourseChapterReviewerRelation(
            @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef, @PathParam("reviewerRelationRef") String reviewerRelationRef) {

        ReviewerRelationResult relationResult = courseProgressController.getReviewerRelationResult(courseRef, chapterRef, reviewerRelationRef);
        return domainToRestModelConverter.convert(relationResult);
    }

    @Path("{courseRef}/progress-export")
    @POST
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @RolesAllowed(SecurityRole.TEACHER)
    public Response getProgressExport(@PathParam("courseRef") String courseRef, CourseProgressExportSpecification exportSpecification) throws IOException {

        // We retrieve all reviewer relation results:
        List<ReviewerRelationResult> relationResults = courseProgressController.getReviewerRelationResults(courseRef);
        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course course = courseController.getCourse(courseRef);

        String filename =
                OffsetDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "_"
                        + course.getName().replaceAll("\\s+", "_")
                        + ".xlsx";

        ByteArrayOutputStream xlsxOutputStream = new ByteArrayOutputStream();
        progressExportConverter.writeProgressExport(xlsxOutputStream, course, relationResults, exportSpecification);

        return Response.ok(xlsxOutputStream.toByteArray())
                .header("Content-Disposition", "attachment; filename=\"" + filename + "\"")
                .build();
    }

    @Path("{courseRef}/user/{userRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseProgressSummary getCourseUser(@PathParam("courseRef") String courseRef, @PathParam("userRef") String userRef) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse userCourse = courseProgressController.getUserCourse(courseRef, userRef);
        return domainToRestModelConverter.convertToCourseProgressSummary(userCourse);
    }

    @Path("{courseRef}/user/{userRef}")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseProgressSummary updateCourseUser(@PathParam("courseRef") String courseRef, @PathParam("userRef") String userRef, CourseProgressSummary courseProgressSummaryWithUpdates) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse userCourseWithUpdates = restToDomainModelConverter.convert(courseProgressSummaryWithUpdates);
        UserCourse updatedUserCourse = courseProgressController.updateUserCourse(courseRef, userRef, userCourseWithUpdates);

        return domainToRestModelConverter.convertToCourseProgressSummary(updatedUserCourse);
    }


    @Path("{courseRef}/user/{userRef}/property/{propertyKey}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Property getCourseUserProperty(@PathParam("courseRef") String courseRef, @PathParam("userRef") String userRef, @PathParam("propertyKey") String propertyKey) {

        UserCourseProperty property = courseProgressController.getUserCourseProperty(courseRef, userRef, propertyKey);
        return new Property()
                .key(propertyKey)
                .value(property == null ? null : property.getPropertyValue());
    }

    @Path("{courseRef}/user/{userRef}/property/{propertyKey}")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Property updateCourseUserProperty(@PathParam("courseRef") String courseRef, @PathParam("userRef") String userRef, @PathParam("propertyKey") String propertyKey, Property propertyWithUpdates) {

        UserCourseProperty updatedProperty = courseProgressController.updateUserCourseProperty(courseRef, userRef, propertyKey, propertyWithUpdates.getKey(), propertyWithUpdates.getValue());
        return new Property()
                .key(propertyKey)
                .value(updatedProperty.getPropertyValue());
    }

    @Path("{courseRef}/chapter/{chapterRef}/generate-reviewer-relations")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public void generateReviewerRelations(@PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef) {

        courseProgressController.generateReviewerRelations(courseRef, chapterRef);
    }

    @Path("{courseRef}/section")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Sections getSections(@PathParam("courseRef") String courseRef) {

        return domainToRestModelConverter.convertToSections(
                courseController.getSections(courseRef));

    }

    @Path("{courseRef}/section")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public Section createSection(@PathParam("courseRef") String courseRef, Section section) {

        return domainToRestModelConverter.convert(
                courseController.createSection(
                        courseRef,
                        restToDomainModelConverter.convert(section)));

    }

    @Path("{courseRef}/section/{sectionRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Section getSection(@PathParam("courseRef") String courseRef, @PathParam("sectionRef") String sectionRef) {

        return domainToRestModelConverter.convert(
                courseController.getSection(courseRef, sectionRef)
        );
    }

    @Path("{courseRef}/section/{sectionRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public Section updateSection(@PathParam("courseRef") String courseRef, @PathParam("sectionRef") String sectionRef, Section section) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section updatedSection =
                courseController.updateSection(courseRef, sectionRef, restToDomainModelConverter.convert(section));
        return domainToRestModelConverter.convert(updatedSection);
    }

    @Path("{courseRef}/section/{sectionRef}")
    @DELETE
    @RolesAllowed(SecurityRole.TEACHER)
    public Section deleteSection(@PathParam("courseRef") String courseRef, @PathParam("sectionRef") String sectionRef) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section deletedSection =
                courseController.deleteSection(courseRef, sectionRef);
        return domainToRestModelConverter.convert(deletedSection);
    }

    @Path("{courseRef}/resource")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseResources getCourseResources(@PathParam("courseRef") String courseRef) {

        return domainToRestModelConverter.convertToCourseResources(
                courseController.getResources(courseRef)
        );
    }

    @Path("{courseRef}/resource")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public CourseResource createCourseResource(@PathParam("courseRef") String courseRef, CourseResource resourceToAdd) {

        return domainToRestModelConverter.convert(
                courseController.createResource(
                        courseRef,
                        restToDomainModelConverter.convert(resourceToAdd)
                )
        );
    }

    @Path("{courseRef}/resource/{resourceRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public CourseResource updateCourseResource(@PathParam("courseRef") String courseRef, @PathParam("resourceRef") String resourceRef, CourseResource courseResourceWithUpdates) {

        Resource updatedResource = courseController.updateResource(courseRef, resourceRef,
                restToDomainModelConverter.convert(courseResourceWithUpdates));

        return domainToRestModelConverter.convert(updatedResource);
    }

    @Path("{courseRef}/resource/{resourceRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseResource getCourseResource(@PathParam("courseRef") String courseRef, @PathParam("resourceRef") String resourceRef) {

        return domainToRestModelConverter.convert(courseController.getResource(courseRef, resourceRef));
    }

    @Path("{courseRef}/resource/{resourceRef}")
    @DELETE
    @RolesAllowed(SecurityRole.TEACHER)
    public CourseResource deleteCourseResource(@PathParam("courseRef") String courseRef, @PathParam("resourceRef") String resourceRef) {

        return domainToRestModelConverter.convert(courseController.deleteResource(courseRef, resourceRef));
    }

    @Path("{courseRef}/section/{sectionRef}/resource")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public ResourceRelation createResourceRelation(@PathParam("courseRef") String courseRef, @PathParam("sectionRef") String sectionRef, ResourceRelation resourceRelation) {

        return domainToRestModelConverter.convert(
                courseController.createSectionResource(
                        courseRef, sectionRef,
                        restToDomainModelConverter.convert(resourceRelation)
                )
        );
    }

    @Path("{courseRef}/skill")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Skills getSkills(@PathParam("courseRef") String courseRef) {

        return domainToRestModelConverter.convert(
                courseController.getSkills(courseRef)
        );
    }

    @Path("{courseRef}/skill")
    @POST
    @RolesAllowed(SecurityRole.TEACHER)
    public Skill createSkill(@PathParam("courseRef") String courseRef, Skill skill) {

        return domainToRestModelConverter.convert(
                courseController.createSkill(
                        courseRef,
                        restToDomainModelConverter.convert(skill)
                )
        );
    }

    @Path("{courseRef}/skill/{skillRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Skill getSkill(@PathParam("courseRef") String courseRef, @PathParam("skillRef") String skillRef) {

        return domainToRestModelConverter.convert(
                courseController.getSkill(courseRef, skillRef)
        );
    }

    @Path("{courseRef}/skill/{skillRef}")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public Skill updateSkill(@PathParam("courseRef") String courseRef, @PathParam("skillRef") String skillRef, Skill skillWithUpdates) {

        return domainToRestModelConverter.convert(
                courseController.updateSkill(courseRef, skillRef,
                        restToDomainModelConverter.convert(skillWithUpdates)
                )
        );
    }

    @Path("{courseRef}/skill/{skillRef}")
    @DELETE
    @RolesAllowed(SecurityRole.TEACHER)
    public Skill deleteSkill(@PathParam("courseRef") String courseRef, @PathParam("skillRef") String skillRef) {

        return domainToRestModelConverter.convert(
                courseController.deleteSkill(courseRef, skillRef)
        );
    }

    @Path("{courseRef}/conversation")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Conversations getConversations(@PathParam("courseRef") String courseRef, @QueryParam("updated") String updatedString,
                                          @QueryParam("messageLimit") Long messageLimit, @QueryParam("conversations") String conversationRefsString) {

        Set<String> conversationRefs = conversationRefsString == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(conversationRefsString.split(",")));
        OffsetDateTime updated = updatedString == null
                ? null
                : OffsetDateTime.parse(updatedString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        List<ConversationWrapper> conversations = this.conversationController.getConversations(courseRef, updated,
                messageLimit == null ? 10L : messageLimit,
                conversationRefs);

        return domainToRestModelConverter.convertConversations(conversations);
    }

    @Path("{courseRef}/conversation")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Conversation createConversation(@PathParam("courseRef") String courseRef, Conversation conversation) {

        ConversationWrapper createdConversation = conversationController.createConversation(courseRef, restToDomainModelConverter.convert(conversation));
        return domainToRestModelConverter.convert(createdConversation);

    }

    @Path("{courseRef}/conversation/{conversationRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Conversation getConversation(@PathParam("courseRef") String courseRef, @PathParam("conversationRef") String conversationRef,
                                         @QueryParam("messageSkip") Long messageSkip, @QueryParam("messageLimit") Long messageLimit, @QueryParam("updated") String updatedString) {

        OffsetDateTime updated = updatedString == null
                ? null
                : OffsetDateTime.parse(updatedString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        ConversationWrapper conversation = this.conversationController.getConversation(courseRef, conversationRef,
                messageSkip == null ? 0 : messageSkip,
                messageLimit == null ? 10 : messageLimit,
                updated);

        return domainToRestModelConverter.convert(conversation);
    }

    @Path("{courseRef}/conversation/{conversationRef}/email-notification-type")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public void updateConversationEmailNotificationType(@PathParam("courseRef") String courseRef, @PathParam("conversationRef") String conversationRef, UpdateConversationEmailNotificationTypeRequest request) {

        this.conversationController.updateEmailNotificationType(courseRef, conversationRef, restToDomainModelConverter.convert(request.getEmailNotificationType()));

    }

    @Path("{courseRef}/conversation/{conversationRef}/message")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ConversationMessage postMessage(@PathParam("courseRef") String courseRef, @PathParam("conversationRef") String conversationRef, PostConversationMessageRequest request) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage postedMessage =
                conversationController.postMessage(courseRef, conversationRef, request.getMessage());

        return domainToRestModelConverter.convert(postedMessage);
    }

    @Path("{courseRef}/conversation/{conversationRef}/read")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Conversation postConversationRead(@PathParam("courseRef") String courseRef, @PathParam("conversationRef") String conversationRef, PostConversationReadRequest request) {

        ConversationWrapper conversation =
                conversationController.markAsRead(courseRef, conversationRef, request.getReadUntil());

        return domainToRestModelConverter.convert(conversation);

    }

    @Path("{courseRef}/deadlines")
    @GET
    @Produces("text/calendar")
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public String getCourseDeadlinesICal(@PathParam("courseRef") String courseRef, @QueryParam("includeDisabled") boolean includeDisabled) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course course = courseController.getCourse(courseRef);
        return deadlineICalConverter.constructICal(course, includeDisabled);
    }

    @Path("{courseRef}/deadlines")
    @PUT
    @RolesAllowed(SecurityRole.TEACHER)
    public CourseDeadlines updateDeadlines(@PathParam("courseRef") String courseRef, CourseDeadlines deadlines) {

        return domainToRestModelConverter.convert(
                courseController.updateDeadlines(courseRef, restToDomainModelConverter.convert(deadlines))
        );
    }
}
