package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import io.swagger.model.Announcement;
import io.swagger.model.Argument;
import io.swagger.model.ArgumentType;
import io.swagger.model.Chapter;
import io.swagger.model.ChapterDeadlines;
import io.swagger.model.ChapterProgress;
import io.swagger.model.ClientVersion;
import io.swagger.model.ClientVersionCheckResponseStatus;
import io.swagger.model.Conversation;
import io.swagger.model.ConversationEmailNotificationType;
import io.swagger.model.ConversationMessage;
import io.swagger.model.ConversationPermissionType;
import io.swagger.model.ConversationRolePermission;
import io.swagger.model.ConversationType;
import io.swagger.model.Conversations;
import io.swagger.model.Course;
import io.swagger.model.CourseDeadlines;
import io.swagger.model.CourseProgress;
import io.swagger.model.CourseProgressSummary;
import io.swagger.model.CourseResource;
import io.swagger.model.CourseResourceResult;
import io.swagger.model.CourseResources;
import io.swagger.model.CourseSummary;
import io.swagger.model.CourseTemplateChapterMetadata;
import io.swagger.model.CourseTemplateMetadata;
import io.swagger.model.CourseTemplateReviewStepMetadata;
import io.swagger.model.CourseTemplates;
import io.swagger.model.DiscussionType;
import io.swagger.model.FileUploadMetadata;
import io.swagger.model.PeerType;
import io.swagger.model.Platform;
import io.swagger.model.QuestionType;
import io.swagger.model.Quiz;
import io.swagger.model.QuizAnswer;
import io.swagger.model.QuizElement;
import io.swagger.model.QuizElementOption;
import io.swagger.model.QuizResult;
import io.swagger.model.ResourceRelation;
import io.swagger.model.ReviewStep;
import io.swagger.model.ReviewStepDeadline;
import io.swagger.model.ReviewStepResult;
import io.swagger.model.ReviewerRelation;
import io.swagger.model.ReviewerRelationGenerationType;
import io.swagger.model.Role;
import io.swagger.model.Section;
import io.swagger.model.SectionRelation;
import io.swagger.model.Sections;
import io.swagger.model.Skill;
import io.swagger.model.Skills;
import io.swagger.model.StepType;
import io.swagger.model.User;
import io.swagger.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepArgument;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourseProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ClientVersionCheckResultStatus;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ResourceResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepResultWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.EnumConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.FileUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DomainToRestModelConverter {

    private final UserSession userSession;

    private final String downloadRootPath;

    private static final Comparator<Skill> SKILL_COMPARATOR = Comparator.comparing(Skill::getName);
    private static final Comparator<CourseResource> RESOURCE_COMPARATOR = Comparator.comparing(CourseResource::getOrder).thenComparing(CourseResource::getRef);
    private static final Comparator<Section> SECTION_COMPARATOR = (o1, o2) -> {
        if (o1.getParentRef() == null && o2.getParentRef() == null) {
            // If it is a root section, we sort them based on sort order
            return o1.getSortOrder().compareTo(o2.getSortOrder());
        } else if (o1.getParentRef() == null) {
            return -1;
        } else if (o2.getParentRef() == null) {
            return 1;
        } else {
            // Apparently both parent refs are "not null", so this is a child
            if (o1.getParentRef().equals(o2.getParentRef())) {
                return o1.getSortOrder().compareTo(o2.getSortOrder());
            } else {
                return o1.getParentRef().compareTo(o2.getParentRef());
            }
        }
    };
    private static final Comparator<ArgumentType> ARGUMENT_TYPE_COMPARATOR = Comparator
            .<ArgumentType, Integer>comparing(a -> a.getParentRef() == null ? Integer.MIN_VALUE : a.getParentRef().hashCode())
            .thenComparing(ArgumentType::getName)
            .thenComparing(ArgumentType::getRef);

    private static final Comparator<CourseTemplateChapterMetadata> COURSE_TEMPLATE_CHAPTER_METADATA_COMPARATOR = Comparator
            .comparing(CourseTemplateChapterMetadata::getSortOrder);
    private static final Comparator<CourseTemplateReviewStepMetadata> COURSE_TEMPLATE_REVIEW_STEP_COMPARATOR = Comparator
            .comparing(CourseTemplateReviewStepMetadata::getSortOrder);

    public DomainToRestModelConverter(
            @Autowired UserSession userSession,
            @Value("${" + YourGreatLibraryBackendApplicationProperties.DOWNLOAD_ROOT_PATH + "}") String downloadRootPath
    ) {
        this.userSession = userSession;

        this.downloadRootPath = downloadRootPath;
    }

    public User convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User domainUser) {
        return convert(domainUser, Collections.emptySet());
    }
    public User convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User domainUser, Set<String> propertiesFilter) {

        if (domainUser == null) {
            return  null;
        }

        return new User()
                .ref(domainUser.getRef())
                .username(domainUser.getUsername())
                .email(domainUser.getEmail())
                .fullName(domainUser.getFullName())
                .studentNumber(domainUser.getStudentNumber())
                .role(convert(domainUser.getRole()))
                .preferredLocale(domainUser.getPreferredLocale() == null ? null : domainUser.getPreferredLocale().toString())
                .signedUserAgreementRef(domainUser.getSignedUserAgreement() == null ? null : domainUser.getSignedUserAgreement().getRef())
                .userAgreementSignDate(domainUser.getUserAgreementSignDate())
                .lastActive(domainUser.getLastActive())
                .created(domainUser.getCreated())
                .courses(
                        domainUser.getUserCourses().stream()
                                .distinct()
                                .sorted(Comparator.comparing(UserCourse::getCreated))
                                .map(UserCourse::getCourse)
                                .map(this::convertToSummary)
                                .collect(Collectors.toList())
                )
                .courseProgressSummary(domainUser.getUserCourses().stream().map(this::convertToCourseProgressSummary).collect(Collectors.toList()))
                .properties(
                        domainUser.getUserProperties().entrySet().stream()
                                .filter(p -> propertiesFilter.isEmpty() || propertiesFilter.contains(p.getKey()))
                                .collect(Collectors.toMap(Map.Entry::getKey, p -> p.getValue().getPropertyValue()))
                );
    }

    public Announcement convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement domainAnnouncement) {

        if (domainAnnouncement == null) {
            return null;
        }

        return new Announcement()
                .ref(domainAnnouncement.getRef())
                .startDate(domainAnnouncement.getStartDate())
                .endDate(domainAnnouncement.getEndDate())
                .title(domainAnnouncement.getTitle())
                .text(domainAnnouncement.getText())
                .url(domainAnnouncement.getUrl());

    }

    public UserAgreement convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement domainUserAgreement) {

        if (domainUserAgreement == null) {
            return null;
        }

        return new UserAgreement()
                .ref(domainUserAgreement.getRef())
                .pdfUrl(domainUserAgreement.getPdfUrl())
                .startDate(domainUserAgreement.getStartDate());
    }

    public ClientVersionCheckResponseStatus convert(ClientVersionCheckResultStatus domainStatus) {
        return EnumConverter.convert(domainStatus, ClientVersionCheckResponseStatus.class);
    }

    public Platform convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Platform domainPlatform) {
        return EnumConverter.convert(domainPlatform, Platform.class);
    }

    public ClientVersion convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion domainClientVersion) {

        if (domainClientVersion == null) {
            return null;
        }

        return new ClientVersion()
                .platform(convert(domainClientVersion.getPlatform()))
                .buildNumber(domainClientVersion.getBuildNumber())
                .version(domainClientVersion.getVersion())
                .url(domainClientVersion.getUrl())
                .releaseNotes(domainClientVersion.getReleaseNotes())
                .forced(domainClientVersion.isForced())
                .releaseDate(domainClientVersion.getReleaseDate())
                .revoked(domainClientVersion.isRevoked())
                .revokeDate(domainClientVersion.getRevokeDate())
                .revokeReason(domainClientVersion.getRevokeReason());
    }

    public CourseSummary convertToSummary(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course course) {

        if (course == null) {
            return null;
        }

        return new CourseSummary()
                .ref(course.getRef())
                .name(course.getName())
                .year(course.getYear())
                .quarter(course.getQuarter())
                .anonymousUsers(course.isAnonymousUsers())
                .anonymousReviews(course.isAnonymousReviews());

    }

    public Course convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course course, boolean includeDisabled) {

        if (course == null) {
            return null;
        }

        return new Course()
                .ref(course.getRef())
                .name(course.getName())
                .year(course.getYear())
                .quarter(course.getQuarter())
                .chapters(CollectionUtils.mapToList(course.getChapters(), c -> this.convert(c, includeDisabled)))
                .sections(CollectionUtils.mapToSortedList(course.getSections(), this::convert, SECTION_COMPARATOR))
                .resources(CollectionUtils.mapToSortedList(course.getResources(), this::convert, RESOURCE_COMPARATOR))
                .code(getProtectedOrNull(course.getCode(), nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role.TEACHER))
                .anonymousUsers(course.isAnonymousUsers())
                .anonymousReviews(course.isAnonymousReviews())
                .users(CollectionUtils.mapToList(course.getUserCourses(), this::convertToCourseProgressSummary))
                .skills(CollectionUtils.mapToSortedList(course.getSkills(), this::convert, SKILL_COMPARATOR));
    }


    public Skills convert(Collection<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill> skills) {

        if (skills == null || skills.isEmpty()) {
            return new Skills().skills(Collections.emptyList());
        }

        return new Skills().skills(
                skills.stream()
                        .map(this::convert)
                        .sorted(SKILL_COMPARATOR)
                        .collect(Collectors.toList())
        );
    }

    public Sections convertToSections(Collection<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section> domainSections) {
        return new Sections().sections(
                domainSections.stream()
                        .map(this::convert)
                        .sorted(SECTION_COMPARATOR)
                        .collect(Collectors.toList()));
    }

    public Section convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section domainSection) {

        if (domainSection == null) {
            return null;
        }

        return new Section()
                .ref(domainSection.getRef())
                .name(domainSection.getName())
                .description(domainSection.getDescription())
                .resourceRelations(domainSection.getSectionResources()
                        .stream()
                        .map(this::convert)
                        .sorted(Comparator.comparing(ResourceRelation::getSortOrder).thenComparing(ResourceRelation::getResourceRef))
                        .collect(Collectors.toList())
                )
                .parentRef(domainSection.getParentSection() == null ? null : domainSection.getParentSection().getRef())
                .sortOrder(domainSection.getSortOrder())
                .parentRelationDescription(domainSection.getParentRelationDescription());

    }

    public ResourceRelation convert(SectionResource sectionResource) {

        if (sectionResource == null) {
            return null;
        }

        return new ResourceRelation()
                .resourceRef(sectionResource.getResource().getRef())
                .optional(sectionResource.isOptional())
                .description(sectionResource.getDescription())
                .sortOrder(sectionResource.getSortOrder());
    }

    public Chapter convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter domainChapter, boolean includeDisabled) {

        if (domainChapter == null) {
            return null;
        }
        return new Chapter()
                .ref(domainChapter.getRef())
                .name(domainChapter.getName())
                .order(domainChapter.getSortOrder())
                .description(domainChapter.getDescription())
                .reviewSteps(domainChapter.getReviewSteps().stream()
                        .filter(r -> includeDisabled || !r.isDisabled())
                        .map(this::convert)
                        .collect(Collectors.toList())
                )
                .sectionRelations(domainChapter.getChapterSections().stream()
                        .map(this::convert)
                        .sorted(Comparator.comparing(SectionRelation::getSortOrder))
                        .collect(Collectors.toList())
                )
                .reviewerCount(domainChapter.getReviewerRelationGenerationSettings().getReviewerCount())
                .reviewerRelationGenerationType(convert(domainChapter.getReviewerRelationGenerationSettings().getGenerationType()));
    }

    public SectionRelation convert(ChapterSection domainChapterSection) {

        if (domainChapterSection == null) {
            return null;
        }

        return new SectionRelation()
                .sectionRef(domainChapterSection.getSection().getRef())
                .description(domainChapterSection.getDescription())
                .sortOrder(domainChapterSection.getSortOrder())
                .optional(domainChapterSection.isOptional());

    }

    public ReviewerRelationGenerationType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationGenerationType domainGenerationType) {
        return EnumConverter.convert(domainGenerationType, ReviewerRelationGenerationType.class);
    }

    public ReviewStep convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep domainReviewStep) {

        if (domainReviewStep == null) {
            return null;
        }

        return new ReviewStep()
                .ref(domainReviewStep.getRef())
                .stepType(convert(domainReviewStep.getStepType()))
                .peerType(convert(domainReviewStep.getStepType().getPostedBy()))
                .order(domainReviewStep.getSortOrder())
                .disabled(domainReviewStep.isDisabled())
                .name(domainReviewStep.getName())
                .description(domainReviewStep.getDescription())
                .deadline(domainReviewStep.getDeadline())
                .discussionType(convert(domainReviewStep.getDiscussionType()))
                .allowedArguments(domainReviewStep.getArgumentTypes().stream()
                        .map(this::convert)
                        .sorted(ARGUMENT_TYPE_COMPARATOR)
                        .collect(Collectors.toList())
                )
                .quiz(convert(domainReviewStep.getQuiz()))
                .fileUpload(domainReviewStep.isFileUpload())
                .fileUploadName(domainReviewStep.getFileUploadName())
                .skills(domainReviewStep.getSkills() == null
                        ? Collections.emptyList()
                        : domainReviewStep.getSkills().stream()
                                .map(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill::getRef)
                                .sorted()
                                .collect(Collectors.toList()));
    }

    public PeerType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.PeerType peerType) {
        return EnumConverter.convert(peerType, PeerType.class);
    }

    public CourseResources convertToCourseResources(Collection<Resource> resources) {

        if (resources == null || resources.isEmpty()) {
            return new CourseResources().resources(Collections.emptyList());
        }

        return new CourseResources()
                .resources(resources.stream()
                        .map(this::convert)
                        .sorted(RESOURCE_COMPARATOR)
                        .collect(Collectors.toList()));
    }

    public CourseResource convert(Resource domainResource) {
        if (domainResource == null) {
            return null;
        }

        return new CourseResource()
                .ref(domainResource.getRef())
                .order(domainResource.getSortOrder())
                .name(domainResource.getName())
                .description(domainResource.getDescription())
                .url(domainResource.getFileUploadMetadata() == null ? domainResource.getUrl() : fileUploadMetadataDownloadUrl(domainResource.getFileUploadMetadata()))
                .fileUploadMetadata(domainResource.getFileUploadMetadata() == null ? null : convert(domainResource.getFileUploadMetadata()))
                .quiz(convert(domainResource.getQuiz()))
                .quizMinimalScore(domainResource.getQuizMinimalScore())
                .skills(domainResource.getSkills() == null
                        ? Collections.emptyList()
                        : domainResource.getSkills().stream()
                                .map(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill::getRef)
                                .sorted()
                                .collect(Collectors.toList())
                );
    }

    public CourseProgressSummary convertToCourseProgressSummary(UserCourse userCourse) {

        if (userCourse == null) {
            return null;
        }

        return new CourseProgressSummary()
                .userRef(userCourse.getUser().getRef())
                .courseRef(userCourse.getCourse().getRef())
                .displayName(userCourse.getDisplayName())
                .active(userCourse.isActive())
                .topic(userCourse.getTopic())
                .fullName(getProtectedOrNull(userCourse.getUser().getFullName(), nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role.TEACHER))
                .role(convert(userCourse.getUser().getRole()))
                .userCourseProperties(userCourse.getUserCourseProperties().entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getPropertyValue()))
                );
    }

    public ArgumentType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType argumentType) {

        if (argumentType == null) {
            return null;
        }

        return new ArgumentType()
                .ref(argumentType.getRef())
                .name(argumentType.getName())
                .description(argumentType.getDescription())
                .required(argumentType.isRequired())
                .parentRef(argumentType.getParent() == null ? null : argumentType.getParent().getRef());
    }


    public Quiz convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz domainQuiz) {

        if (domainQuiz == null) {
            return null;
        }

        return new Quiz()
                .ref(domainQuiz.getRef())
                .name(domainQuiz.getName())
                .description(domainQuiz.getDescription())
                .maxScore(domainQuiz.getMaxScore())
                .elements(domainQuiz.getElements().stream()
                        .map(this::convert)
                        .sorted(Comparator.comparing(QuizElement::getOrder).thenComparing(QuizElement::getRef))
                        .collect(Collectors.toList())
                );

    }

    public QuizElement convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement domainQuizElement) {

        if (domainQuizElement == null) {
            return null;
        }

        return new QuizElement()
                .ref(domainQuizElement.getRef())
                .order(domainQuizElement.getSortOrder())
                .text(domainQuizElement.getText())
                .questionType(convert(domainQuizElement.getQuestionType()))
                .options(
                        domainQuizElement.getOptions() == null ? Collections.emptyList() :
                        domainQuizElement.getOptions().stream()
                        .map(this::convert)
                        .sorted(Comparator.comparing(QuizElementOption::getOrder).thenComparing(QuizElementOption::getRef))
                        .collect(Collectors.toList())
                );
    }

    public QuestionType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType domainQuestionType) {
        return EnumConverter.convert(domainQuestionType, QuestionType.class);
    }

    public QuizElementOption convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption domainElementOption) {

        if (domainElementOption == null) {
            return null;
        }

        return new QuizElementOption()
                .ref(domainElementOption.getRef())
                .order(domainElementOption.getSortOrder())
                .text(domainElementOption.getText())
                // Only teachers can see the amount of points a quiz element option is worth
                .points(domainElementOption.getQuizElement().getQuiz().isHiddenPoints() ? getProtectedOrNull(domainElementOption.getPoints(), nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role.TEACHER) : domainElementOption.getPoints());
    }

    public StepType convert(ReviewStepType domainStepType) {
        return EnumConverter.convert(domainStepType, StepType.class);
    }

    public DiscussionType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType domainDiscussionType) {
        return EnumConverter.convert(domainDiscussionType, DiscussionType.class);
    }

    public CourseDeadlines convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseDeadlines domainDeadlines) {

        if (domainDeadlines == null) {
            return null;
        }

        return new CourseDeadlines()
                .chapters(CollectionUtils.mapToList(domainDeadlines.getChapters(),
                        c -> new ChapterDeadlines()
                        .chapterRef(c.getChapterRef())
                        .reviewSteps(CollectionUtils.mapToList(c.getReviewSteps(),
                                rs -> new ReviewStepDeadline()
                                .reviewStepRef(rs.getReviewStepRef())
                                .deadline(rs.getDeadline()))
                        )));
    }

    public Skill convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill domainSkill) {
        if (domainSkill == null) {
            return null;
        }

        return new Skill()
                .ref(domainSkill.getRef())
                .name(domainSkill.getName())
                .description(domainSkill.getDescription())
                .icon(domainSkill.getIcon());
    }

    public CourseProgress convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseProgress domainCourseProgress) {

        if (domainCourseProgress == null) {
            return null;
        }

        return new CourseProgress()
                .userRef(domainCourseProgress.getUserCourse().getUser().getRef())
                .displayName(domainCourseProgress.getUserCourse().getDisplayName())
                .topic(domainCourseProgress.getUserCourse().getTopic())
                .courseRef(domainCourseProgress.getUserCourse().getCourse().getRef())
                .chapters(domainCourseProgress.getChapterProgress().stream().map(this::convert).collect(Collectors.toList()))
                .resourceResults(domainCourseProgress.getResourceResults().stream().map(this::convert).collect(Collectors.toList()))
                .userCourseProperties(convertToMap(domainCourseProgress.getUserCourse().getUserCourseProperties()));

    }

    public ChapterProgress convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ChapterProgress domainChapterProgress) {

        if (domainChapterProgress == null) {
            return null;
        }

        return new ChapterProgress()
                .chapterRef(domainChapterProgress.getChapter().getRef())
                .reviewerRelations(domainChapterProgress.getReviewerRelationResults().stream().map(this::convert).collect(Collectors.toList()));
    }

    public ReviewerRelation convert(ReviewerRelationResult domainReviewerRelation) {

        if (domainReviewerRelation == null) {
            return null;
        }

        return new ReviewerRelation()
                .ref(domainReviewerRelation.getReviewerRelationRef())
                .submitter(domainReviewerRelation.getSubmitter() == null ? null : domainReviewerRelation.getSubmitter().getRef())
                .reviewer(domainReviewerRelation.getReviewer() == null ? null : domainReviewerRelation.getReviewer().getRef())
                .reviewStepResults(domainReviewerRelation.getStepResults().stream().map(this::convert).collect(Collectors.toList()));

    }

    public ReviewStepResult convert(ReviewStepResultWrapper resultWrapper) {

        if (resultWrapper == null || resultWrapper.getReviewStepResult() == null) {
            return null;
        }

        return new ReviewStepResult()
                .reviewStepRef(resultWrapper.getReviewStepResult().getReviewStep().getRef())
                .lastModified(resultWrapper.getLastModified())
                .readyToComplete(resultWrapper.isReadyToComplete())
                .markedComplete(resultWrapper.getReviewStepResult().getMarkedComplete())
                .closed(resultWrapper.isClosed())
                .fileUploadMetadata(convert(resultWrapper.getReviewStepResult().getFileUploadMetaData()))
                .quizResult(convert(resultWrapper.getReviewStepResult().getQuizResult()))
                .arguments(resultWrapper.getReviewStepResult().getArguments().stream()
                        .sorted(Comparator.comparing(ReviewStepArgument::getId))
                        .map(this::convert)
                        .collect(Collectors.toList()));
    }

    public FileUploadMetadata convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata domainFileUploadMetadata) {

        if (domainFileUploadMetadata == null) {
            return null;
        }

        return new FileUploadMetadata()
                .ref(domainFileUploadMetadata.getRef())
                .filename(domainFileUploadMetadata.getFilename())
                .downloadUrl(fileUploadMetadataDownloadUrl(domainFileUploadMetadata))
                .size(FileUtils.getFileSize(domainFileUploadMetadata.getStoragePath()))
                .created(domainFileUploadMetadata.getCreated());
    }

    private String fileUploadMetadataDownloadUrl(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata domainFileUploadMetadata) {
        try {
            return new URI(downloadRootPath + "/" + domainFileUploadMetadata.getRef() + "/" +
                URLEncoder.encode(domainFileUploadMetadata.getFilename(), StandardCharsets.UTF_8.name())).normalize().toString();
        } catch (URISyntaxException e) {
            throw new OrbitGamesSystemException("Error while generating the download URL");
        } catch (UnsupportedEncodingException e) {
            throw new OrbitGamesSystemException("Can't find UTF-8 Encoding?");
        }
    }

    public QuizResult convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult domainQuizResult) {

        if (domainQuizResult == null) {
            return null;
        }

        return new QuizResult()
                .quizRef(domainQuizResult.getQuiz().getRef())
                .totalScore(domainQuizResult.getScore())
                .answers(domainQuizResult.getQuizElementResults().stream()
                        .sorted(Comparator.comparing(a -> a.getQuizElement().getSortOrder()))
                        .map(this::convert)
                        .collect(Collectors.toList()))
                .created(domainQuizResult.getCreated());
    }

    public QuizAnswer convert(QuizElementResult domainQuizElementResult) {

        if (domainQuizElementResult == null) {
            return null;
        }

        return new QuizAnswer()
                .quizElementRef(domainQuizElementResult.getQuizElement().getRef())
                .answers(domainQuizElementResult.getSelectedOptions().stream()
                        .sorted(Comparator.comparing(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption::getSortOrder))
                        .map(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption::getRef)
                        .collect(Collectors.toList()));
    }

    public Argument convert(ReviewStepArgument domainArgument) {

        if (domainArgument == null) {
            return null;
        }

        return new Argument()
                .ref(domainArgument.getRef())
                .argumentTypeRef(domainArgument.getArgumentType() == null ? null : domainArgument.getArgumentType().getRef())
                .replyToArgumentRef(domainArgument.getReplyTo() == null ? null : domainArgument.getReplyTo().getRef())
                .created(domainArgument.getCreated())
                .message(domainArgument.getMessage());
    }

    public Conversation convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationWrapper domainConversation) {

        if (domainConversation == null) {
            return null;
        }

        return new Conversation()
                .ref(domainConversation.getConversation().getRef())
                .title(domainConversation.getConversation().getTitle())
                .created(domainConversation.getConversation().getCreated())
                .hidden(domainConversation.getConversation().isHidden())
                .anonymousMembers(domainConversation.getConversation().isAnonymousMembers())
                .type(convert(domainConversation.getConversation().getType()))
                .members(domainConversation.getConversation().isAnonymousMembers()
                        ? Collections.emptyList()
                        : domainConversation.getConversation().getUsers().stream()
                                .map(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User::getRef)
                                .collect(Collectors.toList()))
                .roles(domainConversation.getConversation().getConversationRoles().entrySet().stream()
                        .map(e -> new ConversationRolePermission()
                                .role(convert(e.getKey()))
                                .permission(convert(e.getValue())))
                        .collect(Collectors.toList()))
                .messageCount(domainConversation.getConversation().getLastMessageIndex() + 1)
                .emailNotificationType(convert(domainConversation.getNotificationType()))
                .readUntil(domainConversation.getReadUntil())
                .messages(domainConversation.getMessages().stream()
                        .sorted(Comparator.comparing(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage::getIndexInConversation).reversed())
                        .map(this::convert)
                        .collect(Collectors.toList())
                );
    }

    ConversationEmailNotificationType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationEmailNotificationType notificationType) {

        return EnumConverter.convert(notificationType, ConversationEmailNotificationType.class);
    }

    public Role convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role role) {
        return EnumConverter.convert(role, Role.class);
    }

    public ConversationPermissionType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationPermissionType domainPermissionType) {

        return EnumConverter.convert(domainPermissionType, ConversationPermissionType.class);
    }

    public ConversationType convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationType domainConversationType) {
        return EnumConverter.convert(domainConversationType, ConversationType.class);
    }

    public Conversations convertConversations(List<ConversationWrapper> conversations) {

        return new Conversations()
                .currentTime(OffsetDateTime.now())
                .conversations(conversations.stream().map(this::convert).collect(Collectors.toList()));

    }

    public ConversationMessage convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage domainConversationMessage) {

        if (domainConversationMessage == null) {
            return null;
        }

        return new ConversationMessage()
                .ref(domainConversationMessage.getRef())
                .ind(domainConversationMessage.getIndexInConversation())
                .sent(domainConversationMessage.getSent())
                .sender(
                        domainConversationMessage.getConversation().isAnonymousMembers() && !userSession.getUserRef().equals(domainConversationMessage.getSender().getRef())
                                ? null
                                : domainConversationMessage.getSender().getRef())
                .message(domainConversationMessage.getMessage());
    }

    public CourseResourceResult convert(ResourceResult domainResourceResult) {

        if (domainResourceResult == null) {
            return null;
        }

        return new CourseResourceResult()
                .resourceRef(domainResourceResult.getResource().getRef())
                .quizResult(convert(domainResourceResult.getQuizResult()));

    }

    private Map<String, String> convertToMap(Map<String, UserCourseProperty> domainUserCourseProperties) {
        if (domainUserCourseProperties == null) {
            return null;
        }

        return domainUserCourseProperties.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getPropertyValue()));
    }


    public CourseTemplates convertToCourseTemplates(List<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata> courseTemplates) {
        return new CourseTemplates()
                .templates(CollectionUtils.mapToList(courseTemplates, this::convert));
    }

    public CourseTemplateMetadata convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata courseTemplate) {

        if (courseTemplate == null) {
            return null;
        }

        return new CourseTemplateMetadata()
                .id(courseTemplate.getTemplateId())
                .name(courseTemplate.getName())
                .version(courseTemplate.getVersion())
                .description(courseTemplate.getDescription())
                .chapters(CollectionUtils.mapToSortedList(courseTemplate.getChapters(), this::convert, COURSE_TEMPLATE_CHAPTER_METADATA_COMPARATOR));
    }

    private CourseTemplateChapterMetadata convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateChapterMetadata courseTemplateChapterMetadata) {

        if (courseTemplateChapterMetadata == null) {
            return null;
        }

        return new CourseTemplateChapterMetadata()
                .id(courseTemplateChapterMetadata.getChapterId())
                .name(courseTemplateChapterMetadata.getName())
                .sortOrder(courseTemplateChapterMetadata.getSortOrder())
                .reviewSteps(CollectionUtils.mapToSortedList(courseTemplateChapterMetadata.getReviewSteps(), this::convert, COURSE_TEMPLATE_REVIEW_STEP_COMPARATOR));
    }

    private CourseTemplateReviewStepMetadata convert(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateReviewStepMetadata courseTemplateReviewStepMetadata) {

        if (courseTemplateReviewStepMetadata == null) {
            return null;
        }

        return new CourseTemplateReviewStepMetadata()
                .id(courseTemplateReviewStepMetadata.getReviewStepId())
                .name(courseTemplateReviewStepMetadata.getName())
                .sortOrder(courseTemplateReviewStepMetadata.getSortOrder())
                .deadlineOffset(courseTemplateReviewStepMetadata.getDeadlineOffset());
    }

    private <T> T getProtectedOrNull(T original, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role requiredRole) {
        if (SecurityUtils.canRoleAccess(userSession.getRole(), requiredRole)) {
            return original;
        } else {
            return null;
        }
    }

}
