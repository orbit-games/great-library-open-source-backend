package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import io.swagger.model.Argument;
import io.swagger.model.BugReportScreenshot;
import io.swagger.model.CourseProgressSummary;
import io.swagger.model.CourseResource;
import io.swagger.model.QuizAnswer;
import io.swagger.model.ResourceRelation;
import io.swagger.model.SectionRelation;
import io.swagger.model.StepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.BugReport;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementOption;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElementResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepArgument;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourseProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.BugReportScreenshotWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ChapterDeadlines;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationEmailNotificationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationPermissionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseDeadlines;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseUpdateRequest;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Platform;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepDeadline;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationGenerationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.EnumConverter;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

@Component
public class RestToDomainModelConverter {

    public User convert(io.swagger.model.User user) {

        if (user == null) {
            return null;
        }

        User domainUser = new User();
        domainUser.setRef(user.getRef());
        domainUser.setUsername(user.getUsername());
        domainUser.setEmail(user.getEmail());
        domainUser.setFullName(user.getFullName());
        domainUser.setStudentNumber(user.getStudentNumber());
        domainUser.setSignedUserAgreement(stubUserAgreement(user.getSignedUserAgreementRef()));
        domainUser.setUserAgreementSignDate(user.getUserAgreementSignDate());
        domainUser.setPreferredLocale(parseLocale(user.getPreferredLocale()));
        domainUser.setLastActive(user.getLastActive());

        domainUser.setCreated(user.getCreated());
        domainUser.setUserProperties(user.getProperties().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> userPropertyFromValue(e.getValue()))));

        return domainUser;
    }

    private UserProperty userPropertyFromValue(String value) {

        UserProperty res = new UserProperty();
        res.setPropertyValue(value);
        return res;
    }

    private Locale parseLocale(String locale) {

        if (StringUtils.isBlank(locale)) {
            return null;
        }

        try {
            Locale parsedLocale = LocaleUtils.toLocale(locale);
            if (!LocaleUtils.isAvailableLocale(parsedLocale)) {
                throw new OrbitGamesApplicationException("The given locale '" + parsedLocale + "' is not available", GenericErrorCode.CONSTRAINT_VIOLATION);
            }
            return parsedLocale;
        } catch (IllegalArgumentException e) {
            // Apparently the passed locale string is invalid
            throw new OrbitGamesApplicationException("The given locale '" + locale + "' is not valid: " + e.getMessage(), GenericErrorCode.CONSTRAINT_VIOLATION, e);
        }
    }

    public Announcement convert(io.swagger.model.Announcement announcement) {

        if (announcement == null) {
            return null;
        }

        Announcement domainAnnouncement = new Announcement();
        domainAnnouncement.setRef(announcement.getRef());
        domainAnnouncement.setStartDate(announcement.getStartDate());
        domainAnnouncement.setEndDate(announcement.getEndDate());
        domainAnnouncement.setTitle(announcement.getTitle());
        domainAnnouncement.setText(announcement.getText());
        domainAnnouncement.setUrl(announcement.getUrl());

        return domainAnnouncement;
    }

    public UserAgreement convert(io.swagger.model.UserAgreement userAgreement) {

        if (userAgreement == null) {
            return null;
        }

        UserAgreement domainUserAgreement = new UserAgreement();
        domainUserAgreement.setRef(userAgreement.getRef());
        domainUserAgreement.setPdfUrl(userAgreement.getPdfUrl());
        domainUserAgreement.setStartDate(userAgreement.getStartDate());

        return domainUserAgreement;
    }

    public Platform convert(io.swagger.model.Platform platform) {

        if (platform == null) {
            return null;
        }

        switch (platform) {
            case IOS: return Platform.IOS;
            case ANDROID: return Platform.ANDROID;
            case WEBGL: return Platform.WEBGL;
            case WIN: return Platform.WIN;
            case LINUX: return Platform.LINUX;
            case MACOS: return Platform.MACOS;
            default:
                throw new OrbitGamesApplicationException("Could not determine platform for : '" + platform + "'", GenericErrorCode.CONSTRAINT_VIOLATION);
        }
    }

    public ClientVersion convert(io.swagger.model.ClientVersion clientVersion) {

        if (clientVersion == null) {
            return null;
        }

        ClientVersion domainClientVersion = new ClientVersion();
        domainClientVersion.setPlatform(convert(clientVersion.getPlatform()));
        domainClientVersion.setBuildNumber(clientVersion.getBuildNumber());
        domainClientVersion.setVersion(clientVersion.getVersion());
        domainClientVersion.setUrl(clientVersion.getUrl());
        domainClientVersion.setReleaseNotes(clientVersion.getReleaseNotes());
        domainClientVersion.setForced(clientVersion.isisForced());
        domainClientVersion.setReleaseDate(clientVersion.getReleaseDate());
        domainClientVersion.setRevoked(clientVersion.isisRevoked());
        domainClientVersion.setRevokeDate(clientVersion.getRevokeDate());
        domainClientVersion.setRevokeReason(clientVersion.getRevokeReason());

        return domainClientVersion;


    }

    public UserCourse convert(CourseProgressSummary courseUser) {

        if (courseUser == null) {
            return null;
        }

        User userStub = new User();
        userStub.setRef(courseUser.getUserRef());

        Course courseStub = new Course();
        courseStub.setRef(courseUser.getCourseRef());

        UserCourse domainUserCourse = new UserCourse();
        domainUserCourse.setUser(userStub);
        domainUserCourse.setCourse(courseStub);
        domainUserCourse.setDisplayName(courseUser.getDisplayName());
        domainUserCourse.setActive(courseUser.isisActive() == null ? true : courseUser.isisActive());
        domainUserCourse.setTopic(courseUser.getTopic());
        domainUserCourse.setUserCourseProperties(courseUser.getUserCourseProperties() == null
                ? Collections.emptyMap()
                : courseUser.getUserCourseProperties().entrySet()
                        .stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, e -> {
                            UserCourseProperty userCourseProperty = new UserCourseProperty();
                            userCourseProperty.setPropertyValue(e.getValue());
                            return userCourseProperty;
                        }))
        );

        return domainUserCourse;

    }

    public QuizResult convert(io.swagger.model.QuizResult quizResult) {

        if (quizResult == null) {
            return null;
        }

        QuizResult domainQuizResult = new QuizResult();
        domainQuizResult.setQuiz(stubQuiz(quizResult.getQuizRef()));
        domainQuizResult.setScore(quizResult.getTotalScore() == null ? 0 : quizResult.getTotalScore());
        domainQuizResult.setQuizElementResults(CollectionUtils.mapToSet(quizResult.getAnswers(), this::convert));
        domainQuizResult.setCreated(quizResult.getCreated());

        return domainQuizResult;
    }

    private QuizElementResult convert(QuizAnswer quizAnswer) {

        if (quizAnswer == null) {
            return null;
        }

        QuizElementResult domainAnswer = new QuizElementResult();
        domainAnswer.setQuizElement(stubQuizElement(quizAnswer.getQuizElementRef()));
        domainAnswer.setSelectedOptions(CollectionUtils.mapToSet(quizAnswer.getAnswers(), this::stubQuizElementOption));

        return domainAnswer;
    }


    public ReviewStepArgument convert(Argument argument) {

        if (argument == null) {
            return null;
        }

        ReviewStepArgument domainArgument = new ReviewStepArgument();
        domainArgument.setRef(argument.getRef());
        domainArgument.setArgumentType(stubArgumentType(argument.getArgumentTypeRef()));
        domainArgument.setReplyTo(stubReviewStepArgument(argument.getReplyToArgumentRef()));
        domainArgument.setMessage(argument.getMessage());

        return domainArgument;
    }

    public Conversation convert(io.swagger.model.Conversation conversation) {

        if (conversation == null) {
            return null;
        }

        Conversation domainConversation = new Conversation();
        domainConversation.setRef(conversation.getRef());
        domainConversation.setTitle(conversation.getTitle());
        domainConversation.setCreated(conversation.getCreated());
        domainConversation.setHidden(conversation.isisHidden());
        domainConversation.setType(convert(conversation.getType()));
        domainConversation.setAnonymousMembers(conversation.isisAnonymousMembers());
        domainConversation.setUsers(CollectionUtils.mapToSet(conversation.getMembers(), this::stubUser));
        domainConversation.setConversationRoles(conversation.getRoles() == null
                ? Collections.emptyMap()
                : conversation.getRoles().stream()
                        .filter(m -> m.getRole() != null && m.getPermission() != null)
                        .collect(Collectors.toMap(m -> convert(m.getRole()), m -> convert(m.getPermission()))));

        return domainConversation;
    }

    public Role convert(io.swagger.model.Role role) {
        return EnumConverter.convert(role, Role.class);
    }

    public ConversationPermissionType convert(io.swagger.model.ConversationPermissionType permissionType) {
        return EnumConverter.convert(permissionType, ConversationPermissionType.class);
    }

    public ConversationEmailNotificationType convert(io.swagger.model.ConversationEmailNotificationType type) {
        return EnumConverter.convert(type, ConversationEmailNotificationType.class);
    }

    public ConversationType convert(io.swagger.model.ConversationType type) {
        return EnumConverter.convert(type, ConversationType.class);
    }

    public ConversationMessage convert(io.swagger.model.ConversationMessage message) {

        if (message == null) {
            return null;
        }

        ConversationMessage domainMessage = new ConversationMessage();
        domainMessage.setRef(message.getRef());
        domainMessage.setIndexInConversation(message.getInd());
        domainMessage.setSent(message.getSent());
        domainMessage.setSender(stubUser(message.getSender()));
        domainMessage.setMessage(message.getMessage());
        return domainMessage;
    }

    public Chapter convert(io.swagger.model.Chapter chapter) {

        if (chapter == null) {
            return null;
        }

        Chapter domainChapter = new Chapter();
        domainChapter.setRef(chapter.getRef());
        domainChapter.setName(chapter.getName());
        domainChapter.setSortOrder(chapter.getOrder());
        domainChapter.setDescription(chapter.getDescription());
        domainChapter.setChapterSections(CollectionUtils.mapToSet(chapter.getSectionRelations(), this::convert));
        domainChapter.setReviewSteps(CollectionUtils.mapToList(chapter.getReviewSteps(), this::convert));

        ReviewerRelationGenerationSettings reviewerRelationGenerationSettings = new ReviewerRelationGenerationSettings();
        reviewerRelationGenerationSettings.setReviewerCount(chapter.getReviewerCount());
        reviewerRelationGenerationSettings.setGenerationType(this.convert(chapter.getReviewerRelationGenerationType()));

        domainChapter.setReviewerRelationGenerationSettings(reviewerRelationGenerationSettings);

        return domainChapter;
    }

    private ChapterSection convert(SectionRelation sectionRelation) {

        if (sectionRelation == null) {
            return null;
        }

        ChapterSection domainChapterSection = new ChapterSection();
        domainChapterSection.setSection(stubSection(sectionRelation.getSectionRef()));
        domainChapterSection.setDescription(sectionRelation.getDescription());
        domainChapterSection.setSortOrder(sectionRelation.getSortOrder());
        domainChapterSection.setOptional(sectionRelation.isisOptional());

        return domainChapterSection;
    }

    public ReviewerRelationGenerationType convert(io.swagger.model.ReviewerRelationGenerationType reviewerRelationGenerationType) {
        return EnumConverter.convert(reviewerRelationGenerationType, ReviewerRelationGenerationType.class);
    }

    public ReviewStep convert(io.swagger.model.ReviewStep reviewStep) {

        if (reviewStep == null) {
            return null;
        }

        ReviewStep domainReviewStep = new ReviewStep();
        domainReviewStep.setRef(reviewStep.getRef());
        domainReviewStep.setStepType(convert(reviewStep.getStepType()));
        domainReviewStep.setSortOrder(reviewStep.getOrder());
        domainReviewStep.setDisabled(reviewStep.isisDisabled());
        domainReviewStep.setName(reviewStep.getName());
        domainReviewStep.setDescription(reviewStep.getDescription());
        domainReviewStep.setDeadline(reviewStep.getDeadline());
        domainReviewStep.setDiscussionType(convert(reviewStep.getDiscussionType()));
        domainReviewStep.setArgumentTypes(CollectionUtils.mapToSet(reviewStep.getAllowedArguments(), this::convert));
        domainReviewStep.setQuiz(convert(reviewStep.getQuiz()));
        domainReviewStep.setFileUpload(reviewStep.isisFileUpload());
        domainReviewStep.setFileUploadName(reviewStep.getFileUploadName());
        domainReviewStep.setSkills(CollectionUtils.mapToSet(reviewStep.getSkills(), this::stubSkill));

        return domainReviewStep;
    }

    public ArgumentType convert(io.swagger.model.ArgumentType argumentType) {

        if (argumentType == null) {
            return null;
        }

        ArgumentType domainArgumentType = new ArgumentType();
        domainArgumentType.setRef(argumentType.getRef());
        domainArgumentType.setName(argumentType.getName());
        domainArgumentType.setDescription(argumentType.getDescription());
        domainArgumentType.setRequired(argumentType.isisRequired());
        domainArgumentType.setParent(stubArgumentType(argumentType.getParentRef()));

        return domainArgumentType;
    }

    public ReviewStepType convert(StepType stepType) {
        return EnumConverter.convert(stepType, ReviewStepType.class);
    }

    public DiscussionType convert(io.swagger.model.DiscussionType discussionType) {
        return EnumConverter.convert(discussionType, DiscussionType.class);
    }

    public CourseDeadlines convert(io.swagger.model.CourseDeadlines deadlines) {

        if (deadlines == null) {
            return null;
        }

        return new CourseDeadlines(
                CollectionUtils.mapToList(deadlines.getChapters(), c -> new ChapterDeadlines(
                        c.getChapterRef(),
                        CollectionUtils.mapToList(c.getReviewSteps(), rs -> new ReviewStepDeadline(rs.getReviewStepRef(), rs.getDeadline()))
                )
        ));
    }

    public Section convert(io.swagger.model.Section section) {

        if (section == null) {
            return  null;
        }

        Section domainSection = new Section();
        domainSection.setRef(section.getRef());
        domainSection.setName(section.getName());
        domainSection.setDescription(section.getDescription());
        domainSection.setParentSection(stubSection(section.getParentRef()));
        domainSection.setSortOrder(section.getSortOrder() == null ? 0 : section.getSortOrder());
        domainSection.setParentRelationDescription(section.getParentRelationDescription());
        domainSection.setSectionResources(CollectionUtils.mapToSet(section.getResourceRelations(), this::convert));

        return domainSection;
    }

    public SectionResource convert(ResourceRelation resourceRelation) {

        if (resourceRelation == null) {
            return null;
        }

        SectionResource domainSectionResource = new SectionResource();
        domainSectionResource.setResource(stubResource(resourceRelation.getResourceRef()));
        domainSectionResource.setOptional(resourceRelation.isisOptional());
        domainSectionResource.setSortOrder(resourceRelation.getSortOrder() == null ? 0 : resourceRelation.getSortOrder());
        domainSectionResource.setDescription(resourceRelation.getDescription());

        return domainSectionResource;
    }

    public BugReport convert(io.swagger.model.BugReport bugReport) {

        if (bugReport == null) {
            return null;
        }

        BugReport domainBugReport = new BugReport();
        domainBugReport.setEmail(bugReport.getEmail());
        domainBugReport.setUserInput(bugReport.getUserInput());
        domainBugReport.setDeviceInfo(decodeGzippedString(bugReport.getDeviceInfo()));
        domainBugReport.setGameLogs(decodeGzippedString(bugReport.getGameLogs()));
        domainBugReport.setCommunicationLogs(decodeGzippedString(bugReport.getCommunicationLogs()));
        domainBugReport.setPrefs(decodeGzippedString(bugReport.getPrefs()));
        domainBugReport.setKnowledge(decodeGzippedString(bugReport.getKnowledge()));
        domainBugReport.setClientTime(bugReport.getTime());

        return domainBugReport;
    }

    public BugReportScreenshotWrapper convert(BugReportScreenshot bugReportScreenshot) {

        if (bugReportScreenshot == null) {
            return null;
        }

        return new BugReportScreenshotWrapper(
                bugReportScreenshot.getTime(),
                bugReportScreenshot.getScreenshot());

    }


    public Resource convert(CourseResource resource) {

        if (resource == null) {
            return null;
        }

        Resource domainResource = new Resource();
        domainResource.setRef(resource.getRef());
        domainResource.setSortOrder(resource.getOrder() == null ? 0 : resource.getOrder());
        domainResource.setName(resource.getName());
        domainResource.setDescription(resource.getDescription());
        domainResource.setUrl(resource.getUrl());
        domainResource.setFileUploadMetadata(convert(resource.getFileUploadMetadata()));
        domainResource.setQuiz(convert(resource.getQuiz()));
        domainResource.setQuizMinimalScore(resource.getQuizMinimalScore() == null ? 0 : resource.getQuizMinimalScore());
        domainResource.setSkills(CollectionUtils.mapToSet(resource.getSkills(), this::stubSkill));

        return domainResource;
    }

    private FileUploadMetadata convert(io.swagger.model.FileUploadMetadata fileUploadMetadata) {

        if (fileUploadMetadata == null) {
            return null;
        }

        FileUploadMetadata domainFileUploadMetadata = new FileUploadMetadata();
        domainFileUploadMetadata.setRef(fileUploadMetadata.getRef());
        domainFileUploadMetadata.setFilename(fileUploadMetadata.getFilename());
        domainFileUploadMetadata.setCreated(fileUploadMetadata.getCreated());

        // The domain and REST model for file upload metadata are quite different, but since only the ref is used, that's not too big a problem.
        return domainFileUploadMetadata;
    }

    private Quiz convert(io.swagger.model.Quiz quiz) {

        if (quiz == null) {
            return null;
        }

        Quiz domainQuiz = new Quiz();
        domainQuiz.setRef(quiz.getRef());
        domainQuiz.setName(quiz.getName());
        domainQuiz.setDescription(quiz.getDescription());
        domainQuiz.setMaxScore(quiz.getMaxScore());
        domainQuiz.setElements(CollectionUtils.mapToList(quiz.getElements(), this::convert));

        return domainQuiz;
    }

    private QuizElement convert(io.swagger.model.QuizElement quizElement) {

        if (quizElement == null) {
            return null;
        }

        QuizElement domainQuizElement = new QuizElement();
        domainQuizElement.setRef(quizElement.getRef());
        domainQuizElement.setSortOrder(quizElement.getOrder() == null ? 0 : quizElement.getOrder());
        domainQuizElement.setText(quizElement.getText());
        domainQuizElement.setQuestionType(convert(quizElement.getQuestionType()));
        domainQuizElement.setOptions(CollectionUtils.mapToSet(quizElement.getOptions(), this::convert));

        return domainQuizElement;
    }

    private QuizElementOption convert(io.swagger.model.QuizElementOption quizElementOption) {

        if (quizElementOption == null) {
            return null;
        }

        QuizElementOption domainQuizElementOption = new QuizElementOption();
        domainQuizElementOption.setRef(quizElementOption.getRef());
        domainQuizElementOption.setSortOrder(quizElementOption.getOrder() == null ? 0 : quizElementOption.getOrder());
        domainQuizElementOption.setText(quizElementOption.getText());
        domainQuizElementOption.setPoints(quizElementOption.getPoints() == null ? 0 : quizElementOption.getPoints());

        return domainQuizElementOption;
    }

    public QuestionType convert(io.swagger.model.QuestionType questionType) {

        return EnumConverter.convert(questionType, QuestionType.class);
    }


    public Skill convert(io.swagger.model.Skill skill) {

        if (skill == null) {
            return null;
        }

        Skill domainSkill = new Skill();
        domainSkill.setRef(skill.getRef());
        domainSkill.setName(skill.getName());
        domainSkill.setDescription(skill.getDescription());
        domainSkill.setIcon(skill.getIcon());
        return domainSkill;
    }

    public CourseUpdateRequest convert(io.swagger.model.CourseUpdateRequest request) {

        if (request == null) {
            return null;
        }

        return new CourseUpdateRequest(
                request.getName(),
                request.getYear(),
                request.getQuarter(),
                request.getCode(),
                request.isisAnonymousUsers(),
                request.isisAnonymousReviews()
        );
    }

    private String decodeGzippedString(byte[] gzippedString) {

        if (gzippedString == null || gzippedString.length == 0) {
            return null;
        }

        try (InputStream gzippedStream = new ByteArrayInputStream(gzippedString)) {

            return IOUtils.toString(
                    new GZIPInputStream(gzippedStream),
                    StandardCharsets.UTF_8.name());

        } catch (IOException e) {
            throw new OrbitGamesSystemException("Could not read input stream", e);
        }
    }

    private QuizElement stubQuizElement(String quizElementRef) {

        if (StringUtils.isBlank(quizElementRef)) {
            return null;
        }

        QuizElement quizElementStub = new QuizElement();
        quizElementStub.setRef(quizElementRef);
        return quizElementStub;
    }

    private QuizElementOption stubQuizElementOption(String quizElementOptionRef) {

        if (StringUtils.isBlank(quizElementOptionRef)) {
            return null;
        }

        QuizElementOption quizElementOptionStub = new QuizElementOption();
        quizElementOptionStub.setRef(quizElementOptionRef);
        return quizElementOptionStub;
    }

    private Quiz stubQuiz(String quizRef) {

        if (StringUtils.isBlank(quizRef)) {
            return null;
        }

        Quiz quizStub = new Quiz();
        quizStub.setRef(quizRef);
        return quizStub;
    }

    private UserAgreement stubUserAgreement(String userAgreementRef) {
        if (StringUtils.isBlank(userAgreementRef)) {
            return null;
        }
        UserAgreement userAgreementStub = new UserAgreement();
        userAgreementStub.setRef(userAgreementRef);
        return userAgreementStub;
    }

    private ArgumentType stubArgumentType(String argumentTypeRef) {

        if (StringUtils.isBlank(argumentTypeRef)) {
            return null;
        }

        ArgumentType argumentTypeStub = new ArgumentType();
        argumentTypeStub.setRef(argumentTypeRef);
        return argumentTypeStub;
    }

    private ReviewStepArgument stubReviewStepArgument(String reviewStepArgumentRef) {

        if (StringUtils.isBlank(reviewStepArgumentRef)) {
            return null;
        }

        ReviewStepArgument reviewStepArgument = new ReviewStepArgument();
        reviewStepArgument.setRef(reviewStepArgumentRef);
        return reviewStepArgument;

    }

    private Course stubCourse(String courseRef) {

        if (StringUtils.isBlank(courseRef)) {
            return null;
        }

        Course course = new Course();
        course.setRef(courseRef);
        return course;

    }

    private Section stubSection(String sectionRef) {

        if (StringUtils.isBlank(sectionRef)) {
            return null;
        }

        Section section = new Section();
        section.setRef(sectionRef);
        return section;
    }

    private Resource stubResource(String resourceRef) {

        if (StringUtils.isBlank(resourceRef)) {
            return null;
        }

        Resource resource = new Resource();
        resource.setRef(resourceRef);
        return resource;

    }

    private FileUploadMetadata stubFileUploadMetadata(String fileUploadMetadataRef) {

        if (StringUtils.isBlank(fileUploadMetadataRef)) {
            return null;
        }

        FileUploadMetadata fileUploadMetadata = new FileUploadMetadata();
        fileUploadMetadata.setRef(fileUploadMetadataRef);
        return fileUploadMetadata;
    }

    private User stubUser(String userRef) {

        if (StringUtils.isBlank(userRef)) {
            return null;
        }

        User stubbedUser = new User();
        stubbedUser.setRef(userRef);
        return stubbedUser;
    }

    private Skill stubSkill(String skillRef) {

        if (StringUtils.isBlank(skillRef)) {
            return null;
        }

        Skill stubbedSkill = new Skill();
        stubbedSkill.setRef(skillRef);
        return stubbedSkill;
    }
}

