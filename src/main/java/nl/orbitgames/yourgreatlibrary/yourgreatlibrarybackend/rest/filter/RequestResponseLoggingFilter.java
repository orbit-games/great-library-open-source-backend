package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Priority;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Provider
@PreMatching
@Priority(Integer.MIN_VALUE)
public class RequestResponseLoggingFilter implements ContainerRequestFilter, WriterInterceptor {

    private static final Logger logger = LoggerFactory.getLogger("jax-rs-log");

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
    private static final int MAX_ENTITY_SIZE = 1024 * 8;

    private static final List<String> PROPERTIES_TO_MASK = Arrays.asList("password", "newPassword", "forgotPasswordToken", "licenceKey", "token", "sessionToken");
    private static final List<String> HEADERS_TO_MASK = Arrays.asList("X-Token");
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final List<String> MIME_TYPES_TO_LOG = Arrays.asList(
            MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,
            MediaType.TEXT_HTML, MediaType.TEXT_PLAIN, MediaType.TEXT_XML,
            MediaType.APPLICATION_FORM_URLENCODED
    ); 

    private InputStream logInboundEntity(final StringBuilder b, InputStream stream, final Charset charset) throws IOException {
        if (!stream.markSupported()) {
            stream = new BufferedInputStream(stream);
        }
        stream.mark(MAX_ENTITY_SIZE + 1);
        final byte[] entity = new byte[MAX_ENTITY_SIZE + 1];
        final int entitySize = stream.read(entity);
        b.append(new String(entity, 0, Math.min(entitySize, MAX_ENTITY_SIZE), charset));
        if (entitySize > MAX_ENTITY_SIZE) {
            b.append("...more...");
        }
        b.append('\n');
        stream.reset();
        return stream;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        final StringBuilder sb = new StringBuilder();
        String loggedBody = null;

        if (requestContext.hasEntity()) {
            if (allowLogMediaType(requestContext.getHeaderString(CONTENT_TYPE_HEADER))) {
                requestContext.setEntityStream(logInboundEntity(sb, requestContext.getEntityStream(),
                        DEFAULT_CHARSET));
            } else {
                sb.append("<<binary data>>");
            }


            loggedBody = "\r\nBody:\r\n" + mask(sb.toString());
        }
        logger.info(requestContext.getMethod() + " request to " + requestContext.getUriInfo().getAbsolutePath() + ":\r\n" +
                "Headers:\r\n" + toMaskedString(requestContext.getHeaders()) +
                (loggedBody == null ? "" : loggedBody));

    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext context)
            throws IOException, WebApplicationException {

        OutputStream originalStream = context.getOutputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        context.setOutputStream(baos);
        try {
            // We let other writers modify the content (if needed) here
            context.proceed();

            // Time to generate the signature
            String responseBody;
            if (allowLogMediaType(context.getHeaders().getFirst(CONTENT_TYPE_HEADER).toString())) {
                byte[] responseBodyBytes = baos.toByteArray();
                responseBody = new String(responseBodyBytes, StandardCharsets.UTF_8);
            } else {
                responseBody = "<<binary data>>";
            }

            responseBody = mask(responseBody);
            logger.info("Response :\r\n" +
                    "Headers:\r\n" + toMaskedString(context.getHeaders()) +
                    "\r\nBody:\r\n" + responseBody);

        } catch (Exception e) {
            // We don't want a failure during logging to cause an error here, so we log this exception but continue
            logger.error("An error occurred during JAX-RS Response logging", e);
        } finally {
            // Here we write the content of the original stream back to it (since
            // we consumed it in other to generate the signature).
            baos.writeTo(originalStream);
            baos.close();
            context.setOutputStream(originalStream);
        }
    }

    private boolean allowLogMediaType(String contentType) {
        if (StringUtils.isBlank(contentType)) {
            return false;
        }
        String mimeType = contentType.contains(";") ? contentType.split(";")[0].trim() : contentType.trim();
        return MIME_TYPES_TO_LOG.stream().anyMatch(mimeType::equalsIgnoreCase);
    }

    private static String mask(String body) {

        String maskedBody = body;
        for (String propertyToMask: PROPERTIES_TO_MASK) {
            maskedBody = maskedBody.replaceAll("(\"" + propertyToMask + "\"\\s*:\\s*)\"(.*?)\"", "$1\"*****\"");
        }

        return maskedBody;
    }

    private static <T> String toMaskedString(MultivaluedMap<String, T> headers) {
        return headers.keySet()
                .stream()
                .map(k -> "    " + toMaskedString(k, headers.get(k)))
                .collect(Collectors.joining("\r\n"));

    }

    private static <T> String toMaskedString(String headerName, List<T> headerValues) {
        if (HEADERS_TO_MASK.stream().anyMatch(h -> StringUtils.equalsIgnoreCase(h, headerName))) {
            return headerName + ": *****";
        } else {
            return headerName + ": " + headerValues.stream()
                    .map(Object::toString)
                    .map(s -> "\"" + s + "\"")
                    .collect(Collectors.joining(","));
        }
    }
}