package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.ICalComponent;
import biweekly.component.VEvent;
import biweekly.component.VTodo;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Locale;

@Component
public class DeadlineICalConverter {

    private final MessageSource messageSource;

    @Autowired
    public DeadlineICalConverter(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String constructICal(Course course, boolean includeDisabled) {

        ICalendar iCal = new ICalendar();

        for (Chapter chapter: course.getChapters()) {
            for (ReviewStep reviewStep: chapter.getReviewSteps()) {

                if (reviewStep.isDisabled()) continue;

                ICalComponent todo = createReviewStepTodo(reviewStep);
                iCal.addComponent(todo);
            }
        }

        return Biweekly.write(iCal).go();
    }

    private ICalComponent createReviewStepTodo(ReviewStep reviewStep) {

        Locale locale = Locale.getDefault();

        VEvent todo = new VEvent();
//        todo.setDateDue(new Date(reviewStep.getDeadline().toInstant().toEpochMilli()), true);
        todo.setDateStart(new Date(reviewStep.getDeadline().toInstant().toEpochMilli()), true);
        todo.setSummary(messageSource.getMessage(Messages.DEADLINE_CALENDAR_EVENT_SUMMARY,
                new Object[] {
                        reviewStep.getChapter().getName(),
                        reviewStep.getName(),
                        reviewStep.getChapter().getCourse().getName()
                },
                locale));
        todo.setLocation(messageSource.getMessage(Messages.DEADLINE_CALENDAR_EVENT_LOCATION, new Object[0], locale));
        todo.setDescription(messageSource.getMessage(Messages.DEADLINE_CALENDAR_EVENT_DESCRIPTION, new Object[0], locale));
        return todo;
    }

}
