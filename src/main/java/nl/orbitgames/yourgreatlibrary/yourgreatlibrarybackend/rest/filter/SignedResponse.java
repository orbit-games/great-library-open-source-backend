package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * JAX-RS resource methods annotated with @{@link SignedResponse} will have
 * a signature included in the response.
 * @see SignatureFilter
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface SignedResponse {
}
