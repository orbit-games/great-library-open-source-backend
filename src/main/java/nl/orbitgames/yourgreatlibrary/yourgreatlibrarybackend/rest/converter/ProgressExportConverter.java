package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter;

import io.swagger.model.CourseProgressExportColumnDataType;
import io.swagger.model.CourseProgressExportColumnSpecification;
import io.swagger.model.CourseProgressExportColumnType;
import io.swagger.model.CourseProgressExportSpecification;
import io.swagger.model.CourseProgressExportStepExecution;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.PeerType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepResultWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewerRelationResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.Messages;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ProgressExportConverter {

    private static final Logger logger = LoggerFactory.getLogger(ProgressExportConverter.class);

    private static final String STYLE_HEADER = "header";
    private static final String STYLE_HEADER_BOTTOM_BORDER = "header-bottom-border";
    private static final String STYLE_COLUMN_LEFT_BORDER = "column-left-border";

    private final MessageSource messageSource;

    @Autowired
    public ProgressExportConverter(
            MessageSource messageSource
    ) {
        this.messageSource = messageSource;
    }

    public void writeProgressExport(OutputStream os,
                                    Course course,
                                    List<ReviewerRelationResult> reviewerRelationResults,
                                    CourseProgressExportSpecification specification)
            throws IOException {

        Locale locale = Locale.getDefault();

        List<CourseProgressExportColumnSpecification> columns = specification.getColumns();
        if (CollectionUtils.isEmpty(columns)) {
            columns = getAllColumnsForCourse(course);
        }

        Workbook wb = new XSSFWorkbook();
        Map<String, CellStyle> styles = createStyles(wb);
        Sheet sheet = wb.createSheet();

        writeHeaders(columns, course, styles, sheet, locale);
        writeContent(reviewerRelationResults, columns, course, styles, sheet, locale);
        int colCount = sheet.getRow(0).getLastCellNum();
        for (int i = 0; i < colCount; i++) {
            sheet.autoSizeColumn(i);
        }

        wb.write(os);
        wb.close();
    }

    private void writeHeaders(List<CourseProgressExportColumnSpecification> columns, Course course, Map<String, CellStyle> styles, Sheet sheet, Locale locale) {

        // Create the headers
        Row headerRow1 = sheet.createRow(0);
        Row headerRow2 = sheet.createRow(1);
        Row headerRow3 = sheet.createRow(2);
        Row headerRow4 = sheet.createRow(3);

        headerRow1.setRowStyle(styles.get(STYLE_HEADER));
        headerRow2.setRowStyle(styles.get(STYLE_HEADER));
        headerRow3.setRowStyle(styles.get(STYLE_HEADER));
        headerRow4.setRowStyle(styles.get(STYLE_HEADER_BOTTOM_BORDER));

        int col = 0;

        // The first 3 headers are default, sometimes the "student number" header/column is also included:
        {
            Cell fullNameHeaderCell = headerRow1.createCell(col++);
            fullNameHeaderCell.setCellValue(messageSource.getMessage(Messages.FULL_NAME, new Object[]{}, locale));

            Cell avatarNameHeaderCell = headerRow1.createCell(col++);
            avatarNameHeaderCell.setCellValue(messageSource.getMessage(Messages.AVATAR_NAME, new Object[]{}, locale));

            Cell emailHeaderCell = headerRow1.createCell(col++);
            emailHeaderCell.setCellValue(messageSource.getMessage(Messages.EMAIL, new Object[]{}, locale));

            if (showStudentNumberColumn(course)) {
                Cell studentNumberHeaderCell = headerRow1.createCell(col++);
                studentNumberHeaderCell.setCellValue(messageSource.getMessage(Messages.STUDENT_NUMBER, new Object[]{}, locale));
            }

            sheet.setDefaultColumnStyle(col, styles.get(STYLE_COLUMN_LEFT_BORDER));
        }

        Chapter lastChapter = null;

        // The rest are based on the specs:
        for (CourseProgressExportColumnSpecification column: columns) {

            if (column.getColumnType() == CourseProgressExportColumnType.STEP_DETAIL) {

                Chapter chapter = course.getChapters().stream().filter(c -> c.getRef().equals(column.getChapterRef()))
                        .findAny()
                        .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + column.getChapterRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

                ReviewStep reviewStep = nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils.findBy(chapter.getReviewSteps(), r -> r.getRef().equals(column.getReviewStepRef())                )
                        .orElseThrow(() -> new OrbitGamesApplicationException("Could not find review step with ref '" + column.getReviewStepRef() + "' within chapter '" + chapter.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

                int reviewerCount = chapter.getReviewerRelationGenerationSettings().getReviewerCount();

                if (lastChapter != chapter) {
                    sheet.setDefaultColumnStyle(col, styles.get(STYLE_COLUMN_LEFT_BORDER));
                }
                lastChapter = chapter;

                // For all steps except the "minutes late", we need a column for every reviewer
                for (int i = 0; i < (column.getDataType() == CourseProgressExportColumnDataType.MINUTES_LATE ? 1 : reviewerCount); i++) {

                    headerRow1.createCell(col).setCellValue(course.getChapters().indexOf(chapter) + 1);
                    headerRow2.createCell(col).setCellValue(reviewStep.getName());
                    headerRow3.createCell(col).setCellValue(toLocalizedString(column.getDataType(), locale));
                    headerRow4.createCell(col).setCellValue(toLocalizedString(column.getStepExecution(), locale));

                    col++;
                }

            } else if (column.getColumnType() == CourseProgressExportColumnType.TOTAL_MINUTES_LATE) {

                headerRow2.createCell(col).setCellValue(messageSource.getMessage(Messages.TOTAL, new Object[]{}, locale));
                headerRow3.createCell(col).setCellValue(messageSource.getMessage(Messages.MINUTES_LATE, new Object[]{}, locale));

                if (lastChapter != null) {
                    sheet.setDefaultColumnStyle(col, styles.get(STYLE_COLUMN_LEFT_BORDER));
                }
                lastChapter = null;
                col++;
            } else if (column.getColumnType() == CourseProgressExportColumnType.TOTAL_SKIPPED_STEPS) {

                headerRow2.createCell(col).setCellValue(messageSource.getMessage(Messages.TOTAL, new Object[]{}, locale));
                headerRow3.createCell(col).setCellValue(messageSource.getMessage(Messages.INCOMPLETE, new Object[]{}, locale));

                if (lastChapter != null) {
                    sheet.setDefaultColumnStyle(col, styles.get(STYLE_COLUMN_LEFT_BORDER));
                }
                lastChapter = null;
                col++;
            }
        }
    }

    private List<UserCourse> activeLearnerUserCourses(Course course) {
        return course.getUserCourses().stream()
                // Only active learner users
                .filter(u -> u.isActive() && u.getUser().getRole() == Role.LEARNER)
                .collect(Collectors.toList());
    }

    private boolean showStudentNumberColumn(Course course) {
        return activeLearnerUserCourses(course).stream()
                .anyMatch(uc -> org.apache.commons.lang3.StringUtils.isNotBlank(uc.getUser().getStudentNumber()));
    }

    private void writeContent(List<ReviewerRelationResult> reviewerRelationResults, List<CourseProgressExportColumnSpecification> columns, Course course, Map<String, CellStyle> styles, Sheet sheet, Locale locale) {

        List<UserCourse> userCourses = activeLearnerUserCourses(course);
        boolean showStudentNumberColumn = showStudentNumberColumn(course);

        int rowIndex = 4;
        for(UserCourse userCourse: userCourses) {

            Row row = sheet.createRow(rowIndex++);

            User user = userCourse.getUser();

            int columnIndex = 0;
            row.createCell(columnIndex++).setCellValue(user.getFullName());
            row.createCell(columnIndex++).setCellValue(userCourse.getDisplayName());
            row.createCell(columnIndex++).setCellValue(user.getEmail());
            if (showStudentNumberColumn) {
                row.createCell(columnIndex++).setCellValue(user.getEmail());
            }

            for (CourseProgressExportColumnSpecification column: columns) {

                if (column.getColumnType() == CourseProgressExportColumnType.STEP_DETAIL) {

                    Chapter chapter = nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils.findBy(course.getChapters(), c -> c.getRef().equals(column.getChapterRef()))
                            .orElseThrow(() -> new OrbitGamesApplicationException("Could not find chapter with ref '" + column.getChapterRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

                    ReviewStep reviewStep = nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.util.CollectionUtils.findBy(chapter.getReviewSteps(), r -> r.getRef().equals(column.getReviewStepRef()))
                            .orElseThrow(() -> new OrbitGamesApplicationException("Could not find review step with ref '" + column.getReviewStepRef() + "' within chapter '" + chapter.getRef() + "'", GenericErrorCode.ENTITY_NOT_FOUND));

                    int reviewerCount = chapter.getReviewerRelationGenerationSettings().getReviewerCount();

                    List<ReviewStepResultWrapper> results = getReviewStepResults(reviewerRelationResults, reviewStep, user, column.getStepExecution());
                    if (column.getDataType() == CourseProgressExportColumnDataType.MINUTES_LATE) {
                        int curColIdx = columnIndex;
                        results.stream()
                                .mapToLong(r -> calculateMinutesLate(r.getReviewStepResult(), reviewStep))
                                .max()
                                .ifPresent(minutesLate -> row.createCell(curColIdx).setCellValue(minutesLate));
                        columnIndex++;

                    } else if (column.getDataType() == CourseProgressExportColumnDataType.SCORE) {

                        columnIndex = writeStepResults(row, columnIndex, reviewerCount, results,
                                r -> r.getReviewStepResult() != null && r.getReviewStepResult().getQuizResult() != null,
                                r -> r.getReviewStepResult().getQuizResult().getScore());

                    } else if (column.getDataType() == CourseProgressExportColumnDataType.WORD_COUNT) {

                        columnIndex = writeStepResults(row, columnIndex, reviewerCount, results,
                                r -> r.getReviewStepResult() != null && CollectionUtils.isNotEmpty(r.getReviewStepResult().getArguments()),
                                r -> r.getReviewStepResult().getArguments().stream().mapToLong(a -> StringUtils.wordCount(a.getMessage())).sum());
                    }
                } else if (column.getColumnType() == CourseProgressExportColumnType.TOTAL_MINUTES_LATE) {

                    long totalMinutesLate = 0;
                    for (Chapter chapter: course.getChapters()) {
                        int reviewerCount = chapter.getReviewerRelationGenerationSettings().getReviewerCount();
                        for (ReviewStep reviewStep: chapter.getReviewSteps()) {

                            if (reviewStep.isDisabled()) {
                                continue;
                            }

                            List<ReviewStepResultWrapper> reviewStepResults =
                                    getReviewStepResults(reviewerRelationResults, reviewStep, user, CourseProgressExportStepExecution.GIVEN);

                            if (reviewStep.getStepType() == ReviewStepType.SUBMISSION) {
                                // Since the submission is only one action (but we have multiple results), we only count it once
                                totalMinutesLate += reviewStepResults.isEmpty()
                                        ? calculateMinutesLate(null, reviewStep)
                                        : calculateMinutesLate(reviewStepResults.get(0).getReviewStepResult(), reviewStep);
                            } else {

                                totalMinutesLate += reviewStepResults.stream()
                                        .mapToLong(r -> calculateMinutesLate(r.getReviewStepResult(), reviewStep))
                                        .sum();
                                if (reviewStepResults.size() < reviewerCount && reviewStep.getDeadline().isBefore(OffsetDateTime.now())) {
                                    totalMinutesLate += (reviewerCount - reviewStepResults.size()) * calculateMinutesLate(null, reviewStep);
                                }
                            }
                        }
                    }

                    row.createCell(columnIndex).setCellValue(totalMinutesLate);
                    columnIndex++;

                } else if (column.getColumnType() == CourseProgressExportColumnType.TOTAL_SKIPPED_STEPS) {

                    long totalSkippedSteps = 0;
                    for (Chapter chapter: course.getChapters()) {
                        int reviewerCount = chapter.getReviewerRelationGenerationSettings().getReviewerCount();
                        for (ReviewStep reviewStep: chapter.getReviewSteps()) {

                            if (reviewStep.isDisabled() || reviewStep.getDeadline().isAfter(OffsetDateTime.now())) {
                                // Don't count as a skipped step if the deadline has not yet passed
                                continue;
                            }

                            List<ReviewStepResultWrapper> reviewStepResults =
                                    getReviewStepResults(reviewerRelationResults, reviewStep, user, CourseProgressExportStepExecution.GIVEN);

                            long completedTracksForCurrentStep = reviewStepResults.stream()
                                    .filter(r -> r.getReviewStepResult().getMarkedComplete() != null)
                                    .count();

                            if (completedTracksForCurrentStep < reviewerCount) {
                                if (reviewStep.getStepType() == ReviewStepType.SUBMISSION) {
                                    // Submission only counts as one step independent of the number of tracks/reviewers
                                    totalSkippedSteps += 1;
                                } else {
                                    totalSkippedSteps += reviewerCount - completedTracksForCurrentStep;
                                }
                            }
                        }
                    }

                    row.createCell(columnIndex).setCellValue(totalSkippedSteps);
                    columnIndex++;
                }
            }

        }
    }

    private long calculateMinutesLate(ReviewStepResult result, ReviewStep reviewStep) {

        long minutesLate = ChronoUnit.MINUTES.between(reviewStep.getDeadline(), ObjectUtils.firstNonNull(result == null ? null : result.getMarkedComplete(), OffsetDateTime.now()));
        if (minutesLate < 0) {
            return 0;
        } else {
            return minutesLate;
        }
    }

    private int writeStepResults(Row row, int columnIndex, int reviewerCount, List<ReviewStepResultWrapper> results,
                                 CellValueCondition resultCondition,
                                 CellValueExtractor resultExtractor) {
        int i = 0;
        for (ReviewStepResultWrapper result: results) {
            if (i++ >= reviewerCount) {
                logger.warn("Found {} results, but the progress export expected {}", results.size(), reviewerCount);
                break;
            }
            if (resultCondition.evaluate(result)) {
                row.createCell(columnIndex).setCellValue(resultExtractor.getCellValue(result));
            }
            columnIndex++;
        }
        if (results.size() < reviewerCount) {
            columnIndex += reviewerCount - results.size();
        }
        return columnIndex;
    }

    private interface CellValueExtractor {
        long getCellValue(ReviewStepResultWrapper result);
    }

    private interface CellValueCondition {
        boolean evaluate(ReviewStepResultWrapper result);
    }

    private static List<ReviewStepResultWrapper> getReviewStepResults(List<ReviewerRelationResult> reviewerRelationResults,
                                                                      ReviewStep reviewStep,
                                                                      User user,
                                                                      CourseProgressExportStepExecution execution) {

        PeerType peerType;
        if (execution == CourseProgressExportStepExecution.GIVEN) {
            peerType = reviewStep.getStepType().getPostedBy();
        } else {
            peerType = reviewStep.getStepType().getPostedBy().opposite();
        }

        return reviewerRelationResults.stream()
                .filter(r -> (peerType == PeerType.SUBMITTER
                        ? r.getSubmitter().getRef().equals(user.getRef())
                        : (r.getReviewer() != null && r.getReviewer().getRef().equals(user.getRef()))))
                .flatMap(r -> r.getStepResults().stream().filter(s -> s.getReviewStepResult().getReviewStep().getRef().equals(reviewStep.getRef())))
                // We sort by the opponents full name, so that the results are consistent in order
                .sorted(Comparator
                        .comparing(r -> (peerType == PeerType.SUBMITTER && r.getReviewStepResult().getReviewerRelation().getReviewer() != null)
                                ? r.getReviewStepResult().getReviewerRelation().getReviewer().getFullName()
                                : r.getReviewStepResult().getReviewerRelation().getSubmitter().getFullName())
                )
                .collect(Collectors.toList());
    }

    private static Map<String, CellStyle> createStyles(Workbook wb) {

        Map<String, CellStyle> styles = new HashMap<>();
        DataFormat df = wb.createDataFormat();

        short black = IndexedColors.BLACK.getIndex();


        CellStyle headerStyle =  wb.createCellStyle();
        Font headerFont = wb.createFont();
        headerFont.setBold(true);
        headerStyle.setAlignment(HorizontalAlignment.LEFT);
        headerStyle.setFont(headerFont);
        styles.put(STYLE_HEADER, headerStyle);

        CellStyle headerBottomBorderStyle =  wb.createCellStyle();
        headerBottomBorderStyle.setAlignment(HorizontalAlignment.LEFT);
        headerBottomBorderStyle.setFont(headerFont);
        headerBottomBorderStyle.setBorderBottom(BorderStyle.MEDIUM);
        headerBottomBorderStyle.setBottomBorderColor(black);
        styles.put(STYLE_HEADER_BOTTOM_BORDER, headerBottomBorderStyle);

        CellStyle columnLeftBorderStyle =  wb.createCellStyle();
        columnLeftBorderStyle.setBorderLeft(BorderStyle.MEDIUM);
        columnLeftBorderStyle.setLeftBorderColor(black);
        styles.put(STYLE_COLUMN_LEFT_BORDER, columnLeftBorderStyle);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb){
        BorderStyle thin = BorderStyle.THIN;
        short black = IndexedColors.BLACK.getIndex();

        CellStyle style = wb.createCellStyle();
        style.setBorderRight(thin);
        style.setRightBorderColor(black);
        style.setBorderBottom(thin);
        style.setBottomBorderColor(black);
        style.setBorderLeft(thin);
        style.setLeftBorderColor(black);
        style.setBorderTop(thin);
        style.setTopBorderColor(black);
        return style;
    }

    private String toLocalizedString(CourseProgressExportColumnDataType dataType, Locale locale) {

        final String messageId;
        switch(dataType) {
            case MINUTES_LATE: messageId = Messages.MINUTES_LATE; break;
            case SCORE: messageId = Messages.SCORE; break;
            case WORD_COUNT: messageId = Messages.WORD_COUNT; break;
            default: throw new OrbitGamesSystemException("Unknown data type: " + dataType);
        }
        return messageSource.getMessage(messageId, new Object[]{}, locale);
    }

    private String toLocalizedString(CourseProgressExportStepExecution execution, Locale locale) {

        final String messageId;
        switch(execution) {
            case GIVEN: messageId = Messages.GIVEN; break;
            case RECEIVED: messageId = Messages.RECEIVED; break;
            default: throw new OrbitGamesSystemException("Unknown step execution: " + execution);
        }
        return messageSource.getMessage(messageId, new Object[]{}, locale);
    }

    private List<CourseProgressExportColumnSpecification> getAllColumnsForCourse(Course course) {

        List<ReviewStep> reviewSteps = course.getChapters().stream().flatMap(c -> c.getReviewSteps().stream()).collect(Collectors.toList());
        List<CourseProgressExportColumnSpecification> res = new ArrayList<>();

        res.add(new CourseProgressExportColumnSpecification().columnType(CourseProgressExportColumnType.TOTAL_MINUTES_LATE));
        res.add(new CourseProgressExportColumnSpecification().columnType(CourseProgressExportColumnType.TOTAL_SKIPPED_STEPS));

        for (ReviewStep reviewStep: reviewSteps) {

            if (reviewStep.isDisabled()) {
                continue;
            }

            if (reviewStep.getQuiz() != null) {
                res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.SCORE, CourseProgressExportStepExecution.GIVEN));
                res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.SCORE, CourseProgressExportStepExecution.RECEIVED));
            }

            if (reviewStep.getDiscussionType() != DiscussionType.NONE) {
                res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.WORD_COUNT, CourseProgressExportStepExecution.GIVEN));
                res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.WORD_COUNT, CourseProgressExportStepExecution.RECEIVED));
            }

            res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.MINUTES_LATE, CourseProgressExportStepExecution.GIVEN));
            res.add(createSpecificationFor(reviewStep, CourseProgressExportColumnDataType.MINUTES_LATE, CourseProgressExportStepExecution.RECEIVED));
        }

        return res;
    }

    private CourseProgressExportColumnSpecification createSpecificationFor(ReviewStep reviewStep,
                                                                           CourseProgressExportColumnDataType dataType,
                                                                           CourseProgressExportStepExecution execution) {
        return new CourseProgressExportColumnSpecification()
                .columnType(CourseProgressExportColumnType.STEP_DETAIL)
                .chapterRef(reviewStep.getChapter().getRef())
                .reviewStepRef(reviewStep.getRef())
                .dataType(dataType)
                .stepExecution(execution);
    }
}
