package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest;


import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.ClientBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.CourseBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.FileBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.ServerBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.TestBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.UserAgreementBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd.UserBd;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter.RequestResponseLoggingFilter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter.SecurityFilter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter.SignatureFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/rest/v1")
public class YourGreatLibraryRestApplication extends ResourceConfig {

    public YourGreatLibraryRestApplication() {
        // Resource providers
        register(UserBd.class);
        register(CourseBd.class);
        register(ClientBd.class);
        register(UserAgreementBd.class);
        register(FileBd.class);
        register(ServerBd.class);
        register(TestBd.class);

        register(OrbitGamesExceptionHandler.class);
        register(ConstraintViolationExceptionHandler.class);

        // Filters
        register(SecurityFilter.class);
        register(SignatureFilter.class);
        register(RequestResponseLoggingFilter.class);

        // Jersey provider for multipart support:
        register(MultiPartFeature.class);
    }
}
