package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.Announcement;
import io.swagger.model.BugReport;
import io.swagger.model.ClientVersion;
import io.swagger.model.ClientVersionCheckResponse;
import io.swagger.model.Platform;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.client.ClientController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ClientVersionCheckResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.RestToDomainModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("client")
@Produces("application/json")
@Consumes("application/json")
@Timed
public class ClientBd {

    private final ClientController clientController;
    private final DomainToRestModelConverter domainToRestModelConverter;
    private final RestToDomainModelConverter restToDomainModelConverter;

    public ClientBd(
            @Autowired ClientController clientController,
            @Autowired DomainToRestModelConverter domainToRestModelConverter,
            @Autowired RestToDomainModelConverter restToDomainModelConverter
            ) {
        this.clientController = clientController;
        this.domainToRestModelConverter = domainToRestModelConverter;
        this.restToDomainModelConverter = restToDomainModelConverter;
    }

    @GET
    @Path("announcement")
    @PermitAll
    public List<Announcement> getClientAnnouncements() {

        List<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement> announcements = this.clientController.getActiveAnnouncements();
        return announcements.stream().map(domainToRestModelConverter::convert).collect(Collectors.toList());

    }

    @POST
    @Path("announcement")
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public Announcement addAnnouncement(Announcement announcement) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement newAnnouncement =
                clientController.addAnnouncement(restToDomainModelConverter.convert(announcement));

        return domainToRestModelConverter.convert(newAnnouncement);

    }

    @GET
    @Path("version/check")
    @PermitAll
    public ClientVersionCheckResponse getClientVersionCheck(
            @QueryParam("platform") Platform platform,
            @QueryParam("build") int buildNumber
            ) {

        ClientVersionCheckResult versionCheckResult = clientController.versionCheck(restToDomainModelConverter.convert(platform), buildNumber);

        return new ClientVersionCheckResponse()
                .status(domainToRestModelConverter.convert(versionCheckResult.getStatus()))
                .current(domainToRestModelConverter.convert(versionCheckResult.getCurrent()))
                .latest(domainToRestModelConverter.convert(versionCheckResult.getLatest()));
    }

    @POST
    @Path("version")
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public ClientVersion postClientVersion(ClientVersion clientVersion) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion addedClientVersion =
                clientController.addClientVersion(restToDomainModelConverter.convert(clientVersion));

        return domainToRestModelConverter.convert(addedClientVersion);

    }

    @POST
    @Path("bug-report")
    @PermitAll
    public void postClientBugReport(BugReport bugReport) {

        clientController.createBugReport(restToDomainModelConverter.convert(bugReport),
                bugReport.getScreenshots().stream().map(restToDomainModelConverter::convert).collect(Collectors.toSet()));

    }

}
