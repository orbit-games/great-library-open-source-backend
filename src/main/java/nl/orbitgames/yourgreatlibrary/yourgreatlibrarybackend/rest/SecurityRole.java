package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;

public final class SecurityRole {

    /**
     * Students or learners, may not modify courses.
     * @see Role#LEARNER
     */
    public static final String LEARNER = "LEARNER";

    /**
     * Administrators (or superusers)
     * @see Role#ADMINISTRATOR
     */
    public static final String ADMINISTRATOR = "ADMINISTRATOR";

    /**
     * Teachers, may modify courses and view all students
     * @see Role#TEACHER
     */
    public static final String TEACHER = "TEACHER";
    /**
     * Any logged in user
     */
    public static final String LOGGED_IN = "LOGGED_IN";

    public static final String TEST_API = "TEST_API";

    public static String getSecurityRole(Role role) {
        return role.toString();
    }

}
