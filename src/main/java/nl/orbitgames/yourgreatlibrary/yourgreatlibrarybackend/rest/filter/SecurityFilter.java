package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user.AuthenticationController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This filter verify the access permissions for a user based on
 * user name and password provided in request
 * */
@Provider
public class SecurityFilter implements ContainerRequestFilter
{
    private static final String TOKEN_HEADER = "X-Token";

    @Context
    private ResourceInfo resourceInfo;

    private AuthenticationController authenticationController;

    @Autowired
    public void setAuthenticationController(AuthenticationController authenticationController) {
        this.authenticationController = authenticationController;
    }

    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        Method method = resourceInfo.getResourceMethod();

        if (method == null) {
            return;
        }

        //TODO Correctly handle class-level annotations

        //Access allowed for all
        if(!method.isAnnotationPresent(PermitAll.class))
        {
            //Access denied for all
            if(method.isAnnotationPresent(DenyAll.class))
            {
                throw new OrbitGamesAuthorizationException();
            }

            //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();

            //Fetch authorization header
            final String token = headers.getFirst(TOKEN_HEADER);

            //If no authorization information present; block access
            if(StringUtils.isBlank(token))
            {
                throw new OrbitGamesAuthorizationException();
            }

            AuthenticationController.TokenLoginResult loginResult = authenticationController.tokenLogin(token);

            //Verify user access
            if(method.isAnnotationPresent(RolesAllowed.class))
            {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if(!isUserAllowed(loginResult.getSecurityRoles(), rolesSet))
                {
                    throw new OrbitGamesAuthorizationException();
                }
            }
        }
    }

    private boolean isUserAllowed(Set<String> userRoles, final Set<String> allowedRoles) {
        // Admins are allowed to executed all requests (they're like superusers)
        if (userRoles.contains(SecurityRole.ADMINISTRATOR)) {
            return true;
        }

        // Otherwise, the user must have at least one of the allowed roles:
        if (userRoles.stream().anyMatch(allowedRoles::contains)) {
            return true;
        }

        return false;
    }
}