package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest;

import io.swagger.model.ErrorDetails;
import io.swagger.model.ErrorResponse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesApplicationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.YourGreatLibraryLoginException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.validation.ValidationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;

@Provider
public class OrbitGamesExceptionHandler implements ExceptionMapper<Throwable> {

	private static final Logger logger = LoggerFactory.getLogger(OrbitGamesExceptionHandler.class);

	private static final int INITIAL_STACK_SEARCH_DEPTH = 50;

	@Autowired
	private YourGreatLibraryConfig config;

	@Context
    private HttpHeaders headers;

	@Override
	public Response toResponse(Throwable exception) {

		if (exception instanceof OrbitGamesApplicationException) {
			logger.info("Orbit Games Application exception: " + exception.getMessage());
		} else if (exception instanceof RuntimeException) {
			logger.error("Runtime exception while processing the request: " + exception.getMessage(), exception);
		} else if (exception instanceof Exception) {
			logger.warn("Checked exception while processingf the request: " + exception.getMessage());
		} else {
			logger.error("Error while processing the request: " + exception.getMessage(), exception);
		}

		int statusCode = determineStatusCode(exception);
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setError(getErrorDetails(exception));
		
		MediaType m = MediaType.APPLICATION_JSON_TYPE;
		List<MediaType> accepts = headers.getAcceptableMediaTypes();
	    if (accepts != null && !accepts.isEmpty() &&
	    		// If application/json is not allowed, we pick whatever is allowed
	    		!accepts.stream().anyMatch(a -> a.isCompatible(MediaType.APPLICATION_JSON_TYPE))) {
	    	
	        m = accepts.get(0);
	    }
		
		return Response
				.status(statusCode)
				.type(m)
				.entity(errorResponse)
				.build();
	}
	
	private ErrorDetails getErrorDetails(Throwable t) {
		
		Throwable exception = getNonTrivialCause(t);
		
		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setCode(getErrorCode(exception));
		errorDetails.setMessage(exception.getMessage());
		
		// TODO There's currently no way to determine the target of an exception here...
		
		if (config.isTestApplicationEnvironment()) {
			errorDetails.setTrace(ExceptionUtils.getStackTrace(exception));
		}
		
		return errorDetails;
		
	}
	
	private String getErrorCode(Throwable exception) {
		
		if (exception instanceof OrbitGamesApplicationException) {
			OrbitGamesApplicationException applicationException = (OrbitGamesApplicationException)exception;
			return applicationException.getErrorCode().getErrorCode();
		}
		
		// TODO: Maybe we need a better error code for some java exceptions (e.g. ValidationException).
		return GenericErrorCode.UNKNOWN_ERROR.getErrorCode();
	}
	
	private int determineStatusCode(Throwable exception) {
		return determineStatusCode(exception, INITIAL_STACK_SEARCH_DEPTH);
	}
	
	private int determineStatusCode(Throwable exception, int stackSearchDepth) {
		
		if (exception instanceof YourGreatLibraryLoginException || exception instanceof OrbitGamesAuthorizationException) {
			return HttpStatus.UNAUTHORIZED.value();
		} else if (exception instanceof OrbitGamesApplicationException) {
			return HttpStatus.BAD_REQUEST.value();
		} else {
			if (exception.getCause() != null && stackSearchDepth > 0) {
				return determineStatusCode(exception.getCause(), stackSearchDepth - 1);
			}
			
			if (((exception instanceof RuntimeException) && !(exception instanceof ValidationException)) || (exception instanceof Error)) {
				return HttpStatus.INTERNAL_SERVER_ERROR.value();
			} else {
				return HttpStatus.BAD_REQUEST.value();
			}
			
		}
	}
	
	private Throwable getNonTrivialCause(Throwable t) {
		return getNonTrivialCause(t, INITIAL_STACK_SEARCH_DEPTH);
	}
	
	private Throwable getNonTrivialCause(Throwable t, int stackSearchDepth) {
		return t;
	}
}
