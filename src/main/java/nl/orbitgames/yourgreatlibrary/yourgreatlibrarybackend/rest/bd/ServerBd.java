package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.ServerInfo;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.course.CourseController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Objects;

@Component
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("server")
@Timed
public class ServerBd {

    private final YourGreatLibraryConfig config;
    private final CacheManager cacheManager;
    private final CourseController courseController;

    public ServerBd(
            @Autowired YourGreatLibraryConfig config,
            @Autowired CacheManager cacheManage,
            @Autowired CourseController courseController
            ) {
        this.config = config;
        this.cacheManager = cacheManage;
        this.courseController = courseController;
    }

    @Path("info")
    @GET
    @PermitAll
    public ServerInfo getServerInfo() {

        return new ServerInfo()
                .currentTime(OffsetDateTime.now())
                .reviewStepUploadMaxSize(config.getReviewStepUploadMaxSize())
                .reviewStepUploadAllowedFileExtensions(config.getReviewStepUploadAllowedFileTypes())
                .reviewStepUploadAllowedMimeTypes(config.getReviewStepUploadAllowedMimeTypes())
                .contentUploadMaxSize(config.getContentUploadMaxSize())
                .contentUploadAllowedFileExtensions(config.getContentUploadAllowedFileTypes())
                .contentUploadAllowedMimeTypes(config.getContentUploadAllowedMimeTypes())
                .allowedEmailDomains(new ArrayList<>(config.getUserAllowedEmailDomains()));
    }

    @Path("cache")
    @DELETE
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public void clearCache() {
        cacheManager.getCacheNames().forEach(name -> Objects.requireNonNull(cacheManager.getCache(name)).clear());
    }

    @Path("reschedule-jobs")
    @POST
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public void rescheduleJobs() {
        this.courseController.rescheduleJobs();
    }
}
