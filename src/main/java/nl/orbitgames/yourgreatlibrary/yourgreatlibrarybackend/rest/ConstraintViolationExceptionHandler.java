package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest;

import io.swagger.model.ErrorDetails;
import io.swagger.model.ErrorResponse;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.GenericErrorCode;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;


@Provider
public class ConstraintViolationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

	private static final Logger logger = LoggerFactory.getLogger(ConstraintViolationExceptionHandler.class);

	private static final String APPLICATION_ENVIRONMENT_APPLICATION_PROPERTY = "yourgreatlibrary.application.environment";

	@Autowired
	private YourGreatLibraryConfig config;

	@Context
    private HttpHeaders headers;

	@Override
	public Response toResponse(ConstraintViolationException exception) {

		if (config.isTestApplicationEnvironment()) {
			logger.warn("Constraint violation exception while processing the request", exception);
		} else {
            logger.warn("Constraint violation exception while processing the request: " + exception.getMessage());
        }

		int statusCode = determineStatusCode(exception);
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setError(getErrorDetails(exception));

		MediaType m = MediaType.APPLICATION_JSON_TYPE;
		List<MediaType> accepts = headers.getAcceptableMediaTypes();
	    if (accepts != null && !accepts.isEmpty() &&
	    		// If application/json is not allowed, we pick whatever is allowed
	    		!accepts.stream().anyMatch(a -> a.isCompatible(MediaType.APPLICATION_JSON_TYPE))) {
	    	
	        m = accepts.get(0);
	    }
		
		return Response
				.status(statusCode)
				.type(m)
				.entity(errorResponse)
				.build();
	}
	
	private ErrorDetails getErrorDetails(ConstraintViolationException exception) {
		
		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setCode(getErrorCode(exception));
		errorDetails.setMessage(exception.getMessage());
		
		// TODO There's currently no way to determine the target of an exception here...
		
		if (config.isTestApplicationEnvironment()) {
			errorDetails.setTrace(ExceptionUtils.getStackTrace(exception));
		}
		
		return errorDetails;
		
	}
	
	private String getErrorCode(ConstraintViolationException exception) {

		return GenericErrorCode.CONSTRAINT_VIOLATION.getErrorCode();
	}
	
	private int determineStatusCode(ConstraintViolationException exception) {
		return HttpStatus.BAD_REQUEST.value();
	}
}
