package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.Argument;
import io.swagger.model.ChapterProgress;
import io.swagger.model.CourseProgress;
import io.swagger.model.CourseResourceResult;
import io.swagger.model.CourseSummary;
import io.swagger.model.Property;
import io.swagger.model.QuizResult;
import io.swagger.model.ReviewStepResult;
import io.swagger.model.User;
import io.swagger.model.UserCollection;
import io.swagger.model.UserCourseJoinRequest;
import io.swagger.model.UserForgotPasswordRequest;
import io.swagger.model.UserLoginRequest;
import io.swagger.model.UserLoginResponse;
import io.swagger.model.UserRegistrationRequest;
import io.swagger.model.UserResetPasswordRequest;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.courseprogress.CourseProgressController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user.LoginResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.user.UserController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserProperty;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesAuthorizationException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepResultWrapper;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.UserSession;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.RestToDomainModelConverter;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Path("user")
@Produces("application/json")
@Consumes("application/json")
@DenyAll
@Timed
public class UserBd {

    private final UserController userController;
    private final CourseProgressController courseProgressController;

    private final UserSession userSession;

    private final DomainToRestModelConverter domainToRestModelConverter;
    private final RestToDomainModelConverter restToDomainModelConverter;

    public UserBd(
            @Autowired UserController userController,
            @Autowired CourseProgressController courseProgressController,
            @Autowired UserSession userSession,
            @Autowired DomainToRestModelConverter domainToRestModelConverter,
            @Autowired RestToDomainModelConverter restToDomainModelConverter
    ) {
        this.userController = userController;
        this.courseProgressController = courseProgressController;
        this.userSession = userSession;
        this.domainToRestModelConverter = domainToRestModelConverter;
        this.restToDomainModelConverter = restToDomainModelConverter;
    }

    @Path("login")
    @POST
    @PermitAll
    public UserLoginResponse userLogin(UserLoginRequest request) {

        LoginResult loginResult = userController.login(request.getUsername(), request.getPassword());

        return new UserLoginResponse()
                .sessionToken(loginResult.getToken())
                .user(domainToRestModelConverter.convert(loginResult.getUser()));
    }

    @Path("register")
    @POST
    @PermitAll
    public void userRegistration(UserRegistrationRequest request) {

        userController.registerUser(request.getUsername(), request.getEmail(), request.getFullName(), request.getSignedUserAgreementRef(), request.getPassword(), Role.LEARNER);
    }

    @Path("forgot-password")
    @POST
    @PermitAll
    public void userForgotPassword(UserForgotPasswordRequest request) {
        userController.forgotPassword(request.getUsername());
    }

    @Path("reset-password")
    @POST
    @PermitAll
    public void userResetPassword(UserResetPasswordRequest request) {

        userController.resetPassword(request.getUsername(), request.getForgotPasswordToken(), request.getNewPassword());

    }

    @Path("{ref}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public User getUser(@PathParam("ref") String userRef, @QueryParam("property") String propertiesString) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User domainUser = userController.getUser(getActualRef(userRef));

        Set<String> propertiesFilter;
        if (propertiesString == null || StringUtils.isBlank(propertiesString)) {
            propertiesFilter = Collections.emptySet();
        } else {
            propertiesFilter = Arrays.stream(propertiesString.split(","))
                    .map(String::trim)
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.toSet());
        }

        return domainToRestModelConverter.convert(domainUser, propertiesFilter);

    }

    @GET
    @RolesAllowed(SecurityRole.TEACHER)
    public UserCollection getUsers(@QueryParam("course") String courseRef, @QueryParam("roles") String roleFilterString) {

        Set<Role> roleFilter;
        if (StringUtils.isBlank(roleFilterString)) {
            roleFilter = Collections.emptySet();
        } else {
            roleFilter = Arrays.stream(roleFilterString.split(","))
                    .map(String::trim)
                    .map(Role::valueOf)
                    .collect(Collectors.toSet());
        }

        Collection<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User> users = userController.getUsers(courseRef, roleFilter);
        return new UserCollection()
                .users(users.stream()
                        .sorted(Comparator.comparing(nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User::getCreated))
                        .map(domainToRestModelConverter::convert).collect(Collectors.toList()));
    }

    @Path("{ref}")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public User updateUser(@PathParam("ref") String userRef, User userWithUpdates) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User domainUserWithUpdates = restToDomainModelConverter.convert(userWithUpdates);

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User domainUser = userController.updateUser(getActualRef(userRef), domainUserWithUpdates);

        return domainToRestModelConverter.convert(domainUser);
    }

    @Path("{ref}/property/{propertyKey}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Property getUserProperty(@PathParam("ref") String userRef, @PathParam("propertyKey") String propertyKey) {

        UserProperty userProperty = userController.getUserProperty(getActualRef(userRef), propertyKey);
        return new Property()
                .key(propertyKey)
                .value(userProperty == null ? null : userProperty.getPropertyValue());

    }

    @Path("{ref}/property/{propertyKey}")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public Property setUserProperty(@PathParam("ref") String userRef, @PathParam("propertyKey") String propertyKey, Property updatedProperty) {

        UserProperty userProperty = userController.updateUserProperty(getActualRef(userRef), propertyKey, updatedProperty.getKey(), updatedProperty.getValue());

        return new Property()
                .key(updatedProperty.getKey())
                .value(userProperty.getPropertyValue());
    }

    @Path("{userRef}/course/join")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseSummary postUserCourseJoin(@PathParam("userRef") String userRef, UserCourseJoinRequest request) {

        Course course = courseProgressController.joinCourse(getActualRef(userRef), request.getCourseCode());
        return domainToRestModelConverter.convertToSummary(course);

    }

    @Path("{userRef}/course-progress/{courseRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseProgress getUserCourseProgress(@PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @QueryParam("property") String properties) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.CourseProgress courseProgress = courseProgressController.getCourseProgress(getActualRef(userRef), courseRef);
        return domainToRestModelConverter.convert(courseProgress);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}")
    @GET
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ChapterProgress getUserCourseChapterProgress(@PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ChapterProgress chapterProgress = courseProgressController.getCourseChapterProgress(getActualRef(userRef), courseRef, chapterRef);
        return domainToRestModelConverter.convert(chapterProgress);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/quiz")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult postUserReviewStepQuiz(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef,
            QuizResult quizResult
            ) {

        ReviewStepResultWrapper result =
                courseProgressController.setReviewStepQuizResult(
                        getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef,
                        restToDomainModelConverter.convert(quizResult));

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/fileUpload")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ReviewStepResult postUserReviewStepFileUpload(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail
    ) {

        ReviewStepResultWrapper resultWrapper =
                courseProgressController.setReviewStepFileUpload(
                        getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef,
                        fileDetail.getFileName(), uploadedInputStream);

        return domainToRestModelConverter.convert(resultWrapper);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult postUserReviewStepArgument(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef,
            Argument argumentToAdd
    ) {

        ReviewStepResultWrapper result = courseProgressController.addReviewStepArgument(
                getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef,
                restToDomainModelConverter.convert(argumentToAdd));

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument/{argumentRef}")
    @PUT
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult updateUserReviewStepArgument(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef, @PathParam("argumentRef") String argumentRef,
            Argument argumentWithUpdates
    ) {

        ReviewStepResultWrapper result = courseProgressController.updateReviewStepArgument(
                getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef,
                restToDomainModelConverter.convert(argumentWithUpdates));

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument/{argumentRef}")
    @DELETE
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult updateUserReviewStepArgument(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef, @PathParam("argumentRef") String argumentRef
    ) {

        ReviewStepResultWrapper result = courseProgressController.deleteReviewStepArgument(
                getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef);

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/complete")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult postUserReviewStepComplete(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef
    ) {

        ReviewStepResultWrapper result = courseProgressController.markReviewStepComplete(
                getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/complete")
    @DELETE
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public ReviewStepResult deleteUserReviewStepComplete(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("chapterRef") String chapterRef,
            @PathParam("reviewerRelationRef") String reviewerRelationRef, @PathParam("reviewStepRef") String reviewStepRef
    ) {

        ReviewStepResultWrapper result = courseProgressController.revokeReviewStepComplete(
                getActualRef(userRef), courseRef, chapterRef, reviewerRelationRef, reviewStepRef);

        return domainToRestModelConverter.convert(result);
    }

    @Path("{userRef}/course-progress/{courseRef}/resource/{resourceRef}/quiz")
    @POST
    @RolesAllowed(SecurityRole.LOGGED_IN)
    public CourseResourceResult postResourceQuizResult(
            @PathParam("userRef") String userRef, @PathParam("courseRef") String courseRef, @PathParam("resourceRef") String resourceRef,
            QuizResult quizResult
    ) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ResourceResult resourceResult =
                courseProgressController.setResourceQuizResult(getActualRef(userRef), courseRef, resourceRef, restToDomainModelConverter.convert(quizResult));

        return domainToRestModelConverter.convert(resourceResult);
    }


    /**
     * Handles special cases for the user ref (such as "logged-in")
     * @param userRef The user ref as passed in the request
     * @return A domain-model level user ref (no special cases)
     */
    private String getActualRef(String userRef) {

        if (userRef.equals("logged-in")) {
            if (!userSession.isLoggedIn()) {
                throw new OrbitGamesAuthorizationException("User is not logged in");
            } else {
                return userSession.getUserRef();
            }
        }

        return userRef;

    }
}