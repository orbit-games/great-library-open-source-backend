package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.filter;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.YourGreatLibraryBackendApplicationProperties;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Provider
public class SignatureFilter implements WriterInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(SignatureFilter.class);

    private static final String EC_ALGORITHM = "secp256k1";
    private static final String SIGNATURE_ALGORITHM = "ECDSA";
    private static final String CRYPTOGRAPHY_PROVIDER = "BC"; // BouncyCastle
    private static final String SIGNATURE_HEADER = "X-Signature";

    private String encodedPrivateKey;
    private PrivateKey privateKey;

    private String encodedPublicKey;
    private PublicKey publicKey;

    @Context
    private ResourceInfo resourceInfo;

    public SignatureFilter() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Autowired
    public void setEncodedPrivateKey(@Value("${" + YourGreatLibraryBackendApplicationProperties.SIGNATURE_PRIVATE_KEY + "}") String encodedPrivateKey) {
        this.encodedPrivateKey = encodedPrivateKey;
        privateKey = decodePrivateKey(encodedPrivateKey);
    }

    @Autowired
    public void serEncodedPublicKey(@Value("${" + YourGreatLibraryBackendApplicationProperties.SIGNATURE_PUBLIC_KEY + "}") String encodedPublicKey) {
        this.encodedPublicKey = encodedPublicKey;
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(encodedPublicKey));
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("ECDSA", "BC");
            publicKey = keyFactory.generatePublic(pubKeySpec);
            logger.info("Pub key algorithm: " + publicKey.getAlgorithm());
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new RuntimeException("Could not create public key", e);
        }

    }

    private PrivateKey decodePrivateKey(String encodedPrivateKey) {
        try {
            byte[] encodedPrivateKeyBytes = Base64.getDecoder().decode(encodedPrivateKey);
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKeyBytes);

            KeyFactory keyFactory = KeyFactory.getInstance(SIGNATURE_ALGORITHM, CRYPTOGRAPHY_PROVIDER);
            return keyFactory.generatePrivate(privateKeySpec);

        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new OrbitGamesSystemException("Failed to load private key for signing", e);
        }
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {

        Method method = resourceInfo.getResourceMethod();
        // We need a signed response if the annotation is present
        if(method != null && method.isAnnotationPresent(SignedResponse.class)) {

            // We need to buffer the response to a ByteArrayOutputStream,
            // otherwise this filter would consume the actual response stream.
            OutputStream originalStream = context.getOutputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            context.setOutputStream(baos);
            try {
                // We let other writers modify the content (if needed) here
                context.proceed();

                // Time to generate the signature
                byte[] responseBody = baos.toByteArray();

                String encodedSignature = generateSignature(responseBody);

                context.getHeaders().add(SIGNATURE_HEADER, encodedSignature);

            } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                throw new OrbitGamesSystemException("Could not generate response signature", e);
            } finally {
                // Here we write the content of the original stream back to it (since
                // we consumed it in other to generate the signature).
                baos.writeTo(originalStream);
                baos.close();
                context.setOutputStream(originalStream);
            }
        } else {
            context.proceed();
        }
    }

    private String generateSignature(byte[] responseBody) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException {

        Signature signatureFactory = Signature.getInstance("SHA256withECDSA");
        signatureFactory.initSign(privateKey);
        signatureFactory.update(responseBody);
        byte[] signature = signatureFactory.sign();
        logger.debug("Signing response body: " + Base64.getEncoder().encodeToString(responseBody));


        if (logger.isDebugEnabled()) {
            Signature verification = Signature.getInstance("SHA256withECDSA");
            verification.initVerify(publicKey);
            verification.update(responseBody);
            boolean signatureValid = verification.verify(signature);
            logger.debug("Double checked the signature; The signature is " + (signatureValid ? "valid!" : "invalid!"));
        }

        return new String(Base64.getEncoder().encode(signature), "UTF-8");
    }

    /**
     * Helper method: useful to generate the public and private key pairs for the Signature filter
     * @param args Not used at the moment
     * @throws GeneralSecurityException In case some algorithms could not be loaded or receive invalid input
     * @throws UnsupportedEncodingException When Base64 encoding fails...
     */
    public static void main(String... args) throws GeneralSecurityException, UnsupportedEncodingException {

        Security.addProvider(new BouncyCastleProvider());

        KeyPairGenerator keygen = KeyPairGenerator.getInstance(SIGNATURE_ALGORITHM, "BC");
        keygen.initialize(ECNamedCurveTable.getParameterSpec(EC_ALGORITHM));

        KeyPair keyPair = keygen.genKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();
        BCECPublicKey publicKey = (BCECPublicKey)keyPair.getPublic();

        System.out.println("Private key:");
        System.out.println(new String(Base64.getEncoder().encode(privateKey.getEncoded()), "UTF-8"));
        System.out.println("Public key:");
        System.out.println(new String(Base64.getEncoder().encode(publicKey.getEncoded()), "UTF-8"));
        System.out.println("Public key X-coord:");
        System.out.println(Base64.getEncoder().encodeToString(publicKey.getQ().getXCoord().getEncoded()));
        System.out.println("Public key Y-coord:");
        System.out.println(Base64.getEncoder().encodeToString(publicKey.getQ().getYCoord().getEncoded()));

    }
}
