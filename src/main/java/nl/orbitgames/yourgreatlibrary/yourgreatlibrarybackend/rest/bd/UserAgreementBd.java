package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.micrometer.core.annotation.Timed;
import io.swagger.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.useragreement.UserAgreementController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.RestToDomainModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Path("user-agreement")
@Produces("application/json")
@Consumes("application/json")
@Timed
public class UserAgreementBd {

    private final UserAgreementController userAgreementController;
    private final RestToDomainModelConverter restToDomainModelConverter;
    private final DomainToRestModelConverter domainToRestModelConverter;

    public UserAgreementBd(
            @Autowired UserAgreementController userAgreementController,
            @Autowired DomainToRestModelConverter domainToRestModelConverter,
            @Autowired RestToDomainModelConverter restToDomainModelConverter
    ) {
        this.userAgreementController = userAgreementController;
        this.domainToRestModelConverter = domainToRestModelConverter;
        this.restToDomainModelConverter = restToDomainModelConverter;
    }

    @GET
    @RolesAllowed(SecurityRole.TEACHER)
    public List<UserAgreement> getUserAgreements() {

        Collection<nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement> userAgreements = this.userAgreementController.getAll();
        return userAgreements.stream().map(domainToRestModelConverter::convert).collect(Collectors.toList());
    }

    @POST
    @RolesAllowed(SecurityRole.ADMINISTRATOR)
    public UserAgreement postUserAgreement(UserAgreement userAgreement) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement newUserAgreement =
                userAgreementController.addUserAgreement(restToDomainModelConverter.convert(userAgreement));
        return domainToRestModelConverter.convert(newUserAgreement);

    }

    @GET
    @Path("latest")
    @PermitAll
    public UserAgreement getLatestUserAgreement() {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement latestAgreement = this.userAgreementController.getLatestUserAgreement();
        return domainToRestModelConverter.convert(latestAgreement);
    }

    @GET
    @Path("{ref}")
    public UserAgreement getUserAgreement(@PathParam("ref") String ref) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement agreement = this.userAgreementController.getUserAgreement(ref);
        return domainToRestModelConverter.convert(agreement);
    }
}
