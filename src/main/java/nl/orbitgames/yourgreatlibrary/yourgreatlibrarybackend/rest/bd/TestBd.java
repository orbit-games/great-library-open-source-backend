package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.bd;

import io.swagger.model.Course;
import io.swagger.model.Role;
import io.swagger.model.User;
import io.swagger.model.UserAgreement;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.controller.test.TestController;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.SecurityRole;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.DomainToRestModelConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.converter.RestToDomainModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.time.OffsetDateTime;

@Component
@Path("test")
@Produces("application/json")
@Consumes("application/json")
@PermitAll
public class TestBd {

    private final DomainToRestModelConverter domainToRestModelConverter;
    private final RestToDomainModelConverter restToDomainModelConverter;
    private final TestController testController;

    public TestBd(
            @Autowired DomainToRestModelConverter domainToRestModelConverter,
            @Autowired RestToDomainModelConverter restToDomainModelConverter,
            @Autowired TestController testController
    ) {
        this.domainToRestModelConverter = domainToRestModelConverter;
        this.restToDomainModelConverter = restToDomainModelConverter;
        this.testController = testController;
    }

    @Path("course")
    @POST
    @RolesAllowed(SecurityRole.TEST_API)
    public Course createCourse(CreateTestCourseRequest request) {

        nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course testCourse =
                this.testController.createTestCourse(request.firstSubmissionDeadline, request.courseName, request.courseCode, request.anonymousUsers, request.anonymousReviews);

        return domainToRestModelConverter.convert(testCourse, true);
    }

    @Path("course/{courseRef}/generate-relations")
    @POST
    @RolesAllowed(SecurityRole.TEST_API)
    public void generateCourseRelations(@PathParam("courseRef") String courseRef) {
        this.testController.generateCourseReviewerRelations(courseRef);
    }

    @Path("user/exists")
    @GET
    @RolesAllowed(SecurityRole.TEST_API)
    public boolean getUserExists(@QueryParam("username") String username) {

        return testController.checkUserExists(username);

    }

    @Path("user/ref")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed(SecurityRole.TEST_API)
    public String getUserRef(@QueryParam("username") String username) {
        return testController.getUserRef(username);

    }

    @Path("user")
    @POST
    @RolesAllowed(SecurityRole.TEST_API)
    public User createUser(TestUserCreateRequest request) {

        return domainToRestModelConverter.convert(testController.createTestUser(request.username, request.email, request.password, request.fullName, restToDomainModelConverter.convert(request.role)));
    }

    @Path("user-agreement")
    @POST
    @RolesAllowed(SecurityRole.TEST_API)
    public UserAgreement createTestUserAgreement(CreateTestUserAgreementRequest request) {

        return domainToRestModelConverter.convert(testController.createTestUserAgreement(request.pdfUrl));

    }

    @Path("conversation/notification/send-daily")
    @POST
    @RolesAllowed((SecurityRole.TEST_API))
    public void sendDailyConversationNotificationEmail() {

        testController.sendDailyConversationNotificationEmail();

    }

    public static class CreateTestUserAgreementRequest {
        public String pdfUrl;
    }

    public static class CreateTestCourseRequest {
        public OffsetDateTime firstSubmissionDeadline;
        public String courseName;
        public String courseCode;
        public boolean anonymousUsers;
        public boolean anonymousReviews;
    }

    public static class TestUserCreateRequest {
        public String username;
        public String password;
        public String email;
        public String fullName;
        public Role role;
    }
}
