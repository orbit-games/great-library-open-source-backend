package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationReadUntil;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface ConversationReadUntilRepository extends CrudRepository<ConversationReadUntil, Long> {

    Optional<ConversationReadUntil> findByUserAndConversation(User user, Conversation conversation);

    Collection<ConversationReadUntil> findByUserAndConversationIn(User user, Collection<Conversation> conversation);

    Set<ConversationReadUntil> findByConversationIn(Collection<Conversation> relevantConversations);
}
