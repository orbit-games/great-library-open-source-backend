package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.Set;

@Entity
public class BugReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String email;

    private String userInput;

    @Lob
    private String deviceInfo;

    @Lob
    private String gameLogs;

    @Lob
    private String communicationLogs;

    @OneToMany(mappedBy = "bugReport", cascade = CascadeType.ALL)
    private Set<BugReportScreenshot> screenshots;

    @Lob
    private String prefs;

    @Lob
    private String knowledge;

    private OffsetDateTime clientTime;

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    private boolean notificationEmailSent;

    @PrePersist
    public void prePersist() {
        if (this.created == null) {
            this.created = OffsetDateTime.now();
        }
        this.modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getGameLogs() {
        return gameLogs;
    }

    public void setGameLogs(String gameLogs) {
        this.gameLogs = gameLogs;
    }

    public String getCommunicationLogs() {
        return communicationLogs;
    }

    public void setCommunicationLogs(String communicationLogs) {
        this.communicationLogs = communicationLogs;
    }

    public Set<BugReportScreenshot> getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(Set<BugReportScreenshot> screenshots) {
        this.screenshots = screenshots;
    }

    public String getPrefs() {
        return prefs;
    }

    public void setPrefs(String prefs) {
        this.prefs = prefs;
    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public OffsetDateTime getClientTime() {
        return clientTime;
    }

    public void setClientTime(OffsetDateTime clientTime) {
        this.clientTime = clientTime;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

    public boolean isNotificationEmailSent() {
        return notificationEmailSent;
    }

    public void setNotificationEmailSent(boolean notificationEmailSent) {
        this.notificationEmailSent = notificationEmailSent;
    }
}
