package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Section {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    @NotNull
    private String name;

    @Lob
    private String description;

    @ManyToOne
    @JoinColumn(name = "parent_section_id")
    private Section parentSection;

    private int sortOrder;

    private String parentRelationDescription;

    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL)
    private Set<SectionResource> sectionResources = new HashSet<>();

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if(this.created == null) {
            this.created = OffsetDateTime.now();
        }
        this.modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Section getParentSection() {
        return parentSection;
    }

    public void setParentSection(Section parentSection) {
        this.parentSection = parentSection;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getParentRelationDescription() {
        return parentRelationDescription;
    }

    public void setParentRelationDescription(String parentRelationDescription) {
        this.parentRelationDescription = parentRelationDescription;
    }

    public Set<SectionResource> getSectionResources() {
        return sectionResources;
    }

    public void setSectionResources(Set<SectionResource> sectionResources) {
        this.sectionResources = sectionResources;
    }

    public void addSectionResource(SectionResource sectionResource) {
        if (this.getSectionResources() == null) {
            this.sectionResources = new HashSet<>();
        }
        this.sectionResources.add(sectionResource);
    }

    public void removeSectionResource(SectionResource sectionResourceToRemove) {
        if (this.getSectionResources() == null) {
            return;
        }
        this.sectionResources.remove(sectionResourceToRemove);
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }


}
