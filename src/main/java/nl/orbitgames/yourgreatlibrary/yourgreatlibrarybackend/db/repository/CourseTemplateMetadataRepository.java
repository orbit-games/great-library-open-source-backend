package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateMetadata;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CourseTemplateMetadataRepository extends CrudRepository<CourseTemplateMetadata, Long> {

    boolean existsByTemplateId(String templateId);

    @Query("SELECT t FROM CourseTemplateMetadata t WHERE t.templateId = :templateId ORDER BY t.version DESC")
    List<CourseTemplateMetadata> findByTemplateIdOrderByVersionDesc(@Param("templateId") String templateId, Pageable pageable);

    default Optional<CourseTemplateMetadata> findLatestByTemplateId(String templateId) {
        return findByTemplateIdOrderByVersionDesc(templateId, PageRequest.of(0, 1)).stream().findFirst();
    }

    Optional<CourseTemplateMetadata> findByTemplateIdAndVersion(String templateId, int version);
}
