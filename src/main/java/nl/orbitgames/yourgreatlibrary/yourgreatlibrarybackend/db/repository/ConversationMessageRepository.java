package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationMessage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ConversationMessageRepository extends CrudRepository<ConversationMessage, Long> {

    @Query("SELECT m FROM ConversationMessage m " +
            "WHERE m.conversation IN :conversations " +
            "AND m.indexInConversation > m.conversation.lastMessageIndex - :messageLimit " +
            "AND m.sent >= :sentSince " +
            "ORDER BY m.conversation.id, m.indexInConversation DESC")
    List<ConversationMessage> findByConversationIn(
            @Param("conversations") Set<Conversation> conversations,
            @Param("messageLimit") long messageLimit,
            @Param("sentSince") OffsetDateTime sentSince);

    @Query("SELECT m FROM ConversationMessage m " +
            "WHERE m.conversation = :conversation " +
            "AND m.indexInConversation > m.conversation.lastMessageIndex - :messageLimit - :messageSkip " +
            "AND m.indexInConversation <= m.conversation.lastMessageIndex - :messageSkip " +
            "AND m.sent >= :sentSince " +
            "ORDER BY m.indexInConversation DESC")
    List<ConversationMessage> findByConversation(
            @Param("conversation") Conversation conversation,
            @Param("messageSkip") long messageSkip,
            @Param("messageLimit") long messageLimit,
            @Param("sentSince") OffsetDateTime sentSince);

    List<ConversationMessage> findByIdGreaterThan(long lastMessageId);

    List<ConversationMessage> findFirst1ByOrderByIdDesc();
    default Optional<ConversationMessage> findLast() {
        return findFirst1ByOrderByIdDesc().stream().findFirst();
    }
}
