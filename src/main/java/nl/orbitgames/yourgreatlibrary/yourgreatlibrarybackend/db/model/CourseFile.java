package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class CourseFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne(optional = false)
    @JoinColumn(name = "file_upload_metadata_id")
    private FileUploadMetadata fileUploadMetadata;

    @NotNull
    private OffsetDateTime added;

    @PrePersist
    public void prePersist() {
        if (this.added == null) {
            this.added = OffsetDateTime.now();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public FileUploadMetadata getFileUploadMetadata() {
        return fileUploadMetadata;
    }

    public void setFileUploadMetadata(FileUploadMetadata fileUploadMetadata) {
        this.fileUploadMetadata = fileUploadMetadata;
    }

    public OffsetDateTime getAdded() {
        return added;
    }

    public void setAdded(OffsetDateTime added) {
        this.added = added;
    }
}
