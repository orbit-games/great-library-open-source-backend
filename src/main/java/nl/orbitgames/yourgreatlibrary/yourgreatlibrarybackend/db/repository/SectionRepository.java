package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface SectionRepository extends CrudRepository<Section, Long> {

    @Query("SELECT s " +
            "FROM Section s " +
            "LEFT JOIN FETCH s.sectionResources " +
            "WHERE s.ref = :ref")
    Optional<Section> findByRef(@Param("ref") String sectionRef);

    Set<Section> findByParentSection(Section parentToCheck);
}
