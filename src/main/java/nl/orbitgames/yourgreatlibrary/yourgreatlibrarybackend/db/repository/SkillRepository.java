package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Skill;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.Set;

public interface SkillRepository extends CrudRepository<Skill, Long> {

    Set<Skill> findByCourse_Ref(String courseRef);

    Optional<Skill> findByRef(String skillRef);
}
