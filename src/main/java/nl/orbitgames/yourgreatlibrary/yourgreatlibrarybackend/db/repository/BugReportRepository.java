package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.BugReport;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BugReportRepository extends CrudRepository<BugReport, Long> {
    
    List<BugReport> findByNotificationEmailSentFalse();
}
