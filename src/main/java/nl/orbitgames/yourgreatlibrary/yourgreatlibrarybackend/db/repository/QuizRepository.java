package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface QuizRepository extends CrudRepository<Quiz, Long> {

    Optional<Quiz> findByRef(String ref);

}
