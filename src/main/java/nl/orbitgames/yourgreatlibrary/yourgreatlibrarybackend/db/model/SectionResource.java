package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class SectionResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Section section;

    @ManyToOne(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Resource resource;

    @NotNull
    private boolean optional;

    private String description;

    private int sortOrder;

    @NotNull
    private OffsetDateTime created;
    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (this.created == null) {
            this.created = OffsetDateTime.now();
        }
        this.modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }
}
