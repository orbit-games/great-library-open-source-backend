package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.FileUploadMetadata;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FileUploadMetadataRepository extends CrudRepository<FileUploadMetadata, Long> {
    Optional<FileUploadMetadata> findByRef(String fileUploadMetadataRef);
}
