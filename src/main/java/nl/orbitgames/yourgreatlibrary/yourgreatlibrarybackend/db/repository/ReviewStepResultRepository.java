package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ReviewStepResultRepository extends CrudRepository<ReviewStepResult, Long> {


    /**
     * Returns all the review results, for reviewer relations where the user is a submitter, as well as those where the
     * user is a reviewer, within the given course
     * @param user The user
     * @param course The course
     * @return All the review results
     */
    @Query("SELECT rsr FROM ReviewStepResult rsr " +
            "LEFT JOIN FETCH rsr.reviewerRelation rr " +
            "LEFT JOIN FETCH rsr.fileUploadMetaData fump " +
            "LEFT JOIN FETCH rsr.quizResult qr " +
            "LEFT JOIN FETCH qr.quizElementResults qer " +
            "LEFT JOIN FETCH qer.selectedOptions qerso " +
            "LEFT JOIN FETCH rsr.arguments rsra " +
            "WHERE (rsr.reviewerRelation.reviewer = :user OR rsr.reviewerRelation.submitter = :user) " +
            "AND rsr.reviewStep.chapter.course = :course ")
    List<ReviewStepResult> findByUserInCourse(@Param("user") User user, @Param("course") Course course);

    /**
     * Returns all the review results, for reviewer relations where the user is a submitter, as well as those where the
     * user is a reviewer, within the given chapter
     * @param user The user
     * @param chapter The chapter
     * @return All the review results
     */
    @Query("SELECT rsr FROM ReviewStepResult rsr " +
            "LEFT JOIN FETCH rsr.reviewerRelation rr " +
            "LEFT JOIN FETCH rsr.fileUploadMetaData fump " +
            "LEFT JOIN FETCH rsr.quizResult qr " +
            "LEFT JOIN FETCH qr.quizElementResults qer " +
            "LEFT JOIN FETCH qer.selectedOptions qerso " +
            "LEFT JOIN FETCH rsr.arguments rsra " +
            "WHERE (rsr.reviewerRelation.reviewer = :user OR rsr.reviewerRelation.submitter = :user) " +
            "AND rsr.reviewStep.chapter = :chapter ")
    List<ReviewStepResult> findByUserInChapter(@Param("user") User user, @Param("chapter") Chapter chapter);

    @Query("SELECT rsr FROM ReviewStepResult rsr " +
            "LEFT JOIN FETCH rsr.reviewerRelation rr " +
            "LEFT JOIN FETCH rsr.fileUploadMetaData fump " +
            "LEFT JOIN FETCH rsr.quizResult qr " +
            "LEFT JOIN FETCH qr.quizElementResults qer " +
            "LEFT JOIN FETCH qer.selectedOptions qerso " +
            "LEFT JOIN FETCH rsr.arguments rsra " +
            "WHERE rsr.reviewStep.chapter = :chapter ")
    List<ReviewStepResult> findByChapter(@Param("chapter") Chapter chapter);

    @Query("SELECT rsr FROM ReviewStepResult rsr " +
            "LEFT JOIN FETCH rsr.reviewerRelation rr " +
            "LEFT JOIN FETCH rsr.fileUploadMetaData fump " +
            "LEFT JOIN FETCH rsr.quizResult qr " +
            "LEFT JOIN FETCH qr.quizElementResults qer " +
            "LEFT JOIN FETCH qer.selectedOptions qerso " +
            "LEFT JOIN FETCH rsr.arguments rsra " +
            "WHERE rr = :reviewerRelation ")
    List<ReviewStepResult> findByReviewerRelation(@Param("reviewerRelation") ReviewerRelation reviewerRelation);

    @Query("SELECT rsr FROM ReviewStepResult rsr " +
            "LEFT JOIN FETCH rsr.reviewerRelation rr " +
            "LEFT JOIN FETCH rsr.fileUploadMetaData fump " +
            "LEFT JOIN FETCH rsr.quizResult qr " +
            "LEFT JOIN FETCH qr.quizElementResults qer " +
            "LEFT JOIN FETCH qer.selectedOptions qerso " +
            "LEFT JOIN FETCH rsr.arguments rsra " +
            "WHERE rsr.reviewStep.chapter.course = :course")
    List<ReviewStepResult> findByCourse(@Param("course") Course course);

    Optional<ReviewStepResult> findByReviewStepAndReviewerRelation(ReviewStep reviewStep, ReviewerRelation reviewerRelation);
}
