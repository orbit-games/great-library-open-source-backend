package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ReviewStepResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "review_step_id")
    private ReviewStep reviewStep;

    @ManyToOne(optional = false)
    @JoinColumn(name = "reviewer_relation_id")
    private ReviewerRelation reviewerRelation;

    @ManyToOne
    @JoinColumn(name = "file_upload_metadata_id")
    private FileUploadMetadata fileUploadMetaData;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quiz_result_id")
    private QuizResult quizResult;

    @OneToMany(mappedBy = "reviewStepResult")
    private Set<ReviewStepArgument> arguments = new HashSet<>();

    private OffsetDateTime markedComplete;

    @NotNull
    private OffsetDateTime created;


    @PrePersist
    public void prePersist() {
        created = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ReviewStep getReviewStep() {
        return reviewStep;
    }

    public void setReviewStep(ReviewStep reviewStep) {
        this.reviewStep = reviewStep;
    }

    public ReviewerRelation getReviewerRelation() {
        return reviewerRelation;
    }

    public void setReviewerRelation(ReviewerRelation reviewerRelation) {
        this.reviewerRelation = reviewerRelation;
    }

    public FileUploadMetadata getFileUploadMetaData() {
        return fileUploadMetaData;
    }

    public void setFileUploadMetaData(FileUploadMetadata fileUploadMetaData) {
        this.fileUploadMetaData = fileUploadMetaData;
    }

    public QuizResult getQuizResult() {
        return quizResult;
    }

    public void setQuizResult(QuizResult quizResult) {
        this.quizResult = quizResult;
    }

    public Set<ReviewStepArgument> getArguments() {
        return arguments;
    }

    public void setArguments(Set<ReviewStepArgument> arguments) {
        this.arguments = arguments;
    }

    public void addArgument(ReviewStepArgument argument) {

        if (this.getArguments() == null) {
            this.arguments = new HashSet<>();
        }

        this.arguments.add(argument);
    }

    public OffsetDateTime getMarkedComplete() {
        return markedComplete;
    }

    public void setMarkedComplete(OffsetDateTime markedComplete) {
        this.markedComplete = markedComplete;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }
}
