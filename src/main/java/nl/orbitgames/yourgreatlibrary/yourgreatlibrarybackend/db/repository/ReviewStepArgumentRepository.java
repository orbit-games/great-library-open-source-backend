package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStepArgument;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ReviewStepArgumentRepository extends CrudRepository<ReviewStepArgument, Long> {

    Optional<ReviewStepArgument> findByRef(String ref);
}
