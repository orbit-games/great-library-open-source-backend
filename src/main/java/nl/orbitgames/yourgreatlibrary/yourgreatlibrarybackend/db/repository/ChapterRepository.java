package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ChapterRepository extends CrudRepository<Chapter, Long> {

    Optional<Chapter> findByRef(String ref);
}
