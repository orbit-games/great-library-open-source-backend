package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
public class QuizElementResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "quiz_result_id")
    private QuizResult quizResult;

    @ManyToOne
    @JoinColumn(name = "quiz_element_id")
    private QuizElement quizElement;

    @ManyToMany
    @JoinTable(name = "quiz_element_result_selected_option",
            joinColumns = @JoinColumn(name = "quiz_element_result_id"),
            inverseJoinColumns = @JoinColumn(name = "quiz_element_option_id")
    )
    private Set<QuizElementOption> selectedOptions = new HashSet<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public QuizResult getQuizResult() {
        return quizResult;
    }

    public void setQuizResult(QuizResult quizResult) {
        this.quizResult = quizResult;
    }

    public QuizElement getQuizElement() {
        return quizElement;
    }

    public void setQuizElement(QuizElement quizElement) {
        this.quizElement = quizElement;
    }

    public Set<QuizElementOption> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(Set<QuizElementOption> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }
}
