package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ForgotPasswordToken;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.Optional;

public interface ForgotPasswordTokenRepository extends CrudRepository<ForgotPasswordToken, Long> {

    @Query("SELECT t FROM ForgotPasswordToken t WHERE t.user = :user AND t.used = false AND t.validUntil > :now ORDER BY t.created DESC")
    Optional<ForgotPasswordToken> findUnusedValidForUser(@Param("user") User user, @Param("now") OffsetDateTime now);
}
