package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends CrudRepository<User, Long> {

    String FETCH_RELATED =
            "LEFT JOIN FETCH u.userProperties p " +
                    "LEFT JOIN FETCH u.signedUserAgreement a " +
                    "LEFT JOIN FETCH u.userCourses uc " +
                    "LEFT JOIN FETCH uc.userCourseProperties ucp " +
                    "LEFT JOIN FETCH uc.course c ";

    Optional<User> findByUsername(String username);
    Optional<User> findByRef(String ref);

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.userProperties p WHERE u.ref = :ref")
    Optional<User> findByRefFetchUserProperties(@Param("ref") String ref);

    /**
     * Finds the user with the given ref, and loads related entities (such as user properties and the signed user agreement)
     * @param ref The ref of the user
     * @return The user (if it was found)
     */
    @Query("SELECT u FROM User u " + FETCH_RELATED + "WHERE u.ref = :ref")
    Optional<User> findByRefFetchRelated(@Param("ref") String ref);

    @Query("SELECT u FROM User u " + FETCH_RELATED)
    Collection<User> findAllAndFetchRelated();

    @Query("SELECT u FROM User u " + FETCH_RELATED +
            "WHERE EXISTS (" +
            "    SELECT uc1 " +
            "    FROM UserCourse uc1 " +
            "    WHERE uc1.user = u " +
            "    AND uc1.course.ref = :courseRef" +
            ")")
    Collection<User> findByCourseRefAndFetchRelated(@Param("courseRef") String courseRef);

    @Query("SELECT u FROM User u " + FETCH_RELATED +
            "WHERE u.role IN :roles AND EXISTS (" +
            "    SELECT uc1 " +
            "    FROM UserCourse uc1 " +
            "    WHERE uc1.user = u " +
            "    AND uc1.course.ref = :courseRef" +
            ")")
    Collection<User> findByCourseRefAndRolesAndFetchRelated(@Param("courseRef") String courseRef, @Param("roles") Set<Role> roles);

    @Query("SELECT u FROM User u " + FETCH_RELATED + "WHERE u.role IN :roles")
    Collection<User> findByRoleInAndFetchRelated(@Param("roles") Set<Role> roles);

    Set<User> findByRefIn(Set<String> userRefs);

}
