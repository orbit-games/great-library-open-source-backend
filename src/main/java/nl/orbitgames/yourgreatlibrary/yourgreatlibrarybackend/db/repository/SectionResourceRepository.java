package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SectionResource;
import org.springframework.data.repository.CrudRepository;

public interface SectionResourceRepository extends CrudRepository<SectionResource, Long> {
}
