package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private int maxScore;

    @NotNull
    private boolean hiddenPoints;

    @OneToMany(mappedBy = "quiz", cascade = CascadeType.ALL)
    @OrderBy("sortOrder")
    private List<QuizElement> elements = new ArrayList<>();

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {

        recalculateMaxScore();
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }

    }

    @PreUpdate
    public void preUpdate() {

        recalculateMaxScore();
        modified = OffsetDateTime.now();
    }

    @Transient
    public void recalculateMaxScore() {
        // TODO: check why this isn't always called when I expect it to
        setMaxScore(getElements().stream().mapToInt(e ->
        {
            if (e.getOptions() == null) {
                return 0;
            }

            switch (e.getQuestionType()) {
                case NONE:
                    return 0;
                case SELECT_ONE:
                    return e.getOptions().stream().mapToInt(QuizElementOption::getPoints).max().orElse(0);
                case SELECT_MULTIPLE:
                    return e.getOptions().stream().mapToInt(QuizElementOption::getPoints).sum();
                default:
                    throw new OrbitGamesSystemException("Unknown question type: " + e.getQuestionType());
            }
        }).sum());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public List<QuizElement> getElements() {
        return elements;
    }

    public void setElements(List<QuizElement> elements) {
        this.elements = elements;
    }

    public void addElement(QuizElement quizElement) {
        if (this.getElements() == null) {
            this.elements = new ArrayList<>();
        }

        this.elements.add(quizElement);
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setHiddenPoints(boolean hiddenPoints) {
        this.hiddenPoints = hiddenPoints;
    }

    public boolean isHiddenPoints() {
        return hiddenPoints;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

}
