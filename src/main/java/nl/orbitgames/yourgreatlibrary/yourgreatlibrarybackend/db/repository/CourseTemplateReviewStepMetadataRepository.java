package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateReviewStepMetadata;
import org.springframework.data.repository.CrudRepository;

public interface CourseTemplateReviewStepMetadataRepository extends CrudRepository<CourseTemplateReviewStepMetadata, Long> {
}
