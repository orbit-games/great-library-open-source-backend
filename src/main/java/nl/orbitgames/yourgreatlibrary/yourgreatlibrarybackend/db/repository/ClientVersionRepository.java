package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ClientVersion;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Platform;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ClientVersionRepository extends CrudRepository<ClientVersion, Long> {

    Optional<ClientVersion> findByPlatformAndBuildNumber(Platform platform, int buildNumber);

    @Query("SELECT v FROM ClientVersion v WHERE v.platform = :platform AND v.revoked = false ORDER BY v.releaseDate DESC")
    List<ClientVersion> findUnrevokedByPlatformOrderByReleaseDateDesc(@Param("platform") Platform platform, Pageable pageable);

    default Optional<ClientVersion> findLatestUnrevokedByPlatform(Platform platform) {
        return findUnrevokedByPlatformOrderByReleaseDateDesc(platform, PageRequest.of(0, 1)).stream().findFirst();
    }

    @Query("SELECT v FROM ClientVersion v WHERE v.platform = :platform AND v.buildNumber > :buildNumber AND v.forced = true AND v.revoked = false")
    List<ClientVersion> findForcedUpdatesSince(@Param("platform") Platform platform, @Param("buildNumber") int buildNumber, Pageable pageable);

    default Optional<ClientVersion> findForcedUpdateSince(Platform platform, int buildNumber) {
        return findForcedUpdatesSince(platform, buildNumber, PageRequest.of(0, 1)).stream().findFirst();
    }
}
