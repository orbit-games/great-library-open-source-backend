package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationPermissionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
public class Conversation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    @NotNull
    private boolean hidden;

    @NotNull
    private boolean anonymousMembers;

    private String title;

    @Enumerated(EnumType.STRING)
    private ConversationType type;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "conversation_user",
            joinColumns = @JoinColumn(name = "conversation_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> users = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "conversation_role",
            joinColumns = @JoinColumn(name = "conversation_id")
    )
    @MapKeyColumn(name = "role")
    @Column(name="permission")
    @MapKeyEnumerated(EnumType.STRING)
    @Enumerated(EnumType.STRING)
    private Map<Role, ConversationPermissionType> conversationRoles = new HashMap<>();

    @NotNull
    private long lastMessageIndex;
    // NOTE Messages are purposefully NOT included in the conversation, because fetching all the messages in a conversation
    // can be a lot of data, so separate retrieval (with pagination) is required

    private OffsetDateTime created;

    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        this.modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isAnonymousMembers() {
        return anonymousMembers;
    }

    public void setAnonymousMembers(boolean anonymousMembers) {
        this.anonymousMembers = anonymousMembers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ConversationType getType() {
        return type;
    }

    public void setType(ConversationType type) {
        this.type = type;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Map<Role, ConversationPermissionType> getConversationRoles() {
        return conversationRoles;
    }

    public void setConversationRoles(Map<Role, ConversationPermissionType> conversationRoles) {
        this.conversationRoles = conversationRoles;
    }

    public long getLastMessageIndex() {
        return lastMessageIndex;
    }

    public void setLastMessageIndex(long lastMessageIndex) {
        this.lastMessageIndex = lastMessageIndex;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }
}
