package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class ConversationMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne
    @JoinColumn(name = "conversation_id")
    private Conversation conversation;

    @NotNull
    private long indexInConversation;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @NotNull
    private String message;

    @NotNull
    private OffsetDateTime sent;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public long getIndexInConversation() {
        return indexInConversation;
    }

    public void setIndexInConversation(long indexInConversation) {
        this.indexInConversation = indexInConversation;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OffsetDateTime getSent() {
        return sent;
    }

    public void setSent(OffsetDateTime sent) {
        this.sent = sent;
    }
}
