package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.validators.InOffsetDateTimeRange;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.DiscussionType;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ReviewStepType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ReviewStep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne(optional = false)
    @JoinColumn(name = "chapter_id")
    private Chapter chapter;

    private int sortOrder;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReviewStepType stepType;

    private boolean disabled;

    private String name;

    private String description;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

    private String fileUploadName;

    @NotNull
    private boolean fileUpload;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DiscussionType discussionType;

    @ManyToMany
    @JoinTable(name = "review_step_argument_type",
            joinColumns = @JoinColumn(name = "review_step_id"),
            inverseJoinColumns = @JoinColumn(name = "argument_type_id")
    )
    private Set<ArgumentType> argumentTypes = new HashSet<>();

    @NotNull
    @InOffsetDateTimeRange(min = "2000-01-01T00:00:00Z", max = "2100-01-01T00:00:00Z")
    private OffsetDateTime deadline;

    private long deadlineNotificationEmailOffsetSeconds;
    private boolean deadlineNotificationEmailEnabled;

    @ManyToMany
    @JoinTable(
            name = "review_step_skill",
            joinColumns = @JoinColumn(name = "review_step_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id")
    )
    private Set<Skill> skills;

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }
    }

    @PreUpdate
    public void preUpdate() {
        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public ReviewStepType getStepType() {
        return stepType;
    }

    public void setStepType(ReviewStepType stepType) {
        this.stepType = stepType;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public String getFileUploadName() {
        return fileUploadName;
    }

    public void setFileUploadName(String fileUploadName) {
        this.fileUploadName = fileUploadName;
    }

    public boolean isFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(boolean fileUpload) {
        this.fileUpload = fileUpload;
    }

    public DiscussionType getDiscussionType() {
        return discussionType;
    }

    public void setDiscussionType(DiscussionType discussionType) {
        this.discussionType = discussionType;
    }

    public Set<ArgumentType> getArgumentTypes() {
        return argumentTypes;
    }

    public void setArgumentTypes(Set<ArgumentType> argumentTypes) {
        this.argumentTypes = argumentTypes;
    }

    public void addArgumentType(ArgumentType argumentType) {
        this.argumentTypes.add(argumentType);
    }

    public void removeArgumentType(ArgumentType argumentType) {
        this.argumentTypes.remove(argumentType);
    }

    public OffsetDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(OffsetDateTime deadline) {
        this.deadline = deadline;
    }

    public long getDeadlineNotificationEmailOffsetSeconds() {
        return deadlineNotificationEmailOffsetSeconds;
    }

    public void setDeadlineNotificationEmailOffsetSeconds(long deadlineNotificationEmailOffsetSeconds) {
        this.deadlineNotificationEmailOffsetSeconds = deadlineNotificationEmailOffsetSeconds;
    }

    public boolean isDeadlineNotificationEmailEnabled() {
        return deadlineNotificationEmailEnabled;
    }

    public void setDeadlineNotificationEmailEnabled(boolean deadlineNotificationEmailEnabled) {
        this.deadlineNotificationEmailEnabled = deadlineNotificationEmailEnabled;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

}
