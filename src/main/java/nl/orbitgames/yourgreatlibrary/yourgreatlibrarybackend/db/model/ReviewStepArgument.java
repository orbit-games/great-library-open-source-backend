package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class ReviewStepArgument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne(optional = false)
    @JoinColumn(name = "review_step_result_id")
    private ReviewStepResult reviewStepResult;

    @ManyToOne
    @JoinColumn(name = "argument_type_id")
    private ArgumentType argumentType;

    @ManyToOne
    @JoinColumn(name = "reply_to")
    private ReviewStepArgument replyTo;

    @NotNull
    private String message;

    private OffsetDateTime created;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public ReviewStepResult getReviewStepResult() {
        return reviewStepResult;
    }

    public void setReviewStepResult(ReviewStepResult reviewStepResult) {
        this.reviewStepResult = reviewStepResult;
    }

    public ArgumentType getArgumentType() {
        return argumentType;
    }

    public void setArgumentType(ArgumentType argumentType) {
        this.argumentType = argumentType;
    }

    public ReviewStepArgument getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(ReviewStepArgument replyTo) {
        this.replyTo = replyTo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }
}
