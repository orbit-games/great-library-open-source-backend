package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.OffsetDateTime;

public class InOffsetDateTimeRangeValidator implements ConstraintValidator<InOffsetDateTimeRange, OffsetDateTime> {

   private InOffsetDateTimeRange constraintAnnotation;

   @Override
   public void initialize(InOffsetDateTimeRange constraintAnnotation) {
      this.constraintAnnotation = constraintAnnotation;
   }

   @Override
   public boolean isValid(OffsetDateTime value, ConstraintValidatorContext context) {

      final OffsetDateTime min = OffsetDateTime.parse(constraintAnnotation.min());
      final OffsetDateTime max = OffsetDateTime.parse(constraintAnnotation.max());
      return value == null ||
              (value.isAfter(min) && value.isBefore(max));
   }
}