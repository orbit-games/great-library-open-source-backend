package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.SystemProperty;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SystemPropertyRepository extends CrudRepository<SystemProperty, Long> {

    Optional<SystemProperty> findByPropertyKey(String propertyKey);
}
