package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.ConversationEmailNotificationType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
public class ConversationEmailSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "conversation_id")
    private Conversation conversation;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ConversationEmailNotificationType notificationType;

    private long emailNotificationsSentUntil;

    private OffsetDateTime created;

    private OffsetDateTime modified;


    @PrePersist
    public void prePersist() {

        if (created == null) {
            created = OffsetDateTime.now();
        }
        modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {

        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public ConversationEmailNotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(ConversationEmailNotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public long getEmailNotificationsSentUntil() {
        return emailNotificationsSentUntil;
    }

    public void setEmailNotificationsSentUntil(long emailNotificationsSentUntil) {
        this.emailNotificationsSentUntil = emailNotificationsSentUntil;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }
}
