package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CourseRepository extends CrudRepository<Course, Long> {

    Optional<Course> findByRef(String ref);
    Optional<Course> findByCode(String code);

    /**
     * Finds the course using it's ref (reference) and fetches all entities related to the course specification
     * @param courseRef The ref to the course
     * @return The course with all related entities fetched
     */
    @Query("SELECT c FROM Course c" +
            " LEFT JOIN FETCH c.userCourses uc" +
            " LEFT JOIN FETCH uc.userCourseProperties ucp" +
            " LEFT JOIN FETCH uc.user u" +
            " LEFT JOIN FETCH c.chapters ch" +
            // TODO: The code below generates:
            // org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags: [nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course.chapters, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter.reviewSteps, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz.elements, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement.options, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter.recommendedResources, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course.resources, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz.elements, nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizElement.options]

//            " LEFT JOIN FETCH ch.reviewSteps rs" +
//            " LEFT JOIN FETCH rs.quiz rsq" +
//            " LEFT JOIN FETCH rsq.elements rsqe" +
//            " LEFT JOIN FETCH rsqe.options rsqeo" +
//            " LEFT JOIN FETCH rs.argumentTypes rsqa" +
//            " LEFT JOIN FETCH ch.recommendedResources chrr" +
//            " LEFT JOIN FETCH c.resources r" +
//            " LEFT JOIN FETCH r.quiz rq" +
//            " LEFT JOIN FETCH rq.elements rqe" +
//            " LEFT JOIN FETCH rqe.options rqeo" +
            " WHERE c.ref = :courseRef")
    Optional<Course> findByRefAndFetchRelated(@Param("courseRef") String courseRef);
}
