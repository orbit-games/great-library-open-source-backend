package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@Entity
public class UserCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    private String displayName;

    private boolean active;

    private String topic;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "user_course_property",
            joinColumns = { @JoinColumn(name = "user_course_id")}
    )
    @MapKeyColumn(name = "property_key")
    private Map<String, UserCourseProperty> userCourseProperties = new HashMap<>();

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }
    }

    @PreUpdate
    public void preUpdate() {
        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, UserCourseProperty> getUserCourseProperties() {
        return userCourseProperties;
    }

    public void setUserCourseProperties(Map<String, UserCourseProperty> userCourseProperties) {
        this.userCourseProperties = userCourseProperties;
    }

    public void putUserCourseProperty(String key, String propertyValue) {
        if (this.getUserCourseProperties() == null) {
            this.userCourseProperties = new HashMap<>();
        }

        UserCourseProperty existingProperty = this.userCourseProperties.get(key);
        if (existingProperty != null) {
            if (!existingProperty.getPropertyValue().equals(propertyValue)) {
                // The value exists but has changed
                existingProperty.setPropertyValue(propertyValue);
                existingProperty.setModified(OffsetDateTime.now());
            }
        } else {
            // The property doesn't exist yet, let's add it
            UserCourseProperty newProperty = new UserCourseProperty();
            newProperty.setPropertyValue(propertyValue);
            newProperty.setCreated(OffsetDateTime.now());
            newProperty.setModified(OffsetDateTime.now());
            this.userCourseProperties.put(key, newProperty);
        }
    }

    public void removeUserCourseProperty(String oldKey) {

        if (this.getUserCourseProperties() == null) {
            return;
        }

        userCourseProperties.remove(oldKey);

    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }
}
