package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Announcement;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.List;

public interface AnnouncementRepository extends CrudRepository<Announcement, Long> {

    @Query("SELECT a FROM Announcement a WHERE a.startDate <= :dateTime AND a.endDate >= :dateTime")
    List<Announcement> getAnnouncementValidOn(@Param("dateTime") OffsetDateTime dateTime);

}
