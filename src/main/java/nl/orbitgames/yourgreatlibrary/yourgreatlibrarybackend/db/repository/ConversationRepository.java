package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface ConversationRepository extends CrudRepository<Conversation, Long> {

    @Query("SELECT c FROM Conversation c " +
            "LEFT JOIN FETCH c.users u " +
            "LEFT JOIN FETCH c.conversationRoles r " +
            "WHERE c.ref = :ref ")
    Optional<Conversation> findByRef(@Param("ref") String ref);

    /**
     * Finds all the non-hidden conversations the user may view or post messages to (so it is either a member of or has role permissions)
     * @param course The course within which the conversations should be returned
     * @param user The user who's conversation should be shown
     * @param additionalConversationRefs refs of conversations that are hidden but should be retrieved
     * @return The set of conversations the user has permission to view
     */
    @Query("SELECT c FROM Conversation c " +
            "LEFT JOIN FETCH c.users u " +
            "LEFT JOIN FETCH c.conversationRoles r " +
            "WHERE c.course = :course " +
            "AND (" +
            "   EXISTS(SELECT cu FROM c.users cu WHERE cu = :user) OR " +
            "   EXISTS(SELECT cr FROM c.conversationRoles cr WHERE KEY(cr) = :role AND cr != 'NONE')" +
            ") AND (" +
            "   c.hidden = false OR c.ref in :refs " +
            ") AND c.modified >= :updated ")
    Set<Conversation> findConversationsByCourseAndUserUpdatedSince(@Param("course") Course course, @Param("user") User user, @Param("role") Role role,
                                                                    @Param("refs") Set<String> additionalConversationRefs, @Param("updated")OffsetDateTime updated);

    @Query("SELECT c FROM Conversation c " +
            "LEFT JOIN FETCH c.users u " +
            "LEFT JOIN FETCH c.conversationRoles r " +
            "WHERE c.id IN :ids")
    Set<Conversation> findByIds(@Param("ids") Collection<Long> ids);
}
