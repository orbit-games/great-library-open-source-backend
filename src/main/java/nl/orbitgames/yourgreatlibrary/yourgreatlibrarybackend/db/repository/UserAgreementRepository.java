package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserAgreement;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserAgreementRepository extends CrudRepository<UserAgreement, Long> {

    Optional<UserAgreement> findByRef(String ref);

    List<UserAgreement> findAllByOrderByStartDateDesc(Pageable pageable);

    default Optional<UserAgreement> findLatest() {
        return findAllByOrderByStartDateDesc(PageRequest.of(0, 1)).stream().findFirst();
    }
}
