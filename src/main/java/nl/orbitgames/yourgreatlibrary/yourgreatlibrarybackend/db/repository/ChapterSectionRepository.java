package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ChapterSection;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Section;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Set;

public interface ChapterSectionRepository extends CrudRepository<ChapterSection, Long> {

    Set<ChapterSection> findBySectionIn(Collection<Section> sections);
}
