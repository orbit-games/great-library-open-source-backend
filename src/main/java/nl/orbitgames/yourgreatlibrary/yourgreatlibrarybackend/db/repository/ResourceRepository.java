package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Resource;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ResourceRepository extends CrudRepository<Resource, Long> {

    Optional<Resource> findByRef(String ref);
}
