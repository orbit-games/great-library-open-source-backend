package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Conversation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ConversationEmailSettings;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ConversationEmailSettingsRepository extends CrudRepository<ConversationEmailSettings, Long> {

    List<ConversationEmailSettings> findByConversationIn(Collection<Conversation> relevantConversations);

    List<ConversationEmailSettings> findByUserAndConversationIn(User user, Collection<Conversation> conversations);

    Optional<ConversationEmailSettings> findByUserAndConversation(User user, Conversation conversation);
}
