package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.CourseTemplateChapterMetadata;
import org.springframework.data.repository.CrudRepository;

public interface CourseTemplateChapterMetadataRepository extends CrudRepository<CourseTemplateChapterMetadata, Long> {
}
