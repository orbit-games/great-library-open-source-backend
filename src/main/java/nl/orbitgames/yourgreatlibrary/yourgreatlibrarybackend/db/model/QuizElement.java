package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.QuestionType;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class QuizElement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne
    @JoinColumn(name = "quiz_id", nullable = false)
    private Quiz quiz;

    @NotNull
    private int sortOrder;

    @NotNull
    private String text;

    @Enumerated(EnumType.STRING)
    private QuestionType questionType;

    @OneToMany(mappedBy = "quizElement", cascade = CascadeType.ALL)
    private Set<QuizElementOption> options;

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }
    }

    @PreUpdate
    public void preUpdate() {
        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public Set<QuizElementOption> getOptions() {
        return options;
    }

    public void setOptions(Set<QuizElementOption> options) {
        this.options = options;
    }

    public void addOption(QuizElementOption quizOption) {
        if (this.getOptions() == null) {
            this.options = new HashSet<>();
        }

        options.add(quizOption);
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

}
