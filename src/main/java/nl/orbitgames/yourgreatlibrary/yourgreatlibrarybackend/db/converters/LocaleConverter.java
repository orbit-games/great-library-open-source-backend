package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.converters;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import java.util.Locale;

public class LocaleConverter implements AttributeConverter<Locale, String> {
    @Override
    public String convertToDatabaseColumn(Locale locale) {
        if (locale == null) {
            return null;
        } else {
            return locale.toString();
        }
    }

    @Override
    public Locale convertToEntityAttribute(String s) {
        if (StringUtils.isBlank(s)) {
            return null;
        } else {
            return LocaleUtils.toLocale(s);
        }
    }
}
