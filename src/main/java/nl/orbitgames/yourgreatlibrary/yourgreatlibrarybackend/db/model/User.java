package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.converters.LocaleConverter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.model.Role;

import javax.persistence.CollectionTable;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @NotNull
    private String username;

    @NotNull
    private String passwordHash;

    private String email;

    private boolean validated;

    private String fullName;

    private String studentNumber;

    @Convert(converter = LocaleConverter.class)
    private Locale preferredLocale;

    @ManyToOne
    @JoinColumn(name = "signed_user_agreement_id")
    private UserAgreement signedUserAgreement;

    private OffsetDateTime userAgreementSignDate;

    @Enumerated(EnumType.STRING)
    private Role role;

    private OffsetDateTime lastActive;

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @ElementCollection
    @CollectionTable(
            name = "user_property",
            joinColumns = { @JoinColumn(name = "user_id")}
    )
    @MapKeyColumn(name = "property_key")
    private Map<String, UserProperty> userProperties = new HashMap<>();

    @OneToMany(mappedBy = "user")
    @OrderBy("created")
    private List<UserCourse> userCourses = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        this.created = OffsetDateTime.now();
        this.modified = OffsetDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        this.modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public Locale getPreferredLocale() {
        return preferredLocale;
    }

    public void setPreferredLocale(Locale preferredLocale) {
        this.preferredLocale = preferredLocale;
    }

    public UserAgreement getSignedUserAgreement() {
        return signedUserAgreement;
    }

    public void setSignedUserAgreement(UserAgreement signedUserAgreement) {
        this.signedUserAgreement = signedUserAgreement;
    }

    public OffsetDateTime getUserAgreementSignDate() {
        return userAgreementSignDate;
    }

    public void setUserAgreementSignDate(OffsetDateTime userAgreementSignDate) {
        this.userAgreementSignDate = userAgreementSignDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Map<String, UserProperty> getUserProperties() {
        return userProperties;
    }

    public void setUserProperties(Map<String, UserProperty> userProperties) {
        this.userProperties = userProperties;
    }

    public void putUserProperty(String key, String propertyValue) {
        if (this.getUserProperties() == null) {
            this.userProperties = new HashMap<>();
        }

        UserProperty existingProperty = this.userProperties.get(key);
        if (existingProperty != null) {
            if (!existingProperty.getPropertyValue().equals(propertyValue)) {
                // The value exists but has changed
                existingProperty.setPropertyValue(propertyValue);
                existingProperty.setModified(OffsetDateTime.now());
            }
        } else {
            // The property doesn't exist yet, let's add it
            UserProperty newProperty = new UserProperty();
            newProperty.setPropertyValue(propertyValue);
            newProperty.setCreated(OffsetDateTime.now());
            newProperty.setModified(OffsetDateTime.now());
            this.userProperties.put(key, newProperty);
        }
    }

    public void removeUserProperty(String key) {
        if (this.getUserProperties() == null) {
            this.userProperties = new HashMap<>();
        }

        this.userProperties.remove(key);
    }

    public List<UserCourse> getUserCourses() {
        return userCourses;
    }

    public void setUserCourses(List<UserCourse> userCourses) {
        this.userCourses = userCourses;
    }

    public void addUserCourse(UserCourse userCourse) {
        if (this.getUserCourses() == null) {
            userCourses = new ArrayList<>();
        }
        userCourses.add(userCourse);
    }

    public OffsetDateTime getLastActive() {
        return lastActive;
    }

    public void setLastActive(OffsetDateTime lastActive) {
        this.lastActive = lastActive;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

}
