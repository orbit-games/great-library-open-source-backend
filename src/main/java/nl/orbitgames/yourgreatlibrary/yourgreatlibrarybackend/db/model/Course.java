package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @NotNull
    private String name;

    @NotNull
    private String code;

    @NotNull
    private int year;

    @NotNull
    private int quarter;

    @NotNull
    private boolean anonymousUsers;

    @NotNull
    private boolean anonymousReviews;

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @OneToMany(mappedBy = "course")
    private Set<UserCourse> userCourses = new HashSet<>();

    @OneToMany(mappedBy = "course")
    @OrderBy("sortOrder")
    private List<Chapter> chapters = new ArrayList<>();

    @OneToMany(mappedBy = "course")
    private Set<Section> sections = new HashSet<>();

    @OneToMany(mappedBy = "course")
    private Set<Resource> resources = new HashSet<>();

    @OneToMany(mappedBy = "course")
    private Set<Skill> skills = new HashSet<>();

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private Set<CourseFile> files = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="course_template_metadata_id")
    private CourseTemplateMetadata courseTemplateMetadata;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }
    }

    @PreUpdate
    public void preUpdate() {
        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getQuarter() {
        return quarter;
    }

    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }

    public boolean isAnonymousUsers() {
        return anonymousUsers;
    }

    public void setAnonymousUsers(boolean anonymousUsers) {
        this.anonymousUsers = anonymousUsers;
    }

    public boolean isAnonymousReviews() {
        return anonymousReviews;
    }

    public void setAnonymousReviews(boolean anonymousReviews) {
        this.anonymousReviews = anonymousReviews;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Set<UserCourse> getUserCourses() {
        return userCourses;
    }

    public Set<Section> getSections() {
        return sections;
    }

    public void setSections(Set<Section> sections) {
        this.sections = sections;
    }

    public void addSection(Section section) {
        if (this.getSections() == null) {
            this.sections = new HashSet<>();
        }
        this.sections.add(section);
    }

    public void removeSection(Section section) {
        if (this.getSections() != null) {
            this.sections.remove(section);
        }
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    public void addResource(Resource resource) {
        if (this.getResources() == null) {
            this.resources = new HashSet<>();
        }
        this.resources.add(resource);
    }

    public void setUserCourses(Set<UserCourse> userCourses) {
        this.userCourses = userCourses;
    }

    public void addUserCourse(UserCourse userCourse) {
        if (this.getUserCourses() == null) {
            this.userCourses = new HashSet<>();
        }
        this.userCourses.add(userCourse);
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public void addSkill(Skill skill) {
        if (this.getSkills() == null) {
            this.skills = new HashSet<>();
        }
        this.skills.add(skill);
    }

    public Set<CourseFile> getFiles() {
        return files;
    }

    public void setFiles(Set<CourseFile> files) {
        this.files = files;
    }

    public CourseTemplateMetadata getCourseTemplateMetadata() {
        return courseTemplateMetadata;
    }

    public void setCourseTemplateMetadata(CourseTemplateMetadata courseTemplateMetadata) {
        this.courseTemplateMetadata = courseTemplateMetadata;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }

}
