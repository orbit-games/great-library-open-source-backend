package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewStep;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface ReviewStepRepository extends CrudRepository<ReviewStep, Long> {

    Optional<ReviewStep> findByRef(String ref);

    @Query("SELECT rs " +
            "FROM ReviewStep rs " +
            "LEFT JOIN FETCH rs.quiz rsq " +
            "LEFT JOIN FETCH rsq.elements rsqe " +
            "LEFT JOIN FETCH rsqe.options rsqeo " +
            "WHERE rs.chapter = :chapter ")
    Set<ReviewStep> findByChapterFetchRelated(@Param("chapter") Chapter chapter);

    @Query("SELECT rs " +
            "FROM ReviewStep rs " +
            "LEFT JOIN FETCH rs.quiz rsq " +
            "LEFT JOIN FETCH rsq.elements rsqe " +
            "LEFT JOIN FETCH rsqe.options rsqeo " +
            "WHERE rs.chapter.course = :course")
    Set<ReviewStep> findByCourseFetchRelated(@Param("course") Course course);
}
