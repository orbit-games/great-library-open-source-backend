package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;


import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Quiz;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.QuizResult;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface QuizResultRepository extends CrudRepository<QuizResult, Long> {

    List<QuizResult> findByUserAndQuizIn(User user, List<Quiz> resourceQuizes);

    boolean existsByQuiz(Quiz quiz);

    Set<QuizResult> findByQuiz(Quiz quiz);
}
