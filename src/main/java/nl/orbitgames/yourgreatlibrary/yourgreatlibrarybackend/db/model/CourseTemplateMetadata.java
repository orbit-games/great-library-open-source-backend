package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
public class CourseTemplateMetadata {

    private static final Comparator<CourseTemplateChapterMetadata> CHAPTER_METADATA_COMPARATOR = Comparator.comparing(CourseTemplateChapterMetadata::getSortOrder);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String templateId;

    @NotNull
    private int version;

    @NotNull
    private String name;

    @Lob
    private String description;

    @OneToMany(mappedBy = "courseTemplateMetadata")
    @OrderBy("sortOrder")
    private List<CourseTemplateChapterMetadata> chapters;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CourseTemplateChapterMetadata> getChapters() {
        return chapters;
    }

    public void setChapters(List<CourseTemplateChapterMetadata> chapters) {
        this.chapters = chapters;
        this.chapters.sort(CHAPTER_METADATA_COMPARATOR);
    }

    public void addChapter(CourseTemplateChapterMetadata chapter) {
        if (this.getChapters() == null) {
            this.chapters = new ArrayList<>();
        }

        this.chapters.add(chapter);
        this.chapters.sort(CHAPTER_METADATA_COMPARATOR);
    }
}
