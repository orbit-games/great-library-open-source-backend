package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Chapter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String ref;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_id")
    private Course course;

    @NotNull
    private int sortOrder;

    @NotNull
    private String name;

    private String description;

    @OneToMany(mappedBy = "chapter")
    private Set<ChapterSection> chapterSections = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "reviewer_relation_generation_settings_id")
    private ReviewerRelationGenerationSettings reviewerRelationGenerationSettings;

    @OneToMany(mappedBy = "chapter")
    @OrderBy("sortOrder")
    private List<ReviewStep> reviewSteps = new ArrayList<>();

    @NotNull
    private OffsetDateTime created;

    @NotNull
    private OffsetDateTime modified;

    @PrePersist
    public void prePersist() {
        if (created == null) {
            created = OffsetDateTime.now();
        }
        if (modified == null) {
            modified = OffsetDateTime.now();
        }
    }

    public void preUpdate() {
        modified = OffsetDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ReviewerRelationGenerationSettings getReviewerRelationGenerationSettings() {
        return reviewerRelationGenerationSettings;
    }

    public void setReviewerRelationGenerationSettings(ReviewerRelationGenerationSettings reviewerRelationGenerationSettings) {
        this.reviewerRelationGenerationSettings = reviewerRelationGenerationSettings;
    }

    public Set<ChapterSection> getChapterSections() {
        return chapterSections;
    }

    public void setChapterSections(Set<ChapterSection> chapterSections) {
        this.chapterSections = chapterSections;
    }

    public void addChapterSection(ChapterSection chapterSection) {
        if (this.getChapterSections() == null) {
            this.chapterSections = new HashSet<>();
        }

        this.chapterSections.add(chapterSection);
    }

    public void removeChapterSection(ChapterSection chapterSection) {
        if (this.getChapterSections() == null) {
            return;
        }

        this.chapterSections.remove(chapterSection);
    }

    public List<ReviewStep> getReviewSteps() {
        return reviewSteps;
    }

    public void setReviewSteps(List<ReviewStep> reviewSteps) {
        this.reviewSteps = reviewSteps;
    }

    public void addReviewStep(ReviewStep reviewStep) {
        if (this.getReviewSteps() == null) {
            this.reviewSteps = new ArrayList<>();
        }
        this.reviewSteps.add(reviewStep);
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public OffsetDateTime getModified() {
        return modified;
    }

    public void setModified(OffsetDateTime modified) {
        this.modified = modified;
    }
}
