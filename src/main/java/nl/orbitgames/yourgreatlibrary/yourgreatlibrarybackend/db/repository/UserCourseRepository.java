package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.UserCourse;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserCourseRepository extends CrudRepository<UserCourse, Long> {

    Optional<UserCourse> findByCourseRefAndUserRef(String courseRef, String userRef);
}
