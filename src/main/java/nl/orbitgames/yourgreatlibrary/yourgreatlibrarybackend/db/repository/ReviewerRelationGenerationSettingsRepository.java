package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelationGenerationSettings;
import org.springframework.data.repository.CrudRepository;

public interface ReviewerRelationGenerationSettingsRepository extends CrudRepository<ReviewerRelationGenerationSettings, Long> {
}
