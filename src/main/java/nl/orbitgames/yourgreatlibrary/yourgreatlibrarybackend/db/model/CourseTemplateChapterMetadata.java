package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
public class CourseTemplateChapterMetadata {

    public static final Comparator<CourseTemplateReviewStepMetadata> REVIEW_STEP_METADATA_COMPARATOR = Comparator.comparing(CourseTemplateReviewStepMetadata::getSortOrder);
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_template_metadata_id")
    private CourseTemplateMetadata courseTemplateMetadata;

    @NotNull
    private String chapterId;

    @NotNull
    private int sortOrder;

    @NotNull
    private String name;

    @OneToMany(mappedBy = "courseTemplateChapterMetadata")
    @OrderBy("sortOrder")
    private List<CourseTemplateReviewStepMetadata> reviewSteps;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CourseTemplateMetadata getCourseTemplateMetadata() {
        return courseTemplateMetadata;
    }

    public void setCourseTemplateMetadata(CourseTemplateMetadata courseTemplateMetadata) {
        this.courseTemplateMetadata = courseTemplateMetadata;
    }

    public String getChapterId() {
        return chapterId;
    }

    public void setChapterId(String chapterId) {
        this.chapterId = chapterId;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CourseTemplateReviewStepMetadata> getReviewSteps() {
        return reviewSteps;
    }

    public void setReviewSteps(List<CourseTemplateReviewStepMetadata> reviewSteps) {
        this.reviewSteps = reviewSteps;
        this.reviewSteps.sort(REVIEW_STEP_METADATA_COMPARATOR);
    }

    public void addReviewStep(CourseTemplateReviewStepMetadata reviewStep) {
        if (this.getReviewSteps() == null) {
            this.reviewSteps = new ArrayList<>();
        }

        this.reviewSteps.add(reviewStep);
        this.reviewSteps.sort(REVIEW_STEP_METADATA_COMPARATOR);
    }
}
