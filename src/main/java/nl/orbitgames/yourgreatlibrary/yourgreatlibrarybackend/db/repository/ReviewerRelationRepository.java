package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Chapter;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.Course;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ReviewerRelation;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ReviewerRelationRepository extends CrudRepository<ReviewerRelation, Long> {

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "WHERE (rr.reviewer = :user OR rr.submitter = :user) " +
            "AND rr.chapter.course = :course")
    List<ReviewerRelation> findByReviewerOrSubmitterInCourse(@Param("user") User user, @Param("course") Course course);

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "WHERE (rr.reviewer = :user OR rr.submitter = :user) " +
            "AND rr.chapter = :chapter")
    List<ReviewerRelation> findByReviewerOrSubmitterForChapter(@Param("user") User user, @Param("chapter") Chapter chapter);

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "LEFT JOIN FETCH rr.chapter ch " +
            "LEFT JOIN FETCH ch.course c " +
            "LEFT JOIN FETCH rr.reviewer ru " +
            "LEFT JOIN FETCH rr.submitter su " +
            "WHERE rr.ref = :ref")
    Optional<ReviewerRelation> findByRef(@Param("ref") String ref);

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "LEFT JOIN FETCH rr.chapter ch " +
            "LEFT JOIN FETCH ch.course c " +
            "LEFT JOIN FETCH rr.reviewer ru " +
            "LEFT JOIN FETCH rr.submitter su " +
            "WHERE rr.chapter = :chapter")
    List<ReviewerRelation> findByChapter(@Param("chapter") Chapter chapter);

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "LEFT JOIN FETCH rr.reviewer ru " +
            "LEFT JOIN FETCH rr.submitter su " +
            "WHERE rr.chapter.course = :course")
    List<ReviewerRelation> findByCourse(@Param("course") Course course);

    @Query("SELECT rr FROM ReviewerRelation rr " +
            "WHERE rr.submitter = :submitter " +
            "AND rr.chapter = :chapter")
    List<ReviewerRelation> findBySubmitterForChapter(@Param("submitter") User submitter, @Param("chapter") Chapter chapter);


    @Query("SELECT rr FROM ReviewerRelation rr " +
            "WHERE rr.submitter = :reviewer " +
            "AND rr.chapter = :chapter")
    List<ReviewerRelation> findByReviewerForChapter(@Param("reviewer") User reviewer, @Param("chapter") Chapter chapter);


}

