package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.repository;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model.ArgumentType;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface ArgumentTypeRepository extends CrudRepository<ArgumentType, Long> {

    Optional<ArgumentType> findByRef(String ref);

    Collection<ArgumentType> findByRefIn(Set<String> collect);
}
