package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.db.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class CourseTemplateReviewStepMetadata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String reviewStepId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_template_metadata_id")
    private CourseTemplateMetadata courseTemplateMetadata;

    @ManyToOne(optional = false)
    @JoinColumn(name = "course_template_chapter_metadata_id")
    private CourseTemplateChapterMetadata courseTemplateChapterMetadata;

    @NotNull
    private int sortOrder;

    private String name;

    private int deadlineOffset;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReviewStepId() {
        return reviewStepId;
    }

    public void setReviewStepId(String reviewStepId) {
        this.reviewStepId = reviewStepId;
    }

    public CourseTemplateMetadata getCourseTemplateMetadata() {
        return courseTemplateMetadata;
    }

    public void setCourseTemplateMetadata(CourseTemplateMetadata courseTemplateMetadata) {
        this.courseTemplateMetadata = courseTemplateMetadata;
    }

    public CourseTemplateChapterMetadata getCourseTemplateChapterMetadata() {
        return courseTemplateChapterMetadata;
    }

    public void setCourseTemplateChapterMetadata(CourseTemplateChapterMetadata courseTemplateChapterMetadata) {
        this.courseTemplateChapterMetadata = courseTemplateChapterMetadata;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeadlineOffset() {
        return deadlineOffset;
    }

    public void setDeadlineOffset(int deadlineOffset) {
        this.deadlineOffset = deadlineOffset;
    }
}
