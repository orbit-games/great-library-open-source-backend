package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

public interface OrbitGamesErrorCode {

    /**
     * @return A string representation of the error code, which is ready to be presented to a client.
     */
    String getErrorCode();

}
