package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class OrbitGamesApplicationException extends RuntimeException {

    private OrbitGamesErrorCode errorCode;

    public OrbitGamesApplicationException(OrbitGamesErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public OrbitGamesApplicationException(String message, OrbitGamesErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public OrbitGamesApplicationException(String message, OrbitGamesErrorCode errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public OrbitGamesErrorCode getErrorCode() {
        return errorCode;
    }
}
