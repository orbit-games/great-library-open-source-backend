package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

public class OrbitGamesSystemException extends RuntimeException {

    public OrbitGamesSystemException(String message) {
        super(message);
    }

    public OrbitGamesSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrbitGamesSystemException(Throwable cause) {
        super(cause);
    }
}
