package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class OrbitGamesAuthorizationException extends OrbitGamesApplicationException {
    public OrbitGamesAuthorizationException(String message) {
        super(message, GenericErrorCode.AUTHORIZATION_ERROR);
    }

    public OrbitGamesAuthorizationException() {
        super(GenericErrorCode.AUTHORIZATION_ERROR);
    }
}
