package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

public enum GenericErrorCode implements OrbitGamesErrorCode {

    ENTITY_ALREADY_EXISTS("entity.exists"),
    ENTITY_NOT_FOUND("entity.not_found"),
    /**
     * In case a user tries to execute some action for which he/she is not authorized
     */
    AUTHORIZATION_ERROR("error.authorization"),
    UNKNOWN_ERROR("error.unknown"),
    CONSTRAINT_VIOLATION("error.constraint_violation");

    private final String errorCode;

    private GenericErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorCode() {
        return errorCode;
    }
}
