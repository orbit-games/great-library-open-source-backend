package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Generic login exception. Does not include any details about why the login failed, as those details generally
 * compromise the security of the login module.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class YourGreatLibraryLoginException extends OrbitGamesApplicationException {

    public YourGreatLibraryLoginException() {
        super(new LoginExceptionErrorCode());
    }

    public static final class LoginExceptionErrorCode implements OrbitGamesErrorCode {
        @Override
        public String getErrorCode() {
            return "login.invalid";
        }
    }

}
