package nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.interceptor;

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.exception.OrbitGamesSystemException;
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.server.YourGreatLibraryConfig;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RequireTestEnvironmentAspect {

    private final YourGreatLibraryConfig config;

    public RequireTestEnvironmentAspect(@Autowired YourGreatLibraryConfig config) {
        this.config = config;
    }

    @Around("@annotation(RequireTestEnvironment)")
    public Object requireTestEnvironment(ProceedingJoinPoint joinPoint) throws Throwable {

        if (!config.isTestApplicationEnvironment()) {
            throw new OrbitGamesSystemException("Method '" + joinPoint.getSignature().toShortString() + "' may not be called in environments other than DEV or LOCAL");
        }

        return joinPoint.proceed();
    }
}
