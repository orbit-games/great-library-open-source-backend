A design approach may be considered if the focus of your research is as follows.

 • Knowledge gap: void in the functioning of the (complex) socio-technical system
 • Research question: some design needed
 • Objective: make a design
 • Approach: design approach (TIP or design science)
