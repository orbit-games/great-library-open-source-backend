Section properties
------------------
```
.section.title
.section.text
.section.parent.desription
.section.children
.section.resources
```

Resource properties
-------------------
If a resource only has a title and description (and optionally a relation description), it's a so-called "text resource"
```
.title
.description
.url
.skill
.optional
.relation.description
```

Quiz properties
---------------
Quiz can be simple completion quiz, or a full quiz
Simple quiz:
```
.quiz.title
.quiz.description
.quiz.question.text
```
Full quiz:
```
.quiz.title
.quiz.description
.quiz.questions
.quiz.minimal-score
.quiz.question.0.text
.quiz.question.0.type
.quiz.question.0.answers
```