Ideally, the sub deliverable of a previous sub research question is input for the next step. This logic should be visualised in a research flow diagram, which shows the sequential research steps and its main activities. Open the pdf document in which a set of slides address:

 • What is a Research Flow Diagram?
 • Why should I develop a Research Flow Diagram?
 • How to develop a Research Flow Diagram?
 • Examples of Research Flow Diagrams
 • Teachers' tips