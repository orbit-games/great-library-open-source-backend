Do you already know about SCARN? SCARN - Situation, Complication (Question), Approach, Results and Next steps – is the structure of most abstracts of scientific papers, and eventually also on the basis of the structure of the research proposal you are going to write. In this quiz you have to order the sentences of abstracts according to SCARN.

An abstract  has the following functions:

 • To summarize the research project that is proposed
 • To get your line of argumentation straight, and align all elements you've been working on so far
 • To introduce the reader(s) in the line of reasoning and structure of your paper
 • To seduce your reader(s) to continue reading

Take the test to experience how it works and how you can write a perfect SCARN-based abstract!