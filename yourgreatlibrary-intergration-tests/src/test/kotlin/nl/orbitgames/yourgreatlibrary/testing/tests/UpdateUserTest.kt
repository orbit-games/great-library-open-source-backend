package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Property
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test

class UpdateUserTest: BaseTest() {

    @Test
    fun updateUserTest() {

        api.login(testUser.username, testUser.password)

        val user = api.getLoggedInUser()

        val updatedProperties = user.properties?.toMutableMap() ?: HashMap()
        updatedProperties["testProperty"] = "Test Value 1"

        val updatedUser = user.copy(
                fullName = "Full Name Update Test",
                email = "updatedEmail@orbitgames.nl",
                properties = updatedProperties
        )

        val updateUserResponse = api.updateLoggedInUser(updatedUser)

        assertThat(updateUserResponse.fullName, equalTo(updatedUser.fullName))
        assertThat(updateUserResponse.email, equalTo(updatedUser.email))
        assertThat(updateUserResponse.properties?.get("testProperty"), equalTo(updatedProperties["testProperty"]))

        // Change it back for the other tests:
        api.updateLoggedInUser(user)
    }

    @Test
    fun updateUserProperty() {

        api.login(testUser.username, testUser.password)

        val testPropKey = "test_prop_" + RandomData.randomString(10)

        val existingProperty = api.getLoggedInUserProperty(testPropKey)
        assertThat(existingProperty.value, nullValue())

        val propertyWithUpdates1 = Property(testPropKey, RandomData.randomString(10))
        val updatedProperty1 = api.updateLoggedInUserProperty(testPropKey, propertyWithUpdates1)

        assertThat(updatedProperty1.key, equalTo(testPropKey))
        assertThat(updatedProperty1.value, equalTo(propertyWithUpdates1.value))

        // We also expect that when we now get the property, it will also have the updated value:
        val retrievedUpdatedProperty1 = api.getLoggedInUserProperty(testPropKey)
        assertThat(retrievedUpdatedProperty1.key, equalTo(testPropKey))
        assertThat(retrievedUpdatedProperty1.value, equalTo(propertyWithUpdates1.value))

        // Now we're gonna update the property again (the previous request added the property), and it should be changes
        val propertyWithUpdates2 = Property(testPropKey, RandomData.randomString(10))
        val updatedProperty2 = api.updateLoggedInUserProperty(testPropKey, propertyWithUpdates2)

        assertThat(updatedProperty2.key, equalTo(testPropKey))
        assertThat(updatedProperty2.value, equalTo(propertyWithUpdates2.value))

        // We also expect that when we now get the property, it will also have the updated value:
        val retrievedUpdatedProperty2 = api.getLoggedInUserProperty(testPropKey)
        assertThat(retrievedUpdatedProperty2.key, equalTo(testPropKey))
        assertThat(retrievedUpdatedProperty2.value, equalTo(propertyWithUpdates2.value))
    }

}