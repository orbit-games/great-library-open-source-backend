package nl.orbitgames.yourgreatlibrary.testing.tests

import io.restassured.response.ValidatableResponse
import nl.orbitgames.yourgreatlibrary.testing.data.getTestPdf
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test

class TestLogin: BaseTest() {

    @Test
    fun loginTest() {

        val loginResponse = api.userLogin(UserLoginRequest(testUser.username, testUser.password))

        // We check if we can do some operation that requires a login, such as requesting the logged in tests
        val getUserResponse = api.getLoggedInUser(loginResponse.sessionToken)

        assertThat(loginResponse.user!!.ref, equalTo(getUserResponse.ref))
    }

    @Test
    fun loginInvalidPasswordTest() {

        val loginResponse = api.applicationError(api.userLoginRaw(UserLoginRequest(testUser.username, testUser.password + "a")))
        assertThat(loginResponse.error.code, equalTo("login.invalid"))
        // To prevent being able to prone which user exists, no information about why the login failed should be given
        assertThat(loginResponse.error.message, nullValue())
    }

    @Test
    fun loginInvalidUsernameTest() {

        val loginResponse = api.applicationError(api.userLoginRaw(UserLoginRequest("invalid_username", "invalid_password")))

        assertThat(loginResponse.error.code, equalTo("login.invalid"))
        // To prevent being able to prone which user exists, no information about why the login failed should be given
        assertThat(loginResponse.error.message, nullValue())
    }

    @Test
    fun prohibitAccessWhenNotLoggedInTest() {

        val testUserRef = testUserRef
        val testCourseRef = testCourse.ref!!
        val testChapterRef = testCourse.chapters!![0].ref!!
        val testStepRef = testCourse.chapters!![0].reviewSteps!![0].ref!!
        val testResourceRef = testCourse.resources!![0].ref!!

        api.login(testUser.username, testUser.password)
        val testRelationRef = api.getLoggedInUserCourseChapterProgress(testCourseRef, testChapterRef).reviewerRelations!![0].ref!!
        api.logout()

        // User
        testAuthFailure { api.getLoggedInUserRaw() }
        testAuthFailure { api.getUserRaw(testUserRef) }
        testAuthFailure { api.updateLoggedInUserRaw(User(testUserRef, fullName = "Test")) }
        testAuthFailure { api.updateUserRaw(testUserRef, User(testUserRef, fullName = "Test")) }
        testAuthFailure { api.getUserPropertyRaw(testUserRef, "test_key") }
        testAuthFailure { api.getLoggedInUserPropertyRaw("test_key") }
        testAuthFailure { api.updateUserPropertyRaw(testUserRef, "test_key", Property("test_key", "value")) }
        testAuthFailure { api.updateLoggedInUserPropertyRaw("test_key", Property("test_key", "value")) }
        testAuthFailure { api.joinCourseRaw(testUserRef, TestEnvironment.testCourseCode) }
        testAuthFailure { api.loggedInUserJoinCourseRaw(TestEnvironment.testCourseCode) }
        testAuthFailure { api.getUserCourseProgressRaw(testUserRef, testCourseRef) }
        testAuthFailure { api.getLoggedInUserCourseProgressRaw(testCourseRef) }
        testAuthFailure { api.getUserCourseChapterProgressRaw(testUserRef, testCourseRef, testChapterRef) }
        testAuthFailure { api.getLoggedInUserCourseChapterProgressRaw( testCourseRef, testChapterRef ) }
        testAuthFailure { api.postUserReviewStepFileUploadRaw( testUserRef, testCourseRef, testChapterRef, testRelationRef, testStepRef,"document.pdf", getTestPdf() ) }
        testAuthFailure { api.postLoggedInUserReviewStepFileUploadRaw(testCourseRef, testChapterRef, testRelationRef, testStepRef,"document.pdf", getTestPdf() ) }
        testAuthFailure { api.postUserReviewStepQuizRaw( testUserRef, testCourseRef, testChapterRef, testRelationRef, testStepRef, QuizResult( "", arrayOf() ) ) }
        testAuthFailure { api.postLoggedInUserReviewStepQuizRaw(testCourseRef, testChapterRef, testRelationRef, testStepRef, QuizResult("", arrayOf())) }
        testAuthFailure { api.postUserReviewStepArgumentRaw( testUserRef, testCourseRef, testChapterRef, testRelationRef, testStepRef, Argument(message="Some argument") ) }
        testAuthFailure { api.postLoggedInUserReviewStepArgumentRaw(testCourseRef, testChapterRef, testRelationRef, testStepRef,Argument(message="Some argument") ) }
        testAuthFailure { api.postUserResourceQuizResultRaw(testUserRef, testCourseRef, testResourceRef, QuizResult("", arrayOf())) }
        testAuthFailure { api.postLoggedInUserResourceQuizResultRaw(testCourseRef, testResourceRef, QuizResult("", arrayOf())) }

        // Course
        testAuthFailure { api.getCourseRaw(testCourseRef) }
        testAuthFailure { api.getCourseUserRaw(testCourseRef, testUserRef) }
        testAuthFailure { api.updateCourseUserRaw(testCourseRef, testUserRef, CourseProgressSummary(active = true)) }
        testAuthFailure { api.getCourseUserPropertyRaw(testCourseRef, testUserRef, "test_key") }
        testAuthFailure { api.updateCourseUserPropertyRaw(testCourseRef, testUserRef, "test_key", Property("test_key", "test_value")) }
    }

    private fun testAuthFailure(call: () -> ValidatableResponse) {
        val resp = api.applicationError(call.invoke())
        assertThat(resp.error.code, equalTo("error.authorization"))
    }

}