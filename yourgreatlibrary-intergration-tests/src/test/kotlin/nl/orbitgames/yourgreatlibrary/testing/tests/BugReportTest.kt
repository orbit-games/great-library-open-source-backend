package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.getTestImage
import nl.orbitgames.yourgreatlibrary.testing.data.getTestImage2
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.BugReport
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.BugReportScreenshot
import org.junit.Test
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.time.OffsetDateTime
import java.util.zip.GZIPOutputStream

class BugReportTest: BaseTest() {

    @Test
    fun bugReportTest() {

        api.postClientBugReport(BugReport(
                email = "test@example.com",
                userInput = "The feature doesn't work",
                deviceInfo = toGzippedString("Test Device Info"),
                gameLogs = toGzippedString("00:00:00 Example game logs"),
                communicationLogs = toGzippedString("00:00:00: Example communication logs"),
                screenshots = arrayOf(
                    BugReportScreenshot(
                            OffsetDateTime.now(),
                            getTestImage()
                    ),
                    BugReportScreenshot(
                            OffsetDateTime.now(),
                            getTestImage2()
                    )),
                prefs = toGzippedString(""" {"A": 1, "B": "2" } """),
                knowledge = toGzippedString(""" {"A": 1, "B": "2" } """),
                time = OffsetDateTime.now()
        ))

    }

    private fun toGzippedString(str: String): ByteArray {

        val os = ByteArrayOutputStream()
        GZIPOutputStream(os).use { gzip ->
            gzip.write(str.toByteArray(StandardCharsets.UTF_8))
        }
        return os.toByteArray()
    }
}