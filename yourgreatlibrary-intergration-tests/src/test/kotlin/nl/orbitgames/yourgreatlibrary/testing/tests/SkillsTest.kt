package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Skill
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test

class SkillsTest: BaseTest() {

    @Test
    fun getSkillsTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val skills = api.getSkills(courseRef)
        assertThat(skills.skills.size, greaterThan(0))
    }

    @Test
    fun createSectionTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val newSkill = Skill(
                name = "Test skill",
                description = "This is a test skill",
                icon = "some-icon"
        )
        val createdSkill = api.createSkill(courseRef, newSkill);

        assertThat(createdSkill.ref, notNullValue())
        assertSkillsEqual(createdSkill, newSkill)

        // We get all skills here to assert that the new skill is actually returned when looking for all skills in the course
        val skills = api.getSkills(courseRef)
        val skillInCourse = skills.skills.find{ s -> s.ref == createdSkill.ref }
        assertThat(skillInCourse, notNullValue())
        assertSkillsEqual(skillInCourse!!, newSkill)

        val skillInCourseDirect = api.getSkill(courseRef, createdSkill.ref!!)
        assertSkillsEqual(skillInCourseDirect, newSkill)

    }

    @Test
    fun updateSkillTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val newSkill = Skill(
                name = "Test skill 2",
                description = "This is test skill 2",
                icon = "some-icon"
        )
        val createdSkill = api.createSkill(courseRef, newSkill);

        val skillUpdate1 = createdSkill.copy(
                name = "Test skill 2 update",
                description = "This is an updated description of test skill 2",
                icon = "some-updated-icon"
        )

        val updatedSkill1 = api.updateSkill(courseRef, createdSkill.ref!!, skillUpdate1)
        assertSkillsEqual(updatedSkill1, skillUpdate1)

        val retrievedUpdatedSkill1 = api.getSkill(courseRef, createdSkill.ref!!)
        assertSkillsEqual(retrievedUpdatedSkill1, skillUpdate1)
    }

    private fun assertSkillsEqual(actual: Skill, matcher: Skill) {

        assertThat(actual.name, equalTo(matcher.name))
        assertThat(actual.description, equalTo(matcher.description))
        assertThat(actual.icon, equalTo(matcher.icon))
    }
}