package nl.orbitgames.yourgreatlibrary.testing.api

import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Course
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Role
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.User
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.UserAgreement
import java.time.OffsetDateTime

class GreatLibraryTestApi: GreatLibraryApi() {

    fun testUserExists(username: String): Boolean {
        return req()
                .testAuth()
                .queryParam("username", username)
                .`when`()
                .get("/test/user/exists")
                .resp()
                .valid()
                .`as`(Boolean::class)
    }


    fun testGetUserRef(username: String): String {
        return req()
                .testAuth()
                .queryParam("username", username)
                .`when`()
                .get("/test/user/ref")
                .resp()
                .statusCode(200)
                .contentType(ContentType.TEXT)
                .extract()
                .body()
                .asString()
    }

    fun testCourseGenerateReviewerRelations(courseRef: String) {
        req()
                .testAuth()
                .pathParam("courseRef", courseRef)
                .`when`()
                .post("/test/course/{courseRef}/generate-relations")
                .resp()
                .valid()
    }

    fun testCreateAgreement(pdfUrl: String) : UserAgreement {
        return req()
                .testAuth()
                .body(mapOf(Pair("pdfUrl", pdfUrl)))
                .post("/test/user-agreement")
                .resp()
                .valid()
                .`as`(UserAgreement::class)
    }

    fun testCreateCourse(createTestCourseRequest: GreatLibraryTestApi.CreateTestCourseRequest): Course {
        return req()
                .testAuth()
                .body(createTestCourseRequest)
                .post("/test/course")
                .resp()
                .valid()
                .`as`(Course::class)
    }

    fun testCreateUser(request: TestUserCreateRequest): User {

        return req()
                .testAuth()
                .body(request)
                .post("test/user")
                .resp()
                .valid()
                .`as`(User::class)
    }

    private fun RequestSpecification.testAuth(): RequestSpecification {
        return this
                .header("X-Token", "IbYl^Gd3%CYW^81cDwPmicb33#AScJ");
    }

    class CreateTestCourseRequest (
        val firstSubmissionDeadline: OffsetDateTime,
        val courseName: String,
        val courseCode: String,
        val anonymousUsers: Boolean = false,
        val anonymousReviews: Boolean = false
    )

    data class TestUserCreateRequest(
        var username: String,
        var password: String,
        var email: String,
        var fullName: String,
        var role: Role
    )
}