package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.ChapterDeadlines
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Course
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CourseDeadlines
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.ReviewStepDeadline
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.OffsetDateTime

class DeadlinesTest: BaseTest() {

    companion object  {
        const val OFFSET_DAYS = 14L;
    }

    @Test
    fun updateDeadlinesTest() {

        val testCourseRef = testCourse.ref!!

        loginTestTeacher()

        val course = api.getCourse(testCourseRef, true)
        val deadlinesWithUpdates = constructCourseDeadlines(course) { it.plusDays(OFFSET_DAYS) };

        val updatedDeadlines = api.updateDeadlines(testCourseRef, deadlinesWithUpdates)

        assertCourseDeadlinesEqual(updatedDeadlines, deadlinesWithUpdates)
        val updatedCourse = api.getCourse(testCourseRef, true)
        val updatedCourseDeadlines = constructCourseDeadlines(updatedCourse)

        assertCourseDeadlinesEqual(updatedCourseDeadlines, deadlinesWithUpdates)

        // Move it back
        val deadlinesWithUpdates2 = constructCourseDeadlines(updatedCourse) { it.minusDays(OFFSET_DAYS) }
        val updatedDeadlines2 = api.updateDeadlines(testCourseRef, deadlinesWithUpdates2)

        assertCourseDeadlinesEqual(updatedDeadlines2, deadlinesWithUpdates2)
    }

    private fun constructCourseDeadlines(course: Course, dateModifier: (original: OffsetDateTime) -> OffsetDateTime = this::identity): CourseDeadlines {
        return CourseDeadlines(
                chapters = course.chapters!!.map { chapter ->
                    ChapterDeadlines(
                            chapter.ref!!,
                            chapter.reviewSteps!!
                                    .map { reviewStep -> ReviewStepDeadline(reviewStep.ref!!, dateModifier(reviewStep.deadline)) }
                                    .toTypedArray()
                    )
                }.toTypedArray()
        );
    }

    private fun assertCourseDeadlinesEqual(actual: CourseDeadlines, expected: CourseDeadlines) {

        assertThat(actual.chapters.size, equalTo(expected.chapters.size))
        for (i in 0 until actual.chapters.size) {
            assertChapterDeadlinesEqual(actual.chapters[i], expected.chapters[i])
        }
    }

    private fun assertChapterDeadlinesEqual(actual: ChapterDeadlines, expected: ChapterDeadlines) {
        assertThat(actual.chapterRef, equalTo(expected.chapterRef));
        assertThat(actual.reviewSteps.size, equalTo(expected.reviewSteps.size))
        for (i in 0 until actual.reviewSteps.size) {
            assertReviewStepDeadlinesEqual(actual.reviewSteps[i], expected.reviewSteps[i])
        }
    }

    private fun assertReviewStepDeadlinesEqual(actual: ReviewStepDeadline, expected: ReviewStepDeadline) {
        assertThat(actual.reviewStepRef, equalTo(expected.reviewStepRef))
        assertThat(actual.deadline, equalTo(expected.deadline));
    }

    fun <T> identity(x: T): T = x
}