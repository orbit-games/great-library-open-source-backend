package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CreateCourseFromTemplateRequest
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

class CourseTemplateTest: BaseTest() {

    @Test
    fun getCourseTemplatesTest() {

        loginTestTeacher()

        val templates = api.getCourseTemplates()

        assertThat(templates.templates, notNullValue())
        assertThat(templates.templates.size, greaterThan(0))

        // Check that the "minimalistic test course" is there:
        assertThat(templates.templates.find { it.id == "minimalistic-test-course"}, notNullValue())

    }

    @Test
    fun createCourseFromTemplateTest() {

        loginTestTeacher()

        val courseCode = RandomData.randomString(8)

        val firstDeadline = OffsetDateTime.now().plusDays(7)

        val request = CreateCourseFromTemplateRequest(
                templateId = "minimalistic-test-course",
                courseName = "Course created from template test",
                year = 2018,
                quarter = 1,
                courseCode = courseCode,
                anonymousReviews = false,
                anonymousUsers = true,
                deadlinesOverride = mapOf(
                        Pair("1.submission", firstDeadline),
                        Pair("1.review", firstDeadline.plusDays(5)),
                        Pair("1.evaluation", firstDeadline.plusDays(7)),
                        Pair("1.rebuttal", firstDeadline.plusDays(16)),
                        Pair("1.assessment", firstDeadline.plusDays(23))
                )
        )

        val course = api.postCreateCourseFromTemplate(request)

        assertThat(course.name, equalTo(request.courseName))
        assertThat(course.year, equalTo(request.year))
        assertThat(course.quarter, equalTo(request.quarter))
        assertThat(course.code, equalTo(request.courseCode))
        assertThat(course.chapters!!.size, equalTo(1))
        assertThat(ChronoUnit.MILLIS.between(course.chapters!![0].reviewSteps!![0].deadline, firstDeadline), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(course.chapters!![0].reviewSteps!![1].deadline, firstDeadline.plusDays(5)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(course.chapters!![0].reviewSteps!![2].deadline, firstDeadline.plusDays(7)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(course.chapters!![0].reviewSteps!![3].deadline, firstDeadline.plusDays(16)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(course.chapters!![0].reviewSteps!![4].deadline, firstDeadline.plusDays(23)), equalTo(0L))

        val retrievedCourse = api.getCourse(course.ref!!)

        assertThat(retrievedCourse.name, equalTo(request.courseName))
        assertThat(retrievedCourse.year, equalTo(request.year))
        assertThat(retrievedCourse.quarter, equalTo(request.quarter))
        assertThat(retrievedCourse.code, equalTo(request.courseCode))
        assertThat(retrievedCourse.chapters!!.size, equalTo(1))
        assertThat(ChronoUnit.MILLIS.between(retrievedCourse.chapters!![0].reviewSteps!![0].deadline, firstDeadline), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(retrievedCourse.chapters!![0].reviewSteps!![1].deadline, firstDeadline.plusDays(5)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(retrievedCourse.chapters!![0].reviewSteps!![2].deadline, firstDeadline.plusDays(7)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(retrievedCourse.chapters!![0].reviewSteps!![3].deadline, firstDeadline.plusDays(16)), equalTo(0L))
        assertThat(ChronoUnit.MILLIS.between(retrievedCourse.chapters!![0].reviewSteps!![4].deadline, firstDeadline.plusDays(23)), equalTo(0L))
    }
}