package nl.orbitgames.yourgreatlibrary.testing.data

fun getTestPdf(): ByteArray {

    TestData::class.java.classLoader.getResourceAsStream("example.pdf").use {
        return it.readBytes()
    }
}

fun getTestImage(): ByteArray {
    TestData::class.java.classLoader.getResourceAsStream("test-image.jpg").use {
        return it.readBytes()
    }
}

fun getTestImage2(): ByteArray {
    TestData::class.java.classLoader.getResourceAsStream("test-image2.png").use {
        return it.readBytes()
    }
}

private class TestData