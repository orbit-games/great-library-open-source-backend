package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.api.GreatLibraryApi
import nl.orbitgames.yourgreatlibrary.testing.data.TestDataUser
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Course
import org.junit.After
import org.junit.Before

open class BaseTest(
        val api: GreatLibraryApi = GreatLibraryApi()
        ) {

    val testUser: TestDataUser by lazy { TestEnvironment.testUser }
    val testUserRef: String by lazy { TestEnvironment.getUserRef(testUser.username) }
    val testTeacher: TestDataUser by lazy { TestEnvironment.testTeacherUser }
    val testTeacherRef: String by lazy { TestEnvironment.getUserRef(TestEnvironment.testTeacherUser.username) }
    val testCourse: Course by lazy {
        return@lazy TestEnvironment.testCourse
    }


    @Before
    fun before() {

        // Initialize the test environment
        TestEnvironment

        api.logout()

    }

    fun loginTestUser() {
        api.login(testUser.username, testUser.password)
    }

    fun loginTestTeacher() {
        api.login(TestEnvironment.testTeacherUser.username, TestEnvironment.testTeacherUser.password)
    }

    @After
    fun after() {
    }

}