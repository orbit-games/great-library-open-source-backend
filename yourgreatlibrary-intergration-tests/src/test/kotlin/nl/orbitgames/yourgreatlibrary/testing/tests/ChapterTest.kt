package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.DomainAssertions.Companion.assertChaptersEqual
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.ReviewerRelationGenerationType
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.SectionRelation
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test

class ChapterTest: BaseTest() {

    @Test
    fun getChapterTest() {

        val courseRef = testCourse.ref!!
        val testChapter = testCourse.chapters!!.first()
        val testChapterRef = testChapter.ref!!

        loginTestTeacher()

        val chapter = api.getCourseChapter(courseRef, testChapterRef)
        assertThat(chapter, notNullValue())
        assertChaptersEqual(chapter, testChapter, true)

    }

    @Test
    fun updateChapterTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val testChapter = testCourse.chapters!!.first()

        val chapterUpdate1 = testChapter.copy(
                name = testChapter.name + " update",
                description = testChapter.description + " update",
                order = testChapter.order!! + 1,
                reviewerCount = 3,
                reviewerRelationGenerationType =
                    if (testChapter.reviewerRelationGenerationType == ReviewerRelationGenerationType.gROUPBASED)
                        ReviewerRelationGenerationType.rANDOM
                    else ReviewerRelationGenerationType.gROUPBASED,
                reviewSteps = testChapter.reviewSteps,
                sectionRelations = arrayOf(
                        // Modify one of the section relations
                        *testChapter.sectionRelations!!.mapIndexed
                            { index, sectionRelation -> if (index == 0) sectionRelation.copy(description = sectionRelation.description + " update") else sectionRelation }.toTypedArray(),
                        // And also add a new one
                        SectionRelation(
                        // Just add the first section that isn't related yet
                        sectionRef = testCourse.sections!!.first { !testChapter.sectionRelations!!.any { sr -> sr.sectionRef == it.ref } }.ref!!,
                        description = "A test section relation",
                        optional = false,
                        sortOrder = testChapter.sectionRelations!!.size * 10
                ))
        )

        val updatedChapter1 = api.updateCourseChapter(courseRef, testChapter.ref!!, chapterUpdate1)
        assertChaptersEqual(updatedChapter1, chapterUpdate1, false)

        val retrievedUpdatedSection1 = api.getCourseChapter(courseRef, testChapter.ref!!)
        assertChaptersEqual(retrievedUpdatedSection1, chapterUpdate1, false)

        // Update it back to the original
        val chapterUpdate2 = testChapter.copy()

        val updatedChapter2 = api.updateCourseChapter(courseRef, testChapter.ref!!, chapterUpdate2);
        assertChaptersEqual(updatedChapter2, chapterUpdate2, false)

        val retrievedUpdatedChapter2 = api.getCourseChapter(courseRef, testChapter.ref!!)
        assertChaptersEqual(retrievedUpdatedChapter2, chapterUpdate2, false)

    }
}