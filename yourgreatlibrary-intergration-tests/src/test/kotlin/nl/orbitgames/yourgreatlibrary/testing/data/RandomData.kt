package nl.orbitgames.yourgreatlibrary.testing.data

import java.util.*

class RandomData {
    companion object {

        private val rnd = Random(System.nanoTime())
        private const val RANDOM_STRING_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_"

        fun randomString(length: Int): String {


            val res = StringBuilder()
            (0..length).forEach {
                res.append(RANDOM_STRING_ALPHABET[rnd.nextInt(RANDOM_STRING_ALPHABET.length)])
            }
            return res.toString()
        }

        fun randomName(): String {
            return RANDOM_NAMES[rnd.nextInt(RANDOM_NAMES.size)]
        }

        private val RANDOM_NAMES = listOf(
                "Azzie Denning",
                "Elfreda Fambro",
                "Carolina Filler",
                "Luisa Legler",
                "Jeanette Bartell",
                "Delpha Schmeltzer",
                "Lauryn Stephen",
                "Alvin Ruehl",
                "Alethea Tovar",
                "Jacalyn Gothard",
                "Sudie Aldinger",
                "Hubert Winfield",
                "Sherrill Kingston",
                "Mistie Knepper",
                "Jenae Wolter",
                "Pasquale Cheadle",
                "Jacquline Daffron",
                "Marcus Goodwill",
                "Hannelore Fujiwara",
                "Sachiko Mobley",
                "Melody Lichtman",
                "Pearle Anspach",
                "Hildred Bady",
                "Broderick Bouchard",
                "Lisette Boyko",
                "Lakenya Lowell",
                "Jefferson Kitchell",
                "Cameron Lehmann",
                "Giuseppe Rempe",
                "Nicol Ducksworth",
                "Eugene Reinecke",
                "Delisa Stoltenberg",
                "Nikita Macko",
                "Simon Mean",
                "Inell Cawley",
                "Zulma Magner",
                "Bonnie Lawson",
                "Fabian Godbee",
                "Evia Helfrich",
                "Darla Forys",
                "Hiedi Tempel",
                "Pedro Blow",
                "Minnie Sessoms",
                "Paul Cort",
                "Goldie Kerrick",
                "Cleotilde Lauderback",
                "Angeline Barnhill",
                "Jessia Knighten",
                "Nadia Kenton",
                "Jim Bottoms",
                "Ingeborg Moak",
                "Rudy Fox",
                "Ola Winner",
                "Karon Bettinger",
                "Debra Applegate",
                "Pauletta Letendre",
                "Markita Kuehl",
                "Antonina Maltby",
                "Isabelle Oubre",
                "Jeniffer Sane",
                "Charlotte Harvill",
                "Kimberlee Catto",
                "Lorina Riedel",
                "Janyce Alegria",
                "Pura Bouska",
                "Marketta Force",
                "Elinor Meloy",
                "Carita Huffstetler",
                "Teressa Shane",
                "Bryant Gosselin",
                "Chi Kollar",
                "Kristan Nolasco",
                "Dagny Rott",
                "Monique Vice",
                "Elenore Lingo",
                "Monica Mcnamee",
                "Nakisha Holoman",
                "Georgiann Crocker",
                "Hugo Kavanagh",
                "Zackary Mcwilliam",
                "Valda Kole",
                "Ivonne Stubblefield",
                "Melony Brezinski",
                "Hisako Schisler",
                "Marx Garofalo",
                "Gerardo Summey",
                "Nora Lavergne",
                "Jeraldine Bonomo",
                "Rosamond Birnbaum",
                "Caron Grogan",
                "Melba Curran",
                "Jerlene Milazzo",
                "Rex Pinkney",
                "Rosalva Brunelle",
                "Corrine Welk",
                "Leatha Durant",
                "Lanette Melone",
                "Latoya Herz",
                "Ludie Schoenberger",
                "Loraine Teal"
        )
    }
}