package nl.orbitgames.yourgreatlibrary.testing.preparation

import nl.orbitgames.yourgreatlibrary.testing.api.GreatLibraryApi
import nl.orbitgames.yourgreatlibrary.testing.api.GreatLibraryTestApi
import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.data.TestDataUser
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class TestEnvironment {

    companion object {

        val api = GreatLibraryApi()
        val testApi = GreatLibraryTestApi()

        val latestAgreement: UserAgreement by lazy {
            getOrCreateUserAgreement()
        }

        val testUser: TestDataUser by lazy {
            val testUser = testUserData[0]
            if (!testApi.testUserExists(testUser.username))
                api.userRegister(UserRegistrationRequest(
                        username = testUser.username,
                        password = testUser.password,
                        email = testUser.email,
                        fullName = testUser.fullName,
                        signedUserAgreementRef = latestAgreement.ref!!
                ))
            return@lazy testUser
        }
        val testUsers: Array<TestDataUser> by lazy {
            for (testUser in testUserData) {
                if (!testApi.testUserExists(testUser.username))
                    api.userRegister(UserRegistrationRequest(
                            username = testUser.username,
                            password = testUser.password,
                            email = testUser.email,
                            fullName = testUser.fullName,
                            signedUserAgreementRef = latestAgreement.ref!!
                    ))
            }
            return@lazy testUserData.toTypedArray()
        }

        val testTeacherUser: TestDataUser by lazy {
            if (!testApi.testUserExists(teacherTestDataUser.username)) {
                testApi.testCreateUser(GreatLibraryTestApi.TestUserCreateRequest(
                        teacherTestDataUser.username,
                        teacherTestDataUser.password,
                        teacherTestDataUser.email,
                        teacherTestDataUser.fullName,
                        Role.tEACHER
                ))
            }
            return@lazy teacherTestDataUser
        }

        val testCourseCode = RandomData.randomString(8)

        private val testUserRefCache: MutableMap<String, String> = HashMap()

        /**
         * The test course is a new course that is generated for each test run.
         */
        val testCourse: Course by lazy {
            val course = testApi.testCreateCourse(GreatLibraryTestApi.CreateTestCourseRequest (
                    firstSubmissionDeadline = OffsetDateTime.now().minusMonths(3),
                    courseName = "Integration Test Course " + LocalDate.now().format(DateTimeFormatter.ISO_DATE),
                    courseCode = testCourseCode,
                    anonymousReviews = false,
                    anonymousUsers = false
            ))
            for (testUser in testUsers) {
                val loginResponse = api.userLogin(UserLoginRequest(testUser.username, testUser.password))
                api.loggedInUserJoinCourse(testCourseCode, loginResponse.sessionToken!!)
            }

            testApi.testCourseGenerateReviewerRelations(course.ref!!)

            // We finally get the actual course that includes all the reviewer relations and enrolled users
            api.login(testUser.username, testUser.password)
            val finalCourse = api.getCourse(course.ref!!, true)
            api.logout()

            return@lazy finalCourse
        }

        fun getUserRef(username: String): String {

            if (!testUserRefCache.containsKey(username)) {
                val ref = testApi.testGetUserRef(username)
                testUserRefCache[username] = ref
            }
            return testUserRefCache[username]!!
        }

        private fun getOrCreateUserAgreement(): UserAgreement {

            return if (api.hasUserAgreement()) {
                api.getLatestUserAgreement()
            } else {
                testApi.testCreateAgreement("https://orbitgames.nl/av/Algemene%20voorwaarden%20Orbit%20Games.pdf")
            }
        }

        private val testUserData = listOf(
                TestDataUser("user1@orbitgames.nl", "user1@orbitgames.nl", "Test123*", "User 1"),
                TestDataUser("user2@orbitgames.nl", "user2@orbitgames.nl", "Test123*", "User 2"),
                TestDataUser("user3@orbitgames.nl", "user3@orbitgames.nl", "Test123*", "User 3"),
                TestDataUser("user4@orbitgames.nl", "user4@orbitgames.nl", "Test123*", "User 4"),
                TestDataUser("user5@orbitgames.nl", "user5@orbitgames.nl", "Test123*", "User 5"),
                TestDataUser("user6@orbitgames.nl", "user6@orbitgames.nl", "Test123*", "User 6")
        )

        private val teacherTestDataUser = TestDataUser("teacher1@orbitgames.nl", "teacher1@orbitgames.nl", "Test123*", "Teacher 1")
    }
}