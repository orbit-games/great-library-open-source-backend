package nl.orbitgames.yourgreatlibrary.testing

import java.util.*

fun <E> List<E>.getRandomElement() = this[Random().nextInt(this.size)]
fun <E> Array<E>.getRandomElement() = this[Random().nextInt(this.size)]