package nl.orbitgames.yourgreatlibrary.testing.tests

import biweekly.Biweekly
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Course
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CourseUpdateRequest
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test

class CourseTest: BaseTest() {

    @Test
    fun getCourseTest() {

        // Note: we get the test course ref before we login
        // Since the test course is lazily initialized, getting it for the first time after the login
        // might cause a logout to happen.
        val testCourseRef = testCourse.ref!!

        loginTestUser()

        val course = api.getCourse(testCourseRef)

        assertThat(course.ref, equalTo(TestEnvironment.testCourse.ref))
        assertThat(course.chapters, notNullValue())
        assertThat(course.chapters!![0].reviewSteps, notNullValue())
        assertThat(course.chapters!![0].reviewSteps!![0].ref, notNullValue())
        assertThat(course.sections, notNullValue())
        assertThat(course.sections!![0], notNullValue())
        assertThat(course.resources, notNullValue())
        assertThat(course.resources!![0].ref, notNullValue())
    }

    @Test
    fun updateCourseTest() {

        val testCourseRef = testCourse.ref!!

        loginTestTeacher()

        val originalCourse = api.getCourse(testCourseRef)

        val courseUpdateRequest = CourseUpdateRequest(
                originalCourse.name + " update",
                originalCourse.year!! + 1,
                (originalCourse.quarter!! + 1) % 4,
                originalCourse.code + "!",
                !originalCourse.anonymousUsers!!,
                !originalCourse.anonymousReviews!!
        )
        val updatedTestCourse = api.updateCourse(testCourseRef, courseUpdateRequest)
        assertCourseUpdated(updatedTestCourse, courseUpdateRequest)

        val retrievedUpdatedTestCourse = api.getCourse(testCourseRef);
        assertCourseUpdated(retrievedUpdatedTestCourse, courseUpdateRequest)

        val courseUpdateBackToOriginalRequest = CourseUpdateRequest(
                originalCourse.name!!,
                originalCourse.year!!,
                originalCourse.quarter!!,
                originalCourse.code!!,
                originalCourse.anonymousUsers!!,
                originalCourse.anonymousReviews!!
        )

        val changedBackCourse = api.updateCourse(testCourseRef, courseUpdateBackToOriginalRequest)
        assertCourseUpdated(changedBackCourse, courseUpdateBackToOriginalRequest)
    }

    private fun assertCourseUpdated(course: Course, courseUpdateRequest: CourseUpdateRequest) {
        assertThat(course.name, equalTo(courseUpdateRequest.name))
        assertThat(course.year, equalTo(courseUpdateRequest.year))
        assertThat(course.quarter, equalTo(courseUpdateRequest.quarter))
        assertThat(course.code, equalTo(courseUpdateRequest.code))
        assertThat(course.anonymousUsers, equalTo(courseUpdateRequest.anonymousUsers))
        assertThat(course.anonymousReviews, equalTo(courseUpdateRequest.anonymousReviews))

    }

    @Test
    fun getCourseDeadlinesICalTest() {

        val testCourseRef = testCourse.ref!!

        loginTestUser()

        val courseDeadlines = api.getCourseDeadlinesICal(testCourseRef)

        val ical = Biweekly.parse(courseDeadlines).first()

        assertThat(ical.events.count(), equalTo(23))

    }

    @Test
    fun getCourseChapterTest() {

        val testCourseRef = testCourse.ref!!
        val testChapterRef = testCourse.chapters!![0].ref!!

        loginTestUser()

        val chapter1 = api.getCourseChapter(testCourseRef, testChapterRef)
        assertThat(chapter1.ref, equalTo(testChapterRef))
        assertThat(chapter1.reviewSteps, notNullValue())
        assertThat(chapter1.reviewSteps!![0].ref, notNullValue())
    }

    @Test
    fun getCourseChapterReviewerRelationsTest() {

        val testCourseRef = testCourse.ref!!
        val testChapterRef = testCourse.chapters!![0].ref!!

        loginTestTeacher()

        val reviewerRelations = api.getCourseChapterReviewerRelations(testCourseRef, testChapterRef)

        assertThat(reviewerRelations.size, greaterThan(0))
        assertThat(reviewerRelations[0].ref, notNullValue())
        assertThat(reviewerRelations[0].submitter, notNullValue())
        assertThat(reviewerRelations[0].reviewer, notNullValue())
    }

}