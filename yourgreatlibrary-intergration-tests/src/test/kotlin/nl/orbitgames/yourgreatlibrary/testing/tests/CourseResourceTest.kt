package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.DomainAssertions.Companion.assertResourceEqual
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers.greaterThan
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Test

class CourseResourceTest: BaseTest() {

    @Test
    fun testGetResources() {

        val courseRef = testCourse.ref!!

        loginTestUser()

        val resources = api.getCourseResources(courseRef)

        assertThat(resources.resources, notNullValue())
        assertThat(resources.resources.size, greaterThan(0))
    }

    @Test
    fun testCreateResource() {

        val courseRef = testCourse.ref!!
        val resourceToAdd = CourseResource (
                order = 0,
                name = "Integration test resource 1",
                description = "This is an integration test created resource",
                url = "https://orbitgames.nl",
                quiz = Quiz(
                        name = "Integration test resource 1 quiz",
                        description = "Confirm that you've completed integration test resource 1",
                        maxScore = 1,
                        elements = arrayOf(QuizElement(
                                order = 0,
                                text = "Have you completed integration test resource 1",
                                questionType =  QuestionType.sELECTONE,
                                options = arrayOf(
                                        QuizElementOption(order = 0, text = "Yes", points = 1),
                                        QuizElementOption(order = 1, text = "No", points = 0)
                                )
                        ))
                ),
                quizMinimalScore = 1
        )

        loginTestTeacher()
        val newResource = api.createCourseResource(courseRef, resourceToAdd)

        assertThat(newResource.ref, notNullValue())
        assertResourceEqual(newResource, resourceToAdd)

        val course = api.getCourse(courseRef)
        val newCourseResource = course.resources!!.find { r -> r.ref == newResource.ref }!!
        assertResourceEqual(newCourseResource, resourceToAdd)


    }

    @Test
    fun updateResourceTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val testSkillRef = testCourse.skills!!.first().ref!!

        val newResource = CourseResource (
                order = 0,
                name = "Integration test resource 2",
                description = "This is an integration test created resource for the update test",
                url = "https://orbitgames.nl",
                quiz = Quiz(
                        name = "Integration test resource 2 quiz",
                        description = "Confirm that you've completed integration test resource 2",
                        maxScore = 1,
                        elements = arrayOf(QuizElement(
                                order = 0,
                                text = "Have you completed integration test resource 2",
                                questionType =  QuestionType.sELECTONE,
                                options = arrayOf(
                                        QuizElementOption(order = 0, text = "Yes", points = 1),
                                        QuizElementOption(order = 1, text = "No", points = 0)
                                )
                        ))
                ),
                quizMinimalScore = 1,
                skills = arrayOf(testSkillRef)
        )

        val createdResource = api.createCourseResource(courseRef, newResource)

        val resourceUpdate1 = createdResource.copy(
                description = "This is an updated description",
                quiz = Quiz(
                        name = "Integration test resource 2 quiz",
                        description = "Confirm that you've completed integration test resource 2",
                        maxScore = 1,
                        elements = arrayOf(QuizElement(
                                order = 0,
                                text = "Have you completed integration test resource 2",
                                questionType =  QuestionType.sELECTONE,
                                options = arrayOf(
                                        QuizElementOption(order = 0, text = "Yes", points = 1),
                                        QuizElementOption(order = 1, text = "No", points = 0)
                                )),
                                QuizElement(
                                        order = 10,
                                        text = "What is the airspeed velocity of an unladen swallow?",
                                        questionType =  QuestionType.sELECTONE,
                                        options = arrayOf(
                                                QuizElementOption(order = 0, text = "I don't know", points = 1),
                                                QuizElementOption(order = 1, text = "African or European swallow?", points = 0)
                                        )
                                ))
                ),
                quizMinimalScore = 2,
                skills = kotlin.emptyArray()
        )

        val updatedResource1 = api.updateCourseResource(courseRef, createdResource.ref!!, resourceUpdate1)
        assertResourceEqual(updatedResource1, resourceUpdate1)

        val retrievedUpdatedResource1 = api.getCourseResource(courseRef, createdResource.ref!!)
        assertResourceEqual(retrievedUpdatedResource1, resourceUpdate1)

        val resourceUpdate2 = resourceUpdate1.copy(
                quiz = null,
                quizMinimalScore = 0,
                skills = arrayOf(testSkillRef)
        )

        val updatedResource2 = api.updateCourseResource(courseRef, createdResource.ref!!, resourceUpdate2)
        assertResourceEqual(updatedResource2, resourceUpdate2)

        val retrievedUpdatedSection2 = api.getCourseResource(courseRef, createdResource.ref!!)
        assertResourceEqual(retrievedUpdatedSection2, resourceUpdate2)

    }

    @Test
    fun deleteResourceWithoutQuizTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val newResource = CourseResource (
                order = 0,
                name = "Integration test resource 3",
                description = "This is an integration test created resource for the delete without quiz test",
                url = "https://orbitgames.nl"
        )

        val createdResource = api.createCourseResource(courseRef, newResource)

        val courseBeforeDelete = api.getCourse(courseRef)
        assertThat(courseBeforeDelete.resources!!.find { r -> r.ref == createdResource.ref }, notNullValue())

        val deletedResource = api.deleteCourseResource(courseRef, createdResource.ref!!)

        assertResourceEqual(deletedResource, createdResource)

        val courseAfterDelete = api.getCourse(courseRef)
        assertTrue(courseAfterDelete.resources!!.none { r -> r.ref == createdResource.ref })
    }

    @Test
    fun deleteResourceWithQuizTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val testSkillRef = testCourse.skills!!.first().ref!!

        val newResource = CourseResource (
                order = 0,
                name = "Integration test resource 4",
                description = "This is an integration test created resource for the delete with quiz test",
                url = "https://orbitgames.nl",
                quiz = Quiz(
                        name = "Integration test resource 4 quiz",
                        description = "Confirm that you've completed integration test resource 4",
                        maxScore = 1,
                        elements = arrayOf(QuizElement(
                                order = 0,
                                text = "Have you completed integration test resource 4",
                                questionType =  QuestionType.sELECTONE,
                                options = arrayOf(
                                        QuizElementOption(order = 0, text = "Yes", points = 1),
                                        QuizElementOption(order = 1, text = "No", points = 0)
                                )
                        ))
                ),
                quizMinimalScore = 1,
                skills = arrayOf(testSkillRef)
        )

        val createdResource = api.createCourseResource(courseRef, newResource)

        val courseBeforeDelete = api.getCourse(courseRef)
        assertThat(courseBeforeDelete.resources!!.find { r -> r.ref == createdResource.ref }, notNullValue())

        val deletedResource = api.deleteCourseResource(courseRef, createdResource.ref!!)

        assertResourceEqual(deletedResource, createdResource)

        val courseAfterDelete = api.getCourse(courseRef)
        assertTrue(courseAfterDelete.resources!!.none { r -> r.ref == createdResource.ref })
    }

}