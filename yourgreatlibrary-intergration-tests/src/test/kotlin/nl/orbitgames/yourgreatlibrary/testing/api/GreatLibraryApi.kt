package nl.orbitgames.yourgreatlibrary.testing.api

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.restassured.RestAssured
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.http.ContentType
import io.restassured.response.Response
import io.restassured.response.ValidatableResponse
import io.restassured.specification.RequestSpecification
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers.*
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.reflect.KClass


/**
 * A RestAssured based client implementation for the Your Great Library API. Each API call has serveral methods:
 * <li>
 *     <ul> {@code ApiCallRaw()} Will execute the API call without any extra business logic (so no response validation)
 *     <ul> {@code ApiCall()} will execute the API call, validate and parse the result as the correct model class
 *     <ul> {@code ApiLoggedInCall()} will execute the API call using the logged in user
 */
open class GreatLibraryApi(var token: String? = null) {

    /**
     * Maps the usernames to tokens, so we can reuse existing session after a user has logged in for the first time
     */
    private var tokenCache: MutableMap<String, String> = HashMap()

    companion object {
        const val LOGGED_IN_USER_REF = "logged-in"
    }

    init {
        // LOCAL
        RestAssured.port = 8080
        RestAssured.baseURI = "http://localhost"
        RestAssured.basePath = "/rest/v1"

        // DEV
//        RestAssured.baseURI = "https://greatlibrary.orbitgames.nl"
//        RestAssured.basePath = "/api/rest/v1"
    }

    fun userLoginRaw(loginRequest: UserLoginRequest): ValidatableResponse {
        return req()
                .body(loginRequest)
                .`when`()
                .post("/user/login")
                .resp()
    }

    /**
     * Login any type of user.
     * Note that in order to make the API use the logged in user "session", the {@link #login} call should be used.
     */
    fun userLogin(loginRequest: UserLoginRequest): UserLoginResponse {

        val resp = userLoginRaw(loginRequest)
                .valid()
                .`as`(UserLoginResponse::class)

        return resp
    }

    /**
     * Logs in a user. May return an already existing session and not actually
     * send a request to the API.
     */
    fun login(username: String, password: String) {
        if (!tokenCache.containsKey(username)) {
            val loginResponse = userLogin(UserLoginRequest(username, password))
            this.tokenCache[username] = loginResponse.sessionToken!!
        }
        this.token = tokenCache[username]
    }

    /**
     * Logs out the current user.
     * Note that this is not an API call, but will merely reset the default token to be used for authorization to null
     */
    fun logout() {
        this.token = null
    }

    fun userRegisterRaw(registrationRequest: UserRegistrationRequest): ValidatableResponse {
        return req()
                .body(registrationRequest)
                .`when`()
                .post("/user/register")
                .resp()
    }

    fun userRegister(registrationRequest: UserRegistrationRequest) {

        userRegisterRaw(registrationRequest)
                .valid()
    }

    fun getLatestUserAgreement(): UserAgreement {

        return req()
                .get("/user-agreement/latest")
                .resp()
                .valid()
                .`as`(UserAgreement::class)
    }

    fun createUserAgreement(newAgreement: UserAgreement, token: String? = this.token): UserAgreement {

        return req()
                .auth(token)
                .body(newAgreement)
                .post("/user-agreement")
                .resp()
                .valid()
                .`as`(UserAgreement::class)
    }

    fun hasUserAgreement(): Boolean {
        val response = req()
                .get("/user-agreement/latest")
                .resp()
        return response.extract().statusCode() == 200
    }

    fun getUserRaw(ref: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("ref", ref)
                .get("/user/{ref}")
                .resp()
    }

    fun getUser(ref: String, token: String? = this.token): User {
        return getUserRaw(ref, token)
                .valid()
                .`as`(User::class)
    }

    fun getLoggedInUser(token: String? = this.token): User {
        return getUser(LOGGED_IN_USER_REF, token)
    }

    fun getLoggedInUserRaw(token: String? = this.token): ValidatableResponse {
        return getUserRaw(LOGGED_IN_USER_REF, token)
    }

    fun updateUserRaw(ref: String, userWithUpdates: User, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(userWithUpdates)
                .pathParam("ref", ref)
                .put("/user/{ref}")
                .resp()
    }

    fun updateUser(ref: String, userWithUpdates: User, token: String? = this.token): User {

        return updateUserRaw(ref, userWithUpdates, token)
                .valid()
                .`as`(User::class)
    }

    fun updateLoggedInUserRaw(userWithUpdates: User, token: String? = this.token): ValidatableResponse {
        return updateUserRaw(LOGGED_IN_USER_REF, userWithUpdates, token)
    }

    fun updateLoggedInUser(userWithUpdates: User, token: String? = this.token): User {
        return updateUser(LOGGED_IN_USER_REF, userWithUpdates, token)
    }

    fun updateUserPropertyRaw(userRef: String, propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(propertyWithUpdates)
                .pathParam("userRef", userRef)
                .pathParam("propertyKey", propertyKey)
                .put("/user/{userRef}/property/{propertyKey}")
                .resp()
    }

    fun updateUserProperty(userRef: String, propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): Property {

        return updateUserPropertyRaw(userRef, propertyKey, propertyWithUpdates, token)
                .valid()
                .`as`(Property::class)
    }

    fun updateLoggedInUserPropertyRaw(propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): ValidatableResponse {
        return updateUserPropertyRaw(LOGGED_IN_USER_REF, propertyKey, propertyWithUpdates, token)
    }

    fun updateLoggedInUserProperty(propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): Property {
        return updateUserProperty(LOGGED_IN_USER_REF, propertyKey, propertyWithUpdates, token);
    }

    fun getUserPropertyRaw(userRef: String, propertyKey: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("propertyKey", propertyKey)
                .get("/user/{userRef}/property/{propertyKey}")
                .resp()
    }

    fun getUserProperty(userRef: String, propertyKey: String, token: String? = this.token): Property {

        return getUserPropertyRaw(userRef, propertyKey, token)
                .valid()
                .`as`(Property::class)
    }

    fun getLoggedInUserPropertyRaw(propertyKey: String, token: String? = this.token): ValidatableResponse {
        return getUserPropertyRaw(LOGGED_IN_USER_REF, propertyKey, token)
    }

    fun getLoggedInUserProperty(propertyKey: String, token: String? = this.token): Property {
        return getUserProperty(LOGGED_IN_USER_REF, propertyKey, token)
    }

    fun joinCourseRaw(userRef: String, courseCode: String, token: String? = this.token): ValidatableResponse {
        return req()
                .body(UserCourseJoinRequest(courseCode))
                .auth(token)
                .pathParam("userRef", userRef)
                .`when`()
                .post("/user/{userRef}/course/join")
                .resp()
    }

    fun joinCourse(userRef: String, courseCode: String, token: String? = this.token) {
        joinCourseRaw(userRef, courseCode, token)
                .valid()
    }

    fun loggedInUserJoinCourseRaw(testCourseCode: String, token: String? = this.token): ValidatableResponse {
        return joinCourseRaw(LOGGED_IN_USER_REF, testCourseCode, token)
    }

    fun loggedInUserJoinCourse(courseCode: String, token: String? = this.token) {
        joinCourse(LOGGED_IN_USER_REF, courseCode, token)
    }

    fun getCourseRaw(courseRef: String, includeDisabled: Boolean = false, token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .queryParam("includeDisabled", includeDisabled)
                .`when`()
                .get("course/{courseRef}")
                .resp()
    }

    fun getCourse(courseRef: String, includeDisabled: Boolean = false, token: String? = this.token) : Course {
        return getCourseRaw(courseRef, includeDisabled, token)
                .valid()
                .`as`(Course::class)
    }

    fun updateCourseRaw(courseRef: String, courseUpdateRequest: CourseUpdateRequest, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(courseUpdateRequest)
                .put("course/{courseRef}")
                .resp()
    }

    fun updateCourse(courseRef: String, courseUpdateRequest: CourseUpdateRequest, token: String? = this.token): Course {

        return updateCourseRaw(courseRef, courseUpdateRequest, token)
                .valid()
                .`as`(Course::class)
    }

    fun getCourseDeadlinesICalRaw(courseRef: String, token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .`when`()
                .get("course/{courseRef}/deadlines")
                .resp()
    }

    fun getCourseDeadlinesICal(courseRef: String, token: String? = this.token): String {
        return getCourseDeadlinesICalRaw(courseRef, token)
                .statusCode(200)
                .contentType("text/calendar")
                .extract()
                .body()
                .asString()
    }

    fun updateDeadlinesRaw(courseRef: String, courseDeadlines: CourseDeadlines, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(courseDeadlines)
                .put("course/{courseRef}/deadlines")
                .resp()
    }

    fun updateDeadlines(courseRef: String, courseDeadlines: CourseDeadlines, token: String? = this.token): CourseDeadlines {

        return updateDeadlinesRaw(courseRef, courseDeadlines, token)
                .valid()
                .`as`(CourseDeadlines::class)
    }

    fun getCourseTemplatesRaw(token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .`when`()
                .get("course/template")
                .resp()
    }

    fun getCourseTemplates(token: String? = this.token): CourseTemplates {
        return getCourseTemplatesRaw(token)
                .valid()
                .`as`(CourseTemplates::class)
    }

    fun postCreateCourseFromTemplateRaw(createCourseFromTemplateRequest: CreateCourseFromTemplateRequest, token: String?  = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(createCourseFromTemplateRequest)
                .`when`()
                .post("course/from-template")
                .resp()
    }

    fun postCreateCourseFromTemplate(createCourseFromTemplateRequest: CreateCourseFromTemplateRequest, token: String?  = this.token): Course {

        return postCreateCourseFromTemplateRaw(createCourseFromTemplateRequest, token)
                .valid()
                .`as`(Course::class)
    }

    fun postCourseProgressExportRaw(courseRef: String, exportSpecification: CourseProgressExportSpecification, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(exportSpecification)
                .`when`()
                .post("course/{courseRef}/progress-export")
                .resp()
    }

    fun postCourseProgressExport(courseRef: String, exportSpecification: CourseProgressExportSpecification, token: String? = this.token): ByteArray {

        return postCourseProgressExportRaw(courseRef, exportSpecification, token)
                .statusCode(200)
                .extract()
                .body()
                .asByteArray();
    }

    fun getCourseChapterRaw(courseRef: String, chapterRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .`when`()
                .get("course/{courseRef}/chapter/{chapterRef}")
                .resp()
    }

    fun getReviewStepRaw(courseRef: String, chapterRef: String, reviewStepRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .get("course/{courseRef}/chapter/{chapterRef}/review-step/{reviewStepRef}")
                .resp()
    }

    fun getReviewStep(courseRef: String, chapterRef: String, reviewStepRef: String, token: String? = this.token): ReviewStep {

        return getReviewStepRaw(courseRef, chapterRef, reviewStepRef, token)
                .valid()
                .`as`(ReviewStep::class)
    }

    fun updateReviewStepRaw(courseRef: String, chapterRef: String, reviewStepRef: String, reviewStepWithUpdates: ReviewStep, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .body(reviewStepWithUpdates)
                .`when`()
                .put("course/{courseRef}/chapter/{chapterRef}/review-step/{reviewStepRef}")
                .resp()
    }

    fun updateReviewStep(courseRef: String, chapterRef: String, reviewStepRef: String, reviewStepWithUpdates: ReviewStep, token: String? = this.token): ReviewStep {

        return updateReviewStepRaw(courseRef, chapterRef, reviewStepRef, reviewStepWithUpdates, token)
                .valid()
                .`as`(ReviewStep::class)
    }

    fun getCourseChapter(courseRef: String, chapterRef: String, token: String? = this.token): Chapter {

        return getCourseChapterRaw(courseRef, chapterRef, token)
                .valid()
                .`as`(Chapter::class)
    }

    fun updateCourseChapterRaw(courseRef: String, chapterRef: String, chapterWithUpdates: Chapter, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .body(chapterWithUpdates)
                .put("course/{courseRef}/chapter/{chapterRef}")
                .resp()
    }

    fun updateCourseChapter(courseRef: String, chapterRef: String, chapterWithUpdates: Chapter, token: String? = this.token): Chapter {

        return this.updateCourseChapterRaw(courseRef, chapterRef, chapterWithUpdates, token)
                .valid()
                .`as`(Chapter::class)
    }

    fun getCourseChapterReviewerRelationsRaw(courseRef: String, chapterRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .`when`()
                .get("course/{courseRef}/chapter/{chapterRef}/reviewer-relations")
                .resp()
    }

    fun getCourseChapterReviewerRelations(courseRef: String, chapterRef: String, token: String? = this.token): Array<ReviewerRelation> {

        return getCourseChapterReviewerRelationsRaw(courseRef, chapterRef, token)
                .valid()
                .`as`(Array<ReviewerRelation>::class)

    }

    fun getUserCourseProgressRaw(userRef: String, courseRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .`when`()
                .get("/user/{userRef}/course-progress/{courseRef}")
                .resp()
    }

    fun getUserCourseProgress(userRef: String, courseRef: String, token: String? = this.token): CourseProgress {

        return getUserCourseProgressRaw(userRef, courseRef, token)
                .valid()
                .`as`(CourseProgress::class)
    }

    fun getLoggedInUserCourseProgressRaw(courseRef: String, token: String? = this.token): ValidatableResponse {
        return getUserCourseProgressRaw(LOGGED_IN_USER_REF, courseRef, token)
    }

    fun getLoggedInUserCourseProgress(courseRef: String, token: String? = this.token): CourseProgress {

        return getUserCourseProgress(LOGGED_IN_USER_REF, courseRef, token)
    }

    fun getUserCourseChapterProgressRaw(userRef: String, courseRef: String, chapterRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .`when`()
                .get("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}")
                .resp()
    }

    fun getUserCourseChapterProgress(userRef: String, courseRef: String, chapterRef: String, token: String? = this.token): ChapterProgress {
        return getUserCourseChapterProgressRaw(userRef, courseRef, chapterRef, token)
                .valid()
                .`as`(ChapterProgress::class)
    }

    fun getLoggedInUserCourseChapterProgressRaw(courseRef: String, chapterRef: String, token: String? = this.token): ValidatableResponse {
        return getUserCourseChapterProgressRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, token)
    }

    fun getLoggedInUserCourseChapterProgress(courseRef: String, chapterRef: String, token: String? = this.token): ChapterProgress {
        return getLoggedInUserCourseChapterProgressRaw(courseRef, chapterRef, token)
                .valid()
                .`as`(ChapterProgress::class)
    }

    fun postUserReviewStepFileUploadRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                  fileName: String, fileData: ByteArray, token: String? = this.token): ValidatableResponse {

        return req()
                .contentType("multipart/form-data")
                .auth(token)
                .multiPart("file", fileName, fileData)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .post("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/fileUpload")
                .resp()
    }

    fun postUserReviewStepFileUpload(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                               fileName: String, fileData: ByteArray, token: String? = this.token): ReviewStepResult {

        return postUserReviewStepFileUploadRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, fileName, fileData, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postLoggedInUserReviewStepFileUploadRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                          fileName: String, fileData: ByteArray, token: String? = this.token): ValidatableResponse {

        return postUserReviewStepFileUploadRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, fileName, fileData, token)
    }

    fun postLoggedInUserReviewStepFileUpload(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                               fileName: String, fileData: ByteArray, token: String? = this.token): ReviewStepResult {

        return postLoggedInUserReviewStepFileUploadRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, fileName, fileData, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postUserReviewStepQuizRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                  quizResult: QuizResult, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(quizResult)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .post("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/quiz")
                .resp()
    }

    fun postUserReviewStepQuiz(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                               quizResult: QuizResult, token: String? = this.token): ReviewStepResult {

        return postUserReviewStepQuizRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, quizResult, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postLoggedInUserReviewStepQuizRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                          quizResult: QuizResult, token: String? = this.token): ValidatableResponse {

        return postUserReviewStepQuizRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, quizResult, token)
    }

    fun postLoggedInUserReviewStepQuiz(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                       quizResult: QuizResult, token: String? = this.token): ReviewStepResult {

        return postLoggedInUserReviewStepQuizRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, quizResult, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postUserReviewStepArgumentRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                  argument: Argument, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(argument)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .post("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument")
                .resp()
    }

    fun postUserReviewStepArgument(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                   argument: Argument, token: String? = this.token): ReviewStepResult {

        return postUserReviewStepArgumentRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argument, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postLoggedInUserReviewStepArgumentRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                              argument: Argument, token: String? = this.token): ValidatableResponse {

        return postUserReviewStepArgumentRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argument, token)
    }

    fun postLoggedInUserReviewStepArgument(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                           argument: Argument, token: String? = this.token): ReviewStepResult {

        return postLoggedInUserReviewStepArgumentRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argument, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun updateUserReviewStepArgumentRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                      argument: Argument, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .body(argument)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .pathParam("argumentRef", argumentRef)
                .`when`()
                .put("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument/{argumentRef}")
                .resp()
    }

    fun updateUserReviewStepArgument(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                   argument: Argument, token: String? = this.token): ReviewStepResult {

        return updateUserReviewStepArgumentRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, argument, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun updateLoggedInUserReviewStepArgumentRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                              argument: Argument, token: String? = this.token): ValidatableResponse {

        return updateUserReviewStepArgumentRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, argument, token)
    }

    fun updateLoggedInUserReviewStepArgument(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                           argument: Argument, token: String? = this.token): ReviewStepResult {

        return updateLoggedInUserReviewStepArgumentRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, argument, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun deleteUserReviewStepArgumentRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                        token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .pathParam("argumentRef", argumentRef)
                .`when`()
                .delete("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/argument/{argumentRef}")
                .resp()
    }

    fun deleteUserReviewStepArgument(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                     token: String? = this.token): ReviewStepResult {

        return deleteUserReviewStepArgumentRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun deleteLoggedInUserReviewStepArgumentRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                                token: String? = this.token): ValidatableResponse {

        return deleteUserReviewStepArgumentRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, token)
    }

    fun deleteLoggedInUserReviewStepArgument(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String, argumentRef: String,
                                             token: String? = this.token): ReviewStepResult {

        return deleteLoggedInUserReviewStepArgumentRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, argumentRef, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postUserReviewStepCompleteRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                      token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .post("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/complete")
                .resp()
    }

    fun postUserReviewStepComplete(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                   token: String? = this.token): ReviewStepResult {

        return postUserReviewStepCompleteRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun postLoggedInUserReviewStepCompleteRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                              token: String? = this.token): ValidatableResponse {
        return postUserReviewStepCompleteRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
    }

    fun postLoggedInUserReviewStepComplete(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                           token: String? = this.token): ReviewStepResult {
        return postLoggedInUserReviewStepCompleteRaw(courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun deleteUserReviewStepCompleteRaw(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                      token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("chapterRef", chapterRef)
                .pathParam("reviewerRelationRef", reviewerRelationRef)
                .pathParam("reviewStepRef", reviewStepRef)
                .`when`()
                .delete("/user/{userRef}/course-progress/{courseRef}/chapter/{chapterRef}/relation/{reviewerRelationRef}/review-step/{reviewStepRef}/complete")
                .resp()
    }

    fun deleteUserReviewStepComplete(userRef: String, courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                        token: String? = this.token): ReviewStepResult {

        return deleteUserReviewStepCompleteRaw(userRef, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
                .valid()
                .`as`(ReviewStepResult::class)
    }

    fun deleteLoggedInUserReviewStepCompleteRaw(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                                token: String? = this.token): ValidatableResponse {
        return deleteUserReviewStepCompleteRaw(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
    }

    fun deleteLoggedInUserReviewStepComplete(courseRef: String, chapterRef: String, reviewerRelationRef: String, reviewStepRef: String,
                                             token: String? = this.token): ReviewStepResult {
        return deleteUserReviewStepComplete(LOGGED_IN_USER_REF, courseRef, chapterRef, reviewerRelationRef, reviewStepRef, token)
    }

    fun postUserResourceQuizResultRaw(userRef: String, courseRef: String, resourceRef: String, quizResult: QuizResult, token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("userRef", userRef)
                .pathParam("courseRef", courseRef)
                .pathParam("resourceRef", resourceRef)
                .body(quizResult)
                .post("/user/{userRef}/course-progress/{courseRef}/resource/{resourceRef}/quiz")
                .resp()
    }

    fun postUserResourceQuizResult(userRef: String, courseRef: String, resourceRef: String, quizResult: QuizResult, token: String? = this.token): CourseResourceResult {
        return postUserResourceQuizResultRaw(userRef, courseRef, resourceRef, quizResult, token)
                .valid()
                .`as`(CourseResourceResult::class)
    }

    fun postLoggedInUserResourceQuizResultRaw(courseRef: String, resourceRef: String, quizResult: QuizResult, token: String? = this.token): ValidatableResponse {
        return postUserResourceQuizResultRaw(LOGGED_IN_USER_REF, courseRef, resourceRef, quizResult, token)
    }

    fun postLoggedInUserResourceQuizResult(courseRef: String, resourceRef: String, quizResult: QuizResult, token: String? = this.token): CourseResourceResult {
        return postUserResourceQuizResult(LOGGED_IN_USER_REF, courseRef, resourceRef, quizResult, token)
    }

    fun getCourseUserRaw(courseRef: String, userRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("userRef", userRef)
                .get("/course/{courseRef}/user/{userRef}")
                .resp()
    }

    fun getCourseUser(courseRef: String, userRef: String, token: String? = this.token): CourseProgressSummary {
        return getCourseUserRaw(courseRef, userRef, token)
                .valid()
                .`as`(CourseProgressSummary::class)
    }

    fun updateCourseUserRaw(courseRef: String, userRef: String, courseProgressSummaryWithUpdates: CourseProgressSummary, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("userRef", userRef)
                .body(courseProgressSummaryWithUpdates)
                .put("/course/{courseRef}/user/{userRef}")
                .resp()
    }

    fun updateCourseUser(courseRef: String, userRef: String, courseProgressSummaryWithUpdates: CourseProgressSummary, token: String? = this.token): CourseProgressSummary {
        return updateCourseUserRaw(courseRef, userRef, courseProgressSummaryWithUpdates, token)
                .valid()
                .`as`(CourseProgressSummary::class)
    }

    fun getCourseUserPropertyRaw(courseRef: String, userRef: String, propertyKey: String, token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("userRef", userRef)
                .pathParam("propertyKey", propertyKey)
                .get("/course/{courseRef}/user/{userRef}/property/{propertyKey}")
                .resp()
    }

    fun getCourseUserProperty(courseRef: String, userRef: String, propertyKey: String, token: String? = this.token): Property {
        return getCourseUserPropertyRaw(courseRef, userRef, propertyKey, token)
                .valid()
                .`as`(Property::class)
    }


    fun updateCourseUserPropertyRaw(courseRef: String, userRef: String, propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("userRef", userRef)
                .pathParam("propertyKey", propertyKey)
                .body(propertyWithUpdates)
                .put("/course/{courseRef}/user/{userRef}/property/{propertyKey}")
                .resp()
    }

    fun updateCourseUserProperty(courseRef: String, userRef: String, propertyKey: String, propertyWithUpdates: Property, token: String? = this.token): Property {
        return updateCourseUserPropertyRaw(courseRef, userRef, propertyKey, propertyWithUpdates, token)
                .valid()
                .`as`(Property::class)
    }

    fun getCourseSectionsRaw(courseRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .get("/course/{courseRef}/section")
                .resp()
    }

    fun getCourseSections(courseRef: String, token: String? = this.token): Sections {
        return getCourseSectionsRaw(courseRef, token)
                .valid()
                .`as`(Sections::class)
    }

    fun createCourseSectionRaw(courseRef: String, section: Section, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(section)
                .post("/course/{courseRef}/section")
                .resp()
    }

    fun createCourseSection(courseRef: String, section: Section, token: String? = this.token): Section {

        return createCourseSectionRaw(courseRef, section, token)
                .valid()
                .`as`(Section::class)
    }

    fun getCourseSectionRaw(courseRef: String, sectionRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("sectionRef", sectionRef)
                .get("/course/{courseRef}/section/{sectionRef}")
                .resp()
    }

    fun getCourseSection(courseRef: String, sectionRef: String, token: String? = this.token): Section {

        return getCourseSectionRaw(courseRef, sectionRef, token)
                .valid()
                .`as`(Section::class)
    }

    fun updateCourseSectionRaw(courseRef: String, sectionRef: String, sectionWithUpdates: Section, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("sectionRef", sectionRef)
                .body(sectionWithUpdates)
                .put("/course/{courseRef}/section/{sectionRef}")
                .resp()
    }

    fun updateCourseSection(courseRef: String, sectionRef: String, sectionWithUpdates: Section, token: String? = this.token): Section {

        return updateCourseSectionRaw(courseRef, sectionRef, sectionWithUpdates, token)
                .valid()
                .`as`(Section::class)
    }

    fun deleteCourseSectionRaw(courseRef: String, sectionRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("sectionRef", sectionRef)
                .delete("/course/{courseRef}/section/{sectionRef}")
                .resp()
    }

    fun deleteCourseSection(courseRef: String, sectionRef: String, token: String? = this.token): Section {

        return deleteCourseSectionRaw(courseRef, sectionRef, token)
                .valid()
                .`as`(Section::class)
    }

    fun getCourseResourcesRaw(courseRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .get("/course/{courseRef}/resource")
                .resp()
    }

    fun getCourseResources(courseRef: String, token: String? = this.token): CourseResources {

        return getCourseResourcesRaw(courseRef, token)
                .valid()
                .`as`(CourseResources::class)
    }

    fun createCourseResourceRaw(courseRef: String, resource: CourseResource, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(resource)
                .post("/course/{courseRef}/resource")
                .resp()
    }

    fun createCourseResource(courseRef: String, resource: CourseResource, token: String? = this.token): CourseResource {

        return createCourseResourceRaw(courseRef, resource, token)
                .valid()
                .`as`(CourseResource::class)
    }


    fun updateCourseResourceRaw(courseRef: String, resourceRef: String, resourceWithUpdates: CourseResource, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("resourceRef", resourceRef)
                .body(resourceWithUpdates)
                .put("/course/{courseRef}/resource/{resourceRef}")
                .resp()
    }

    fun updateCourseResource(courseRef: String, resourceRef: String, resourceWithUpdates: CourseResource, token: String? = this.token): CourseResource {

        return updateCourseResourceRaw(courseRef, resourceRef, resourceWithUpdates, token)
                .valid()
                .`as`(CourseResource::class)
    }

    fun getCourseResourceRaw(courseRef: String, resourceRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("resourceRef", resourceRef)
                .get("/course/{courseRef}/resource/{resourceRef}")
                .resp()
    }

    fun getCourseResource(courseRef: String, resourceRef: String, token: String? = this.token): CourseResource {

        return getCourseResourceRaw(courseRef, resourceRef, token)
                .valid()
                .`as`(CourseResource::class)
    }

    fun deleteCourseResourceRaw(courseRef: String, resourceRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("resourceRef", resourceRef)
                .delete("/course/{courseRef}/resource/{resourceRef}")
                .resp()
    }

    fun deleteCourseResource(courseRef: String, resourceRef: String, token: String? = this.token): CourseResource {

        return deleteCourseResourceRaw(courseRef, resourceRef, token)
                .valid()
                .`as`(CourseResource::class)
    }

    fun getSkillsRaw(courseRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .get("/course/{courseRef}/skill")
                .resp()
    }

    fun getSkills(courseRef: String, token: String? = this.token): Skills {

        return getSkillsRaw(courseRef, token)
                .valid()
                .`as`(Skills::class)
    }

    fun createSkillRaw(courseRef: String, skill: Skill, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(skill)
                .post("/course/{courseRef}/skill")
                .resp()
    }

    fun createSkill(courseRef: String, skill: Skill, token: String? = this.token): Skill {

        return createSkillRaw(courseRef, skill, token)
                .valid()
                .`as`(Skill::class)
    }

    fun getSkillRaw(courseRef: String, skillRef: String, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("skillRef", skillRef)
                .get("/course/{courseRef}/skill/{skillRef}")
                .resp()
    }

    fun getSkill(courseRef: String, skillRef: String, token: String? = this.token): Skill {

        return getSkillRaw(courseRef, skillRef, token)
                .valid()
                .`as`(Skill::class)
    }

    fun updateSkillRaw(courseRef: String, skillRef: String, skill: Skill, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("skillRef", skillRef)
                .body(skill)
                .put("/course/{courseRef}/skill/{skillRef}")
                .resp()
    }

    fun updateSkill(courseRef: String, skillRef: String, skill: Skill, token: String? = this.token): Skill {

        return updateSkillRaw(courseRef, skillRef, skill, token)
                .valid()
                .`as`(Skill::class)
    }

    fun getConversationsRaw(courseRef: String,
                            updated: OffsetDateTime? = null, messageLimit: Long? = null, conversations: String? = null,
                            token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .optionalQueryParam("updated", updated?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                .optionalQueryParam("messageLimit", messageLimit)
                .optionalQueryParam("conversations", conversations)
                .get("/course/{courseRef}/conversation")
                .resp()
    }


    fun getConversations(courseRef: String,
                         updated: OffsetDateTime? = null, messageLimit: Long? = null, conversations: String? = null,
                         token: String? = this.token): Conversations {
        return getConversationsRaw(courseRef, updated, messageLimit, conversations, token)
                .valid()
                .`as`(Conversations::class)
    }

    fun createConversationRaw(courseRef: String, conversation: Conversation,
                              token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .body(conversation)
                .post("/course/{courseRef}/conversation")
                .resp()
    }

    fun createConversation(courseRef: String, conversation: Conversation, token: String? = this.token): Conversation {
        return createConversationRaw(courseRef, conversation, token)
                .valid()
                .`as`(Conversation::class)
    }

    fun getConversationRaw(courseRef: String, conversationRef: String,
                           updated: OffsetDateTime? = null, messageSkip: Long? = null, messageLimit: Long? = null,
                           token: String? = this.token): ValidatableResponse {
        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("conversationRef", conversationRef)
                .optionalQueryParam("updated", updated?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                .optionalQueryParam("messageSkip", messageSkip)
                .optionalQueryParam("messageLimit", messageLimit)
                .get("/course/{courseRef}/conversation/{conversationRef}")
                .resp()
    }

    fun getConversation(courseRef: String, conversationRef: String,
                           updated: OffsetDateTime? = null, messageSkip: Long? = null, messageLimit: Long? = null,
                           token: String? = this.token): Conversation {
        return getConversationRaw(courseRef, conversationRef, updated, messageSkip, messageLimit, token)
                .valid()
                .`as`(Conversation::class)
    }

    fun updateConversationEmailNotificationTypeRaw(
            courseRef: String, conversationRef: String,
            request: UpdateConversationEmailNotificationTypeRequest,
            token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("conversationRef", conversationRef)
                .body(request)
                .put("/course/{courseRef}/conversation/{conversationRef}/email-notification-type")
                .resp()
    }

    fun updateConversationEmailNotificationType(courseRef: String, conversationRef: String,
                                                request: UpdateConversationEmailNotificationTypeRequest,
                                                token: String? = this.token) {

        updateConversationEmailNotificationTypeRaw(courseRef, conversationRef, request, token)
                .valid()
    }

    fun postConversationMessageRaw(courseRef: String, conversationRef: String, message: PostConversationMessageRequest, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("conversationRef", conversationRef)
                .body(message)
                .post("/course/{courseRef}/conversation/{conversationRef}/message")
                .resp()
    }

    fun postConversationMessage(courseRef: String, conversationRef: String, message: PostConversationMessageRequest, token: String? = this.token): ConversationMessage {

        return postConversationMessageRaw(courseRef, conversationRef, message, token)
                .valid()
                .`as`(ConversationMessage::class)
    }

    fun postConversationReadRaw(courseRef: String, conversationRef: String, request: PostConversationReadRequest, token: String? = this.token): ValidatableResponse {

        return req()
                .auth(token)
                .pathParam("courseRef", courseRef)
                .pathParam("conversationRef", conversationRef)
                .body(request)
                .post("/course/{courseRef}/conversation/{conversationRef}/read")
                .resp()
    }

    fun postConversationRead(courseRef: String, conversationRef: String, request: PostConversationReadRequest, token: String? = this.token): Conversation {

        return postConversationReadRaw(courseRef, conversationRef, request, token)
                .valid()
                .`as`(Conversation::class)
    }


    fun downloadFileRaw(downloadUrl: String?, token: String? = this.token): ValidatableResponse {
        return req()
                .urlEncodingEnabled(false)
                .auth(token)
                .get(downloadUrl)
                // Explicitly don't call log here
                .then()
    }

    fun downloadFile(downloadUrl: String?, token: String? = this.token): ByteArray {

        return downloadFileRaw(downloadUrl, token)
                .statusCode(200)
                .extract()
                .body()
                .asByteArray()
    }

    fun uploadFileRaw(fileName: String, fileData: ByteArray, token: String? = this.token): ValidatableResponse {

        return req()
                .log().params()
                .contentType("multipart/form-data")
                .auth(token)
                .multiPart("file", fileName, fileData)
                .`when`()
                .post("/file")
                .resp()
    }

    fun uploadFile(fileName: String, fileData: ByteArray, token: String? = this.token): FileUploadMetadata {
        return uploadFileRaw(fileName, fileData, token)
                .valid()
                .`as`(FileUploadMetadata::class)
    }


    fun postClientBugReportRaw(bugReport: BugReport): ValidatableResponse {

        return req()
                .log().params()
                .body(bugReport)
                .`when`()
                .post("client/bug-report")
                .resp()
    }

    fun postClientBugReport(bugReport: BugReport) {

        postClientBugReportRaw(bugReport)
                .valid()

    }

    fun getServerInfoRaw(): ValidatableResponse {

        return req()
                .`when`()
                .get("server/info")
                .resp()
    }

    fun getServerInfo(): ServerInfo {

        return getServerInfoRaw()
                .valid()
                .`as`(ServerInfo::class)
    }

    fun applicationError(resp: ValidatableResponse): ErrorResponse {
        return resp.statusCode(both(greaterThanOrEqualTo(400)).and(lessThan(500)))
                .`as`(ErrorResponse::class)
    }

    fun error(resp: ValidatableResponse): ErrorResponse {
        return resp.statusCode(greaterThanOrEqualTo(500))
                .`as`(ErrorResponse::class)
    }


    protected fun RequestSpecification.auth(token: String?): RequestSpecification {
        return if (token == null) this else this.header("X-Token", token)
    }

    protected fun RequestSpecification.optionalQueryParam(name: String, value: Any?): RequestSpecification {
        return if (value != null) return this.queryParam(name, value) else this

    }

    protected fun req(): RequestSpecification {
        return RestAssured
                .given()
                .contentType(ContentType.JSON)
                .log().all()
    }

    fun Response.resp(): ValidatableResponse {
        return this
                .then()
                .log().all()
    }

    fun ValidatableResponse.valid(): ValidatableResponse {
        return if (this.extract().statusCode() == 204) {
            // An empty 204 response is always valid
            this
        } else {
            this
                    .statusCode(200)
                    .contentType(ContentType.JSON)
        }
    }


    fun <T : Any> ValidatableResponse.`as`(c: KClass<T>): T {
        return this.extract()
                .body()
                .`as`(c.java)
    }


    init {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(ObjectMapperConfig().jackson2ObjectMapperFactory { _, _ ->
            val objectMapper = ObjectMapper()
            // Due to the generated classes not having correct capitalization when for properties or enums that start
            // with a capital letter, so we'll just be case insensitive...
            objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
            objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
            objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING)
            objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING)

            val yourGreatLibraryEnumModule = SimpleModule()
            yourGreatLibraryEnumModule.addSerializer(Enum::class.java, YourGreatLibraryEnumSerializer())

            // TODO: Maybe we can determine which serializer to use based in a ContextualSerializer
            yourGreatLibraryEnumModule.addDeserializer(ClientVersionCheckResponseStatus::class.java, YourGreatLibraryEnumDeserializer(ClientVersionCheckResponseStatus::class.java))
            yourGreatLibraryEnumModule.addDeserializer(StepType::class.java, YourGreatLibraryEnumDeserializer(StepType::class.java))
            yourGreatLibraryEnumModule.addDeserializer(QuestionType::class.java, YourGreatLibraryEnumDeserializer(QuestionType::class.java))
            yourGreatLibraryEnumModule.addDeserializer(DiscussionType::class.java, YourGreatLibraryEnumDeserializer(DiscussionType::class.java))
            yourGreatLibraryEnumModule.addDeserializer(Platform::class.java, YourGreatLibraryEnumDeserializer(Platform::class.java))
            yourGreatLibraryEnumModule.addDeserializer(ReviewerRelationGenerationType::class.java, YourGreatLibraryEnumDeserializer(ReviewerRelationGenerationType::class.java))
            yourGreatLibraryEnumModule.addDeserializer(PeerType::class.java, YourGreatLibraryEnumDeserializer(PeerType::class.java))
            yourGreatLibraryEnumModule.addDeserializer(ConversationType::class.java, YourGreatLibraryEnumDeserializer(ConversationType::class.java))

            objectMapper.registerModule(yourGreatLibraryEnumModule)

            // Register Java8 Date-Time modules
            objectMapper.registerModule(Jdk8Module())
            objectMapper.registerModule(JavaTimeModule())

            // Kotlin module required for some object (de)serialization
            objectMapper.registerModule(KotlinModule())
            objectMapper
        })
    }

    class YourGreatLibraryEnumSerializer: StdSerializer<Enum<*>>(Enum::class.java) {

        override fun serialize(value: Enum<*>, gen: JsonGenerator, provider: SerializerProvider) {

            val enumClass = value::class.java
            val propertyValue = Arrays.stream(enumClass.methods)
                    .filter { p -> p.name == "getValue" && p.returnType == String::class.java }
                    .findAny()
                    .map { p -> p.invoke(value) as String }
                    .orElse(value.toString())

            gen.writeString(propertyValue)
        }
    }

    @Suppress("UNCHECKED_CAST")
    class YourGreatLibraryEnumDeserializer<E: Enum<E>>(private val enumClass: Class<E>): StdDeserializer<E>(enumClass::class.java) {

        override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): E {

            val stringValue = jp.valueAsString

            val valueGetter = enumClass.methods.find { m -> m.name == "getValue" }
            val enumValuesGetter = enumClass.methods.find { m -> m.name == "values" }!!
            val enumValues = enumValuesGetter.invoke(null) as Array<*>
            for(enumValue in enumValues) {
                if (valueGetter != null) {
                    if (valueGetter.invoke(enumValue) == stringValue) {
                        return enumValue as E
                    }
                } else {
                    if (enumValue.toString() == stringValue) {
                        return enumValue as E
                    }
                }
            }
            throw InvalidFormatException(jp, "Failed to serialize enum '" + ctxt.contextualType.typeName + "' value '" + stringValue + "'", stringValue, ctxt.contextualType.rawClass)
        }
    }
}
