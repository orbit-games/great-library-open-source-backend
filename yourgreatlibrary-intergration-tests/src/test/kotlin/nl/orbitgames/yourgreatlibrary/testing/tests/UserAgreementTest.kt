package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.OffsetDateTime

class UserAgreementTest: BaseTest() {

    @Test
    fun getLatestAgreementTest() {

        val testAgreementRef = TestEnvironment.latestAgreement.ref

        val agreement = api.getLatestUserAgreement()
        assertThat(agreement.ref, equalTo(testAgreementRef))
        assertThat(agreement.pdfUrl, notNullValue())
        assertThat(agreement.startDate, lessThan(OffsetDateTime.now()))

    }

}