package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CourseProgressSummary
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Property
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test

class CourseUserTest: BaseTest() {

    @Test
    fun getCourseUserTest() {

        loginTestUser()

        val retrievedCourseUser = api.getCourseUser(testCourse.ref!!, testUserRef)

        assertThat(retrievedCourseUser.userRef, equalTo(testUserRef))
        assertThat(retrievedCourseUser.courseRef, equalTo(testCourse.ref!!))
    }

    @Test
    fun updateCourseUserTest() {

        loginTestUser()

        val testPropertyKey = "test_key" + RandomData.randomString(10)
        val courseProgressWithUpdates = CourseProgressSummary(
                displayName = RandomData.randomName(),
                topic = "On The Origin of Species",
                userCourseProperties = mapOf(
                        Pair(testPropertyKey, RandomData.randomString(10))
                ),
                active = true
        )
        val retrievedCourseUser = api.updateCourseUser(testCourse.ref!!, testUserRef, courseProgressWithUpdates)

        assertThat(retrievedCourseUser.userRef, equalTo(testUserRef))
        assertThat(retrievedCourseUser.courseRef, equalTo(testCourse.ref!!))
        assertThat(retrievedCourseUser.displayName, equalTo(courseProgressWithUpdates.displayName))
        assertThat(retrievedCourseUser.topic, equalTo(courseProgressWithUpdates.topic))
        assertThat(retrievedCourseUser.userCourseProperties!![testPropertyKey], equalTo(courseProgressWithUpdates.userCourseProperties!![testPropertyKey]))
        assertThat(retrievedCourseUser.active, equalTo(courseProgressWithUpdates.active))

        val updatedCourseProgress = api.getUserCourseProgress(testUserRef, testCourse.ref!!)

        assertThat(updatedCourseProgress.userRef, equalTo(testUserRef))
        assertThat(updatedCourseProgress.courseRef, equalTo(testCourse.ref!!))
        assertThat(updatedCourseProgress.displayName, equalTo(courseProgressWithUpdates.displayName))
        assertThat(updatedCourseProgress.topic, equalTo(courseProgressWithUpdates.topic))
        assertThat(updatedCourseProgress.userCourseProperties!![testPropertyKey], equalTo(courseProgressWithUpdates.userCourseProperties!![testPropertyKey]))
    }

    @Test
    fun getCourseUserPropertyTest() {

        loginTestUser()

        val testPropertyKey = "test_key" + RandomData.randomString(10)
        val retrievedProperty = api.getCourseUserProperty(testCourse.ref!!, testUserRef, testPropertyKey)

        assertThat(retrievedProperty.key, equalTo(testPropertyKey))
        assertThat(retrievedProperty.value, nullValue())
    }

    @Test
    fun updateCourseUserPropertyTest() {

        loginTestUser()

        val testPropertyKey = "test_key" + RandomData.randomString(10)
        val testPropertyValue1 = RandomData.randomString(10)
        val testPropertyValue2 = RandomData.randomString(10)

        val updatedProperty1 = api.updateCourseUserProperty(testCourse.ref!!, testUserRef, testPropertyKey, Property(testPropertyKey, testPropertyValue1))

        assertThat(updatedProperty1.key, equalTo(testPropertyKey))
        assertThat(updatedProperty1.value, equalTo(testPropertyValue1))

        val retrievedProperty1 = api.getCourseUserProperty(testCourse.ref!!, testUserRef, testPropertyKey)

        assertThat(retrievedProperty1.key, equalTo(testPropertyKey))
        assertThat(retrievedProperty1.value, equalTo(testPropertyValue1))

        // Now update the property again to ensure that updating an existing property works
        val updatedProperty2 = api.updateCourseUserProperty(testCourse.ref!!, testUserRef, testPropertyKey, Property(testPropertyKey, testPropertyValue2))

        assertThat(updatedProperty2.key, equalTo(testPropertyKey))
        assertThat(updatedProperty2.value, equalTo(testPropertyValue2))

        val retrievedProperty2 = api.getCourseUserProperty(testCourse.ref!!, testUserRef, testPropertyKey)

        assertThat(retrievedProperty2.key, equalTo(testPropertyKey))
        assertThat(retrievedProperty2.value, equalTo(testPropertyValue2))
    }

    @Test
    fun updateAllUserDisplayNames() {

        val testCourseRef = testCourse.ref!!

        for (user in TestEnvironment.testUsers) {
            api.login(user.username, user.password)
            api.updateCourseUser(testCourseRef, TestEnvironment.getUserRef(user.username), CourseProgressSummary(
                    displayName = RandomData.randomName(),
                    active = true
                    ))
        }
    }
}