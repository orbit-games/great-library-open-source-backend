package nl.orbitgames.yourgreatlibrary.testing.data

data class TestDataUser(val username: String, val email: String, val password: String, val fullName: String)