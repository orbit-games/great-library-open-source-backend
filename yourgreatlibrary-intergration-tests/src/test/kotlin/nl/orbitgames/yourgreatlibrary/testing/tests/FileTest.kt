package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.getTestImage
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test

class FileTest: BaseTest() {

    @Test
    fun fileUploadTest() {

        loginTestTeacher()

        val testFileName = "test file.jpg"
        val testFileData = getTestImage()
        val uploadedFileMetaData = api.uploadFile(testFileName, testFileData )

        assertThat(uploadedFileMetaData.ref, notNullValue())
        assertThat(uploadedFileMetaData.filename, equalTo(testFileName))
        assertThat(uploadedFileMetaData.size, equalTo(testFileData.size.toLong()))
        assertThat(uploadedFileMetaData.downloadUrl, notNullValue())

        val downloadedFileData = api.downloadFile(uploadedFileMetaData.downloadUrl)

        assertThat(downloadedFileData, equalTo(testFileData))

    }

}