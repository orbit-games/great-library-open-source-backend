package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.getRandomElement
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuestionType
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuizAnswer
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuizResult
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Test

class ResourceQuizTest: BaseTest() {

    @Test
    fun testResourceQuiz() {

        val resourceWithQuiz = testCourse.resources!!.find { r -> r.quiz != null }!!
        val quiz = resourceWithQuiz.quiz!!

        loginTestUser()

        val answers: MutableList<QuizAnswer> = mutableListOf()
        for (elem in quiz.elements!!) {
            if (elem.questionType == QuestionType.sELECTONE) {
                answers.add(QuizAnswer(elem.ref!!, arrayOf(elem.options!!.getRandomElement().ref!!)))
            } else if (elem.questionType == QuestionType.sELECTMULTIPLE) {

                // Randomly select at most 2 options
                val selectedOptions = Array(Math.min(2, elem.options!!.size)) { elem.options!!.getRandomElement().ref!! }
                answers.add(QuizAnswer(elem.ref!!, selectedOptions))
            }
        }
        val resourceResult = api.postLoggedInUserResourceQuizResult(testCourse.ref!!, resourceWithQuiz.ref!!, QuizResult(quiz.ref!!, answers.toTypedArray()))

        Assert.assertThat(resourceResult.quizResult, Matchers.notNullValue())
        Assert.assertThat(resourceResult.quizResult?.created, Matchers.notNullValue())
        Assert.assertThat(resourceResult.quizResult?.totalScore, Matchers.notNullValue())

        // If we retrieve the course progress now, it should also have the submitted quiz result:
        val courseProgress = api.getLoggedInUserCourseProgress(testCourse.ref!!)
        val retrievedResourceResult = courseProgress.resourceResults!!.find { r -> r.resourceRef == resourceWithQuiz.ref }!!
        Assert.assertThat(retrievedResourceResult.quizResult, Matchers.notNullValue())
        Assert.assertThat(retrievedResourceResult.quizResult?.created, Matchers.notNullValue())
        Assert.assertThat(retrievedResourceResult.quizResult?.totalScore, Matchers.notNullValue())

        // Also check that if we log on as another user, we don't get the same quiz result back:
        api.login(TestEnvironment.testUsers[1].username, TestEnvironment.testUsers[1].password);
        val otherUserCourseProgress = api.getLoggedInUserCourseProgress(testCourse.ref!!)
        val otherUserQuizResult = otherUserCourseProgress.resourceResults?.find { r -> r.resourceRef == resourceWithQuiz.ref }
        Assert.assertThat(otherUserQuizResult?.quizResult, Matchers.nullValue());

    }
}