package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.DomainAssertions.Companion.assertSectionsEqual
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CourseResource
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.ResourceRelation
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.Section
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.SectionRelation
import org.hamcrest.Matchers.*
import org.junit.Assert.assertFalse
import org.junit.Assert.assertThat
import org.junit.Test

class SectionTest : BaseTest() {

    @Test
    fun getSectionsTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val sections = api.getCourseSections(courseRef)
        assertThat(sections.sections.size, greaterThan(0))
    }

    @Test
    fun createSectionTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val newResource = CourseResource(
                order = 1,
                name = "Section test resource",
                description = "This is a resource for the section test"
        )
        val addedResource = api.createCourseResource(courseRef, newResource);

        val newSection = Section(
                "Test section",
                null,
                "This is a test section",
                arrayOf(ResourceRelation(
                        resourceRef = addedResource.ref!!,
                        description = "Resource relation description",
                        optional = false,
                        sortOrder = 0
                ))
        )

        val createdSection = api.createCourseSection(courseRef, newSection)

        assertThat(createdSection.ref, notNullValue())
        assertSectionsEqual(createdSection, newSection)

        // We get all sections here to assert that the new section is actually returned when looking for all sections in the course
        val sections = api.getCourseSections(courseRef)
        val sectionInCourse = sections.sections.find{ s -> s.ref == createdSection.ref }
        assertThat(sectionInCourse, notNullValue())
        assertSectionsEqual(sectionInCourse!!, newSection)

        val sectionInCourseDirect = api.getCourseSection(courseRef, createdSection.ref!!)
        assertSectionsEqual(sectionInCourseDirect, newSection)

    }

    @Test
    fun updateSectionTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val testResource = testCourse.resources!!.first()

        val newSection = Section(
                "Test section 2",
                null,
                "This is a test section",
                kotlin.emptyArray()
        )

        val createdSection = api.createCourseSection(courseRef, newSection)

        val sectionUpdate1 = createdSection.copy(
                description = "This is an updated description",
                resourceRelations = arrayOf(
                        ResourceRelation(
                                testResource.ref!!,
                                false,
                                0,
                                "This is the relation description for a resource"
                        )
                )
        )

        val updatedSection1 = api.updateCourseSection(courseRef, createdSection.ref!!, sectionUpdate1)
        assertSectionsEqual(updatedSection1, sectionUpdate1)

        val retrievedUpdatedSection1 = api.getCourseSection(courseRef, createdSection.ref!!)
        assertSectionsEqual(retrievedUpdatedSection1, sectionUpdate1)

        val sectionUpdate2 = updatedSection1.copy(
                resourceRelations = kotlin.emptyArray()
        )

        val updatedSection2 = api.updateCourseSection(courseRef, createdSection.ref!!, sectionUpdate2);
        assertSectionsEqual(updatedSection2, sectionUpdate2)

        val retrievedUpdatedSection2 = api.getCourseSection(courseRef, createdSection.ref!!)
        assertSectionsEqual(retrievedUpdatedSection2, sectionUpdate2)

    }

    @Test
    fun deleteSectionTest() {

        val courseRef = testCourse.ref!!

        loginTestTeacher()

        val testResource = testCourse.resources!!.first()

        val newSection = Section(
                "Test section 3",
                null,
                "This is a test section for section delete",
                arrayOf(
                        ResourceRelation(
                                testResource.ref!!,
                                false,
                                0,
                                "This is the relation description for a resource"
                        )
                )
        )

        val createdTestSection = api.createCourseSection(courseRef, newSection)

        val newChildSection = Section(
                "Test child section 3.1",
                null,
                "This is a child section for the section delete test",
                parentRef = createdTestSection.ref!!
        )
        val createdChildSection = api.createCourseSection(courseRef, newChildSection)

        // Make sure that we have a chapter referencing this section:
        val testChapter = testCourse.chapters!!.first()
        val chapterWithUpdates = testChapter.copy(
                sectionRelations = arrayOf(*testChapter.sectionRelations!!, SectionRelation(
                        sectionRef = createdTestSection.ref!!,
                        optional = true,
                        sortOrder = 100
                ))
        )
        api.updateCourseChapter(courseRef, testChapter.ref!!, chapterWithUpdates)

        api.deleteCourseSection(courseRef, createdTestSection.ref!!)

        val course = api.getCourse(courseRef)

        assertFalse(course.sections!!.any { it.ref == createdTestSection.ref })
        assertFalse(course.sections!!.any { it.ref == createdChildSection.ref })
    }
}
