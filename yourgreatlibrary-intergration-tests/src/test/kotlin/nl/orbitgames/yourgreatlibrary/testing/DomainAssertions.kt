package nl.orbitgames.yourgreatlibrary.testing

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers
import org.hamcrest.Matchers.equalTo
import org.junit.Assert
import org.junit.Assert.assertThat
import java.time.Duration
import kotlin.math.exp

class DomainAssertions {
    companion object {


        fun assertChaptersEqual(actual: Chapter, expected: Chapter, includeReviewSteps: Boolean) {

            assertThat(actual.name, equalTo(expected.name))
            assertThat(actual.order, equalTo(expected.order))
            assertThat(actual.description, equalTo(expected.description))
            assertThat(actual.sectionRelations?.size, equalTo(expected.sectionRelations?.size))

            for (i in 0 until actual.sectionRelations!!.size) {
                assertSectionRelationsEqual(actual.sectionRelations!![i], expected.sectionRelations!![i])
            }

            if (includeReviewSteps) {
                assertThat(actual.reviewSteps?.size, equalTo(expected.reviewSteps?.size))
                for (i in 0 until actual.reviewSteps!!.size) {
                    assertReviewStepsEqual(actual.reviewSteps!![i], expected.reviewSteps!![i])
                }
            }
        }

        fun assertReviewStepsEqual(actual: ReviewStep, expected: ReviewStep) {

            assertThat(actual.stepType, equalTo(expected.stepType))
            assertThat(actual.peerType, equalTo(expected.peerType))
            assertThat(actual.disabled, equalTo(expected.disabled))
            assertThat(actual.order, equalTo(expected.order))
            assertThat(actual.name, equalTo(expected.name))
            assertThat(actual.description, equalTo(expected.description))
            assertThat(Duration.between(actual.deadline, expected.deadline).toMillis(), equalTo(0L))
            assertThat(actual.discussionType, equalTo(expected.discussionType))
            assertThat(actual.fileUpload, equalTo(expected.fileUpload))
            assertThat(actual.fileUploadName, equalTo(expected.fileUploadName))

            assertThat(actual.allowedArguments?.size, equalTo(expected.allowedArguments?.size))
            if (actual.allowedArguments != null) {
                for (i in 0 until actual.allowedArguments!!.size) {
                    assertArgumentTypesEqual(actual.allowedArguments!![i], expected.allowedArguments!![i])
                }
            }

            assertQuizEqual(actual.quiz, expected.quiz)

            if (expected.skills == null) {
                assertThat(actual.skills, Matchers.nullValue())
            } else {
                assertThat(actual.skills, Matchers.arrayContaining(*expected.skills!!))
            }
        }

        fun assertArgumentTypesEqual(actual: ArgumentType, expected: ArgumentType) {

            assertThat(actual.name, equalTo(expected.name))
            assertThat(actual.description, equalTo(expected.description))
            assertThat(actual.required, equalTo(expected.required))
            assertThat(actual.parentRef, equalTo(expected.parentRef))
        }

        fun assertSectionRelationsEqual(actual: SectionRelation, expected: SectionRelation) {

            assertThat(actual.sectionRef, equalTo(expected.sectionRef))
            assertThat(actual.description, equalTo(expected.description))
            assertThat(actual.sortOrder, equalTo(expected.sortOrder))
            assertThat(actual.optional, equalTo(expected.optional))
        }

        fun assertResourceEqual(actual: CourseResource, reference: CourseResource) {

            assertThat(actual.order, equalTo(reference.order))
            assertThat(actual.name, equalTo(reference.name))
            assertThat(actual.description, equalTo(reference.description))
            assertThat(actual.url, equalTo(reference.url))
            assertQuizEqual(actual.quiz, reference.quiz)
            assertThat(actual.quizMinimalScore, equalTo(reference.quizMinimalScore))
            Assert.assertTrue(
                    (actual.skills.isNullOrEmpty() && reference.skills.isNullOrEmpty()) ||
                            (!actual.skills.isNullOrEmpty() && !reference.skills.isNullOrEmpty()))

            if (actual.skills != null && actual.skills!!.isNotEmpty()) {
                assertThat(actual.skills, equalTo(reference.skills))
            }
        }

        fun assertQuizEqual(actual: Quiz?, reference: Quiz?) {

            Assert.assertFalse((actual == null && reference != null) ||
                    (actual != null && reference == null))

            // If the both quizes are null, there's no need to check any further
            if (actual == null && reference == null) {
                return
            }

            assertThat(actual?.name, equalTo(reference?.name))
            assertThat(actual?.description, equalTo(reference?.description))
            assertThat(actual?.elements?.size, equalTo(reference?.elements?.size))

            for (i in 0 until actual!!.elements!!.size) {
                assertQuizElementsEqual(actual.elements!![i], reference!!.elements!![i])
            }
        }

        fun assertQuizElementsEqual(actual: QuizElement, reference: QuizElement) {

            assertThat(actual.order, equalTo(reference.order))
            assertThat(actual.text, equalTo(reference.text))
            assertThat(actual.questionType, equalTo(reference.questionType))
            assertThat(actual.options?.size, equalTo(reference.options?.size))

            for (i in 0 until actual.options!!.size) {
                assertQuizElementOptionEqual(actual.options!![i], reference.options!![i])
            }
        }

        fun assertQuizElementOptionEqual(actual: QuizElementOption, reference: QuizElementOption) {

            assertThat(actual.order, equalTo(reference.order))
            assertThat(actual.text, equalTo(reference.text))
            assertThat(actual.points, equalTo(reference.points))
        }

        fun assertSectionsEqual(actual: Section, matcher: Section) {
            assertThat(actual.name, equalTo(matcher.name))
            assertThat(actual.description, equalTo(matcher.description))
            assertThat(actual.resourceRelations?.size, equalTo(matcher.resourceRelations?.size))

            for (i in 0 until actual.resourceRelations!!.size) {
                assertResourceRelationEqual(actual.resourceRelations!![i], matcher.resourceRelations!![i])
            }
        }

        fun assertResourceRelationEqual(actual: ResourceRelation, reference: ResourceRelation) {

            assertThat(actual.resourceRef, equalTo(reference.resourceRef))
            assertThat(actual.optional, equalTo(reference.optional))
            assertThat(actual.sortOrder, equalTo(reference.sortOrder))
            assertThat(actual.description, equalTo(reference.description))
        }

    }
}