package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.DomainAssertions.Companion.assertReviewStepsEqual
import nl.orbitgames.yourgreatlibrary.testing.api.GreatLibraryTestApi
import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuestionType
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuizElement
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.QuizElementOption
import org.junit.Assert.assertTrue
import org.junit.Test
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import kotlin.test.assertNotNull

class ReviewStepTest: BaseTest() {

    private val reviewStepTestCourseCode = RandomData.randomString(8)
    private val reviewStepTestCourse by lazy {

        val course = TestEnvironment.testApi.testCreateCourse(GreatLibraryTestApi.CreateTestCourseRequest (
                firstSubmissionDeadline = OffsetDateTime.now().minusMonths(3),
                courseName = "Review Step Integration Test Course " + LocalDate.now().format(DateTimeFormatter.ISO_DATE),
                courseCode = reviewStepTestCourseCode,
                anonymousReviews = false,
                anonymousUsers = false
        ));

        loginTestTeacher()

        api.loggedInUserJoinCourse(reviewStepTestCourseCode)

        return@lazy course
    }

    @Test
    fun getReviewStepsTest() {

        val courseRef = reviewStepTestCourse.ref!!
        val testChapter = reviewStepTestCourse.chapters!![0];

        loginTestTeacher()

        testChapter.reviewSteps!!.forEach {
            val reviewStep = api.getReviewStep(courseRef, testChapter.ref!!, it.ref!!)
            assertNotNull(reviewStep)
            assertReviewStepsEqual(reviewStep, it)
        }
    }

    @Test
    fun updateReviewStepTest() {

        val courseRef = reviewStepTestCourse.ref!!
        val testChapter = reviewStepTestCourse.chapters!![0]
        val testReviewStep = testChapter.reviewSteps!![1]

        loginTestTeacher()

        val reviewStepUpdate1 = testReviewStep.copy(
                name = testReviewStep.name + " update",
                description = "This is an updated description",
                deadline = testReviewStep.deadline.plusDays(1L),
                quiz = testReviewStep.quiz!!.copy(
                        name = testReviewStep.quiz!!.name + " updated",
                        description = testReviewStep.quiz!!.description + " updated",
                        elements = arrayOf(*testReviewStep.quiz!!.elements!!,
                                QuizElement(
                                        order = 100,
                                        text = "What is your favorite color?",
                                        questionType = QuestionType.sELECTONE,
                                        options = arrayOf(
                                                QuizElementOption(
                                                        order = 0,
                                                        text = "Blue",
                                                        points = 0
                                                ),
                                                QuizElementOption(
                                                        order = 10,
                                                        text = "Yellow",
                                                        points = 10
                                                )
                                        )
                                )
                        )
                )
        )

        val updatedReviewStep1 = api.updateReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!, reviewStepUpdate1)
        assertReviewStepsEqual(updatedReviewStep1, reviewStepUpdate1)

        val retrievedUpdatedReviewStep1 = api.getReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!)
        assertReviewStepsEqual(retrievedUpdatedReviewStep1, reviewStepUpdate1)

        // Change it back to the original
        val updatedReviewStep2 = api.updateReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!, testReviewStep)
        assertReviewStepsEqual(updatedReviewStep2, testReviewStep)

        val retrievedUpdatedReviewStep2 = api.getReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!)
        assertReviewStepsEqual(retrievedUpdatedReviewStep2, testReviewStep)

    }

    @Test
    fun disableEnableReviewStepTest() {

        // It is know that review step "rebuttal" and "assessment" are disabled for the test course
        val courseRef = reviewStepTestCourse.ref!!
        val testChapter = reviewStepTestCourse.chapters!![2]
        val testReviewStep = testChapter.reviewSteps!![3]

        assertTrue(testReviewStep.disabled)

        loginTestTeacher()

        val reviewStepUpdate1 = testReviewStep.copy(
                disabled = false
        )

        val updatedReviewStep1 = api.updateReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!, reviewStepUpdate1)
        assertReviewStepsEqual(updatedReviewStep1, reviewStepUpdate1)

        val retrievedUpdatedReviewStep1 = api.getReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!)
        assertReviewStepsEqual(retrievedUpdatedReviewStep1, reviewStepUpdate1)


        val reviewStepUpdate2 = testReviewStep.copy(
                disabled = true
        )

        val updatedReviewStep2 = api.updateReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!, reviewStepUpdate2)
        assertReviewStepsEqual(updatedReviewStep2, reviewStepUpdate2)

        val retrievedUpdatedReviewStep2 = api.getReviewStep(courseRef, testChapter.ref!!, testReviewStep.ref!!)
        assertReviewStepsEqual(retrievedUpdatedReviewStep2, reviewStepUpdate2)
    }
}