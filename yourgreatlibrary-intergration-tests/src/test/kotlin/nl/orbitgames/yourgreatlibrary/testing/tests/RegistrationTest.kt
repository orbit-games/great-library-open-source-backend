package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.UserRegistrationRequest
import org.junit.Assert
import org.junit.Test

class RegistrationTest: BaseTest() {

    @Test
    internal fun testRegister() {

        val randomEmail = RandomData.randomString(15) + "@orbitgames.nl";
        val req = UserRegistrationRequest(
                email = randomEmail,
                fullName = RandomData.randomName(),
                username = randomEmail,
                password = "Test123*",
                signedUserAgreementRef = TestEnvironment.latestAgreement.ref!!
        )

        api.userRegister(req)

        // The API already validates that no error was thrown. We can't really test anything other after registration
        // We can test howeven, that registering a tests with the same username leads to an error:
        val err = api.applicationError(api.userRegisterRaw(req))

        Assert.assertEquals("entity.exists", err.error.code)
    }
}
