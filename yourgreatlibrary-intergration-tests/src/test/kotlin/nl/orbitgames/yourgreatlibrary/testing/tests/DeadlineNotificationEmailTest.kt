package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CreateCourseFromTemplateRequest
import org.junit.Test
import java.time.OffsetDateTime

class DeadlineNotificationEmailTest : BaseTest() {

    @Test
    fun testDeadlineNotificationEmailJob() {

        // We create a minimalistic test course with the deadlines such that the first deadline expires right after
        // this test runs. Then we wait for it to expire, and check if the reviewer relations have been assigned.

        loginTestTeacher()
        val firstDeadline = OffsetDateTime.now()
                // Subtract the deadline mail sent timeout
                .plusHours(24)
                // Give this test some time to run before the e-mails get sent
                .plusSeconds(10)

        val courseCode = RandomData.randomString(8)

        val course = api.postCreateCourseFromTemplate(CreateCourseFromTemplateRequest(
                templateId = "minimalistic-test-course",
                courseName = "Deadline notification e-mail test course",
                year = 2019,
                quarter = 1,
                courseCode = courseCode,
                anonymousReviews = false,
                anonymousUsers = false,
                deadlinesOverride = mapOf(
                        Pair("1.submission", firstDeadline),
                        Pair("1.review", firstDeadline.plusDays(5)),
                        Pair("1.evaluation", firstDeadline.plusDays(7)),
                        Pair("1.rebuttal", firstDeadline.plusDays(16)),
                        Pair("1.assessment", firstDeadline.plusDays(23))
                )
        ))

        for (testUser in TestEnvironment.testUsers) {
            val testUserRef = TestEnvironment.getUserRef(testUser.username)
            api.login(testUser.username, testUser.password)
            api.joinCourse(testUserRef, courseCode)
        }

        // TODO: Somehow verify that the e-mail has actually been sent
    }

}
