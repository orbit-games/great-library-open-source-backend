package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.junit.Test

class ProgressExportTest: BaseTest() {

    @Test
    fun fullExportTest() {

        loginTestTeacher()
        api.postCourseProgressExport(this.testCourse.ref!!, CourseProgressExportSpecification())
    }

    @Test
    fun gradingColumnExportTest() {

        loginTestTeacher()

        val columns = mutableListOf<CourseProgressExportColumnSpecification>(
                CourseProgressExportColumnSpecification(CourseProgressExportColumnType.tOTALMINUTESLATE),
                CourseProgressExportColumnSpecification(CourseProgressExportColumnType.tOTALSKIPPEDSTEPS)
        )
        for (chapter in this.testCourse.chapters!!) {
            for (reviewStep in chapter.reviewSteps!!) {
                if (reviewStep.stepType == StepType.eVALUATION || reviewStep.stepType == StepType.aSSESSMENT) {
                    columns.add(CourseProgressExportColumnSpecification(
                            columnType = CourseProgressExportColumnType.sTEPDETAIL,
                            chapterRef = chapter.ref,
                            reviewStepRef = reviewStep.ref,
                            dataType = CourseProgressExportColumnDataType.sCORE,
                            stepExecution = CourseProgressExportStepExecution.rECEIVED
                    ))
                }
            }
        }
        api.postCourseProgressExport(this.testCourse.ref!!, CourseProgressExportSpecification(columns.toTypedArray()))

    }
}