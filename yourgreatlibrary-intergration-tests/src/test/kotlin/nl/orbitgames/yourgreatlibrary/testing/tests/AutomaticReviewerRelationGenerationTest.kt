package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.RandomData
import nl.orbitgames.yourgreatlibrary.testing.data.getTestPdf
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.CreateCourseFromTemplateRequest
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Test
import java.time.OffsetDateTime

class AutomaticReviewerRelationGenerationTest : BaseTest() {

    @Test
    fun testAutomaticReviewerRelation() {

        // We create a minimalistic test course with the deadlines such that the first deadline expires right after
        // this test runs. Then we wait for it to expire, and check if the reviewer relations have been assigned.

        loginTestTeacher()
        val firstDeadline = OffsetDateTime.now()
                // Subtract the default relation generation deadline
                .minusSeconds(50340)
                // Give this test some time to run before the auto-generation takes place
                .plusSeconds(15)

        val courseCode = RandomData.randomString(8)

        val course = api.postCreateCourseFromTemplate(CreateCourseFromTemplateRequest(
                templateId = "minimalistic-test-course",
                courseName = "Reviewer generation test course",
                year = OffsetDateTime.now().year,
                quarter = 1,
                courseCode = courseCode,
                anonymousReviews = false,
                anonymousUsers = false,
                deadlinesOverride = mapOf(
                        Pair("1.submission", firstDeadline),
                        Pair("1.review", firstDeadline.plusDays(5)),
                        Pair("1.evaluation", firstDeadline.plusDays(7)),
                        Pair("1.rebuttal", firstDeadline.plusDays(16)),
                        Pair("1.assessment", firstDeadline.plusDays(23))
                )
        ))

        val chapter = course.chapters!![0]
        val reviewStep = chapter.reviewSteps!![0]

        val userThatMissesDeadline = TestEnvironment.testUsers[0]
        val userThatMissesDeadlineRef = TestEnvironment.getUserRef(userThatMissesDeadline.username)

        for (testUser in TestEnvironment.testUsers) {
            val testUserRef = TestEnvironment.getUserRef(testUser.username)
            api.login(testUser.username, testUser.password)
            api.joinCourse(testUserRef, courseCode)

            if (testUser != userThatMissesDeadline) {
                val userChapterProgress = api.getLoggedInUserCourseChapterProgress(course.ref!!, chapter.ref!!)

                val reviewerRelation = userChapterProgress.reviewerRelations!![0]

                // Submit the first review step as this may be required to be included in the peer review proces
                val fileName = "reviewerGenerationTest.pdf"
                val testPdfData = getTestPdf()
                api.postLoggedInUserReviewStepFileUpload(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, fileName, testPdfData)
            }
        }

        println(OffsetDateTime.now().toString() + " Let's wait a bit")
        // Now we wait for the deadline to pass and the reviewer relations to be generated
        Thread.sleep(15000)
        println(OffsetDateTime.now().toString() + " Done waiting, let's check")

        // And now we expect all the test users to have reviewers and submitters assigned
        for (testUser in TestEnvironment.testUsers) {

            api.login(testUser.username, testUser.password)
            val userChapterProgress = api.getLoggedInUserCourseChapterProgress(course.ref!!, chapter.ref!!)

            for (reviewerRelation in userChapterProgress.reviewerRelations!!) {

                if (testUser != userThatMissesDeadline) {
                    Assert.assertThat(reviewerRelation.reviewer, Matchers.not(Matchers.isEmptyOrNullString()))
                    // Check that the user that missed the deadline isn't assigned as a reviewer
                    Assert.assertThat(reviewerRelation.reviewer, Matchers.not(Matchers.equalTo(userThatMissesDeadlineRef)))
                } else {
                    Assert.assertThat(reviewerRelation.reviewer, Matchers.isEmptyOrNullString())
                }

                Assert.assertThat(reviewerRelation.submitter, Matchers.not(Matchers.isEmptyOrNullString()))

            }

            if (testUser == userThatMissesDeadline) {
                Assert.assertThat(userChapterProgress.reviewerRelations!!.size, Matchers.equalTo(2))
            } else {
                Assert.assertThat(userChapterProgress.reviewerRelations!!.size, Matchers.equalTo(4))
            }
        }
    }
}
