package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.data.TestDataUser
import nl.orbitgames.yourgreatlibrary.testing.data.getTestPdf
import nl.orbitgames.yourgreatlibrary.testing.getRandomElement
import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test

class ReviewProgressTest: BaseTest() {

    // TODO at a tests that posts arguments, completes a step, and then deletes the completion again

    @Test
    fun courseProgressTest() {

        loginTestUser()

        val beforeProcessCourseProgress = api.getLoggedInUserCourseProgress(testCourse.ref!!)

        for (chapter in testCourse.chapters!!) {

            // We select a reviewer and a submitter to work with
            // To do this, we find relations in which the test reviewer is the opposite peer type
            val reviewerUserRef = beforeProcessCourseProgress
                    .chapters!!.find { c -> c.chapterRef == chapter.ref }!!
                    .reviewerRelations!!.find { r -> r.submitter == testUserRef }!!
                    .reviewer!!
            val submitterUserRef = beforeProcessCourseProgress
                    .chapters!!.find { c -> c.chapterRef == chapter.ref }!!
                    .reviewerRelations!!.find { r -> r.reviewer == testUserRef }!!
                    .submitter!!

            val reviewer = TestEnvironment.testUsers.find { u -> TestEnvironment.getUserRef(u.username) == reviewerUserRef }!!
            val submitter =TestEnvironment.testUsers.find { u -> TestEnvironment.getUserRef(u.username) == submitterUserRef }!!

            for (reviewStep in chapter.reviewSteps!!) {

                if (reviewStep.disabled) {
                    continue
                }

                // To ensure that we can retrieve the chapter progress at any time during the review process, we just get it each time
                // This also ensures that we also have all the latest review step results, which are useful for, for example, replying to arguments
                api.login(testUser.username, testUser.password)
                val chapterProgress = api.getLoggedInUserCourseChapterProgress(testCourse.ref!!, chapter.ref!!)

                // Find a relation in which testUser is the submitter:
                val submitterRelation = chapterProgress.reviewerRelations!!.find { r -> r.submitter == testUserRef && r.reviewer == reviewerUserRef }!!
                val reviewerRelation = chapterProgress.reviewerRelations!!.find { r -> r.reviewer == testUserRef && r.submitter == submitterUserRef }!!

                if (reviewStep.peerType == PeerType.sUBMITTER) {
                    doStep(testUser, testCourse, chapter, reviewStep, submitterRelation)
                    doStep(submitter, testCourse, chapter, reviewStep, reviewerRelation)
                } else {
                    doStep(testUser, testCourse, chapter, reviewStep, reviewerRelation)
                    doStep(reviewer, testCourse, chapter, reviewStep, submitterRelation)
                }
            }
        }

        api.login(testUser.username, testUser.password)
        val courseProgress = api.getLoggedInUserCourseProgress(testCourse.ref!!)

        // Validate that in each chapter, we have at least one "submitter relation" and one "reviewer relation" with review step results for all steps
        for (chapterProgress in courseProgress.chapters!!) {
            var validSubmitterRelationsFound = 0
            var validReviewerRelationsFound = 0

            val chapter = testCourse.chapters!!.find { c -> c.ref == chapterProgress.chapterRef }!!
            for (relation in chapterProgress.reviewerRelations!!) {
                if (relation.reviewStepResults != null &&
                        chapter.reviewSteps!!
                                .all{ step -> step.disabled || relation.reviewStepResults!!.any{ result -> result.reviewStepRef == step.ref && result.readyToComplete && result.markedComplete != null }}) {
                    // This is a valid relation
                    if (relation.submitter == testUserRef) {
                        validSubmitterRelationsFound++
                    }
                    if (relation.reviewer == testUserRef) {
                        validReviewerRelationsFound++
                    }
                }
            }
            assertThat(validSubmitterRelationsFound, greaterThanOrEqualTo(1))
            assertThat(validReviewerRelationsFound, greaterThanOrEqualTo(1))
        }

        // Validate that in all reviewer-relations where the test user if the submitter, the submission step has been completed
        for (chapterProgress in courseProgress.chapters!!) {
            val chapter = testCourse.chapters!!.find { c -> c.ref == chapterProgress.chapterRef }!!
            // Assuming that the submission is the first step
            val submissionStep = chapter.reviewSteps!![0]
            for (relation in chapterProgress.reviewerRelations!!) {
                if (relation.submitter == testUserRef) {
                    val submissionResult = relation.reviewStepResults!!.find { r ->
                        r.reviewStepRef == submissionStep.ref &&
                        r.readyToComplete && r.markedComplete != null}!!
                    assertThat(submissionResult.fileUploadMetadata, notNullValue())
                }
            }
        }
    }

    private fun doStep(user: TestDataUser, course: Course, chapter: Chapter, reviewStep: ReviewStep, reviewerRelation: ReviewerRelation) {

        api.login(user.username, user.password)

        // Do file upload if needed
        if (reviewStep.fileUpload == true) {
            // To make sure it works for all cases, we put some harder characters in the filename, e.g. '+' and ' ':
            val fileName = "integration test + document & v1.pdf"
            val testPdfData = getTestPdf()
            val stepResult = api.postLoggedInUserReviewStepFileUpload(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, fileName, testPdfData)

            // Check if we can download the submitted document:
            assertThat(stepResult.fileUploadMetadata?.filename, equalTo(fileName))
            val downloadedFile = api.downloadFile(stepResult.fileUploadMetadata!!.downloadUrl)
            assertThat(downloadedFile, equalTo(testPdfData))
        }

        // Fill in quiz if needed
        if (reviewStep.quiz != null) {
            val quiz = reviewStep.quiz!!
            val answers: MutableList<QuizAnswer> = mutableListOf()
            for (elem in quiz.elements!!) {
                if (elem.questionType == QuestionType.sELECTONE) {
                    answers.add(QuizAnswer(elem.ref!!, arrayOf(elem.options!!.getRandomElement().ref!!)))
                } else if (elem.questionType == QuestionType.sELECTMULTIPLE) {

                    // Randomly select at most 2 options
                    val selectedOptions = Array(Math.min(2, elem.options!!.size)) { elem.options!!.getRandomElement().ref!! }
                    answers.add(QuizAnswer(elem.ref!!, selectedOptions))
                }
            }
            val stepResult = api.postLoggedInUserReviewStepQuiz(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, QuizResult(quiz.ref!!, answers.toTypedArray()))

            assertThat(stepResult.quizResult?.created, notNullValue())
            assertThat(stepResult.quizResult?.totalScore, notNullValue())
        }

        // Add arguments/discussions if allowed
        when {
            reviewStep.discussionType == DiscussionType.rEPLY -> {

                // Find the arguments we have to reply to, by looking at the most recent step in the reviewer relation that
                // has arguments
                val resultToRespondTo = reviewerRelation.reviewStepResults!!.findLast { stepResult -> stepResult.arguments?.any() ?: false }
                assertThat("Can't find review step to respond to but all steps should be fully filled!", resultToRespondTo, notNullValue())
                for (argumentToReplyTo in resultToRespondTo!!.arguments!!) {

                    val message = "This is a response to " + argumentToReplyTo.ref
                    val stepResult = api.postLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, Argument(
                            replyToArgumentRef = argumentToReplyTo.ref!!,
                            message = message
                    ))
                    val addedArgument = stepResult.arguments?.last()!!
                    assertThat(addedArgument.ref, notNullValue())
                    assertThat(addedArgument.message, equalTo(message))
                    assertThat(addedArgument.replyToArgumentRef, equalTo(argumentToReplyTo.ref))
                }
            }
            reviewStep.discussionType == DiscussionType.rESTRICTEDARGUMENTS -> {
                val requiredArgumentTypes = reviewStep.allowedArguments!!.filter { a -> a.required }

                // At least add all the required arguments:
                for(argumentType in requiredArgumentTypes) {

                    val message = "Post required argument ${argumentType.name}"
                    val stepResult = api.postLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, Argument(
                            argumentTypeRef = argumentType.ref,
                            message = message
                    ))
                    val addedArgument = stepResult.arguments?.last()!!
                    assertThat(addedArgument.ref, notNullValue())
                    assertThat(addedArgument.message, equalTo(message))
                    assertThat(addedArgument.argumentTypeRef, equalTo(argumentType.ref))

                }
                repeat(3) {
                    val argumentType = reviewStep.allowedArguments!!.getRandomElement()
                    val message = "This is test argument $it"

                    val stepResult = api.postLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, Argument(
                            argumentTypeRef = argumentType.ref,
                            message = message
                    ))
                    val addedArgument = stepResult.arguments?.last()!!
                    assertThat(addedArgument.ref, notNullValue())
                    assertThat(addedArgument.message, equalTo(message))
                    assertThat(addedArgument.argumentTypeRef, equalTo(argumentType.ref))

                    // We modify the third argument and then delete it
                    if (it == 2) {
                        val argumentWithUpdates = Argument(
                                ref = addedArgument.ref,
                                argumentTypeRef = addedArgument.argumentTypeRef,
                                message = "modified " + addedArgument.message
                        )
                        val stepResult2 = api.updateLoggedInUserReviewStepArgument(
                                course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, addedArgument.ref!!, argumentWithUpdates)
                        val updatedArgument = stepResult2.arguments?.last()!!
                        assertThat(updatedArgument.ref, equalTo(addedArgument.ref))
                        assertThat(updatedArgument.message, equalTo("modified " + addedArgument.message))

                        val stepResult3 = api.deleteLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, addedArgument.ref!!)
                        assertThat(stepResult3.arguments!!.any {a -> a.ref == addedArgument.ref}, equalTo(false))
                        assertThat(stepResult3.arguments!!.size, equalTo(requiredArgumentTypes.size + 2))
                    }
                }
            }
            reviewStep.discussionType == DiscussionType.fREEFORMARGUMENTS -> {

                repeat(2) {

                    val message = "This is test argument $it"

                    val stepResult = api.postLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, Argument(
                            message = message
                    ))
                    val addedArgument = stepResult.arguments?.last()!!
                    assertThat(addedArgument.ref, notNullValue())
                    assertThat(addedArgument.message, equalTo(message))

                    // We modify the third argument and then delete it
                    if (it == 2) {
                        val argumentWithUpdates = Argument(
                                ref = addedArgument.ref,
                                message = "modified " + addedArgument.message
                        )
                        val stepResult2 = api.updateLoggedInUserReviewStepArgument(
                                course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, addedArgument.ref!!, argumentWithUpdates)
                        val updatedArgument = stepResult2.arguments?.last()!!
                        assertThat(updatedArgument.ref, equalTo(addedArgument.ref))
                        assertThat(updatedArgument.message, equalTo("modified " + addedArgument.message))

                        val stepResult3 = api.deleteLoggedInUserReviewStepArgument(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!, addedArgument.ref!!)
                        assertThat(stepResult3.arguments!!.any {a -> a.ref == addedArgument.ref}, equalTo(false))
                        assertThat(stepResult3.arguments!!.size, equalTo(2))
                    }
                }
            }
        }

        // By now, the review step is complete, so we mark is as complete
        val stepCompleteResult1 = api.postLoggedInUserReviewStepComplete(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!)
        assertThat(stepCompleteResult1.markedComplete, notNullValue())
        assertThat("The deadlines for the test course have already passed, so after the step is marked complete, it should also be closed",
                stepCompleteResult1.closed, `is`(true))

        // Test if we can revoke the completion
        val stepRevokeResult = api.deleteLoggedInUserReviewStepComplete(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!)
        assertThat(stepRevokeResult .markedComplete, nullValue())
        assertThat(stepRevokeResult.closed, `is`(false))


        // And now complete it again/do the actual completion
        val stepCompleteResult2 = api.postLoggedInUserReviewStepComplete(course.ref!!, chapter.ref!!, reviewerRelation.ref!!, reviewStep.ref!!)
        assertThat(stepCompleteResult2.markedComplete, notNullValue())
        assertThat("The deadlines for the test course have already passed, so after the step is marked complete, it should also be closed",
                stepCompleteResult2.closed, `is`(true))
    }
}