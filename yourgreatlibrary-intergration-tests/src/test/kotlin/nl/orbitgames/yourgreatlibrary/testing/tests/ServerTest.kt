package nl.orbitgames.yourgreatlibrary.testing.tests

import org.hamcrest.Matchers.*
import org.junit.Assert.assertThat
import org.junit.Test

class ServerTest : BaseTest() {

    @Test
    fun getServerInfoTest() {

        val serverInfo = api.getServerInfo()

        assertThat(serverInfo.currentTime, notNullValue())
        assertThat(serverInfo.allowedEmailDomains, not(emptyArray()))
        assertThat(serverInfo.reviewStepUploadMaxSize, notNullValue())
        assertThat(serverInfo.reviewStepUploadAllowedFileExtensions, notNullValue())
        assertThat(serverInfo.reviewStepUploadAllowedMimeTypes, notNullValue())
        assertThat(serverInfo.contentUploadMaxSize, notNullValue())
        assertThat(serverInfo.contentUploadAllowedFileExtensions, notNullValue())
        assertThat(serverInfo.contentUploadAllowedMimeTypes, notNullValue())

    }
}
