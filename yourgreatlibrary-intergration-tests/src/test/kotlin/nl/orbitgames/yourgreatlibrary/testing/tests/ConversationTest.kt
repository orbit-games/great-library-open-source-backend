package nl.orbitgames.yourgreatlibrary.testing.tests

import nl.orbitgames.yourgreatlibrary.testing.preparation.TestEnvironment
import nl.orbitgames.yourgreatlibrary.yourgreatlibrarybackend.rest.model.generated.*
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.Duration

class ConversationTest: BaseTest() {

    @Test
    fun testGetConversations() {

        val testCourseRef = testCourse.ref!!

        loginTestUser()
        val conversations = api.getConversations(testCourseRef)

        assertThat(conversations.conversations, notNullValue())
        assertThat(conversations.currentTime, notNullValue())
    }

    @Test
    fun testCreateConversation() {

        val testCourseRef = testCourse.ref!!
        val conversationToAdd = Conversation(
                hidden = false,
                anonymousMembers = false,
                messageCount = 0,
                title = "Test Conv 1",
                type = ConversationType.tOPIC,
                members = arrayOf(
                    TestEnvironment.getUserRef(TestEnvironment.testUsers[0].username),
                    TestEnvironment.getUserRef(TestEnvironment.testUsers[1].username)
                ),
                roles = arrayOf(
                        ConversationRolePermission(Role.tEACHER, ConversationPermissionType.vIEW)
                )
        )

        loginTestUser()
        val addedConversation = api.createConversation(testCourseRef, conversationToAdd)

        assertThat(addedConversation.ref, notNullValue())
        assertThat(addedConversation.hidden, equalTo(conversationToAdd.hidden))
        assertThat(addedConversation.anonymousMembers, equalTo(conversationToAdd.anonymousMembers))
        assertThat(addedConversation.title, equalTo(conversationToAdd.title))
        assertThat(addedConversation.members!!.sorted(), equalTo(conversationToAdd.members?.sorted()))
        assertThat(addedConversation.roles!!.sortedBy { r -> r.role }, equalTo(conversationToAdd.roles?.sortedBy { r -> r.role }))

        validateConversationsEquals(api.getConversations(testCourseRef).conversations!!.find { c -> c.ref == addedConversation.ref }!!, addedConversation)
        validateConversationsEquals(api.getConversation(testCourseRef, addedConversation.ref!!), addedConversation)
    }

    @Test
    fun testPostConversationMessage() {

        val testCourseRef = testCourse.ref!!
        val testUser1 = TestEnvironment.testUsers[0]
        val testUser2 = TestEnvironment.testUsers[1]
        val conversationToAdd = Conversation(
                hidden = false,
                anonymousMembers = false,
                messageCount = 0,
                type = ConversationType.dIRECT,
                members = arrayOf(
                        TestEnvironment.getUserRef(testUser1.username),
                        TestEnvironment.getUserRef(testUser2.username)
                ),
                roles = arrayOf(
                        ConversationRolePermission(Role.tEACHER, ConversationPermissionType.vIEW)
                )
        )

        loginTestUser()
        // First we create a conversation to post messages to
        val conversation = api.createConversation(testCourseRef, conversationToAdd)

        val testMessageCount = 5L
        for (i in (0 until testMessageCount)) {
            val testUser = if (i % 2 == 0L) testUser1 else testUser2
            api.login(testUser.username, testUser.password)

            val message = "Hello from Message $i"
            val postedMessage = api.postConversationMessage(testCourseRef, conversation.ref!!, PostConversationMessageRequest(message))

            assertThat(postedMessage.ref, notNullValue())
            assertThat(postedMessage.message, equalTo(message))

            val retrievedConvo = api.getConversation(testCourseRef, conversation.ref!!)

            assertThat(retrievedConvo.messageCount, equalTo(i + 1))
            assertThat(retrievedConvo.messages!![0].ref, equalTo(postedMessage.ref))
        }

        // We also check if pagination in the messages works:
        val retrievedConvo = api.getConversation(testCourseRef, conversation.ref!!, messageSkip = 1, messageLimit = 2)
        assertThat(retrievedConvo.messageCount, equalTo(testMessageCount))
        assertThat(retrievedConvo.messages?.size, equalTo(2))
        assertThat(retrievedConvo.messages!![0].ind, equalTo(testMessageCount  - 2))
        assertThat(retrievedConvo.messages!![1].ind, equalTo(testMessageCount  - 3))
    }

    @Test
    fun testMarkAsRead() {

        val testCourseRef = testCourse.ref!!
        val testUser1 = TestEnvironment.testUsers[0]
        val testUser2 = TestEnvironment.testUsers[3]
        val conversationToAdd = Conversation(
                hidden = false,
                anonymousMembers = false,
                messageCount = 0,
                type = ConversationType.dIRECT,
                members = arrayOf(
                        TestEnvironment.getUserRef(testUser1.username),
                        TestEnvironment.getUserRef(testUser2.username)
                ),
                roles = arrayOf(
                        ConversationRolePermission(Role.tEACHER, ConversationPermissionType.vIEW)
                )
        )

        loginTestUser()
        // First we create a conversation to post messages to
        val conversation = api.createConversation(testCourseRef, conversationToAdd)

        api.login(testUser1.username, testUser1.password)
        val message1 = api.postConversationMessage(testCourseRef, conversation.ref!!, PostConversationMessageRequest("Message 1"))

        val retrievedConvo1User1 = api.getConversation(testCourseRef, conversation.ref!!)
        assertThat(retrievedConvo1User1.messageCount, equalTo(1L))
        assertThat(retrievedConvo1User1.readUntil, equalTo(0L))
        assertMessagesEqual(retrievedConvo1User1.messages!![0], message1)

        api.login(testUser2.username, testUser2.password)
        val retrievedConvo1User2 = api.getConversation(testCourseRef, conversation.ref!!)
        assertThat(retrievedConvo1User2.messageCount, equalTo(1L))
        assertThat(retrievedConvo1User2.readUntil, equalTo(-1L))

        val readConversation1 = api.postConversationRead(testCourseRef, conversation.ref!!, PostConversationReadRequest(0L))
        assertThat(readConversation1 .messageCount, equalTo(1L))
        assertThat(readConversation1 .readUntil, equalTo(0L))
        // The call should only return any new unread messages, so in this case, no messages
        assertThat(readConversation1 .messages!!.size, equalTo(0))

        // Now user1 is gonna post another message, and when user2 posts again that he only read until message 1, it should receive this second message.
        api.login(testUser1.username, testUser1.password)
        val message2 = api.postConversationMessage(testCourseRef, conversation.ref!!, PostConversationMessageRequest("Message 2"))

        val retrievedConvo2User1 = api.getConversation(testCourseRef, conversation.ref!!)
        assertThat(retrievedConvo2User1.messageCount, equalTo(2L))
        assertThat(retrievedConvo2User1.readUntil, equalTo(1L))
        assertMessagesEqual(retrievedConvo2User1.messages!![0], message2)

        api.login(testUser2.username, testUser2.password)
        val readConversation2 = api.postConversationRead(testCourseRef, conversation.ref!!, PostConversationReadRequest(0L))
        assertThat(readConversation2.messageCount, equalTo(2L))
        assertThat(readConversation2.readUntil, equalTo(0L))
        assertThat(readConversation2.messages!!.size, equalTo(1))

        // Finally, we mark the whole conversation as read for user2:
        api.login(testUser2.username, testUser2.password)
        val readConversation3 = api.postConversationRead(testCourseRef, conversation.ref!!, PostConversationReadRequest(1L))
        assertThat(readConversation3.messageCount, equalTo(2L))
        assertThat(readConversation3.readUntil, equalTo(1L))
        assertThat(readConversation3.messages!!.size, equalTo(0))

    }

    @Test
    fun testUpdateEmailNotificationType() {

        val testCourseRef = testCourse.ref!!
        val conversationToAdd = Conversation(
                hidden = false,
                anonymousMembers = false,
                messageCount = 0,
                title = "Conversation Notification Type Test Conv",
                type = ConversationType.gROUP,
                members = arrayOf(
                        TestEnvironment.getUserRef(TestEnvironment.testUsers[0].username),
                        TestEnvironment.getUserRef(TestEnvironment.testUsers[1].username)
                ),
                roles = arrayOf(
                        ConversationRolePermission(Role.tEACHER, ConversationPermissionType.vIEW)
                )
        )

        loginTestUser()
        val addedConversation = api.createConversation(testCourseRef, conversationToAdd)
        api.updateConversationEmailNotificationType(testCourseRef, addedConversation.ref!!, UpdateConversationEmailNotificationTypeRequest(ConversationEmailNotificationType.dAILY))

        val updatedConversation = api.getConversation(testCourseRef, addedConversation.ref!!)
        assertThat(updatedConversation.emailNotificationType, equalTo(ConversationEmailNotificationType.dAILY))
    }

    private fun validateConversationsEquals(conversation1: Conversation, conversation2: Conversation) {

        assertThat(conversation1.ref, equalTo(conversation2.ref))
        assertThat(conversation1.hidden, equalTo(conversation2.hidden))
        assertThat(conversation1.anonymousMembers, equalTo(conversation2.anonymousMembers))
        assertThat(conversation1.title, equalTo(conversation2.title))
        assertThat(conversation1.members!!.sorted(), equalTo(conversation2.members?.sorted()))
        assertThat(conversation1.roles!!.sortedBy { r -> r.role }, equalTo(conversation2.roles?.sortedBy { r -> r.role }))
    }

    private fun assertMessagesEqual(actual: ConversationMessage, expected: ConversationMessage) {

        assertThat(actual.message, equalTo(expected.message))
        assertThat(actual.ind, equalTo(expected.ind))
        assertThat(actual.sender, equalTo(expected.sender))
        assertThat(Duration.between(actual.sent, expected.sent).toMillis(), equalTo(0L))
        assertThat(actual.ref, equalTo(expected.ref))
    }
}