FROM openjdk:11-stretch
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
ARG ADDITIONAL_APPLICATION_PROPERTIES
COPY ${ADDITIONAL_APPLICATION_PROPERTIES} docker.application.properties
RUN apt-get install fonts-dejavu-core
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.config.location=classpath:/application.properties,/docker.application.properties"]